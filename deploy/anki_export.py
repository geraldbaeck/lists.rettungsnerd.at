#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
anki_export.py
Exports all markdown files from the docs folder to anki importable txt files into the anki folder.

Usage: anki_export.py.py [-hv]
       anki_export.py.py [--logLevel=<LOGLEVEL>][--environment=<ENV>]

Options:
  -h --help             Show this screen.
  -v --version          Show version.
  --logLevel=LOGLEVEL   The level of the logging output  [default: INFO]
  --environment=ENV     The execution environment
                        (development, staging or production)  [default: development]
"""

__appname__ = "JSON export to Anki"
__author__  = "Gerald Bäck (https://github.com/geraldbaeck/)"
__version__ = "0.0.1"
__license__ = "UNLICENSE"

DEFAULT_ENVIRONMENT = "development"

# System libraries
import glob
import json
import logging
import logging.config
import os

# 3rd party libraries
## from docopt import docopt

def set_up_logging(logtofile=False, coloredlog=True):
    try:
        logging.config.fileConfig(fname='deploy/logging.conf', disable_existing_loggers=False)

        # Get the logger specified in the file
        logger = logging.getLogger("defaultLogger")
        
        if coloredlog:
            pass

        #markdown.logger.setLevel(logging.WARNING)

        logger.debug("Logging configured.")
        return logger

    except AttributeError:
        raise Exception(
            "Unsupported logLevel. Use [DEBUG, INFO, WARNING, ERROR, CRITICAL]")


def createAnkiExportLine(q):

    def formatChoiceStyles(choice):
        return "font-weight:bold;" if choice['valid'] else "text-decoration:line-through;color:#A9A9A9;"

    question = f"<h1>{q['question']}</h1>"
    choices = "<ul style='list-style-type:none;'>"
    answers = "<ul style='list-style-type:none;'>"
    for choice in q['choices']:
        checked = 'checked' if choice['valid'] else ''
        choices += f"<li><label><input type='checkbox' style='position:relative;top:-3px;'>&nbsp;{choice['content']}</label></li>"
        answers += f"<li style='{formatChoiceStyles(choice)}'><label><input type='checkbox' {checked} style='position:relative;top:-3px;'>&nbsp;{choice['content']}</label></li>"
    choices += "</ul>"
    answers += "</ul>"


    return f"{question.replace(os.linesep, '').replace(',', '&#44;')}{choices.replace(os.linesep, '').replace(',', '&#44;')}, {answers.replace(os.linesep, '').replace(',', '&#44;')}\n"

def main():
    """ Main entry point of the app """
    logger.debug("{!s} started".format(__appname__))

    # del all files in the anki folder first, to avoid old files mitmatch
    files = glob.glob('anki/*')
    for f in files:
        os.remove(f)

    anki_cards = list()  # contains the final export data

    filename = "public/wrk.ausbildung.nfs.einstiegstest.json"
    logger.info(filename)

    with open(filename, 'r', encoding='utf-8') as f:

        # open file and convert to html
        questions_json = json.load(f)
        
        for q in questions_json:
            anki_cards.append(createAnkiExportLine(q))


    with open(f"public/wrk.ausbildung.nfs.einstiegstest.anki.txt", "w") as f:
        f.write(''.join(anki_cards))


if __name__ == "__main__":
    """ This is executed when run from the command line """
    # arguments = docopt(__doc__, version=__version__)
    # ENVIRONMENT = arguments['--environment']
    logger = set_up_logging()
    main()
