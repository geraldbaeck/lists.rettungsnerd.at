#!/bin/bash

# create all json files 
shopt -s nullglob extglob
cd src
for f in *.jsonnet
do
  newFileName=${f/jsonnet/json}
  jsonnet "$f" >  "../public/$newFileName"
done

# create anki file
cd ./..
python3 deploy/anki_export.py

# create an index.html file
cd public
list=""
for f in *.@(json|txt)
do
  link="https://lists.rettungsnerd.at/$f"
  list="$list\n\t\t\t<li>\n\t\t\t\t<a href='$f'>$link</a>\n\t\t\t</li>"
done
cp ../deploy/index.html ./index.html
sed -i "s|#LINKS|$list|g" index.html
