local questions = import 'wrk.ausbildung.nfs.einstiegstest.questions.libsonnet';

local createQuestion(question) = {
  id: question.id,
  question: question.question,
  choices: [
    question.choices[i] {
      id: question.id + '_' + (i + 1),
      valid: if 'valid' in question.choices[i] then question.choices[i].valid else false,
      }
    for i in std.range(0, std.length(question.choices) - 1)
  ],
};

[
  createQuestion(question)
  for question in questions
]
