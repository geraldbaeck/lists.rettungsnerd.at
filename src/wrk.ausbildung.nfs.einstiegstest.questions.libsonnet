[  
  {
    id: 2388677,
    question: 'Ab wann soll bei Verbrennungen ein Arzt aufgesucht werden?',
    choices: [
      {
        content: 'Immer',
      },
      {
        valid: true,
        content: 'Verbrennungen mit Blasenbildung größer als eine Handfläche'
      },
      {
        valid: true,
        content: 'Verbrennungen 1. Grades bei Symptomen wie Kopfschmerzen, Fieber und Unwohlsein'
      },
      {
        valid: true,
        content: 'Verbrennungen 3. Grades'
      },
    ],
  },
  {
    id: 2387587,
    question: 'Ab welchem Verbrennungsgrad ist eine Blasenbildung möglich und auch erkennbar?',
    choices: [
      {
        content: 'Verbrennungen 1. Grades'
      },
      {
        valid: true,
        content: 'Verbrennungen 2. Grades'
      },
      {
        content: 'Verbrennungen 3. Grades'
      },
      {
        content: 'Verbrennungen 4. Grades'
      },
    ],
  },
  {
    id: 2387588,
    question: 'Ab welchem Zeitpunkt der Schwangerschaft spricht man von einer Frühgeburt?',
    choices: [
      {
        content: 'Die Beendigung der Schwangerschaft zwischen 37. und 39. Woche'
      },
      {
        content: 'Die Beendigung der Schwangerschaft zwischen 28. und 35. Woche'
      },
      {
        valid: true,
        content: 'Die Beendigung der Schwangerschaft zwischen 24. und Ende 37. Woche'
      },
      {
        content: 'Die Beendigung der Schwangerschaft vor der 24. Woche'
      },
    ],
  },
  {
    id: 2387589,
    question: 'Ab welcher Körperkerntemperatur kommt es bei einer Unterkühlung zum Atem-Kreislaufstillstand?',
    choices: [
      {
        content: '29°C'
      },
      {
        content: '34°C'
      },
      {
        valid: true,
        content: 'Unter 27°C'
      },
      {
        content: '30°C'
      },
    ],
  },
  {
    id: 2387590,
    question: 'Ab welcher Körpertemperatur spricht man von Unterkühlung?',
    choices: [
      {
        content: 'Unter 32°C'
      },
      {
        content: 'Unter 25°C'
      },
      {
        content: 'Unter 29°C'
      },
      {
        valid: true,
        content: 'Unter 36°C'
      },
    ],
  },
  {
    id: 2388174,
    question: 'An welchen Fingern soll die Blutzuckermessung durchgeführt werden?',
    choices: [
      {
        content: 'Daumen'
      },
      {
        content: 'Zeigefinger'
      },
      {
        valid: true,
        content: 'Ringfinger'
      },
      {
        valid: true,
        content: 'Mittelfinger'
      },
    ],
  },
  {
    id: 2388310,
    question: 'Auf welche Seite soll die stabile Seitenlagerung durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'auf die versorgungstechnisch günstigere Seite'
      },
      {
        valid: true,
        content: 'bei Kopfverletzungen (Wunden) auf die verletzte Seite, außer bei Impressionsfraktur'
      },
      {
        valid: true,
        content: 'bei Brustkorbverletzungen auf die verletzte Seite (falls für den Patienten erträglich)'
      },
      {
        content: 'die Seite ist egal'
      },
    ],
  },
  {
    id: 2388175,
    question: 'Aus welchen Bestandteilen besteht ein Infusionsgerät?',
    choices: [
      {
        valid: true,
        content: 'Einstichteil'
      },
      {
        valid: true,
        content: 'Durchflussregler'
      },
      {
        valid: true,
        content: 'Tropfkammer'
      },
      {
        valid: true,
        content: 'Anschlussstück'
      },
    ],
  },
  {
    id: 2388419,
    question: 'Aus welchen Phasen besteht in der Regel ein Rettungseinsatz?',
    choices: [
      {
        valid: true,
        content: 'Rettung des Patienten'
      },
      {
        content: 'Stabilisierung des Patienten'
      },
      {
        valid: true,
        content: 'Erstversorgung des Patienten'
      },
      {
        valid: true,
        content: 'Transport des Patienten'
      },
    ],
  },
  {
    id: 2389642,
    question: 'Bei einer Atemkontrolle musst du…',
    choices: [
      {
        valid: true,
        content: '... den Kopf überstrecken'
      },
      {
        content: '... sehen, ob sich der Fuß bewegt'
      },
      {
        valid: true,
        content: '... auf Ein- und Ausatemgeräusche hören'
      },
      {
        content: '... die Person fragen, ob sie noch atmet'
      },
    ],
  },
  {
    id: 2387591,
    question: 'Bei welchen Brüchen wird der Patient wahrscheinlich immer als kritisch eingestuft?',
    choices: [
      {
        valid: true,
        content: 'Gesichtsschädel- und Schädelbrüche, Wirbelsäulenbrüche, Oberarm-, Becken-, und Oberschenkelbrüche'
      },
      {
        content: 'Rippenbrüche, Schlüsselbeinbruch'
      },
      {
        valid: true,
        content: 'Brüche und Verrenkungen mit Fehlstellung, Hüftverletzung, Brüche mit starken Blutungen'
      },
      {
        content: 'Finger-, Zehen- und Schienbeinbrüche'
      },
    ],
  },
  {
    id: 2388687,
    question: 'Bei welchen Notfällen ist eine Seitenlage sinnvoll?',
    choices: [
      {
        valid: true,
        content: 'Bewusstseinsstörung'
      }, 
      {
        valid: true,
        content: 'Gefahr des Erbrechens'
      }, 
      {
        content: 'Herzbeschwerden mit Atemnot'
      }, 
      {
        content: 'verstauchtem Knöchel'
      },
    ],
  },
  {
    id: 2388869,
    question: 'Bei welchen Notfällen wird eine Lagerung mit erhöhtem Oberkörper empfohlen?',
    choices: [
      {
        valid: true,
        content: 'Atemnot'
      }, 
      {
        valid: true,
        content: 'Herzbeschwerden'
      }, 
      {
        content: 'Blutungen'
      }, 
      {
        content: 'Kollaps'
      }, 
    ],
  },
  {
    id: 2388899,
    question: 'Bei welchen Notfällen wird eine Lagerung mit erhöhtem Oberkörper empfohlen?',
    choices: [
      {
        valid: true,
        content: 'Atemnot'
      }, 
      {
        valid: true,
        content: 'Herzbeschwerden'
      }, 
      {
        content: 'Blutungen'
      }, 
      {
        content: 'Kollaps'
      }, 
    ],
  },
  {
    id: 2388917,
    question: 'Bei welchen Notfällen wird eine Lagerung mit erhöhtem Oberkörper empfohlen?',
    choices: [
      {
        valid: true,
        content: 'Atemnot'
      }, 
      {
        valid: true,
        content: 'Herzbeschwerden'
      }, 
      {
        content: 'Blutungen'
      }, 
      {
        content: 'Kollaps'
      }, 
    ],
  },
  {
    id: 2387592,
    question: 'Bei welchen Personen besteht erhöhte Selbstmordgefahr?',
    choices: [
      {
        valid: true,
        content: 'Ältere, einsame oder schwerkranke Menschen'
      },
      {
        valid: true,
        content: 'Gefangene'
      },
      {
        valid: true,
        content: 'Süchtige (Alkohol, Drogen und Medikamente)'
      },
      {
        valid: true,
        content: 'Psychisch Kranke'
      },
    ],
  },
  {
    id: 2388576,
    question: 'Bei welchen Verletzungen mit starker Blutung ist der Fingerdruck die einzige Möglichkeit zur Blutstillung?',
    choices: [	  
      {
        valid: true,
        content: 'Halsschlagaderverletzung, Verletzung in der Leistenbeuge'
      },
      {
        content: 'Schürfwunde'
      },
      {
        content: 'Magenblutung'
      },
      {
        content: 'Bei jeder starken Blutung'
      },
    ],
  },
  {
    id: 2387593,
    question: 'Beschreiben Sie die San-Hilfe-Maßnahmen bei einer Verrenkung!',
    choices: [
      {
        valid: true,
        content: 'Ruhigstellung in der vorgefundenen Stellung (keinesfalls Einrenkungsversuche unternehmen, um zusätzliche Verletzungen zu vermeiden); zur Abklärung eventueller Bandverletzungen müssen eine ärztliche Untersuchung und spezielle Röntgenaufnahmen durchgeführt werden.'
      },
      {
        valid: true,
        content: 'Alle entsprechenden Maßnahmen der Schockbekämpfung, Notarztindikation'
      },
      {
        content: 'Einrenkungsversuche unternehmen, um weitere Schmerzen zu vermeiden; zur  Abklärung eventueller Bandverletzungen müssen eine ärztliche Untersuchung und spezielle Röntgenaufnahmen durchgeführt werden.'
      },
      {
        content: 'Alle entsprechenden Maßnahmen der Schockbekämpfung, Transport ins Krankenhaus; es ist kein Notarzt erforderlich.'
      },
    ],
  },
  {
    id: 2387594,
    question: 'Beschreiben Sie die Symptome und Komplikationen einer offenen Bauchverletzung!',
    choices: [
      {
        content: 'Wunde im Bereich der Bauchdecke (eventuell Herausragen eines pfählenden Fremdkörpers), leichte Schmerzen'
      }, 
      {
        content: 'Leichte Blutung, Absinken der Herzfrequenz, Schock'
      }, 
      {
        valid: true,
        content: 'Wunde im Bereich der Bauchdecke (eventuell Herausragen eines pfählenden Fremdkörpers), eventuell Austreten von Darmschlingen, starke Schmerzen, Blutung, Schockzeichen'
      }, 
      {
        valid: true,
        content: 'Starke Blutung, Schock'
      },
    ],
  },
  {
    id: 2388577,
    question: 'Durch körperliche Anstrengung und schwere Arbeit in heißer oder feuchtwarmer Umgebung (hohe Luftfeuchtigkeit) und erschwerter Schweißabgabe (Kleidung) kommt es zu einem Wärmestau und zu einer...',
    choices: [
      {
        content: 'Hypotonie'
      },
      {
        content: 'Hypertonie'
      },
      {
        valid: true,
        content: 'Hitzschlag'
      },
      {
        content: 'Hyperventilation'
      },
    ],
  },
  {
    id: 2388578,
    question: 'Durch starke Sonnenbestrahlung kann es zu Kopfschmerzen, Übelkeit, Erbrechen, Nackensteife, Bewusstseinsstörungen und Krämpfen kommen. Mit welchen Notfällen ist zu rechnen?',
    choices: [	  
      {
        valid: true,
        content: 'Hitzeerschöpfung'
      },
      {
        valid: true,
        content: 'Sonnenstich'
      },
      {
        valid: true,
        content: 'Hitzschlag'
      },
      {
        content: 'Hitzekrampf'
      },
    ],
  },
  {
    id: 2387595,
    question: 'Ein erwachsener Patient hat eine zweit- bzw. drittgradige Verbrennung des linken Beines und des gesamten rechten Unterschenkels. Wie viel Prozent der Körperoberfläche sind das?',
    choices: [
      {
        content: '50 % der Körperoberfläche sind verbrannt.'
      },
      {
        content: '36 % der Körperoberfläche sind verbrannt.'
      },
      {
        valid: true,
        content: '27 % der Körperoberfläche sind verbrannt.'
      },
      {
        content: '9 % der Körperoberfläche sind verbrannt.'
      },
    ],
  },
  {
    id: 2387596,
    question: 'Gegen wen können sich aufgestaute Aggressionen bei selbstmordgefährdeten Patienten richten?',
    choices: [
      {
        valid: true,
        content: 'Gegen alle anwesenden Personen'
      },
      {
        content: 'Ausschließlich gegen die Exekutive'
      },
      {
        valid: true,
        content: 'Gegen den Helfer'
      },
      {
        valid: true,
        content: 'Gegen sich selbst'
      },
    ],
  },
  {
    id: 2388606,
    question: 'Gibt es Konsequenzen bei Verletzungen der Verschwiegenheitspflicht durch einen Sanitäter?',
    choices: [
      {
        content: 'Nein, da gewerkschaftlich bzw. durch die Versicherung abgedeckt'
      },
      {
        valid: true,
        content: 'Ja, verwaltungsbehördliche Konsequenzen'
      },
      {
        valid: true,
        content: 'Ja, gerichtliche Konsequenzen'
      },
      {
        content: 'Nein, obwohl keine gewerkschaftliche Deckung besteht'
      },
    ],
  },
  {
    id: 2388311,
    question: 'In welchem Bewusstseinszustand befindet sich ein Mensch, wenn er auf äußere Reize zwar reagiert, die Reaktion jedoch verlangsamt erfolgt?',
    choices: [
      {
        content: 'Komatös'
      },
      {
        content: 'Agitiert'
      },
      {
        content: 'Bewusstlos'
      },
      {
        valid: true,
        content: 'Bewusstseinsgetrübt'
      },
    ],
  },
  {
    id: 2388176,
    question: 'Kann ein Defibrillator einen Puls feststellen?',
    choices: [
      {
        content: 'Ja, nur AEDs können einen Puls feststellen.'
      },
      {
        content: 'Ja, nur Defibrillatoren im Rettungsdienst können einen Puls feststellen.'
      },
      {
        valid: true,
        content: 'Nein, kein Defibrillator kann einen Puls feststellen.'
      },
      {
        content: 'Ja, nur Defibrillatoren im Krankenhaus können einen Puls feststellen.'
      },
    ],
  },
  {
    id: 2387597,
    question: 'Nennen Sie das Prinzip der Schienung!',
    choices: [
      {
        valid: true,
        content: 'Alle beengenden Teile (Kleider, Schmuckstücke wie Uhr, Ringe etc.) wegen möglicher Störung der Durchblutung entfernen.'
      },
      {
        valid: true,
        content: 'Bei jeder Schienung müssen die beiden der Verletzung benachbarten Gelenke mitgeschient werden, da ansonsten die Ruhigstellung nicht gewährleistet ist.'
      },
      {
        content: 'Das Schienen ist nur bei offenen Brüchen notwendig.'
      },
      {
        content: 'Das Schienen dient zum besseren und leichteren Transport ins Krankenhaus.'
      },
    ],
  },
  {
    id: 2387598,
    question: 'Nennen Sie den möglichen Blutverlust bei Frakturen!',
    choices: [
      {
        content: 'Unterarm bis ca. 500 ml, Unterschenkel bis ca. 800 ml, Oberarm bis ca. 600 ml'
      },
      {
        valid: true,
        content: 'Unterarm bis ca. 400 ml, Unterschenkel bis ca. 1000 ml, Oberarm bis ca. 800 ml'
      },
      {
        content: 'Becken bis ca. 4000 ml, Oberschenkel bis ca. 1500 ml'
      },
      {
        valid: true,
        content: 'Becken bis ca. 5000 ml, Oberschenkel bis ca. 2000 ml'
      },
    ],
  },
  {
    id: 2387600,
    question: 'Nennen Sie die Definition eines Polytraumas!',
    choices: [
      {
        content: 'Eine große Anzahl von Verletzungen'
      },
      {
        valid: true,
        content: 'Unter einer Mehrfachverletzung (Polytrauma) versteht man gleichzeitig entstandene Verletzungen mehrerer Körperregionen oder Organsysteme, wobei wenigstens eine Verletzung oder die Kombination mehrerer lebensbedrohlich ist.'
      },
      {
        content: 'Mehrere Verletzungen, die alle lebensbedrohlich sind.'
      },
      {
        content: 'Bruch von zumindest zwei Knochen gleichzeitig und Abfall des Blutdruckes'
      },
    ],
  },
  {
    id: 2387601,
    question: 'Nennen Sie die Gefahren, die eine Wunde mit sich bringt.',
    choices: [
      {
        valid: true,
        content: 'Infektion'
      },
      {
        valid: true,
        content: 'Blutung, Schock'
      },
      {
        valid: true,
        content: 'Schmerz'
      },
      {
        content: 'Atemnot'
      },
    ],
  },
  {
    id: 2387602,
    question: 'Nennen Sie die San-Hilfe-Maßnahmen bei einem Patienten, der ätzende Stoffe verschluckt hat!',
    choices: [
      {
        valid: true,
        content: 'Reinigung der Mundhöhle (Ausspülen), alle entsprechenden Maßnahmen der Schockbekämpfung'
      },
      {
        valid: true,
        content: 'Falls Verätzungsmittel bekannt: Vergiftungsinformationszentrale, Tel: 01/406 43 43, kontaktieren und die Anweisungen durchführen'
      },
      {
        valid: true,
        content: 'Verätzungsmittel sicherstellen, Notarztindikation'
      },
      {
        content: 'Lagerung auf Schaufeltrage und Vakuummatratze'
      },
    ],
  },
  {
    id: 2387603,
    question: 'Nennen Sie die San-Hilfe-Maßnahmen bei einer mechanischen Augenverletzung!',
    choices: [
      {
        content: 'Fremdkörper entfernen, lockeren Verband anlegen (Druck auf das Auge vermeiden), zur Ruhigstellung beide Augen verbinden'
      },
      {
        valid: true,
        content: 'Fremdkörper müssen im Auge belassen werden, eventuell fixieren, lockeren Verband anlegen (Druck auf das Auge vermeiden), zur Ruhigstellung beide Augen verbinden'
      },
      {
        content: 'Lagerung mit erhöhtem Oberkörper, alle entsprechenden Maßnahmen der Schockbekämpfung, Transport ins nächste Krankenhaus'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper, alle entsprechenden Maßnahmen der Schockbekämpfung, bei starken Schmerzen Notarztindikation, eine medizinische Erstversorgung in einer Spezialabteilung ist wünschenswert!'
      },
    ],
  },
  {
    id: 2387604,
    question: 'Nennen Sie die San-Hilfe-Maßnahmen beim geschlossenen Pneumothorax?',
    choices: [
      {
        valid: true,
        content: 'Psychische Betreuung, Anregung zu tiefer ruhiger Atmung, Öffnen beengender Kleidungsstücke, Lagerung mit erhöhtem Oberkörper, wenn erträglich, auf die verletzte Seite oder Stabilisierung durch Handauflegen (Druck mit der flachen Hand)'
      }, 
      {
        valid: true,
        content: 'Sauerstoffgabe 10 - 15 l/min, alle entsprechenden Maßnahmen der Schockbekämpfung, wenn erforderlich: Absaugbereitschaft, Notarztindikation'
      }, 
      {
        content: 'Psychische Betreuung, Öffnen beengender Kleidungsstücke, Lagerung mit erhöhtem Oberkörper auf die unverletzte Seite'
      }, 
      {
        content: 'Sauerstoffgabe 6 - 8 l/min, alle entsprechenden Maßnahmen der Schockbekämpfung, wenn erforderlich: assistierte Beatmung, Absaugbereitschaft, Notarztindikation'
      }, 
    ],
  },
  {
    id: 2387605,
    question: 'Nennen Sie die San-Hilfe-Maßnahmen, wenn der Patient ätzende Stoffe eingeatmet hat!',
    choices: [
      {
        valid: true,
        content: 'Falls Gefahrenzone: Rettung durch die Feuerwehr, sofortige Ruhigstellung mit erhöhtem Oberkörper bei Atemnot'
      },
      {
        valid: true,
        content: 'Öffnen beengender Kleidung, Sauerstoffgabe 10 - 15 l/min, Notarztindikation'
      }, 
      {
        content: 'Schluckweise Wasser verabreichen'
      },
      {
        valid: true,
        content: 'Alle entsprechenden Maßnahmen der Schockbekämpfung'
      }, 
    ],
  },
  {
    id: 2387606,
    question: 'Nennen Sie die Stadien des Unterkühlungsvorgangs mit Temperaturangabe!',
    choices: [
      {
        content: 'Erregungsstadium 32 - 36°C Kerntemperatur, Erschöpfungsstadium 30 - 32°C Kerntemperatur, Lähmungsstadium 27 - 30°C Kerntemperatur, Endstadium unter 26°C Kerntemperatur'
      },
      {
        content: 'Aufregungsstadium 34 - 36°C Kerntemperatur, Kollapsstadium 30 - 34°C Kerntemperatur, Verwirrtheitsstadium 27 - 30°C Kerntemperatur, Endstadium unter 27°C Kerntemperatur'
      },
      {
        valid: true,
        content: 'Erregungsstadium 34 - 36°C Kerntemperatur, Erschöpfungsstadium 30 - 34°C Kerntemperatur, Lähmungsstadium 27 - 30°C Kerntemperatur, Endstadium unter 27°C Kerntemperatur'
      },
      {
        content: 'Erregungsstadium 33 - 37°C Kerntemperatur, Erschöpfungsstadium 29 - 33°C Kerntemperatur, Lähmungsstadium 29 - 27°C Kerntemperatur, Endstadium unter 27°C Kerntemperatur'
      },
    ],
  },
  {
    id: 2387607,
    question: 'Nennen Sie die Symptome bei einem geschlossenen Pneumothorax!',
    choices: [
      {
        valid: true,
        content: 'Schmerzen im Bereich des Brustkorbs, Atemnot, Kurzatmigkeit, Reizhusten, Abschürfungen und Prellmarken, Abnorme Brustkorbbewegungen mit paradoxer Atmung'
      },
      {
        valid: true,
        content: 'Blaufärbung der Haut (Zyanose) und Venenstauung, besonders im Halsbereich (obere Einflussstauung), Schockentwicklung, Hautknistern'
      },
      {
        content: 'Kurzatmigkeit, Abschürfungen und Prellmarken, Abnorme Brustkorbbewegungen, Luftbläschen in der Wunde'
      },
      {
        content: 'Blaufärbung der Haut (Zyanose) und Venenstauung, besonders im Beinbereich (untere Einflussstauung), Hautknistern'
      },
    ],
  },
  {
    id: 2387608,
    question: 'Nennen Sie die Symptome bei einem Wirbelsäulentrauma!',
    choices: [
      {
        valid: true,
        content: 'Schmerzen im Bereich der Wirbelsäule'
      },
      {
        valid: true,
        content: 'Schockzeichen'
      },
      {
        valid: true,
        content: 'Kraftlosigkeit bis zur Bewegungsunfähigkeit (Kontrolle Motorik), Taubheitsgefühl (Kontrolle Sensibilität) '
      }, 
      {
        valid: true,
        content: 'Manche Personen haben nur geringe Beschwerden, keine Ausfallserscheinungen und können sogar aufstehen und gehen.'
      },
    ],
  },
  {
    id: 2387609,
    question: 'Nennen Sie die Symptome bei Quetschungen und welche San-Hilfe-Maßnahmen Sie durchführen!',
    choices: [
      {
        valid: true,
        content: 'Schmerzen, Schwellung, Bluterguss'
      },
      {
        valid: true,
        content: 'Hochlagerung, Ruhigstellung'
      },
      {
        content: 'Keine Schmerzen, Bluterguss'
      },
      {
        content: 'Tieflagerung, Ruhigstellung'
      },
    ],
  },
  {
    id: 2387610,
    question: 'Nennen Sie die Symptome einer Verstauchung und welche San-Hilfe-Maßnahmen Sie durchführen!',
    choices: [
      {
        content: 'Leichte Schmerzen, Blutung, Schwellung'
      },
      {
        content: 'Tieflagerung, Ruhigstellung'
      },
      {
        valid: true,
        content: 'Schmerzen, Schwellung, Blaufärbung'
      },
      {
        valid: true,
        content: 'Hochlagerung, Ruhigstellung'
      },
    ],
  },
  {
    id: 2387611,
    question: 'Nennen Sie die Symptome und Komplikationen bei Verschlucken ätzender Stoffe!',
    choices: [
      {
        valid: true,
        content: 'Starke Schmerzen, Atemnot, Schockzeichen'
      },
      {
        valid: true,
        content: 'Bleibende Schäden der Schleimhäute, Magendurchbruch, Schock'
      },
      {
        content: 'Leichte Schmerzen, Bluterbrechen, Übelkeit'
      },
      {
        content: 'Blutungen, Steigerung der Herzfrequenz bis zum Kammerflimmern'
      },
    ],
  },
  {
    id: 2387612,
    question: 'Nennen Sie die Symptome und Komplikationen, die bei Erfrierungen auftreten bzw. auftreten können!',
    choices: [
      {
        valid: true,
        content: 'Oberflächliche Kälteschädigung: anfangs Gefühllosigkeit und Blässe, später prickelnde Schmerzen und blaurote Verfärbung; tiefer gehende Erfrierungen: Blasenbildung mit weiß bis blauroter Verfärbung der Haut, Bewegungseinschränkung der betroffenen Körperteile, bei Berührung empfindungslos, starke Schmerzen'
      },
      {
        valid: true,
        content: 'Schock, Infektion, Verlust der betroffenen Körperteile'
      },
      {
        content: 'Oberflächliche Kälteschädigung: prickelnde Schmerzen und blaurote Verfärbung; tiefer gehende Erfrierungen: Blasenbildung mit weiß bis blauroter Verfärbung der Haut, Bewegungseinschränkung der betroffenen Körperteile, keine Schmerzen'
      },
      {
        content: 'Bluthochdruck, Infektion, Verlust der betroffenen Körperteile'
      },
    ],
  },
  {
    id: 2387613,
    question: 'Nennen Sie die Symptome und Komplikationen, die beim Spannungspneumothorax auftreten können!',
    choices: [
      {
        content: 'Austritt von Blut aus der Wunde, Blaufärbung der Haut (Zyanose) und Venenstauung, besonders im Beinbereich (untere Einflussstauung), Schockentwicklung'
      },
      {
        content: 'leichte Atemstörung, Schock'
      },
      {
        valid: true,
        content: 'Austritt von schaumigem Blut aus der Wunde, Blaufärbung der Haut (Zyanose) und Venenstauung, besonders im Halsbereich (obere Einflussstauung), Schockentwicklung'
      },
      {
        valid: true,
        content: 'schwerste Atemstörung, Schock'
      },
    ],
  },
  {
    id: 2388656,
    question: 'Nennen Sie die Versorgung von Wunden.',
    choices: [
      {
        content: 'Den Verletzten hinsetzen oder hinlegen, Wunden keimfrei verbinden, dabei die Wunde auf jeden Fall mit klarem Wasser auswaschen und Desinfektion vornehmen. Keine Salben, Puder oder Hausmittel verwenden, Fremdkörper zur Schmerzlinderung entfernen.'
      },
      {
        content: 'Den Verletzten hinsetzen, Wunde nicht berühren, aber von den gröbsten Verunreinigungen befreien und einen desinfizierenden Puder auftragen'
      },
      {
        valid: true,
        content: 'Den Verletzten hinsetzen oder hinlegen, Wunden keimfrei verbinden, wenn notwendig Arzt oder Krankenhaus aufsuchen - möglichst sofort, jedoch innerhalb von 6 Stunden'
      },
      {
        content: 'Wunden keimfrei verbinden, dabei die Wunde nicht berühren und sicherheitshalber so nahe wie möglich bei der Wunde abbinden, Arzt oder Krankenhaus aufsuchen - möglichst sofort, jedoch innerhalb von 9 Stunden'
      },
    ],
  },
  {
    id: 2387614,
    question: 'Sie werden zu einem Patienten mit Erfrierungen gerufen. Welche San-Hilfe-Maßnahmen ergreifen Sie?',
    choices: [
      {
        content: 'Beengende Kleidung öffnen, betroffenen Körperteil zudecken, vor mechanischer Einwirkung schützen'
      },
      {
        content: 'Verabreichung kalter Getränke, Körperwärme erhalten (Decken, Rettungsdecke), alle entsprechenden Maßnahmen der Schockbekämpfung'
      },
      {
        valid: true,
        content: 'Beengende Kleidung öffnen, betroffenen Körperteil keimfrei bedecken (Finger und Zehen einzeln verbinden, keinen "Gesamtfäustlingsverband" anlegen), vor weiterer Kälteeinwirkung und mechanischer Einwirkung schützen (betroffene Teile, z.B. Finger, nicht bewegen, keinen Druck ausüben)'
      },
      {
        valid: true,
        content: 'Verabreichung warmer Getränke (Alkohol- und Rauchverbot), Körperwärme erhalten (Decken, Rettungsdecke), alle entsprechenden Maßnahmen der Schockbekämpfung'
      },
    ],
  },
  {
    id: 2387615,
    question: 'Sie werden zu einem Patienten mit Unterkühlung gerufen und stellen fest, dass dieser bereits das Erschöpfungsstadium (30 - 34°C Kerntemperatur) erreicht hat. Welche San-Hilfe-Maßnahmen ...',
    choices: [
      {
        valid: true,
        content: 'Aktives und passives Bewegungsverbot für den Patienten, Manipulationen am Patienten auf ein Minimum reduzieren, nasse Kleidung vorsichtig entfernen'
      },
      {
        valid: true,
        content: 'Schutz vor weiterem Wärmeverlust, Sauerstoffgabe 6 - 8 l/min; ab Erschöpfungsstadium Notarztindikation'
      },
      {
        content: 'Manipulationen am Patienten auf ein Minimum reduzieren, daher nasse Kleidung belassen'
      }, 
      {
        content: 'Schutz vor weiterem Wärmeverlust, Sauerstoffgabe 10 - 15 l/min, ab Erschöpfungsstadium rasch ins nächste Krankenhaus'
      },
    ],
  },
  {
    id: 2387616,
    question: 'Sie werden zu einem Verletzten mit einer offenen Bauchverletzung gerufen. Welche San-Hilfe-Maßnahmen führen Sie durch?',
    choices: [
      {
        valid: true,
        content: 'Keimfreie Wundversorgung: ohne Druck fixieren; bei größeren Defekten am besten mit Brandwundtüchern, Wundauflagen mit kristalloider Lösung feucht halten, Rettungsdecke über die Wundversorgung legen, pfählende Gegenstände sind zu belassen und zu fixieren'
      },
      {
        valid: true,
        content: 'Vorsichtige Flachlagerung (mit Schaufeltrage) auf einer Vakuummatratze mit angezogenen Beinen, Sauerstoffgabe 6 - 8 l/min, alle entsprechenden Maßnahmen der Schockbekämpfung, Notarztindikation'
      },
      {
        content: 'Keimfreie Wundversorgung: fest fixieren; bei größeren Defekten am besten mit Brandwundtüchern, Rettungsdecke über die Wundversorgung legen, pfählende Gegenstände sind vorsichtig zu entfernen'
      },
      {
        content: 'Vorsichtige Flachlagerung (mit Schaufeltrage) auf einer Vakuummatratze mit angezogenen Beinen, Sauerstoffgabe 10 - 15 l/min, alle entsprechenden Maßnahmen der Schockbekämpfung, Notarztindikation'
      },
    ],
  },
  {
    id: 2387617,
    question: 'Sie werden zu einem Verletzten mit stumpfer Bauchverletzung gerufen. Welche San-Hilfe-Maßnahmen führen Sie durch?',
    choices: [
      {
        valid: true,
        content: 'Vorsichtige Flachlagerung (mit Schaufeltrage) auf einer Vakuummatratze mit angezogenen Beinen oder Lagerung nach Wunsch des Patienten, Sauerstoffgabe 6 - 8 l/min'
      },
      {
        valid: true,
        content: 'Alle entsprechenden Maßnahmen der Schockbekämpfung, Notarztindikation'
      },
      {
        content: 'Vorsichtige Flachlagerung (mit Schaufeltrage) auf einer Vakuummatratze oder Lagerung nach Wunsch des Patienten, Sauerstoffgabe 2 - 3 l/min'
      },
      {
        content: 'Alle entsprechenden Maßnahmen der Schockbekämpfung, schneller Transport ins Krankenhaus auch ohne Notarzt'
      },
    ],
  },
  {
    id: 2387618,
    question: 'Von welchen Faktoren ist die Schwere einer Verbrennung abhängig?',
    choices: [
      {
        valid: true,
        content: 'Ausdehnung der Verbrennung'
      },
      {
        valid: true,
        content: 'Tiefenwirkung der Verbrennung'
      },
      {
        valid: true,
        content: 'Alter des Betroffenen'
      },
      {
        content: 'Ab dem 4. Verbrennungsgrad erhöhte Schockgefahr'
      },
    ],
  },
  {
    id: 2388607,
    question: 'Von wem muss die Zustimmung für einen Krankentransport eingeholt werden?',
    choices: [
      {
        valid: true,
        content: 'Jede Hilfeleistung setzt die Einwilligung des Betroffenen oder dessen gesetzlichen Vertreters voraus'
      },
      {
        content: 'Bei "besachwalteten" Personen ist keine Zustimmung notwendig'
      },
      {
        valid: true,
        content: 'Bei "besachwalteten" Personen ist die Zustimmung des Sachwalters notwendig'
      },
      {
        content: 'Keine Hilfeleistung setzt die Einwilligung des Betroffenen voraus'
      },
    ],
  },
  {
    id: 2387619,
    question: 'Wann besteht der Verdacht auf ein SHT (Schädelhirntrauma)?',
    choices: [
      {
        content: 'Nur dann, wenn es zu einer sichtbaren Blutung im Bereich des Kopfes kommt'
      },
      {
        valid: true,
        content: 'Bei jeder Bewusstseinsstörung nach einer Gewalteinwirkung auf den Kopf'
      },
      {
        content: 'Nur dann, wenn der Betroffene bewusstlos ist'
      },
      {
        content: 'Nur dann, wenn es zu Blutaustritt aus Nase, Ohr oder Mund kommt'
      },
    ],
  },
  {
    id: 2387620,
    question: 'Wann besteht die absolute Notwendigkeit zum liegenden Transport einer Schwangeren?',
    choices: [
      {
        content: 'Bei Wehenbeginn'
      },
      {
        content: 'Bei einer Erstgebärenden'
      },
      {
        valid: true,
        content: 'Wenn der Blasensprung erfolgt ist'
      },
      {
        content: 'Wenn der kindliche Kopf im Geburtskanal sichtbar wird'
      },
    ],
  },
  {
    id: 2388420,
    question: 'Wann besteht im allgemeinen die Einsatzbereitschaft des NAH?',
    choices: [
      {
        valid: true,
        content: 'Allgemein von Tagesanbruch bis kurz nach Sonnenuntergang'
      },
      {
        content: 'Bei starkem Nebel'
      },
      {
        content: 'Bei starkem Schneefall und Hagel'
      },
      {
        valid: true,
        content: 'Bei Mindestsichtweiten von 2500 Fuß horizontal und 450 Fuß vertikal'
      },
    ],
  },
  {
    id: 2387621,
    question: 'Wann besteht Vergiftungsverdacht?',
    choices: [
      {
        valid: true,
        content: 'Leere Medikamentenpackungen'
      },
      {
        valid: true,
        content: 'Entsprechende Angaben des Betroffenen'
      },
      {
        valid: true,
        content: 'Regloser Notfallpatient in Silo, Weinkeller'
      },
      {
        valid: true,
        content: 'Wenn bei mehreren Personen gleichzeitig ähnliche Symptome auftreten'
      },
    ],
  },
  {
    id: 2388867,
    question: 'Wann bzw. wozu schalten Sie die Alarmblinkanlage ein?',
    choices: [
      {
        valid: true,
        content: 'vor dem Anlegen der Warnweste'
      }, 
      {
        content: 'nach dem Anlegen der Warnweste'
      }, 
      {
        valid: true,
        content: 'wenn Gefahr vom eigenen Fahrzeug ausgeht'
      }, 
      {
        valid: true,
        content: 'wenn Sie vor Gefahren warnen wollen'
      }, 
    ],
  },
  {
    id: 2388901,
    question: 'Wann bzw. wozu schalten Sie die Alarmblinkanlage ein?',
    choices: [
      {
        valid: true,
        content: 'vor dem Anlegen der Warnweste'
      }, 
      {
        content: 'nach dem Anlegen der Warnweste'
      }, 
      {
        valid: true,
        content: 'wenn Gefahr vom eigenen Fahrzeug ausgeht'
      }, 
      {
        valid: true,
        content: 'wenn Sie vor Gefahren warnen wollen'
      }, 
    ],
  },
  {
    id: 2388919,
    question: 'Wann bzw. wozu schalten Sie die Alarmblinkanlage ein?',
    choices: [
      {
        valid: true,
        content: 'vor dem Anlegen der Warnweste'
      }, 
      {
        content: 'nach dem Anlegen der Warnweste'
      }, 
      {
        valid: true,
        content: 'wenn Gefahr vom eigenen Fahrzeug ausgeht'
      }, 
      {
        valid: true,
        content: 'wenn Sie vor Gefahren warnen wollen'
      }, 
    ],
  },
  {
    id: 2388312,
    question: 'Wann darf der halbautomatische Defibrillator nicht verwendet werden?',
    choices: [
      {
        valid: true,
        content: 'Bei Säuglingen'
      },
      {
        valid: true,
        content: 'Bei Patienten mit normaler Atmung und Lebenszeichen'
      },
      {
        content: 'Bei Patienten, die eine Kammertachykardie haben'
      },
      {
        valid: true,
        content: 'Bei feuchtem oder nassem Brustkorb des Patienten'
      },
    ],
  },
  {
    id: 2388678,
    question: 'Wann darf eine Ersthelferdefibrillation nicht angewendet werden?',
    choices: [
      {
        content: 'Es gibt keine Kontraindikationen - die Defibrillation darf immer durchgeführt werden'
      },
      {
        valid: true,
        content: 'z.B. bei Säuglingen'
      },
      {
        valid: true,
        content: 'z.B. Patient liegt auf Eisenbahnschienen'
      },
      {
        valid: true,
        content: 'z.B. Patient liegt in einer Wasserlacke'
      },
    ],
  },
  {
    id: 2388698,
    question: 'Wann führt der Sanitäter die hygienische Händedesinfektion durch?',
    choices: [
      {
        valid: true,
        content: 'Vor jedem Patientenkontakt'
      },
      {
        valid: true,
        content: 'Vor dem Vorbereiten von Medikamenten und Infusionen'
      },
      {
        valid: true,
        content: 'Nach Dienstschluss'
      },
      {
        valid: true,
        content: 'Nach jedem Patientenkontakt bzw. nach Kontakt mit kontaminiertem Material (Blut, Eiter, ...)'
      },
    ],
  },
  {
    id: 2387622,
    question: 'Wann kann die sogenannte "paradoxe Atmung" auftreten?',
    choices: [
      {
        content: 'Bei Verlegung der Atemwege'
      },
      {
        content: 'Bei Asthma'
      },
      {
        valid: true,
        content: 'Bei einem Serienrippentrümmerbruch'
      },
      {
        content: 'Bei Vergiftungen'
      },
    ],
  },
  {
    id: 2387623,
    question: 'Wann kann es zum Bluterbrechen kommen?',
    choices: [
      {
        valid: true,
        content: 'Entzündungen und Tumore in der Speiseröhre, im Magen und im Zwölffingerdarm'
      },
      {
        content: 'Dünndarmerkrankungen'
      },
      {
        valid: true,
        content: 'Magen- oder Zwölffingerdarmgeschwür'
      },
      {
        valid: true,
        content: 'Krampfadern in der Speiseröhre (Ösophagusvarizen)'
      },
    ],
  },
  {
    id: 2388608,
    question: 'Wann kommt ein Revers zur Anwendung?',
    choices: [
      {
        content: 'Der Revers muss von jedem Patienten vor Transportantritt unterfertigt werden'
      },
      {
        valid: true,
        content: 'Der Revers ist eine Dokumentation der Ablehnung eines Transportes durch Unterschrift auf einem entsprechenden Formular'
      },
      {
        content: 'Ein Schreiben, das die Zwangseinweisung des Patienten in ein Krankenhaus bescheinigt'
      },
      {
        content: 'Der Revers ist eine Bescheinigung, dass der Patient die Mehrkosten für den Transport übernimmt'
      },
    ],
  },
  {
    id: 2388609,
    question: 'Wann liegt ein Fall von unterlassener Hilfeleistung vor?',
    choices: [
      {
        valid: true,
        content: 'Wenn man eine zumutbare und sichtlich erforderliche Hilfsmaßnahme unterlässt'
      },
      {
        content: 'Wenn man eine Hilfeleistung unterlässt, da sie nur mit Gefahr für Leib und Leben durchführbar wäre'
      },
      {
        valid: true,
        content: 'Wenn man eine den Kenntnissen und Fähigkeiten entsprechende, wirksame Hilfe nicht leistet'
      },
      {
        content: 'Diesen Fall gibt es nicht'
      },
    ],
  },
  {
    id: 2388313,
    question: 'Wann muss bei einem Todesfall die Exekutive zugezogen werden?',
    choices: [
      {
        valid: true,
        content: 'Plötzlicher Tod von Säuglingen (SIDS)'
      },
      {
        valid: true,
        content: 'Tod an öffentlichen Orten und bei Selbstmord'
      },
      {
        content: 'Infektionskrankheiten'
      },
      {
        valid: true,
        content: 'Tod eines ausländischen Staatsbürgers'
      },
    ],
  },
  {
    id: 2388314,
    question: 'Wann muss bei Vorliegen einer Abtrennung oder Teilabtrennung abgebunden werden?',
    choices: [
      {
        content: 'Wenn die Wunde schwach zu bluten beginnt'
      },
      {
        content: 'Sofort'
      },
      {
        valid: true,
        content: 'Erst wenn eine nicht beherrschbare Blutung auftritt, Abbindung aber auf jeden Fall vorbereiten'
      },
      {
        content: 'Abbindung ist laut Chefarztbeschluss verboten'
      },
    ],
  },
  {
    id: 2387624,
    question: 'Wann muss die HWS-Schiene (Patient bei Bewusstsein) angelegt werden?',
    choices: [
      {
        valid: true,
        content: 'Prinzipiell muss eine HWS-Schienung frühest möglich durchgeführt werden'
      },
      {
        valid: true,
        content: 'Die HWS-Schiene ist vor jeder Manipulation anzulegen, dabei ist der Kopf manuell zu stabilisieren'
      },
      {
        valid: true,
        content: 'Aufgrund des Unfallhergangs und der Patientenbeurteilung an eine HWS-Schiene denken'
      },
      {
        content: 'Der Patient wird grundsätzlich in der Vakuummatratze immobilisiert'
      },
    ],
  },
  {
    id: 2388860,
    question: 'Wann muss ein Pannendreieck auf Freilandstraßen aufgestellt werden?',
    choices: [
      {
        content: 'Bei Nebel muss kein Pannendreieck aufgestellt werden.'
      },
      {
        valid: true,
        content: 'Wenn das Fahrzeug auf einer unübersichtlichen Straßenstelle zum Stillstand gekommen ist'
      },
      {
        valid: true,
        content: 'Wenn das Fahrzeug bei schlechter Sicht zum Stillstand gekommen ist'
      },
      {
        valid: true,
        content: 'Wenn das Fahrzeug bei Dämmerung oder Dunkelheit zum Stillstand gekommen ist'
      },
    ],
  },
  {
    id: 2388657,
    question: 'Wann muss man auch mit einer geringfügigen Verletzung (Wunde) zum Arzt?',
    choices: [
      {
        content: 'Wenn eine kleine Rötung zu sehen ist'
      },
      {
        content: 'Wenn die Wunde schmerzt'
      },
      {
        valid: true,
        content: 'Wenn der Verletzte nicht aktiv gegen Tetanus geimpft ist'
      },
      {
        content: 'Wenn der verletzende Gegenstand weniger als 1,5 mm eindringt'
      },
    ],
  },
  {
    id: 2388929,
    question: 'Wann soll bei Gelenksverletzungen ein Arzt aufgesucht werden?',
    choices: [
      {
        valid: true,
        content: 'bei leichten Schmerzen, keine Besserung nach einigen Tagen'
      },
      {
        valid: true,
        content: 'im Zweifelsfall, ob eine Verletzung vorliegt'
      },
      {
        content: 'Belastung des Körperteils ist möglich, leichte Schmerzen'
      },
      {
        content: 'Wenn nach 24 Stunden noch keine Besserung eingetreten ist'
      },
    ],
  },
  {
    id: 2388315,
    question: 'Wann spricht man von Bewusstlosigkeit?',
    choices: [
      {
        valid: true,
        content: 'normale Atmung und Lebenszeichen vorhanden'
      },
      {
        valid: true,
        content: 'keine Reaktion auf sanftes Schütteln an den Schultern'
      },
      {
        valid: true,
        content: 'keine Reaktion auf lautes Ansprechen'
      },
      {
        content: 'Reaktion auf Schmerzreiz'
      },
    ],
  },
  {
    id: 2388316,
    question: 'Wann spricht man von Bewusstseinsklarheit?',
    choices: [
      {
        valid: true,
        content: 'wenn der Patient auf äußere Reize situationsgerecht reagiert'
      },
      {
        valid: true,
        content: 'wenn der Patient persönlich, zeitlich und örtlich voll orientiert ist'
      },
      {
        valid: true,
        content: 'wenn der Patient in der Lage ist, sich selbst und seine Umgebung ungestört wahrzunehmen'
      },
      {
        content: 'wenn der Patient nicht alkoholisiert ist'
      },
    ],
  },
  {
    id: 2388317,
    question: 'Wann spricht man von Bewusstseinstrübung?',
    choices: [
      {
        valid: true,
        content: 'wenn der Patient auf äußere Reize zwar reagiert, die Reaktion jedoch verlangsamt erfolgt'
      },
      {
        valid: true,
        content: 'es handelt sich um einen Zustand verminderter Wahrnehmung'
      },
      {
        valid: true,
        content: 'der Patient wirkt teilnahmslos und apathisch, ist aber erweckbar'
      },
      {
        content: 'der Patient kann ungestört seine Umgebung wahrnehmen'
      },
    ],
  },
  {
    id: 2388936,
    question: 'Wann spricht man von Bluthochdruck (Hypertonie)?',
    choices: [
      {
        content: 'Blutdruckwerte von systolisch über 120 mm Hg und diastolisch über 90 mm Hg'
      },
      {
        valid: true,
        content: 'Blutdruckwerte von systolisch ab 140 mm Hg und diastolisch ab 90 mm Hg'
      },
      {
        content: 'Blutdruckwert von systolisch unter 100 mm Hg'
      },
      {
        content: 'Blutdruckwerte von systolisch 100 - 140 mm Hg und diastolisch von 60 - 90 mm Hg'
      },
    ],
  },
  {
    id: 2388688,
    question: 'Wann spricht man von einem bewusstlosen Notfallpatienten?',
    choices: [
      {
        content: 'Patient reagiert nur auf Schmerzreize wie Zwicken in die Wangen'
      }, 
      {
        content: 'Notfallpatient ist verwirrt und kann sich an nichts erinnern'
      }, 
      {
        valid: true,
        content: 'keine Reaktion auf lautes Ansprechen und sanftes Schütteln'
      },
      {
        content: 'Bewusstseinslage kann durch den Ersthelfer nicht überprüft werden'
      }, 
    ],
  },
  {
    id: 2387625,
    question: 'Wann spricht man von einem geschlossenen Knochenbruch?',
    choices: [
      {
        valid: true,
        content: 'Bruch ohne sichtbare Wunde'
      },
      {
        content: 'Bruch mit sichtbarer Wunde'
      },
      {
        content: 'Einfacher Bruch ohne Zertrümmerung'
      },
      {
        content: 'Bruch ohne Weichteilverletzung'
      },
    ],
  },
  {
    id: 2388699,
    question: 'Wann spricht man von einem Infektionstransport?',
    choices: [
      {
        content: 'Transport von infektiösem Material'
      },
      {
        content: 'Transport des RTW zur Desinfektion'
      },
      {
        valid: true,
        content: 'Transport eines Patienten, von dem bekannt oder zu vermuten ist, dass er an einer übertragbaren Krankheit leidet'
      },
      {
        content: 'Rücktransport des RTW, wenn ein infektiöser Patient transportiert wurde'
      },
    ],
  },
  {
    id: 2387626,
    question: 'Wann spricht man von einem offenen Knochenbruch?',
    choices: [
      {
        content: 'Bruch ohne sichtbare Wunde im Bereich der Bruchstelle'
      },
      {
        valid: true,
        content: 'Bruch mit sichtbarer Wunde im Bereich der Bruchstelle'
      },
      {
        content: 'Bruch mit Weichteilverletzung'
      },
      {
        content: 'Bruch mit Gefäßverletzung'
      },
    ],
  },
  {
    id: 2388318,
    question: 'Wann spricht man von einer Bewusstseinsveränderung?',
    choices: [
      {
        content: 'Bewusstseinsverändertes Verhalten zeigt ein Mensch dann, wenn er auf äußere Reize zwar reagiert, die Reaktion jedoch verlangsamt erfolgt'
      },
      {
        valid: true,
        content: 'Bewusstseinsverändertes Verhalten zeigt ein Mensch dann, wenn sein Bewusstsein zwar erhalten, aber eine situationsgerechte Realitätseinschätzung nicht mehr gewährleistet ist'
      },
      {
        content: 'es handelt sich um einen Zustand ungestörter Wahrnehmung, der Patient wirkt zwar teilnahmslos und apathisch, ist aber erweckbar'
      },
      {
        valid: true,
        content: 'es kann zu abnormen Reaktionen kommen: Angst-Erregungszustände, Rausch, Verwirrtheit, Orientierungslosigkeit, Wahnvorstellungen, Sinnestäuschungen usw.'
      },
    ],
  },
  {
    id: 2388700,
    question: 'Wann werden unsterile Einmalhandschuhe verwendet?',
    choices: [
      {
        valid: true,
        content: 'Hilfe bei notärztlichen Maßnahmen, bei denen keine Sterilität vonnöten ist (z. B. Assistenz bei Intubation)'
      },
      {
        content: 'Bei notärztlichen Tätigkeiten wie z.B. Thoraxpunktion und Legen von Harnkathetern'
      },
      {
        valid: true,
        content: 'Kontakt mit Blut und Körperausscheidungen (Erbrochenes, Stuhl, Harn)'
      },
      {
        content: 'Geburt'
      },
    ],
  },
  {
    id: 2387627,
    question: 'Wann wird die Abnabelung durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'beim reglosen Neugeborenen sofort'
      },
      {
        content: 'nach der Absaugung'
      },
      {
        valid: true,
        content: 'auf jeden Fall vor dem Weitertransport'
      },
      {
        valid: true,
        content: 'bei reifen Neugeborenen mindestens 1 Minute bis zum Abnabeln warten'
      },
    ],
  },
  {
    id: 2388658,
    question: 'Wann wird ein Pflasterwundverband verwendet?',
    choices: [
      {
        content: 'Zeckenbiss'
      },
      {
        valid: true,
        content: 'Bei kleinen, nicht stark blutenden Wunden'
      },
      {
        content: 'Verbrennungen'
      },
      {
        content: 'Erfrierungen'
      },
    ],
  },
  {
    id: 2387628,
    question: 'Warum besteht bei Knochenbrüchen Schockgefahr?',
    choices: [
      {
        content: 'Schock entsteht nur bei offenen Knochenbrüchen mit starker Blutung'
      },
      {
        content: 'Weil jeder Verletzte durch den Unfall erschrocken ist'
      },
      {
        valid: true,
        content: 'Weil jeder Knochenbruch zu Blutverlusten führen kann'
      },
      {
        valid: true,
        content: 'Weil der Blutverlust ins Gewebe beträchtlich sein kann'
      },
    ],
  },
  {
    id: 2388177,
    question: 'Warum dürfen Sauerstoffgeräte niemals mit Fett oder Öl in Berührung kommen?',
    choices: [
      {
        content: 'Weil die Gummischläuche und Masken porös werden und dann nicht mehr dicht sind.'
      },
      {
        content: 'Wegen der Gefahr einer Verunreinigung der Armaturen und der Gefahr einer Fehleinstellung'
      },
      {
        valid: true,
        content: 'Weil Sauerstoff eine Selbstentzündung von Öl und Fett bewirken kann.'
      },
      {
        content: 'Weil die Anlage verschmutzt wird und eine Reinigung schwer möglich ist.'
      },
    ],
  },
  {
    id: 2387629,
    question: 'Warum ist ein Wirbelsäulentrauma gefährlich?',
    choices: [
      {
        valid: true,
        content: 'Schädigung des Wirbelkanals'
      },
      {
        valid: true,
        content: 'Eine Schädigung des Rückenmarks durch Quetschung, Einblutung oder Durchtrennung kann zu einer Querschnittlähmung führen'
      },
      {
        content: 'Es führt sofort zum Atem-Kreislauf-Stillstand'
      },
      {
        content: 'Nur eine Durchtrennung des Rückenmarks führt zu einer Querschnittlähmung.'
      },
    ],
  },
  {
    id: 2388610,
    question: 'Warum ist man zur Hilfeleistung verpflichtet?',
    choices: [
      {
        valid: true,
        content: 'Aufgrund gesetzlicher Verpflichtung'
      },
      {
        content: 'Aufgrund einer Weisungspflicht'
      },
      {
        content: 'Aufgrund des Erste-Hilfe-Kurses'
      },
      {
        valid: true,
        content: 'Aufgrund moralischer Verpflichtung'
      },
    ],
  },
  {
    id: 2388659,
    question: 'Warum kann ein Zeckenbiss gefährlich sein?',
    choices: [
      {
        content: 'Übertragung von Tollwut'
      },
      {
        valid: true,
        content: 'Übertragung von anderen Krankheiten (z.B. Borreliose)'
      },
      {
        valid: true,
        content: 'Übertragung von Hirnhautentzündung'
      },
      {
        content: 'Übertragung von Wundstarrkrampf'
      },
    ],
  },
  {
    id: 2388875,
    question: 'Warum muss der Sturzhelm (Verletzer reagiert nicht) abgenommen werden?',
    choices: [
      {
        valid: true,
        content: 'zum Überprüfen der Atmung'
      }, 
      {
        valid: true,
        content: 'Gefahr des Erstickens'
      }, 
      {
        valid: true,
        content: 'um die Atmung zu ermöglichen bzw. zu erleichtern'
      }, 
      {
        valid: true,
        content: 'zur Durchführung des Notfallchecks'
      }, 
    ],
  },
  {
    id: 2387630,
    question: 'Warum muss eine Gebärende nach erfolgtem Blasensprung liegend transportiert werden?',
    choices: [
      {
        content: 'Gefahr der vorzeitigen Plazentalösung'
      },
      {
        content: 'Gefahr der Eklampsie'
      },
      {
        valid: true,
        content: 'Gefahr des Nabelschnurvorfalles'
      },
      {
        content: 'Gefahr einer Fehlgeburt'
      },
    ],
  },
  {
    id: 2387631,
    question: 'Warum muss eine Schwangere nach erfolgtem Blasensprung unbedingt liegend transportiert werden?',
    choices: [
      {
        content: 'Gefahr der vorzeitigen Plazentalösung'
      },
      {
        valid: true,
        content: 'Gefahr des Nabelschnurvorfalles'
      },
      {
        content: 'Gefahr der Eklampsie'
      },
      {
        content: 'Gefahr des Erbrechens'
      },
    ],
  },
  {
    id: 2387632,
    question: 'Warum soll das vorgefundene Ätzmittel ins Krankenhaus mitgegeben werden?',
    choices: [
      {
        valid: true,
        content: 'Zur Identifizierung'
      },
      {
        content: 'Zur Entsorgung'
      },
      {
        content: 'Zum Schutz anderer Personen (Unfallverhütung)'
      },
      {
        content: 'Um geeignetes Material zum Neutralisieren zu finden'
      },
    ],
  },
  {
    id: 2387633,
    question: 'Warum soll jede Bewegung eines Unterkühlten vermieden werden?',
    choices: [
      {
        valid: true,
        content: 'Unter Umständen Bergungstod'
      },
      {
        valid: true,
        content: 'Erhöhter Sauerstoffverbrauch'
      },
      {
        content: 'Gefahr eines Knochenbruchs'
      },
      {
        valid: true,
        content: 'Wärmeverlust und Muskelrisse'
      },
    ],
  },
  {
    id: 2388660,
    question: 'Warum werden bei schwerer Schädigung eines Auges beide Augen keimfrei bedeckt?',
    choices: [
      {
        content: 'Um die Lichtstärke zu vermindern'
      },
      {
        valid: true,
        content: 'Ohne Bedeckung würde das verletzte Auge synchron den Bewegungen des unverletzten Auges folgen'
      },
      {
        content: 'Damit die Schädigung nicht auf das unverletzte Auge übergreifen kann'
      },
      {
        content: 'Um bleibendes Schielen zu verhindern'
      },
    ],
  },
  {
    id: 2388319,
    question: 'Warum werden beim Überstrecken des Kopfes nackenwärts die Atemwege frei?',
    choices: [
      {
        content: 'Die Speiseröhre wird voll geöffnet, die Atemwege sind frei'
      },
      {
        content: 'Es wird verhindert, dass der Unterkiefer zurücksinkt - die Atemwege sind frei'
      },
      {
        content: 'Da Fremdkörper im Rachen entfernt werden - die Atemwege sind frei'
      },
      {
        valid: true,
        content: 'Da die zurückgesunkene Zunge von der Rachenhinterwand angehoben wird und somit die Atemwege frei sind'
      },
    ],
  },
  {
    id: 2389098,
    question: 'Was bedeuten die Buchstaben A und B im Rahmen der Patientenbeurteilung?',
    choices: [
      {
        valid: true,
        content: 'A=Atemweg'
      },
      {
        content: 'A=Atmung'
      },
      {
        valid: true,
        content: 'B=Beurteilung der Atmung'
      },
      {
        content: 'B=Beurteilung der Haut'
      },
    ],
  },
  {
    id: 2389099,
    question: 'Was bedeuten die Buchstaben C und D im Rahmen der Patientenbeurteilung?',
    choices: [
      {
        valid: true,
        content: 'C=Circulation/Kreislauf'
      },
      {
        valid: true,
        content: 'D=Disability/Defizite in der Neurologie'
      },
      {
        content: 'D=Beurteilung der Durchblutung'
      },
      {
        content: 'C=Beurteilung der kardialen Leistung (Herzminutenvolumen)'
      },
    ],
  },
  {
    id: 2389100,
    question: 'Was bedeuten die Buchstaben L und E im Rahmen der SAMPLE Anamnese?',
    choices: [
      {
        valid: true,
        content: 'L=letzte Nahrungsaufnahme'
      },
      {
        valid: true,
        content: 'E=Ereignis (was hat der Patient gemacht, bevor die Beschwerden begonnen haben)'
      },
      {
        content: 'L=Lagerung gemäß der Anamnese berücksichtigen'
      },
      {
        content: 'E=Exposure (erheben/erweiterte Untersuchung)'
      },
    ],
  },
  {
    id: 2389101,
    question: 'Was bedeuten die Buchstaben M und P im Rahmen der SAMPLE Anamnese?',
    choices: [
      {
        valid: true,
        content: 'M=Medikamente, die der Patient regelmäßig einnimmt'
      },
      {
        valid: true,
        content: 'P=Patientengeschichte'
      },
      {
        content: 'P=Pulsoxymetrie anwenden'
      },
      {
        content: 'M=Motorik überprüfen'
      },
    ],
  },
  {
    id: 2389102,
    question: 'Was bedeuten die Buchstaben S und A im Rahmen der SAMPLE Anamnese?',
    choices: [
      {
        valid: true,
        content: 'S=Symptome'
      },
      {
        valid: true,
        content: 'S=Schmerzen'
      },
      {
        valid: true,
        content: 'A=Allergien'
      },
      {
        content: 'A=Atmung'
      },
    ],
  },
  {
    id: 2388689,
    question: 'Was bedeutet "defibrillieren"?',
    choices: [
      {
        content: 'flimmern'
      }, 
      {
        content: 'früherkennen'
      }, 
      {
        valid: true,
        content: 'entflimmern'
      },
      {
        content: 'reanimieren'
      }, 
    ],
  },
  {
    id: 2388421,
    question: 'Was bedeutet "die Einsatzbereitschaft zu erhalten"?',
    choices: [
      {
        valid: true,
        content: 'Nicht übermüdet den Dienst anzutreten'
      },
      {
        valid: true,
        content: 'Keinen Alkohol in angemessener Zeit vor und während des Dienstes zu sich zu nehmen'
      },
      {
        valid: true,
        content: 'Einhaltung seiner Pflichten'
      },
      {
        content: 'Nur die vorgeschriebenen Dienste durchzuführen, um in Übung zu bleiben'
      },
    ],
  },
  {
    id: 2389132,
    question: 'Was bedeutet ADR und was wird damit geregelt?',
    choices: [
      {
        content: 'Austria Direkt Route'
      },
      {
        valid: true,
        content: 'European Agreement concerning the International Carriage of Dangerous Goods by Road'
      },
      {
        valid: true,
        content: 'Europäisches Übereinkommen über die internationale Beförderung gefährlicher Güter auf der Straße'
      },
      {
        content: 'Internationale Straßenverkehrsregeln'
      },
    ],
  },
  {
    id: 2388937,
    question: 'Was bedeutet der Begriff Venenthrombose?',
    choices: [
      {
        content: 'Luft in den Venen'
      },
      {
        valid: true,
        content: 'Blutgerinnselbildung, vor allem in den tiefen Bein- und Beckenvenen'
      },
      {
        content: 'Blutgerinnselbildung an den Herzklappen und der Herzinnenwand'
      },
      {
        content: 'Entzündung einer Vene'
      },
    ],
  },
  {
    id: 2388178,
    question: 'Was bedeutet der Fachausdruck "intravenös"?',
    choices: [
      {
        content: 'Unter die Haut'
      },
      {
        content: 'In die Arterie'
      },
      {
        content: 'In die Haut'
      },
      {
        valid: true,
        content: 'In die Vene'
      },
    ],
  },
  {
    id: 2387634,
    question: 'Was bedeutet der Fachausdruck Eklampsie?',
    choices: [
      {
        content: 'Streckkrämpfe nach einem Epilepsieanfall'
      },
      {
        valid: true,
        content: 'Krampfanfälle in der Spätschwangerschaft'
      },
      {
        content: 'Blitzartige Muskelzuckungen mit plötzlichem Hinstürzen'
      },
      {
        content: 'Fieberkrampf'
      },
    ],
  },
  {
    id: 2388679,
    question: 'Was bedeutet die Abkürzung "AED"?',
    choices: [
      {
        content: 'Automatische - Eingangs - Dosis'
      },
      {
        content: 'Automatische - Erdgas - Dauerfunktion'
      },
      {
        valid: true,
        content: 'Automatisierte - Externe - Defibrillation'
      },
      {
        content: 'Automatisierte - Extrem - Defibrillation'
      },
    ],
  },
  {
    id: 2388179,
    question: 'Was bedeutet die Abkürzung "PEEP"?',
    choices: [
      {
        content: 'Negativer extrasystolischer Druck'
      },
      {
        valid: true,
        content: 'Positiver endexspiratorischer Druck (Positive EndExpiratory Pressure)'
      },
      {
        content: 'Negativer respiratorischer Beatmungsdruck'
      },
      {
        content: 'Positiver enddiastolischer Druck'
      },
    ],
  },
  {
    id: 2388180,
    question: 'Was bedeutet die Abkürzung AED?',
    choices: [
      {
        content: 'Automated Exorbitant Defibrillator'
      },
      {
        valid: true,
        content: 'Automated External Defibrillator'
      },
      {
        content: 'Autonom External Defibrillator'
      },
      {
        content: 'Automated External Defusing'
      },
    ],
  },
  {
    id: 2389133,
    question: 'Was bedeutet die obere Nummer auf einer Warntafel für Gefahrenguttransporte?',
    choices: [
      {
        content: 'Dient zu Kennzeichnung des Stoffes'
      },
      {
        valid: true,
        content: 'Dient zu Kennzeichnung der Gefahr'
      },
      {
        content: 'Dient zu Kennzeichnung der Viskosität'
      },
      {
        content: 'Dient zu Kennzeichnung des Fahrers'
      },
    ],
  },
  {
    id: 2389134,
    question: 'Was bedeutet die untere Nummer auf einer Warntafel für Gefahrenguttransporte?',
    choices: [
      {
        content: 'Gibt an um welche Gefahr es sich handelt (Gefahrennummer)'
      },
      {
        content: 'Gibt an um welches Herstellungsland es sich handelt (Ursprungsnummer)'
      },
      {
        valid: true,
        content: 'Gibt an um welchen Stoff es sich handelt (Stoffnummer)'
      },
      {
        content: 'Gibt an um welches Alter sich handelt (Geburtsjahr)'
      },
    ],
  },
  {
    id: 2389135,
    question: 'Was bedeutet MLS?',
    choices: [
      {
        valid: true,
        content: 'Mobile Leitstelle'
      },
      {
        content: 'Material und Leitstelle'
      },
      {
        content: 'Magazinlagersystem'
      },
      {
        content: 'Mediale Leitstelle'
      },
    ],
  },
  {
    id: 2388701,
    question: 'Was bedeutet Sterilisation und was muss steril sein?',
    choices: [
      {
        content: 'Die Abtötung von Keimen ist nur durch radioaktive Lösungen möglich'
      },
      {
        valid: true,
        content: 'Die Abtötung aller pathogenen und apathogenen Keime'
      },
      {
        content: 'Die Behandlung von Infektionskrankheiten'
      },
      {
        valid: true,
        content: 'Alles Material für die Wundversorgung, Material, welches unter die Hautoberfläche gelangt'
      },
    ],
  },
  {
    id: 2388600,
    question: 'Was benötigen Sie für einen Fingerdruck?',
    choices: [
      {
        valid: true,
        content: 'saugendes Material, zb. Wundauflage'
      }, 
      {
        content: 'Druckkörper'
      }, 
      {
        content: 'Klemme'
      }, 
      {
        content: 'Wunddesinfektion'
      }, 
    ],
  },
  {
    id: 2389136,
    question: 'Was besagt die Kemler-Nummer?',
    choices: [
      {
        content: 'Nummer zur Kennzeichnung des Stoffes'
      },
      {
        content: 'Allgemeiner Hinweis auf das Gefahrengut'
      },
      {
        content: 'Chemische Bezeichnung des transportierten Stoffes'
      },
      {
        valid: true,
        content: 'Die Gefährlichkeit eines Stoffes'
      },
    ],
  },
  {
    id: 2388661,
    question: 'Was bewirkt die Anwendung von Wasser bei Verbrennungen und wie leistet man am besten Erste Hilfe?',
    choices: [
      {
        valid: true,
        content: 'Verhindert das Nachbrennen im Gewebe und führt zur Schmerzlinderung'
      },
      {
        valid: true,
        content: 'Sofort unter reines, fließendes handwarmes Wasser halten'
      },
      {
        content: 'Führt zu einer erhöhten Infektionsgefahr und Blutvergiftung'
      },
      {
        valid: true,
        content: 'Ist dem Verletzten kalt, ist die kühlende Spülung zu stoppen'
      },
    ],
  },
  {
    id: 2387635,
    question: 'Was bewirkt ein Kältereiz (Erfrierung) im betroffenen Gewebe?',
    choices: [
      {
        content: 'Verstärkung der Blutzufuhr'
      },
      {
        valid: true,
        content: 'Drosselung der Blutzufuhr ins betroffene Gewebe'
      },
      {
        content: 'Verursacht nur leichte Schmerzen, wirkt sich auf die Durchblutung nicht aus'
      },
      {
        content: 'Führt relativ rasch zu einer Vereisung der flüssigen Blutbestandteile'
      },
    ],
  },
  {
    id: 2388422,
    question: 'Was dient als Erkennungszeichen für Einsatzfahrzeuge?',
    choices: [
      {
        valid: true,
        content: 'Blaulicht'
      },
      {
        content: 'Corporate Design'
      },
      {
        valid: true,
        content: 'Folgetonhorn'
      },
      {
        content: 'Umfeldbeleuchtung'
      },
    ],
  },
  {
    id: 2388686,
    question: 'Was gehört zum Notfallcheck?',
    choices: [
      {
        content: 'Oberkörper hoch'
      }, 
      {
        content: 'Beine hoch lagern'
      }, 
      {
        valid: true,
        content: 'laut ansprechen und sanft schütteln'
      }, 
      {
        valid: true,
        content: 'Atemwege freimachen und Atmung kontrollieren'
      }, 
    ],
  },
  {
    id: 2388423,
    question: 'Was gehört zur persönlichen Schutzausrüstung?',
    choices: [
      {
        valid: true,
        content: 'Einmalhandschuhe'
      },
      {
        valid: true,
        content: 'knöchelhohe Berufschuhe gemäß EN 344'
      },
      {
        valid: true,
        content: 'Einsatzbekleidung mit reflektierenden Streifen'
      },
      {
        valid: true,
        content: 'ggf. Schutzhelm mit Gesichtsschutz'
      },
    ],
  },
  {
    id: 2388424,
    question: 'Was ist bei der Betreuung während des Transports zu beachten?',
    choices: [
      {
        content: 'Die Aufgabe des Sanitäters ist allein die fachgerechte Hilfeleistung'
      },
      {
        valid: true,
        content: 'Die psychische Betreuung des Patienten ist eine wichtige Sanitätshilfemaßnahme'
      },
      {
        valid: true,
        content: 'Der Körperkontakt mit Patienten soll einfühlsam, aber nicht aufdringlich sein'
      },
      {
        content: 'Die psychische Betreuung ist nicht Aufgabe des Sanitäters'
      },
    ],
  },
  {
    id: 2388181,
    question: 'Was ist bei einer Verschlechterung des Patientenzustandes durch eine laufende Infusion zu tun?',
    choices: [
      {
        valid: true,
        content: 'Infusion abklemmen und Meldung an den Notarzt'
      },
      {
        content: 'Infusion entfernen'
      },
      {
        content: 'Wechsel der Infusionslösung'
      },
      {
        valid: true,
        content: 'Protokollierung der Einzelheiten am Einsatzprotokoll'
      },
    ],
  },
  {
    id: 2389103,
    question: 'Was ist beim Umgang mit älteren Patienten zu beachten?',
    choices: [
      {
        valid: true,
        content: 'Eine tiefe Stimmlage ist von Vorteil'
      },
      {
        valid: true,
        content: 'Eine deutliche Aussprache beugt Missverständnissen vor'
      },
      {
        valid: true,
        content: 'Die Sätze sollten kurz gehalten werden'
      },
      {
        content: 'Es ist leise zu sprechen, damit der Patient nicht ängstlich wird'
      },
    ],
  },
  {
    id: 2389104,
    question: 'Was ist beim Umgang mit Kindern als Patienten zu beachten?',
    choices: [
      {
        valid: true,
        content: 'Kinder mit dem Vornamen ansprechen'
      },
      {
        valid: true,
        content: 'Wenn möglich, Spielzeug mitnehmen lassen'
      },
      {
        valid: true,
        content: 'Wunden rasch versorgen und dabei die Handgriffe erklären'
      },
      {
        valid: true,
        content: 'Im Gespräch auf Gemeinsamkeiten hinweisen'
      },
    ],
  },
  {
    id: 2387636,
    question: 'Was ist beim Vorliegen einer Erfrierung erlaubt?',
    choices: [
      {
        content: 'Direkt erwärmen'
      },
      {
        content: 'Frottieren'
      },
      {
        content: 'Mit Schnee einreiben'
      },
      {
        valid: true,
        content: 'Warme und gezuckerte Getränke verabreichen'
      },
    ],
  },
  {
    id: 2388662,
    question: 'Was ist das Prinzip bei der Versorgung von chemischen Wunden?',
    choices: [
      {
        content: 'Neutralisierung durch die Verabreichung von Gegenmitteln'
      },
      {
        content: 'Bei chemischen Wunden im Verdauungstrakt stets zum Erbrechen bringen'
      },
      {
        valid: true,
        content: 'Die rasche Entfernung bzw. Verdünnung der ätzenden Substanz'
      },
      {
        content: 'Patienten in sitzender Position so schnell wie möglich zum Betriebsarzt bringen'
      },
    ],
  },
  {
    id: 2389137,
    question: 'Was ist das Ziel der Triage?',
    choices: [
      {
        content: 'Die Einteilung der Einsatzkräfte im Schadensraum'
      },
      {
        valid: true,
        content: 'Möglichst viele Verletzte mit Überlebenschancen zu behandeln'
      },
      {
        valid: true,
        content: 'Die erste Triage ist innerhalb kürzester Zeit und unter minimalem Zeitaufwand durchzuführen'
      },
      {
        content: 'Die Überlebenden und Toten zu trennen'
      },
    ],
  },
  {
    id: 2388320,
    question: 'Was ist der Auslöser eines anaphylaktischen Schocks?',
    choices: [
      {
        content: 'Überschwemmung des Körpers mit Bakteriengiften'
      },
      {
        content: 'Flüssigkeitsverlust bei Durchfallerkrankungen'
      },
      {
        content: 'Starker Blutverlust'
      },
      {
        valid: true,
        content: 'Kontakt mit einem Stoff, gegen den Überempfindlichkeit besteht'
      },
    ],
  },
  {
    id: 2387637,
    question: 'Was ist der Druckpuls?',
    choices: [
      {
        valid: true,
        content: 'Langsamer, kräftiger Puls = Druckpuls (unter 50 /min), z.B. infolge einer Hirndrucksteigerung'
      },
      {
        content: 'Wenn jemand von außen auf den Puls drückt'
      },
      {
        content: 'Wenn sich der Blutdruck in den Arterien erhöht und somit stärker auf die Arterienwände drückt'
      },
      {
        content: 'Wenn die Pulsfrequenz auf über 120 /min steigt und der Puls somit gut tastbar ist'
      },
    ],
  },
  {
    id: 2389138,
    question: 'Was ist der Unterschied zwischen einem Großunfall und einer Katastrophe?',
    choices: [
      {
        content: 'Der Großunfall wird vom Bezirkshauptmann ausgerufen'
      },
      {
        valid: true,
        content: 'Der Unterschied liegt darin, dass eine Katastrophe von der Behörde ausgerufen werden muss. Ein Großunfall bedarf keiner behördlichen Intervention.'
      },
      {
        content: 'Von einem Großunfall spricht man ab 18 Verletzten'
      },
      {
        content: 'Von einer Katastrophe spricht man ab einer Zahl von 16 Verletzten'
      },
    ],
  },
  {
    id: 2388703,
    question: 'Was ist die "Routinewartung" von Rettungsmitteln?',
    choices: [
      {
        valid: true,
        content: 'Routinemäßig durchzuführende Desinfektions- und Reinigungsmaßnahmen'
      },
      {
        content: 'Überprüfung des Öl-, und Wasserstandes'
      },
      {
        valid: true,
        content: 'Vorbeugende Maßnahmen, um Kreuzinfektionen zwischen Patienten zu verhindern'
      },
      {
        content: 'Wird einmal jährlich mit dem "Pickerl" gemacht'
      },
    ],
  },
  {
    id: 2389139,
    question: 'Was ist die Behandlungsstelle I?',
    choices: [
      {
        content: 'Hier werden leichtverletzte Patienten versorgt'
      },
      {
        content: 'Hier werden Patienten mit Transportpriorität versorgt'
      },
      {
        valid: true,
        content: 'Hier werden sofortige Therapie und Noteingriffe durchgeführt'
      },
      {
        content: 'Hier warten Patienten auf den Abflug mit dem Hubschrauber'
      },
    ],
  },
  {
    id: 2389140,
    question: 'Was ist die Behandlungsstelle II?',
    choices: [
      {
        content: 'Hier werden leichtverletzte Patienten versorgt'
      },
      {
        content: 'Hier werden Unverletzte versorgt'
      },
      {
        content: 'Hier erfolgt die Reanimation von Patienten'
      },
      {
        valid: true,
        content: 'Hier wird die Behandlung und Herstellung der Transportfähigkeit von schwerverletzten bzw. erkrankten Patienten durchgeführt.'
      },
    ],
  },
  {
    id: 2389141,
    question: 'Was ist die Behandlungsstelle III?',
    choices: [
      {
        content: 'Hier erfolgt die Reanimation von Patienten'
      },
      {
        content: 'Hier werden Patienten mit Transportpriorität versorgt'
      },
      {
        valid: true,
        content: 'Hier erfolgt die Betreuung von Leichtverletzten'
      },
      {
        content: 'Hier erfolgt die sofortige Therapie von Schwerstverletzten'
      },
    ],
  },
  {
    id: 2389142,
    question: 'Was ist die Behandlungsstelle IV?',
    choices: [
      {
        content: 'Hier werden sofortige Therapie und Noteingriffe durchgeführt'
      },
      {
        content: 'Hier werden Patienten mit Transportpriorität versorgt'
      },
      {
        valid: true,
        content: 'Dies ist der Behandlungsraum für Schwerstverletzte und Patienten zur aufgeschobenen Behandlung'
      },
      {
        content: 'Dies ist die Sammelstelle für Tote'
      },
    ],
  },
  {
    id: 2388182,
    question: 'Was ist die Grundvoraussetzung für die Durchführung einer Sauerstoffinhalation?',
    choices: [
      {
        valid: true,
        content: 'Die Eigenatmung muss vorhanden sein'
      },
      {
        valid: true,
        content: 'Die Ausatmung muss möglich sein'
      },
      {
        valid: true,
        content: 'Der Patient muss vor der Maßnahme informiert werden'
      },
      {
        content: 'Der Patient muss sich vorher Mund und Nase einfetten (Gefahr der Austrocknung - Schleimhäute)'
      },
    ],
  },
  {
    id: 2388702,
    question: 'Was ist die Inkubationszeit?',
    choices: [
      {
        valid: true,
        content: 'Zeitspanne zwischen Infektion und Ausbruch der Infektionskrankheit'
      },
      {
        content: 'Zeit vom Auftreten der ersten Symptome bis zur Heilung'
      },
      {
        content: 'Zeit vom Eindringen der Krankheitserreger bis zur Heilung'
      },
      {
        content: 'Zeitspanne zwischen Heilung und Immunisierung'
      },
    ],
  },
  {
    id: 2387638,
    question: 'Was ist ein "geschlossener Pneumothorax"?',
    choices: [
      {
        content: 'Durch eine Verletzung der Lunge tritt Blut in den Brustkorb aus und behindert die Atmung.'
      },
      {
        content: 'Durch eine Verletzung der Lunge tritt Blut und Luft in den Brustkorb und es kommt zum Atemstillstand.'
      },
      {
        content: 'Durch Überdruck in der Lunge platzen die Lungenbläschen und es kommt zur Atembehinderung und in weiterer Folge zum Atemstillstand.'
      },
      {
        valid: true,
        content: 'Durch eine innere Verletzung (z.B. Anspießung durch eine gebrochene Rippe, Riss eines Lungenflügels durch einen Aufprall) kann Luft aus dem Atemwegen in den Pleuraspalt (= zwischen Rippen- und Lungenfell) kommen.'
      },
    ],
  },
  {
    id: 2387639,
    question: 'Was ist eine Beckenendlage (Steißgeburt)?',
    choices: [
      {
        content: 'der kindliche Kopf erscheint als erstes bei der Geburt'
      },
      {
        valid: true,
        content: 'bei der Steißgeburt erfolgt zuerst der Durchtritt des Steißes und zuletzt der Durchtritt des Kopfes'
      },
      {
        content: 'die Hand erscheint als erstes im Geburtskanal'
      },
      {
        content: 'bei einer Beckenendlage muss immer ein Kaiserschnitt durchgeführt werden'
      },
    ],
  },
  {
    id: 2388704,
    question: 'Was ist eine Infektion?',
    choices: [
      {
        content: 'Einstich der Injektionsnadel in die Haut'
      },
      {
        valid: true,
        content: 'Eindringen von Mikroorganismen in den Körper mit anschließender Ansiedelung und Vermehrung'
      },
      {
        content: 'Ausbruch von Fieber während einer Krankheit'
      },
      {
        content: 'Symptome bei grippalen Erkrankungen'
      },
    ],
  },
  {
    id: 2388599,
    question: 'Was ist kein Schockzeichen?',
    choices: [
      {
        content: 'erhöhte Schweissproduktion'
      }, 
      {
        valid: true,
        content: 'plötzlicher Zahnverlust'
      }, 
      {
        content: 'blasse Haut'
      }, 
      {
        content: 'zittern'
      }, 
    ],
  },
  {
    id: 2389143,
    question: 'Was ist PLS?',
    choices: [
      {
        content: 'Prokonforme Leitstelle'
      },
      {
        valid: true,
        content: 'Patientenleitsystem'
      },
      {
        valid: true,
        content: 'Dient zur Kennzeichnung von Patienten bei einem Massenunfall'
      },
      {
        content: 'Eine Leitstelle, in der Notrufe aller Hilfsorganisationen entgegengenommen werden.'
      },
    ],
  },
  {
    id: 2388183,
    question: 'Was ist vom Sanitäter bei der Vorbereitung von Infusionen zu beachten?',
    choices: [
      {
        valid: true,
        content: 'Auswahl der richtigen Infusion'
      },
      {
        valid: true,
        content: 'Kontrolle auf Verfärbungen'
      },
      {
        valid: true,
        content: 'Kontrolle des Ablaufdatums'
      },
      {
        valid: true,
        content: 'Kontrolle auf Ausflockungen'
      },
    ],
  },
  {
    id: 2388321,
    question: 'Was ist zu beachten, wenn der bewusstlose Notfallpatient bereits in die stabile Seitenlagerung gebracht wurde?',
    choices: [
      {
        valid: true,
        content: 'allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        valid: true,
        content: 'Kontrolle der Atmung alle 2 Minuten'
      },
      {
        valid: true,
        content: 'weiter mit C, D, E und SAMPLE'
      },
    ],
  },
  {
    id: 2388425,
    question: 'Was ist zu tun, wenn die Kleidung eines Patienten Feuer gefangen hat?',
    choices: [
      {
        valid: true,
        content: 'Betroffenen auf dem Boden wälzen'
      },
      {
        valid: true,
        content: 'Brennende Person am Weglaufen hindern'
      },
      {
        valid: true,
        content: 'Flammen mit Tüchern, Decken oder Kleidungsstücken ersticken'
      },
      {
        content: 'Gar nichts, da diese Situation eine Gefahr für den Helfer darstellt'
      },
    ],
  },
  {
    id: 2388322,
    question: 'Was ist zu tun, wenn eine starke Blutung trotz eines angelegten Druckverbands weiterhin anhält?',
    choices: [
      {
        content: 'Mit Heftpflaster zusätzlich befestigen'
      },
      {
        content: 'Sofort den Druckverband entfernen und einen neuen anlegen'
      },
      {
        valid: true,
        content: 'Einen zweiten Druckverband darüber legen und/oder Fingerdruck ausüben'
      },
      {
        content: 'Eine Abbindung durchführen'
      },
    ],
  },
  {
    id: 2388938,
    question: 'Was kann die Ursache für blutigen Stuhl bei einem Patienten sein?',
    choices: [
      {
        valid: true,
        content: 'Mastdarmkrebs'
      },
      {
        content: 'Verletzungen der Harnröhre'
      },
      {
        valid: true,
        content: 'Darminfektion, schwere Dickdarmerkrankung'
      },
      {
        valid: true,
        content: 'Blutende Hämorrhoiden'
      },
    ],
  },
  {
    id: 2389598,
    question: 'Was kann durch einen Insektenstich entstehen?',
    choices: [
      {
        valid: true,
        content: 'Die Einstichstelle kann sich entzünden.'
      },
      {
        content: 'gar nichts'
      },
      {
        valid: true,
        content: 'allergische Reaktion'
      },
      {
        content: 'Blutung'
      },
    ],
  },
  {
    id: 2387640,
    question: 'Was kann zum Hämatothorax führen?',
    choices: [
      {
        valid: true,
        content: 'Verletzung der Lunge'
      },
      {
        valid: true,
        content: 'Zerreißung großer Lungengefäße'
      },
      {
        valid: true,
        content: 'Blutansammlung im Brustkorbbereich'
      },
      {
        content: 'Beeinträchtigung der Atmung, führt zum Schock'
      },
    ],
  },
  {
    id: 2388870,
    question: 'Was machen Sie, wenn ein verletzter  Motorradfahrer auf dem Bauch liegt und nicht reagiert?',
    choices: [
      {
        valid: true,
        content: 'umdrehen und Helm abnehmen'
      }, 
      {
        content: 'umdrehen und Helm oben lassen'
      }, 
      {
        content: 'Helm in Bauchlage abnehmen'
      }, 
      {
        content: 'stabile Seitenlage mit Helm'
      }, 
    ],
  },
  {
    id: 2388898,
    question: 'Was machen Sie, wenn ein verletzter  Motorradfahrer auf dem Bauch liegt und nicht reagiert?',
    choices: [
      {
        valid: true,
        content: 'umdrehen und Helm abnehmen'
      }, 
      {
        content: 'umdrehen und Helm oben lassen'
      }, 
      {
        content: 'Helm in Bauchlage abnehmen'
      }, 
      {
        content: 'stabile Seitenlage mit Helm'
      }, 
    ],
  },
  {
    id: 2388916,
    question: 'Was machen Sie, wenn ein verletzter  Motorradfahrer auf dem Bauch liegt und nicht reagiert?',
    choices: [
      {
        valid: true,
        content: 'umdrehen und Helm abnehmen'
      }, 
      {
        content: 'umdrehen und Helm oben lassen'
      }, 
      {
        content: 'Helm in Bauchlage abnehmen'
      }, 
      {
        content: 'stabile Seitenlage mit Helm'
      }, 
    ],
  },
  {
    id: 2388184,
    question: 'Was muss beachtet werden, um Messfehler bei der Pulsoxymetrie zu vermeiden?',
    choices: [
      {
        valid: true,
        content: 'bei Zentralisation werden periphere Körperteile schlecht durchblutet'
      },
      {
        valid: true,
        content: 'Nagellack oder Handcreme entfernen'
      },
      {
        valid: true,
        content: 'bei Patienten mit CO-Vergiftung zeigt das Gerät falsch positive Werte'
      },
      {
        content: 'bei Patienten mit CO²-Vergiftung zeigt das Gerät falsch positive Werte'
      },
    ],
  },
  {
    id: 2388705,
    question: 'Was muss beim Transport von Patienten mit Verdacht auf Meningitis bzw. Enzephalitis beachtet werden?',
    choices: [
      {
        valid: true,
        content: 'Es besteht mitunter (z.B. bei Meningokokken) Ansteckungsgefahr'
      },
      {
        valid: true,
        content: 'Kontakt mit dem Patienten während des Transportes auf das unbedingt notwendige Maß beschränken'
      },
      {
        content: 'Patient ist nicht ansteckend, weitere Maßnahmen sind daher nicht erforderlich'
      },
      {
        valid: true,
        content: 'Desinfektion, vorbeugende Medikamenteneinnahmen für betroffene Sanitäter usw. sind vom Aufnahmearzt im Krankenhaus zu erfahren'
      },
    ],
  },
  {
    id: 2388426,
    question: 'Was muss der Einsatzfahrer auf der Fahrt zum Berufungsort beachten?',
    choices: [
      {
        content: 'Dass Blaulicht und Folgetonhorn stets eingeschaltet sind'
      },
      {
        content: 'Dass der Einsatzort binnen 3 Minuten zu erreichen ist'
      },
      {
        valid: true,
        content: 'Direkte Fahrt zum Einsatzort unter Einhaltung der StVO'
      },
      {
        content: 'Das allgemeine bürgerliche Gesetzbuch'
      },
    ],
  },
  {
    id: 2388185,
    question: 'Was muss ein Sanitäter bei einer laufenden Infusion überwachen?',
    choices: [
      {
        valid: true,
        content: 'Patient'
      },
      {
        valid: true,
        content: 'peripherer Venenzugang'
      },
      {
        content: 'Medikamentenkonzentration'
      },
      {
        valid: true,
        content: 'Infusion'
      },
    ],
  },
  {
    id: 2388611,
    question: 'Was muss vom Sanitäter dokumentiert werden?',
    choices: [
      {
        content: 'Nur die Durchführung von lebensrettenden Sofortmaßnahmen'
      },
      {
        valid: true,
        content: 'Die von ihm gesetzten sanitätsdienstlichen Maßnahmen'
      },
      {
        content: 'Das Wetter'
      },
      {
        content: 'Die Einsatzbereitschaft des NAW'
      },
    ],
  },
  {
    id: 2389144,
    question: 'Was sind die Aufgaben der ersten am Schadensplatz eintreffenden Mannschaft bei einem Großunfall?',
    choices: [
      {
        content: 'Sofortige Bergung und Abtransport der Verletzten'
      },
      {
        content: 'Beobachten und warten auf die weiteren Mannschaften'
      },
      {
        content: 'Sofortige santechnische Versorgung der Schwerstverletzten'
      },
      {
        valid: true,
        content: 'Erörterung der Lage, Festlegen der SanHiSt, eventuell Kontaktaufnahme mit Feuerwehr und Exekutive.'
      },
    ],
  },
  {
    id: 2388323,
    question: 'Was sind die häufigsten Ursachen für den Atem-Kreislauf-Stillstand?',
    choices: [
      {
        valid: true,
        content: 'Herzinfarkt'
      },
      {
        valid: true,
        content: 'Elektrounfall'
      },
      {
        valid: true,
        content: 'Elektrolytstörung'
      },
      {
        valid: true,
        content: 'Koronare Herzkrankheiten'
      },
    ],
  },
  {
    id: 2388690,
    question: 'Was sind Erste-Hilfe-Maßnahmen bei einem Kollaps?',
    choices: [
      {
        valid: true,
        content: 'Basismaßnahme - Beine hoch lagern'
      }, 
      {
        content: 'flach am Rücken liegen lassen'
      }, 
      {
        valid: true,
        content: 'Basismaßnahme - Für frische Luft sorgen'
      }, 
      {
        content: 'Eis zum Lutschen verabreichen'
      }, 
    ],
  },
  {
    id: 2388876,
    question: 'Was sind Glieder der Rettungskette?',
    choices: [
      {
        valid: true,
        content: 'Rettungsdienst'
      }, 
      {
        content: 'Vergiftungsinformationszentrale'
      }, 
      {
        valid: true,
        content: 'Erste Hilfe leisten'
      }, 
      {
        content: 'Warnweste anziehen'
      }, 
    ],
  },
  {
    id: 2389573,
    question: 'Was sind normale Stressreaktionen im ersten Monat nach einem traumatischen Einsatz?',
    choices: [
      {
        valid: true,
        content: 'Aufdrängenden Erinnerungen'
      },
      {
        content: 'Pupillenerweiterung'
      },
      {
        valid: true,
        content: 'Erhöhte Erregbarkeit'
      },
      {
        valid: true,
        content: 'Vermeidungs-, Abstumpfungs- und Rückzugsverhalten'
      },
    ],
  },
  {
    id: 2389145,
    question: 'Was sind Rotkreuz-Hilfseinheiten (RK-HE)?',
    choices: [
      {
        content: '2 Rotkreuz-Abteilungen'
      },
      {
        content: 'Internationale Hilfseinheiten'
      },
      {
        valid: true,
        content: 'Standardisierte und einheitliche Module zur Bewältigung von Großschadens- und Katastrophenereignissen.'
      },
      {
        content: 'Jede Dienststelle entscheidet selbst über die Zusammenstellung der RK-HE.'
      },
    ],
  },
  {
    id: 2388324,
    question: 'Was sind unsichere Todeszeichen?',
    choices: [
      {
        content: 'Totenflecken'
      },
      {
        valid: true,
        content: 'fehlende Atmung'
      },
      {
        valid: true,
        content: 'lichtstarre Pupillen'
      },
      {
        valid: true,
        content: 'Reflexlosigkeit'
      },
    ],
  },
  {
    id: 2388028,
    question: 'Was sind Wehen?',
    choices: [
      {
        content: 'Bewegungen des Kindes im Uterus'
      },
      {
        valid: true,
        content: 'Rhythmische Kontraktionen der Gebärmuttermuskulatur'
      },
      {
        content: 'Krampfartige Schmerzen bei Blasenentzündung'
      },
      {
        content: 'Anzeichen einer Schwangerschaft'
      },
    ],
  },
  {
    id: 2387641,
    question: 'Was sind Wehen?',
    choices: [
      {
        content: 'Bewegungen des Kindes im Uterus'
      },
      {
        valid: true,
        content: 'Rhythmische Kontraktionen der Gebärmuttermuskulatur'
      },
      {
        content: 'Krampfartige Schmerzen bei Blasenentzündung'
      },
      {
        content: 'Anzeichen einer Schwangerschaft'
      },
    ],
  },
  {
    id: 2387909,
    question: 'Was sind Wehen?',
    choices: [
      {
        content: 'Bewegungen des Kindes im Uterus'
      },
      {
        valid: true,
        content: 'Rhythmische Kontraktionen der Gebärmuttermuskulatur'
      },
      {
        content: 'Krampfartige Schmerzen bei Blasenentzündung'
      },
      {
        content: 'Anzeichen einer Schwangerschaft'
      },
    ],
  },
  {
    id: 2388691,
    question: 'Was soll ein Helfer bei der Defibrillation beachten?',
    choices: [
      {
        valid: true,
        content: 'Die Elektroden sollen fest auf den Brustkorb geklebt werden'
      }, 
      {
        valid: true,
        content: 'Den Patienten während dem Auslösen des Schocks nicht berühren'
      }, 
      {
        valid: true,
        content: 'nach Möglichkeit einen nassen Brustkorb vorher abtrocknen'
      }, 
      {
        content: 'den Verletzten während der Schockabgabe berühren (Qualitätskontrolle)'
      }, 
    ],
  },
  {
    id: 2388877,
    question: 'Was soll man bei einem Notruf beachten?',
    choices: [
      {
        valid: true,
        content: 'Den Anweisungen der Leitstelle folgen'
      }, 
      {
        valid: true,
        content: 'Sich Zeit nehmen und frühestmöglich den Notruf wählen'
      }, 
      {
        content: 'Notruf wählen, Notfallort nennen, auflegen und Erste Hilfe leisten'
      }, 
      {
        content: 'Notrufsäulen nur bei Autopannen verwenden'
      }, 
    ],
  },
  {
    id: 2388601,
    question: 'Was sollte der Ersthelfer bei der Blutstillung vermeiden?',
    choices: [
      {
        content: 'Verwendung einer keimfreien Wundauflage'
      }, 
      {
        content: 'Durchführung der Basismaßnahmen'
      }, 
      {
        valid: true,
        content: 'Direkter Kontakt mit Blut'
      },
      {
        content: 'Verwendung von Mullbindenverbänden'
      }, 
    ],
  },
  {
    id: 2389600,
    question: 'Was tust du bei einem Insektenstich im Mund-Rachen-Raum?',
    choices: [
      {
        valid: true,
        content: 'Notruf'
      },
      {
        content: 'mit Wasser gurgeln lassen'
      },
      {
        valid: true,
        content: 'Eis lutschen lassen'
      },
      {
        content: 'warme Umschläge um den Hals legen'
      },
    ],
  },
  {
    id: 2388612,
    question: 'Was umfasst (lt. SanG) der Tätigkeitsbereich des Rettungssanitäters?',
    choices: [
      {
        valid: true,
        content: 'Aufrechterhaltung und Beendigung von Infusionen'
      },
      {
        valid: true,
        content: 'Verabreichung von Sauerstoff'
      },
      {
        valid: true,
        content: 'Durchführung lebensrettender Sofortmaßnahmen'
      },
      {
        content: 'Assistenz bei notärztlichen Maßnahmen'
      },
    ],
  },
  {
    id: 2388427,
    question: 'Was umfasst der Betreuungseinsatz?',
    choices: [
      {
        valid: true,
        content: 'Soziale Betreuung von Betroffenen'
      },
      {
        content: 'Psychologische/psychiatrische Betreuung von Betroffenen'
      },
      {
        valid: true,
        content: 'Errichten und Betreiben von Notunterkünften'
      },
      {
        valid: true,
        content: 'Beschaffung, Zubereitung und Ausgabe von Wasser und Lebensmittel'
      },
    ],
  },
  {
    id: 2388428,
    question: 'Was umfasst die Übergabe des Patienten im Krankenhaus?',
    choices: [
      {
        valid: true,
        content: 'Information über den Zustand des Patienten an das medizinische Krankenhauspersonal'
      },
      {
        valid: true,
        content: 'Information über gesetzte Sanitätshilfemaßnahmen'
      },
      {
        content: 'Anmeldung beim Portier'
      },
      {
        valid: true,
        content: 'Übergabe von Medikamenten, Befunden'
      },
    ],
  },
  {
    id: 2388029,
    question: 'Was verstehen Sie unter Präsuizidalem Syndrom?',
    choices: [
      {
        content: 'Versuch, durch Angriff auf das eigene Leben die Umwelt zu alarmieren'
      },
      {
        valid: true,
        content: 'Ein dem Selbstmord bzw. dem Versuch vorausgehender Ausnahmezustand, dessen Symptome weitgehend konstant, dessen Dauer aber sehr unterschiedlich sein kann'
      },
      {
        content: 'Drohung, dem eigenen Leben ein Ende zu machen'
      },
      {
        content: 'Gezielte Handlung, um das eigene Leben zu beenden'
      },
    ],
  },
  {
    id: 2387642,
    question: 'Was verstehen Sie unter Präsuizidalem Syndrom?',
    choices: [
      {
        content: 'Versuch, durch Angriff auf das eigene Leben die Umwelt zu alarmieren'
      },
      {
        valid: true,
        content: 'Ein dem Selbstmord bzw. dem Versuch vorausgehender Ausnahmezustand, dessen Symptome weitgehend konstant, dessen Dauer aber sehr unterschiedlich sein kann'
      },
      {
        content: 'Drohung, dem eigenen Leben ein Ende zu machen'
      },
      {
        content: 'Gezielte Handlung, um das eigene Leben zu beenden'
      },
    ],
  },
  {
    id: 2387910,
    question: 'Was verstehen Sie unter Präsuizidalem Syndrom?',
    choices: [
      {
        content: 'Versuch, durch Angriff auf das eigene Leben die Umwelt zu alarmieren'
      },
      {
        valid: true,
        content: 'Ein dem Selbstmord bzw. dem Versuch vorausgehender Ausnahmezustand, dessen Symptome weitgehend konstant, dessen Dauer aber sehr unterschiedlich sein kann'
      },
      {
        content: 'Drohung, dem eigenen Leben ein Ende zu machen'
      },
      {
        content: 'Gezielte Handlung, um das eigene Leben zu beenden'
      },
    ],
  },
  {
    id: 2388613,
    question: 'Was versteht man in der Sanitätshilfe unter Hilfeleistungspflicht?',
    choices: [
      {
        content: 'Verpflichtung des Sanitäters, jedem Erwachsenen Hilfe zu leisten'
      },
      {
        content: 'Darunter versteht man die Verpflichtung des Sanitäters, regelmäßig seinen Dienst zu leisten'
      },
      {
        valid: true,
        content: 'Sanitätspersonal kann sich nicht darauf berufen, es sei ihnen eine erforderliche Hilfeleistung nicht zumutbar gewesen (Selbstschutz beachten)'
      },
      {
        valid: true,
        content: 'Verpflichtung bei Notfällen zumutbare Hilfe zu leisten'
      },
    ],
  },
  {
    id: 2388939,
    question: 'Was versteht man unter "Angina Pectoris"?',
    choices: [
      {
        valid: true,
        content: 'Akuter Herzschmerz'
      },
      {
        content: 'Verschluss der Herzkranzgefäße'
      },
      {
        content: 'Eine entzündliche Erkrankung des Herzens'
      },
      {
        content: 'Atemnot infolge einer Verengung der kleinen Bronchien'
      },
    ],
  },
  {
    id: 2388706,
    question: 'Was versteht man unter "Desinfektion"?',
    choices: [
      {
        content: 'Entfernung von radioaktivem Material von der Körperoberfläche'
      },
      {
        valid: true,
        content: 'Die Abtötung bzw. irreversible Inaktivierung von krankheitserregenden Keimen'
      },
      {
        content: 'Die Reinigung von Schmutz'
      },
      {
        content: 'Die Abtötung bzw. Inaktivierung sämtlicher Keime'
      },
    ],
  },
  {
    id: 2388046,
    question: 'Was versteht man unter "Eklampsie"?',
    choices: [
      {
        content: 'Eine angeborene Stoffwechselstörung'
      },
      {
        valid: true,
        content: 'Eine Störung der Nierenfunktion während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Lebensbedrohliche epilepsieartige Anfälle während der Schwangerschaft oder Geburt'
      },
      {
        content: 'Relativ harmlose Krampfanfälle während der Schwangerschaft oder Geburt'
      },
    ],
  },
  {
    id: 2387659,
    question: 'Was versteht man unter "Eklampsie"?',
    choices: [
      {
        content: 'Eine angeborene Stoffwechselstörung'
      },
      {
        valid: true,
        content: 'Eine Störung der Nierenfunktion während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Lebensbedrohliche epilepsieartige Anfälle während der Schwangerschaft oder Geburt'
      },
      {
        content: 'Relativ harmlose Krampfanfälle während der Schwangerschaft oder Geburt'
      },
    ],
  },
  {
    id: 2387927,
    question: 'Was versteht man unter "Eklampsie"?',
    choices: [
      {
        content: 'Eine angeborene Stoffwechselstörung'
      },
      {
        valid: true,
        content: 'Eine Störung der Nierenfunktion während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Lebensbedrohliche epilepsieartige Anfälle während der Schwangerschaft oder Geburt'
      },
      {
        content: 'Relativ harmlose Krampfanfälle während der Schwangerschaft oder Geburt'
      },
    ],
  },
  {
    id: 2388715,
    question: 'Was versteht man unter "Epidemie"?',
    choices: [
      {
        content: 'Immer wieder auftretende Einzelerkrankungen in einem bestimmten Gebiet'
      },
      {
        valid: true,
        content: 'Zeitlich und örtlich begrenztes, gehäuftes Auftreten einer bestimmten Infektionskrankheit'
      },
      {
        content: 'Die Summe aller klinischen Symptome, die als Folge einer Infektion auftreten'
      },
      {
        content: 'Eine große Anzahl von Erkrankungen über Kontinente hinweg'
      },
    ],
  },
  {
    id: 2388707,
    question: 'Was versteht man unter "Kontamination"?',
    choices: [
      {
        content: 'Strahlende Partikel sind in den Körper gelangt.'
      },
      {
        content: 'Einwirken energiereicher Strahlen, ohne dann Strahlung auszusenden'
      },
      {
        content: 'Einverleiben eines Stoffes über die Atemwege'
      },
      {
        valid: true,
        content: 'Schädliches Material haftet auf der Kleidung, der Haut, den Haaren bzw. in Wunden.'
      },
    ],
  },
  {
    id: 2388047,
    question: 'Was versteht man unter "Placenta Praevia"?',
    choices: [
      {
        content: 'die Placenta liegt im Gebärmutterhals'
      },
      {
        content: 'die Placenta liegt im Gebärmutterkopf'
      },
      {
        content: 'es gibt keine Placenta'
      },
      {
        valid: true,
        content: 'die Placenta liegt in der Nähe des Gebärmutterhalses und überdeckt den Ausgang teilweise oder zur Gänze'
      },
    ],
  },
  {
    id: 2387660,
    question: 'Was versteht man unter "Placenta Praevia"?',
    choices: [
      {
        content: 'die Placenta liegt im Gebärmutterhals'
      },
      {
        content: 'die Placenta liegt im Gebärmutterkopf'
      },
      {
        content: 'es gibt keine Placenta'
      },
      {
        valid: true,
        content: 'die Placenta liegt in der Nähe des Gebärmutterhalses und überdeckt den Ausgang teilweise oder zur Gänze'
      },
    ],
  },
  {
    id: 2387928,
    question: 'Was versteht man unter "Placenta Praevia"?',
    choices: [
      {
        content: 'die Placenta liegt im Gebärmutterhals'
      },
      {
        content: 'die Placenta liegt im Gebärmutterkopf'
      },
      {
        content: 'es gibt keine Placenta'
      },
      {
        valid: true,
        content: 'die Placenta liegt in der Nähe des Gebärmutterhalses und überdeckt den Ausgang teilweise oder zur Gänze'
      },
    ],
  },
  {
    id: 2389232,
    question: 'Was versteht man unter "Totraum"?',
    choices: [
      {
        content: 'Jenen Teil der eingeatmeten Luft, der versehentlich in den Magen gelangt'
      },
      {
        valid: true,
        content: 'Jenen Teil der eingeatmeten Luft, der sich nicht aktiv am Gasaustausch beteiligt'
      },
      {
        content: 'Jenen Teil der Lunge, der durch eine Verletzung keine Luft mehr aufnehmen kann'
      },
      {
        content: 'Jenen Teil der Lunge, der zuwenig belüftet wird'
      },
    ],
  },
  {
    id: 2389233,
    question: 'Was versteht man unter "Zyanose"?',
    choices: [
      {
        content: 'Die durch den Schock bedingte Bläue von Haut und Schleimhaut'
      },
      {
        content: 'Eine durch CO-Vergiftung bedingte Rotfärbung der Haut'
      },
      {
        valid: true,
        content: 'Eine durch Sauerstoffmangel bedingte Blaufärbung von Haut und Schleimhaut'
      },
      {
        content: 'Eine durch Kohlendioxyd-Mangel bedingte Blaufärbung der Haut und der Schleimhäute'
      },
    ],
  },
  {
    id: 2388950,
    question: 'Was versteht man unter „Diabetes Mellitus“?',
    choices: [
      {
        valid: true,
        content: 'Störung des Kohlenhydratstoffwechsels'
      },
      {
        content: 'zuviel Insulin in der Zelle'
      },
      {
        content: 'Störung des Eiweißstoffwechsels'
      },
      {
        content: 'Störung des Elektrolythaushaltes'
      },
    ],
  },
  {
    id: 2388708,
    question: 'Was versteht man unter aktiver Immunisierung?',
    choices: [
      {
        content: 'Das Eindringen von Krankheitserregern in den Körper'
      },
      {
        content: 'Verabreichung von aktiven Krankheitserregern, die Wirkung tritt verzögert ein, die Wirkungsdauer ist lang anhaltend'
      },
      {
        valid: true,
        content: 'Verabreichung von Krankheitserregern oder deren Giften in abgeschwächter Form, die Wirkung tritt verzögert ein, die Wirkungsdauer ist lang anhaltend'
      },
      {
        content: 'Verabreichung von spezifischen Antikörpern, die Wirkung tritt sofort ein, die Wirkungsdauer beträgt nur wenige Wochen'
      },
    ],
  },
  {
    id: 2389231,
    question: 'Was versteht man unter Blausucht (=Zyanose)?',
    choices: [
      {
        content: 'Ein anderes Wort für lang anhaltenden, übermäßigen Alkoholkonsum'
      },
      {
        valid: true,
        content: 'Ist eine Blaufärbung an Lippen, Haut und Fingernägeln infolge mangelnder Sauerstoffsättigung des Blutes.'
      },
      {
        content: 'Erscheinungsbild bei Erfrierungen'
      },
      {
        content: 'Blaufärbung nach übermäßiger Sauerstoffzufuhr'
      },
    ],
  },
  {
    id: 2389567,
    question: 'Was versteht man unter Debriefing?',
    choices: [
      {
        valid: true,
        content: 'Eine Form der Gruppenintervention'
      },
      {
        content: 'Eine Entspannungsübung (Brief über das Erlebte schreiben)'
      },
      {
        valid: true,
        content: 'Soll gemeinsam mit einer klinisch-psychologischen Fachkraft und einem Peer erfolgen'
      },
      {
        content: 'Eine Form der Psychotherapie'
      },
    ],
  },
  {
    id: 2389568,
    question: 'Was versteht man unter Defusing?',
    choices: [
      {
        valid: true,
        content: 'Gruppenintervention, für Mitarbeiter, die vor Ort eingesetzt waren'
      },
      {
        content: 'Soll nach Möglichkeit an einem der nächsten dienstfreien Tage geschehen'
      },
      {
        content: 'Absichern einer Hochspannungsquelle'
      },
      {
        valid: true,
        content: 'Soll auf jeden Fall noch am selben Tag geschehen'
      },
    ],
  },
  {
    id: 2388030,
    question: 'Was versteht man unter dem Begriff "Akutes Abdomen"?',
    choices: [
      {
        content: 'Einbringen von Medikamenten in die Bauchhöhle'
      },
      {
        content: 'Eine spezielle Röntgenuntersuchung des Bauchraumes'
      },
      {
        valid: true,
        content: 'Akut einsetzende lebensbedrohliche Erkrankung im Bereich der Bauchhöhle'
      },
      {
        content: 'Eine nicht bedrohliche Baucherkrankung'
      },
    ],
  },
  {
    id: 2387643,
    question: 'Was versteht man unter dem Begriff "Akutes Abdomen"?',
    choices: [
      {
        content: 'Einbringen von Medikamenten in die Bauchhöhle'
      },
      {
        content: 'Eine spezielle Röntgenuntersuchung des Bauchraumes'
      },
      {
        valid: true,
        content: 'Akut einsetzende lebensbedrohliche Erkrankung im Bereich der Bauchhöhle'
      },
      {
        content: 'Eine nicht bedrohliche Baucherkrankung'
      },
    ],
  },
  {
    id: 2387911,
    question: 'Was versteht man unter dem Begriff "Akutes Abdomen"?',
    choices: [
      {
        content: 'Einbringen von Medikamenten in die Bauchhöhle'
      },
      {
        content: 'Eine spezielle Röntgenuntersuchung des Bauchraumes'
      },
      {
        valid: true,
        content: 'Akut einsetzende lebensbedrohliche Erkrankung im Bereich der Bauchhöhle'
      },
      {
        content: 'Eine nicht bedrohliche Baucherkrankung'
      },
    ],
  },
  {
    id: 2388325,
    question: 'Was versteht man unter dem Begriff "Bewusstsein"?',
    choices: [
      {
        content: 'Gegenteil von Schlaf'
      },
      {
        valid: true,
        content: 'Aufnahme und Verarbeitung von Sinneseindrücken'
      },
      {
        valid: true,
        content: 'Intaktheit der Schutzreflexe, Ausführung gezielter Bewegungen'
      },
      {
        valid: true,
        content: 'Ausdruck des menschlichen Lebens'
      },
    ],
  },
  {
    id: 2388940,
    question: 'Was versteht man unter dem Begriff "Bradykardie"?',
    choices: [
      {
        content: 'Hohe Herzfrequenz von über 100 /min'
      },
      {
        valid: true,
        content: 'Niedrige Herzfrequenz von unter 60 /min'
      },
      {
        content: 'Hohe Herzfrequenz von über 80 /min'
      },
      {
        content: 'Niedrige Herzfrequenz von unter 80 /min'
      },
    ],
  },
  {
    id: 2388031,
    question: 'Was versteht man unter dem Begriff "Erfrierung"?',
    choices: [
      {
        valid: true,
        content: 'Erfrierungen sind durch Kälte und Nässe hervorgerufene Gewebsschädigungen'
      },
      {
        valid: true,
        content: 'Vor allem Finger, Zehen, Nase und Ohrmuscheln (vorstehende Körperteile) sind betroffen'
      },
      {
        content: 'Es gibt 5 unterschiedliche Erfrierungsgrade'
      },
      {
        valid: true,
        content: 'Im Zweifelsfall von einer Erfrierung ausgehen'
      },
    ],
  },
  {
    id: 2387644,
    question: 'Was versteht man unter dem Begriff "Erfrierung"?',
    choices: [
      {
        valid: true,
        content: 'Erfrierungen sind durch Kälte und Nässe hervorgerufene Gewebsschädigungen'
      },
      {
        valid: true,
        content: 'Vor allem Finger, Zehen, Nase und Ohrmuscheln (vorstehende Körperteile) sind betroffen'
      },
      {
        content: 'Es gibt 5 unterschiedliche Erfrierungsgrade'
      },
      {
        valid: true,
        content: 'Im Zweifelsfall von einer Erfrierung ausgehen'
      },
    ],
  },
  {
    id: 2387912,
    question: 'Was versteht man unter dem Begriff "Erfrierung"?',
    choices: [
      {
        valid: true,
        content: 'Erfrierungen sind durch Kälte und Nässe hervorgerufene Gewebsschädigungen'
      },
      {
        valid: true,
        content: 'Vor allem Finger, Zehen, Nase und Ohrmuscheln (vorstehende Körperteile) sind betroffen'
      },
      {
        content: 'Es gibt 5 unterschiedliche Erfrierungsgrade'
      },
      {
        valid: true,
        content: 'Im Zweifelsfall von einer Erfrierung ausgehen'
      },
    ],
  },
  {
    id: 2388186,
    question: 'Was versteht man unter dem Begriff "Lagerungsschäden"?',
    choices: [
      {
        valid: true,
        content: 'Im weitesten Sinn kann ein Lagerungsschaden durch ungenügende Fixierung (Trage, ...) gewertet werden'
      },
      {
        valid: true,
        content: 'Nichtdurchführen einer entsprechenden Lagerung'
      },
      {
        valid: true,
        content: 'Haut- und Nervenschädigungen durch Schienendruck, Kompression von Gefäßen'
      },
      {
        content: 'Lagerungsschäden sind durch das Rettungsdienstpersonal nicht möglich'
      },
    ],
  },
  {
    id: 2388713,
    question: 'Was versteht man unter dem Begriff "Meningitis"?',
    choices: [
      {
        content: 'Eine Bauchfellentzündung mit brettharter Bauchdecke und hohem Fieber'
      },
      {
        valid: true,
        content: 'Eine durch Krankheitserreger ausgelöste Entzündung der Hirnhäute'
      },
      {
        content: 'Eine Entzündung des Rückenmarks mit Lähmungserscheinungen'
      },
      {
        content: 'Eine Entzündung des Gehirns durch Bakterien, Viren oder andere Ursachen'
      },
    ],
  },
  {
    id: 2388709,
    question: 'Was versteht man unter dem Begriff "nosokomiale Infektion“?',
    choices: [
      {
        content: 'Nosokomiale Infektionen haben für den Rettungsdienst keine Bedeutung'
      },
      {
        content: 'Nosokomiale Infektionen sind bis dato nur in asiatischen Ländern bekannt'
      },
      {
        valid: true,
        content: 'Nosokomiale Infektionen können im Zuge von Krankenhausaufenthalten (Hospitalismuserreger) zusätzlich übertragen werden'
      },
      {
        content: 'Nosokomiale Infektionen kommen nur auf internen Abteilungen vor'
      },
    ],
  },
  {
    id: 2388710,
    question: 'Was versteht man unter dem Begriff "PEP"?',
    choices: [
      {
        content: 'Die posttraumatische Verarbeitung nach Einsätzen im RKT'
      },
      {
        valid: true,
        content: 'Die postexpositionelle Prophylaxe (Medikation z.B. nach HCV-Exposition durch Nadelstich)'
      },
      {
        content: 'Die hygienische Prophylaxe des Peepventils'
      },
      {
        content: 'Die Aufbereitung und Prophylaxe von Instrumenten allgemein'
      },
    ],
  },
  {
    id: 2388941,
    question: 'Was versteht man unter dem Begriff "Schlaganfall"?',
    choices: [
      {
        content: 'Eine vorübergehende Mangeldurchblutung im Gehirn mit vorübergehenden Symptomen'
      },
      {
        content: 'Ein plötzlich auftretender Verschluss eines Herzkranzgefäßes, der ein Absterben von Herzmuskelfasern bewirkt'
      },
      {
        content: 'Eine Störung der Schlagfolge des Herzens durch eine schwere Entgleisung des Stoffwechsels'
      },
      {
        valid: true,
        content: 'Das plötzliche Auftreten von Krankheitserscheinungen, die auf eine akute Minderdurchblutung des Gehirns oder eine Hirnblutung zurückzuführen sind'
      },
    ],
  },
  {
    id: 2388711,
    question: 'Was versteht man unter dem Begriff "Sepsis"?',
    choices: [
      {
        valid: true,
        content: 'Die Anwesenheit vieler Krankheitserreger in der Blutbahn, ausgehend von einem "Herd"'
      },
      {
        content: 'Das Eindringen von Krankheitserregern in den Körper'
      },
      {
        content: 'Der rote Strich am Unterarm'
      },
      {
        content: 'Eitriger Liquor'
      },
    ],
  },
  {
    id: 2388942,
    question: 'Was versteht man unter dem Begriff "Tachykardie"?',
    choices: [
      {
        content: 'Hohes Atemminutenvolumen'
      },
      {
        valid: true,
        content: 'Hohe Herzschlagfrequenz über 100 /min'
      },
      {
        content: 'Niedrige Herzschlagfrequenz unter 60 /min'
      },
      {
        content: 'Hohe Herzschlagfrequenz über 80 /min'
      },
    ],
  },
  {
    id: 2388712,
    question: 'Was versteht man unter dem Begriff „Enzephalitis“?',
    choices: [
      {
        content: 'Eine Entzündung der Hirnhäute'
      },
      {
        valid: true,
        content: 'Eine Entzündung des Gehirns'
      },
      {
        content: 'Eine Entzündung des Bauchfells'
      },
      {
        content: 'Eine Wasseransammlung im Gehirn'
      },
    ],
  },
  {
    id: 2388032,
    question: 'Was versteht man unter dem Krankheitsbild einer "Epiglottitis"?',
    choices: [
      {
        content: 'Eine Verlegung der Bronchien'
      },
      {
        valid: true,
        content: 'Eine bakterielle Entzündung'
      },
      {
        content: 'Eine virale Entzündung'
      },
      {
        content: 'Hustenanfall'
      },
    ],
  },
  {
    id: 2387645,
    question: 'Was versteht man unter dem Krankheitsbild einer "Epiglottitis"?',
    choices: [
      {
        content: 'Eine Verlegung der Bronchien'
      },
      {
        valid: true,
        content: 'Eine bakterielle Entzündung'
      },
      {
        content: 'Eine virale Entzündung'
      },
      {
        content: 'Hustenanfall'
      },
    ],
  },
  {
    id: 2387913,
    question: 'Was versteht man unter dem Krankheitsbild einer "Epiglottitis"?',
    choices: [
      {
        content: 'Eine Verlegung der Bronchien'
      },
      {
        valid: true,
        content: 'Eine bakterielle Entzündung'
      },
      {
        content: 'Eine virale Entzündung'
      },
      {
        content: 'Hustenanfall'
      },
    ],
  },
  {
    id: 2389569,
    question: 'Was versteht man unter Demobilization?',
    choices: [
      {
        valid: true,
        content: 'ca. 10- bis 20-minütiges Informationsgespräch'
      },
      {
        content: 'Einzelgespräch mit einem Psychologen'
      },
      {
        valid: true,
        content: 'Phase der Entspannung und Stärkung, mit dem eine Deeskalation von Emotionen erreicht werden soll'
      },
      {
        content: 'Ruhigstellung von Knochenbrüchen'
      },
    ],
  },
  {
    id: 2388326,
    question: 'Was versteht man unter den sicheren Todeszeichen?',
    choices: [
      {
        valid: true,
        content: 'Leichenstarre, Verwesungserscheinungen'
      },
      {
        valid: true,
        content: 'Totenflecken'
      },
      {
        content: 'fehlendes Bewusstsein, fehlende Atmung und fehlende Lebenszeichen'
      },
      {
        valid: true,
        content: 'Tödliche Verletzungen wie z.B. die Abtrennung des Kopfes'
      },
    ],
  },
  {
    id: 2388033,
    question: 'Was versteht man unter der Lagerung nach Fritsch und wann wird sie angewendet?',
    choices: [
      {
        valid: true,
        content: 'Rückenlage mit überkreuzten Beinen und einer sterilen Vorlage'
      },
      {
        valid: true,
        content: 'Blutungen nach der Geburt oder während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Anwendung bei Blutungen aus der Vagina'
      },
      {
        content: 'Linksseitenlage bei Schwangeren'
      },
    ],
  },
  {
    id: 2387646,
    question: 'Was versteht man unter der Lagerung nach Fritsch und wann wird sie angewendet?',
    choices: [
      {
        valid: true,
        content: 'Rückenlage mit überkreuzten Beinen und einer sterilen Vorlage'
      },
      {
        valid: true,
        content: 'Blutungen nach der Geburt oder während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Anwendung bei Blutungen aus der Vagina'
      },
      {
        content: 'Linksseitenlage bei Schwangeren'
      },
    ],
  },
  {
    id: 2387914,
    question: 'Was versteht man unter der Lagerung nach Fritsch und wann wird sie angewendet?',
    choices: [
      {
        valid: true,
        content: 'Rückenlage mit überkreuzten Beinen und einer sterilen Vorlage'
      },
      {
        valid: true,
        content: 'Blutungen nach der Geburt oder während der Schwangerschaft'
      },
      {
        valid: true,
        content: 'Anwendung bei Blutungen aus der Vagina'
      },
      {
        content: 'Linksseitenlage bei Schwangeren'
      },
    ],
  },
  {
    id: 2388034,
    question: 'Was versteht man unter der Verbrennungskrankheit?',
    choices: [
      {
        content: 'Infektion der Brandwunden'
      },
      {
        content: 'Auftreten einer Hyperventilation ist immer zu erkennen'
      },
      {
        content: 'Lungenödem durch Hitzeinhalation (Inhalationstrauma)'
      },
      {
        valid: true,
        content: 'Durch komplexe Mechanismen kommt es zum Versagen lebenswichtiger Organe (besonders Nieren und Lunge)'
      },
    ],
  },
  {
    id: 2387647,
    question: 'Was versteht man unter der Verbrennungskrankheit?',
    choices: [
      {
        content: 'Infektion der Brandwunden'
      },
      {
        content: 'Auftreten einer Hyperventilation ist immer zu erkennen'
      },
      {
        content: 'Lungenödem durch Hitzeinhalation (Inhalationstrauma)'
      },
      {
        valid: true,
        content: 'Durch komplexe Mechanismen kommt es zum Versagen lebenswichtiger Organe (besonders Nieren und Lunge)'
      },
    ],
  },
  {
    id: 2387915,
    question: 'Was versteht man unter der Verbrennungskrankheit?',
    choices: [
      {
        content: 'Infektion der Brandwunden'
      },
      {
        content: 'Auftreten einer Hyperventilation ist immer zu erkennen'
      },
      {
        content: 'Lungenödem durch Hitzeinhalation (Inhalationstrauma)'
      },
      {
        valid: true,
        content: 'Durch komplexe Mechanismen kommt es zum Versagen lebenswichtiger Organe (besonders Nieren und Lunge)'
      },
    ],
  },
  {
    id: 2388035,
    question: 'Was versteht man unter einem Blasensprung?',
    choices: [
      {
        content: 'Eine für Mutter und Kind akut lebensbedrohliche Geburtskomplikation'
      },
      {
        valid: true,
        content: 'Platzen der Fruchtblase am Ende der Eröffnungsperiode'
      },
      {
        content: 'Aufplatzen einer Eierstockzyste'
      },
      {
        content: 'Einreißen der mütterlichen Harnblase während der Geburt'
      },
    ],
  },
  {
    id: 2387648,
    question: 'Was versteht man unter einem Blasensprung?',
    choices: [
      {
        content: 'Eine für Mutter und Kind akut lebensbedrohliche Geburtskomplikation'
      },
      {
        valid: true,
        content: 'Platzen der Fruchtblase am Ende der Eröffnungsperiode'
      },
      {
        content: 'Aufplatzen einer Eierstockzyste'
      },
      {
        content: 'Einreißen der mütterlichen Harnblase während der Geburt'
      },
    ],
  },
  {
    id: 2387916,
    question: 'Was versteht man unter einem Blasensprung?',
    choices: [
      {
        content: 'Eine für Mutter und Kind akut lebensbedrohliche Geburtskomplikation'
      },
      {
        valid: true,
        content: 'Platzen der Fruchtblase am Ende der Eröffnungsperiode'
      },
      {
        content: 'Aufplatzen einer Eierstockzyste'
      },
      {
        content: 'Einreißen der mütterlichen Harnblase während der Geburt'
      },
    ],
  },
  {
    id: 2388036,
    question: 'Was versteht man unter einem geschlossenen Pneumothorax?',
    choices: [
      {
        valid: true,
        content: 'Durch eine innere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
      {
        content: 'Ausbildung eines Unterdruckes im Inneren des Brustraumes'
      },
      {
        content: 'Einblutungen in den Pleuraspalt'
      },
      {
        content: 'Durch eine äußere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
    ],
  },
  {
    id: 2387649,
    question: 'Was versteht man unter einem geschlossenen Pneumothorax?',
    choices: [
      {
        valid: true,
        content: 'Durch eine innere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
      {
        content: 'Ausbildung eines Unterdruckes im Inneren des Brustraumes'
      },
      {
        content: 'Einblutungen in den Pleuraspalt'
      },
      {
        content: 'Durch eine äußere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
    ],
  },
  {
    id: 2387917,
    question: 'Was versteht man unter einem geschlossenen Pneumothorax?',
    choices: [
      {
        valid: true,
        content: 'Durch eine innere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
      {
        content: 'Ausbildung eines Unterdruckes im Inneren des Brustraumes'
      },
      {
        content: 'Einblutungen in den Pleuraspalt'
      },
      {
        content: 'Durch eine äußere Verletzung kann Luft aus den Atemwegen in den Pleuraraum kommen'
      },
    ],
  },
  {
    id: 2388327,
    question: 'Was versteht man unter einem Koma?',
    choices: [
      {
        content: 'Einen Zustand leichter Bewusstseinstrübung'
      },
      {
        content: 'Einen Zustand, aus dem der Patient durch heftiges Rütteln und Schütteln erwacht'
      },
      {
        valid: true,
        content: 'Einen Zustand tiefster Bewusstlosigkeit, bei dem der Patient nicht erweckbar ist, Atmung und Kreislauf sind erhalten'
      },
      {
        content: 'Einen Zustand tiefster Bewusstlosigkeit, bei dem der Patient nicht mehr atmet'
      },
    ],
  },
  {
    id: 2388429,
    question: 'Was versteht man unter einem Rettungsflug (Primäreinsatz)?',
    choices: [
      {
        valid: true,
        content: 'Transport des Notfallpatienten vom Notfallort ins Krankenhaus'
      },
      {
        content: 'Der primäre Suchflug zum Auffinden eines Vermissten'
      },
      {
        valid: true,
        content: 'Einsatz zum Heranbringen von Rettungspersonal'
      },
      {
        content: 'Dringender Verlegungsflug von einem Krankenhaus in ein Schwerpunktkrankenhaus'
      },
    ],
  },
  {
    id: 2388328,
    question: 'Was versteht man unter einem Schock?',
    choices: [
      {
        content: 'Ein Zustand tiefer Bewusstlosigkeit'
      },
      {
        content: 'Blutdruckerniedrigung allein ist gleichbedeutend mit Schock'
      },
      {
        content: 'Eine Veränderung der Psyche, die zum Nervenzusammenbruch führen kann'
      },
      {
        valid: true,
        content: 'Schock ist eine schwere Kreislaufstörung mit Mangelversorgung wichtiger Organe durch Sauerstoff und Nährstoffe'
      },
    ],
  },
  {
    id: 2388037,
    question: 'Was versteht man unter einem Serienrippenbruch?',
    choices: [
      {
        valid: true,
        content: 'Mehrere Rippen (ab drei) sind in einer Linie untereinander einfach gebrochen.'
      },
      {
        content: 'Wenn alle Rippen gebrochen sind'
      },
      {
        content: 'Wenn mehrere Rippen mehrfach gebrochen sind'
      },
      {
        content: 'Bruch von zwei Rippen auf einer Seite'
      },
    ],
  },
  {
    id: 2387650,
    question: 'Was versteht man unter einem Serienrippenbruch?',
    choices: [
      {
        valid: true,
        content: 'Mehrere Rippen (ab drei) sind in einer Linie untereinander einfach gebrochen.'
      },
      {
        content: 'Wenn alle Rippen gebrochen sind'
      },
      {
        content: 'Wenn mehrere Rippen mehrfach gebrochen sind'
      },
      {
        content: 'Bruch von zwei Rippen auf einer Seite'
      },
    ],
  },
  {
    id: 2387918,
    question: 'Was versteht man unter einem Serienrippenbruch?',
    choices: [
      {
        valid: true,
        content: 'Mehrere Rippen (ab drei) sind in einer Linie untereinander einfach gebrochen.'
      },
      {
        content: 'Wenn alle Rippen gebrochen sind'
      },
      {
        content: 'Wenn mehrere Rippen mehrfach gebrochen sind'
      },
      {
        content: 'Bruch von zwei Rippen auf einer Seite'
      },
    ],
  },
  {
    id: 2388943,
    question: 'Was versteht man unter einem Shunt (Dialyse)?',
    choices: [
      {
        content: 'Platzen der feinen Adern unter der Gesichtshaut'
      },
      {
        content: 'Versacken des Blutes in die Venen des Bauchraumes ähnlich wie beim Schock'
      },
      {
        valid: true,
        content: 'Künstliche Verbindung von zwei blutführenden Gefäßen (Arterie und Vene)'
      },
      {
        content: 'Eine Drainage bei einem Lymphödem'
      },
    ],
  },
  {
    id: 2388483,
    question: 'Was versteht man unter einem Verlegungsflug (Sekundäreinsatz)?',
    choices: [
      {
        content: 'Transport des Notfallpatienten vom Notfallort ins Krankenhaus'
      },
      {
        valid: true,
        content: 'Verlegungsflug von einem Krankenhaus in ein Schwerpunktkrankenhaus oder ein Krankenhaus mit einer speziellen Fachabteilung'
      },
      {
        content: 'Einsatz zum Heranbringen von Rettungspersonal'
      },
      {
        content: 'Der zweite Flug zum Notfallort, um Material heranzubringen'
      },
    ],
  },
  {
    id: 2388947,
    question: 'Was versteht man unter einer "arteriellen Embolie"?',
    choices: [
      {
        content: 'Erweiterung der Arterie'
      },
      {
        valid: true,
        content: 'Blutgerinnsel von Herzklappen oder Herzinnenwand reißt sich los und verstopft eine Arterie.'
      },
      {
        content: 'Entzündung einer Arterie'
      },
      {
        content: 'Blutgerinnselbildung in tiefen Bein- und Beckengefäßen'
      },
    ],
  },
  {
    id: 2388944,
    question: 'Was versteht man unter einer "Aura" vor einem epileptischen Anfall?',
    choices: [
      {
        content: 'Die Umgebung des Patienten'
      },
      {
        valid: true,
        content: 'Vorahnung des Patienten vor einem epileptischen Anfall'
      },
      {
        content: 'Erinnerung des Patienten an den Anfall'
      },
      {
        valid: true,
        content: 'Optische, akustische bzw. Geruchswahrnehmungen des Patienten vor einem Anfall'
      },
    ],
  },
  {
    id: 2388040,
    question: 'Was versteht man unter einer "Commotio Cerebri"?',
    choices: [
      {
        content: 'Eine Gehirnprellung'
      },
      {
        content: 'Einen raumfordernden Prozess und Erinnerungslücken'
      },
      {
        valid: true,
        content: 'Eine Gehirnerschütterung'
      },
      {
        content: 'Eine Gehirnquetschung'
      },
    ],
  },
  {
    id: 2387653,
    question: 'Was versteht man unter einer "Commotio Cerebri"?',
    choices: [
      {
        content: 'Eine Gehirnprellung'
      },
      {
        content: 'Einen raumfordernden Prozess und Erinnerungslücken'
      },
      {
        valid: true,
        content: 'Eine Gehirnerschütterung'
      },
      {
        content: 'Eine Gehirnquetschung'
      },
    ],
  },
  {
    id: 2387921,
    question: 'Was versteht man unter einer "Commotio Cerebri"?',
    choices: [
      {
        content: 'Eine Gehirnprellung'
      },
      {
        content: 'Einen raumfordernden Prozess und Erinnerungslücken'
      },
      {
        valid: true,
        content: 'Eine Gehirnerschütterung'
      },
      {
        content: 'Eine Gehirnquetschung'
      },
    ],
  },
  {
    id: 2388187,
    question: 'Was versteht man unter einer "endotrachealen Intubation“?',
    choices: [
      {
        valid: true,
        content: 'Einführen eines Tubus in die Luftröhre'
      },
      {
        content: 'Einführen eines Tubus in die Speiseröhre'
      },
      {
        content: 'Einführen eines Tubus in die Scheide'
      },
      {
        content: 'Einführen eines Tubus in die Harnröhre'
      },
    ],
  },
  {
    id: 2388038,
    question: 'Was versteht man unter einer Gehirnerschütterung?',
    choices: [
      {
        content: 'Generelle Gewalteinwirkung auf das Gehirn'
      },
      {
        content: 'Reversible Verletzung eines Gehirnareals'
      },
      {
        valid: true,
        content: 'Funktionsstörung des Gehirns ohne nachweisbare Verletzung'
      },
      {
        content: 'Gehirnquetschung'
      },
    ],
  },
  {
    id: 2387651,
    question: 'Was versteht man unter einer Gehirnerschütterung?',
    choices: [
      {
        content: 'Generelle Gewalteinwirkung auf das Gehirn'
      },
      {
        content: 'Reversible Verletzung eines Gehirnareals'
      },
      {
        valid: true,
        content: 'Funktionsstörung des Gehirns ohne nachweisbare Verletzung'
      },
      {
        content: 'Gehirnquetschung'
      },
    ],
  },
  {
    id: 2387919,
    question: 'Was versteht man unter einer Gehirnerschütterung?',
    choices: [
      {
        content: 'Generelle Gewalteinwirkung auf das Gehirn'
      },
      {
        content: 'Reversible Verletzung eines Gehirnareals'
      },
      {
        valid: true,
        content: 'Funktionsstörung des Gehirns ohne nachweisbare Verletzung'
      },
      {
        content: 'Gehirnquetschung'
      },
    ],
  },
  {
    id: 2388039,
    question: 'Was versteht man unter einer Hirndrucksteigerung?',
    choices: [
      {
        content: 'Wenn von außen Druck auf das Gehirn ausgeübt wird'
      },
      {
        valid: true,
        content: 'Anstieg des intrakraniellen Drucks (Hirndrucks) über den Normwert.'
      },
      {
        content: 'Wenn bei einem Kopfstand mehr Blut ins Gehirn fließt'
      },
      {
        content: 'Abfall des intrakraniellen Drucks (Hirndrucks) unter den Normwert.'
      },
    ],
  },
  {
    id: 2387652,
    question: 'Was versteht man unter einer Hirndrucksteigerung?',
    choices: [
      {
        content: 'Wenn von außen Druck auf das Gehirn ausgeübt wird'
      },
      {
        valid: true,
        content: 'Anstieg des intrakraniellen Drucks (Hirndrucks) über den Normwert.'
      },
      {
        content: 'Wenn bei einem Kopfstand mehr Blut ins Gehirn fließt'
      },
      {
        content: 'Abfall des intrakraniellen Drucks (Hirndrucks) unter den Normwert.'
      },
    ],
  },
  {
    id: 2387920,
    question: 'Was versteht man unter einer Hirndrucksteigerung?',
    choices: [
      {
        content: 'Wenn von außen Druck auf das Gehirn ausgeübt wird'
      },
      {
        valid: true,
        content: 'Anstieg des intrakraniellen Drucks (Hirndrucks) über den Normwert.'
      },
      {
        content: 'Wenn bei einem Kopfstand mehr Blut ins Gehirn fließt'
      },
      {
        content: 'Abfall des intrakraniellen Drucks (Hirndrucks) unter den Normwert.'
      },
    ],
  },
  {
    id: 2388945,
    question: 'Was versteht man unter einer Linksherzschwäche?',
    choices: [
      {
        content: 'Nachlassen der Leistung der linken Herzkammer, welche jedoch nicht bedrohlich ist'
      },
      {
        content: 'Nachlassen der Leistung der linken Herzkammer, führt zur Lungenembolie'
      },
      {
        valid: true,
        content: 'Nachlassen der Leistung der linken Herzkammer'
      },
      {
        content: 'Ursachen für ein akutes Linksherzversagen können Rhythmusstörungen und Hypotonie sein'
      },
    ],
  },
  {
    id: 2388946,
    question: 'Was versteht man unter einer Lungenembolie?',
    choices: [
      {
        content: 'Das Einatmen von Reizgasen'
      },
      {
        content: 'Der Verschluss eines kleinen Bronchus durch einen Fremdkörper'
      },
      {
        valid: true,
        content: 'Der Verschluss eines Lungengefäßes durch ein Blutgerinnsel'
      },
      {
        content: 'Die Verkrampfung der kleinen Bronchien'
      },
    ],
  },
  {
    id: 2388714,
    question: 'Was versteht man unter einer passiven Immunisierung?',
    choices: [
      {
        content: 'Verabreichung von spezifischen Antikörpern in hohen Dosen, Wirkungseintritt langsam, Wirkungsdauer einige Monate'
      },
      {
        content: 'Verabreichung von spezifischen Antikörpern in hohen Dosen, Wirkungseintritt sofort, Wirkungsdauer mehr als 10 Jahre'
      },
      {
        content: 'Verabreichung von Krankheitserregern in abgeschwächter Form, Wirkungseintritt sofort, Wirkungsdauer für viele Wochen'
      },
      {
        valid: true,
        content: 'Verabreichung von spezifischen Antikörpern in hohen Dosen, Wirkungseintritt sofort, Wirkungsdauer nur wenige Wochen'
      },
    ],
  },
  {
    id: 2388579,
    question: 'Was versteht man unter einer starken Blutung?',
    choices: [
      {
        valid: true,
        content: 'Wenn aus einer Wunde innerhalb kurzer Zeit eine große Blutmenge verloren geht'
      },
      {
        content: 'Wenn das Blut aus der Wunde tropft'
      },
      {
        valid: true,
        content: 'Wenn das Blut aus der Wunde spritzt'
      },
      {
        valid: true,
        content: 'Wenn das Blut im Schwall austritt'
      },
    ],
  },
  {
    id: 2388580,
    question: 'Was versteht man unter einer Vergiftung?',
    choices: [
      {
        valid: true,
        content: 'Das Auftreten schwerer, oft lebensbedrohlicher Krankheitsbilder nach Aufnahme giftiger Substanzen'
      },
      {
        content: 'Vergiftungen entstehen am häufigsten bei Gefahrengutunfällen'
      },
      {
        content: 'Nur bei Aufnahme großer Mengen schädlicher Substanzen kann von einer Vergiftung gesprochen werden.'
      },
      {
        content: 'Nur feste Stoffe können Vergiftungen hervorrufen.'
      },
    ],
  },
  {
    id: 2388614,
    question: 'Was versteht man unter Fortbildungspflicht?',
    choices: [
      {
        valid: true,
        content: 'Gemäß Sanitätergesetz (§ 50) ist eine verpflichtende Fortbildung vorgeschrieben'
      },
      {
        content: 'Diese besteht nur für jene Mitarbeiter, die nicht regelmäßig ihren Dienst versehen'
      },
      {
        valid: true,
        content: 'Die Rettungssanitäter-Ausbildung stellt eine Basis dar, auf der ein Aufbau des Wissensstandes erfolgen muss'
      },
      {
        content: 'Die Rettungssanitäter-Ausbildung vermittelt ausreichendes Wissen, eine Fortbildung ist daher nicht notwendig'
      },
    ],
  },
  {
    id: 2388948,
    question: 'Was versteht man unter Herzversagen?',
    choices: [
      {
        content: 'Leistungseinschränkung des Herzens mit Verminderung der Pumpleistung bei unzureichendem Blutangebot'
      },
      {
        valid: true,
        content: 'Leistungseinschränkung des Herzens mit Verminderung der Pumpleistung bei ausreichendem Blutangebot'
      },
      {
        content: 'Leistungserhöhung des Herzens mit Verminderung der Pumpleistung bei ausreichendem Blutangebot'
      },
      {
        content: 'Leitungssteigerung des Herzens durch Medikamente'
      },
    ],
  },
  {
    id: 2388041,
    question: 'Was versteht man unter Lungenüberdehnung (Barotrauma) bei einem Tauchunfall?',
    choices: [
      {
        valid: true,
        content: 'Einreißen des Lungenfells (Pleura Visceralis)'
      },
      {
        valid: true,
        content: 'kann auch nach einem einzigen Atemzug aus einem Pressluftgerät in geringen Tiefen auftreten'
      },
      {
        content: 'Tritt nur beim Tauchen in großen Tiefen auf'
      },
      {
        content: 'bewirkt eine Überfüllung der Lunge mit Stickstoff'
      },
    ],
  },
  {
    id: 2387654,
    question: 'Was versteht man unter Lungenüberdehnung (Barotrauma) bei einem Tauchunfall?',
    choices: [
      {
        valid: true,
        content: 'Einreißen des Lungenfells (Pleura Visceralis)'
      },
      {
        valid: true,
        content: 'kann auch nach einem einzigen Atemzug aus einem Pressluftgerät in geringen Tiefen auftreten'
      },
      {
        content: 'Tritt nur beim Tauchen in großen Tiefen auf'
      },
      {
        content: 'bewirkt eine Überfüllung der Lunge mit Stickstoff'
      },
    ],
  },
  {
    id: 2387922,
    question: 'Was versteht man unter Lungenüberdehnung (Barotrauma) bei einem Tauchunfall?',
    choices: [
      {
        valid: true,
        content: 'Einreißen des Lungenfells (Pleura Visceralis)'
      },
      {
        valid: true,
        content: 'kann auch nach einem einzigen Atemzug aus einem Pressluftgerät in geringen Tiefen auftreten'
      },
      {
        content: 'Tritt nur beim Tauchen in großen Tiefen auf'
      },
      {
        content: 'bewirkt eine Überfüllung der Lunge mit Stickstoff'
      },
    ],
  },
  {
    id: 2388042,
    question: 'Was versteht man unter MDS?',
    choices: [
      {
        content: 'Eine Automarke'
      },
      {
        content: 'Eine Untersuchungsmethode in der Psychiatrie'
      },
      {
        valid: true,
        content: 'Motorik, Durchblutung, Sensibilität'
      },
      {
        content: 'Eine Sanitätshilfemaßnahme bei der Reanimation'
      },
    ],
  },
  {
    id: 2387655,
    question: 'Was versteht man unter MDS?',
    choices: [
      {
        content: 'Eine Automarke'
      },
      {
        content: 'Eine Untersuchungsmethode in der Psychiatrie'
      },
      {
        valid: true,
        content: 'Motorik, Durchblutung, Sensibilität'
      },
      {
        content: 'Eine Sanitätshilfemaßnahme bei der Reanimation'
      },
    ],
  },
  {
    id: 2387923,
    question: 'Was versteht man unter MDS?',
    choices: [
      {
        content: 'Eine Automarke'
      },
      {
        content: 'Eine Untersuchungsmethode in der Psychiatrie'
      },
      {
        valid: true,
        content: 'Motorik, Durchblutung, Sensibilität'
      },
      {
        content: 'Eine Sanitätshilfemaßnahme bei der Reanimation'
      },
    ],
  },
  {
    id: 2388949,
    question: 'Was versteht man unter Rechtsherzschwäche?',
    choices: [
      {
        valid: true,
        content: 'Leistungsminderung des rechten Herzens, Stauung des Blutes in den Körper'
      },
      {
        content: 'Leistungsminderung des rechten Herzens, Stauung des Blutes in die Lunge'
      },
      {
        valid: true,
        content: 'Weitung der rechten Herzkammer infolge einer chronischen Lungenstauung durch eine Linksherzinsuffizienz'
      },
      {
        content: 'Als Folge einer Rechtsherzschwäche kommt es innerhalb kurzer Zeit auch zur Linksherzschwäche'
      },
    ],
  },
  {
    id: 2388043,
    question: 'Was versteht man unter sekundärem Ertrinken?',
    choices: [
      {
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 8 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Eine Sekundärkomplikation nach einem Ertrinkungsunfall durch die Entstehung eines Lungenödems'
      },
      {
        content: 'Verbesserung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
    ],
  },
  {
    id: 2387656,
    question: 'Was versteht man unter sekundärem Ertrinken?',
    choices: [
      {
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 8 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Eine Sekundärkomplikation nach einem Ertrinkungsunfall durch die Entstehung eines Lungenödems'
      },
      {
        content: 'Verbesserung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
    ],
  },
  {
    id: 2387924,
    question: 'Was versteht man unter sekundärem Ertrinken?',
    choices: [
      {
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 8 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Verschlechterung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
      {
        valid: true,
        content: 'Eine Sekundärkomplikation nach einem Ertrinkungsunfall durch die Entstehung eines Lungenödems'
      },
      {
        content: 'Verbesserung des Allgemeinzustandes nach bis zu 48 Stunden nach erfolgreicher Rettung'
      },
    ],
  },
  {
    id: 2388615,
    question: 'Was versteht man unter Sorgfaltspflicht?',
    choices: [
      {
        content: 'Sorgfältiges, faltenloses Aufspannen des Leintuches auf der Krankentrage'
      },
      {
        valid: true,
        content: 'Gewissenhaftes Vorgehen bei der Versorgung und Betreuung des Patienten'
      },
      {
        content: 'Sorgfältige Überprüfung des Wageninhaltes'
      },
      {
        content: 'Saubere, gepflegte Erscheinung des Sanitäters'
      },
    ],
  },
  {
    id: 2388044,
    question: 'Was versteht man unter thermischen Wunden?',
    choices: [
      {
        valid: true,
        content: 'Thermische Wunden sind Schädigungen der Haut'
      },
      {
        content: 'Verletzungen durch Säuren und/oder Laugen'
      },
      {
        valid: true,
        content: 'zum Teil mit Beteiligung tiefer liegender Gewebeschichten'
      },
      {
        valid: true,
        content: 'wird durch Hitze- und Kälteeinwirkung hervorgerufen'
      },
    ],
  },
  {
    id: 2387657,
    question: 'Was versteht man unter thermischen Wunden?',
    choices: [
      {
        valid: true,
        content: 'Thermische Wunden sind Schädigungen der Haut'
      },
      {
        content: 'Verletzungen durch Säuren und/oder Laugen'
      },
      {
        valid: true,
        content: 'zum Teil mit Beteiligung tiefer liegender Gewebeschichten'
      },
      {
        valid: true,
        content: 'wird durch Hitze- und Kälteeinwirkung hervorgerufen'
      },
    ],
  },
  {
    id: 2387925,
    question: 'Was versteht man unter thermischen Wunden?',
    choices: [
      {
        valid: true,
        content: 'Thermische Wunden sind Schädigungen der Haut'
      },
      {
        content: 'Verletzungen durch Säuren und/oder Laugen'
      },
      {
        valid: true,
        content: 'zum Teil mit Beteiligung tiefer liegender Gewebeschichten'
      },
      {
        valid: true,
        content: 'wird durch Hitze- und Kälteeinwirkung hervorgerufen'
      },
    ],
  },
  {
    id: 2388045,
    question: 'Was versteht man unter Verbrennunsgskrankheit?',
    choices: [
      {
        content: 'Möglicher Zustand von Helfern beim Anblick eines verbrannten Menschen'
      }, 
      {
        content: 'Erwärmung des Körpers und des Blutes durch Einwirkung von großer Hitze auf den Körper'
      }, 
      {
        content: 'Austritt von roten und weißen Blutkörperchen ins Gewebe'
      }, 
      {
        valid: true,
        content: 'Die Freisetzung von gefäßaktiven Substanzen im Körper bewirkt einen Flüssigkeitsverlust ins Gewebe'
      }, 
    ],
  },
  {
    id: 2387658,
    question: 'Was versteht man unter Verbrennunsgskrankheit?',
    choices: [
      {
        content: 'Möglicher Zustand von Helfern beim Anblick eines verbrannten Menschen'
      }, 
      {
        content: 'Erwärmung des Körpers und des Blutes durch Einwirkung von großer Hitze auf den Körper'
      }, 
      {
        content: 'Austritt von roten und weißen Blutkörperchen ins Gewebe'
      }, 
      {
        valid: true,
        content: 'Die Freisetzung von gefäßaktiven Substanzen im Körper bewirkt einen Flüssigkeitsverlust ins Gewebe'
      }, 
    ],
  },
  {
    id: 2387926,
    question: 'Was versteht man unter Verbrennunsgskrankheit?',
    choices: [
      {
        content: 'Möglicher Zustand von Helfern beim Anblick eines verbrannten Menschen'
      }, 
      {
        content: 'Erwärmung des Körpers und des Blutes durch Einwirkung von großer Hitze auf den Körper'
      }, 
      {
        content: 'Austritt von roten und weißen Blutkörperchen ins Gewebe'
      }, 
      {
        valid: true,
        content: 'Die Freisetzung von gefäßaktiven Substanzen im Körper bewirkt einen Flüssigkeitsverlust ins Gewebe'
      }, 
    ],
  },
  {
    id: 2389234,
    question: 'Was wird im Knochenmark gebildet?',
    choices: [
      {
        valid: true,
        content: 'Zum Teil die weißen Blutkörperchen'
      },
      {
        content: 'Insulin'
      },
      {
        valid: true,
        content: 'Die roten Blutkörperchen'
      },
      {
        content: 'Blutplasma'
      },
    ],
  },
  {
    id: 2388663,
    question: 'Welche  Erste-Hilfe-Maßnahmen werden bei Verätzungen im Bereich des Verdauungstraktes gesetzt?',
    choices: [
      {
        content: 'Sofort Wasser in kleinen Schlucken zu trinken geben'
      },
      {
        content: 'Mund ausspülen, anschließend mit Salzwasser zum Erbrechen bringen'
      },
      {
        valid: true,
        content: 'Kontaktaufnahme mit der Vergiftungsinformationszentrale: 01/4064343'
      },
      {
        valid: true,
        content: 'Mund ausspülen, Seitenlage, Notruf und Basismaßnahmen'
      },
    ],
  },
  {
    id: 2388878,
    question: 'Welche "Techniken" der Helmabnahme werden in der Ersten Hilfe empfohlen?',
    choices: [
      {
        valid: true,
        content: 'Helmabnahme durch einen Helfer'
      }, 
      {
        content: 'Helmabnahme durch zwei Helfer'
      }, 
      {
        content: 'Helmabnahme durch drei Helfer'
      }, 
      {
        content: 'Helmabnahme durch vier Helfer'
      }, 
    ],
  },
  {
    id: 2388951,
    question: 'Welche "Verbote" sind bei Verletzungen und unklaren Beschwerden im Bauchraum zu berücksichtigen?',
    choices: [
      {
        content: 'Basismaßnahmen'
      },
      {
        valid: true,
        content: 'Wärmendes auflegen (z.B. Thermophor)'
      },
      {
        valid: true,
        content: 'Zu essen oder trinken geben, rauchen lassen'
      },
      {
        content: 'Lagerung mit einer Knierolle'
      },
    ],
  },
  {
    id: 2389105,
    question: 'Welche abnormen Befunde gibt es im Rahmen der ABC-Beurteilung?',
    choices: [
      {
        valid: true,
        content: 'Atemfrequenz <10 bzw. >30/Minute'
      },
      {
        valid: true,
        content: 'Pulsfrequenz <40 bzw. >140/Minute'
      },
      {
        content: 'Blutzucker <80 bzw. >300 mg/dl'
      },
      {
        valid: true,
        content: 'Hautzustand: blass, zyanotisch, kaltschweißig'
      },
    ],
  },
  {
    id: 2389106,
    question: 'Welche abnormen Befunde im Rahmen der SAMPLE-Anamnese sind Warnzeichen?',
    choices: [
      {
        valid: true,
        content: 'SpO² <94% unter O²-Gabe'
      },
      {
        valid: true,
        content: 'Blutdruck systolisch <90 mmHG'
      },
      {
        valid: true,
        content: 'Blutzucker <40 bzw. >300 mg/dl'
      },
      {
        content: 'Pulsfrequenz <80 bzw. >140/Minute'
      },
    ],
  },
  {
    id: 2389107,
    question: 'Welche abweichenden Werte kann der Puls haben?',
    choices: [
      {
        content: 'Tachykardie Herzfrequenz unter 100/min'
      },
      {
        valid: true,
        content: 'Bradykardie Herzfrequenz unter 60/min'
      },
      {
        content: 'Bradykardie Herzfrequenz unter 80/min'
      },
      {
        valid: true,
        content: 'Tachykardie Herzfrequenz über 100/min'
      },
    ],
  },
  {
    id: 2388048,
    question: 'Welche akut auftretenden Blutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Blut im Stuhl'
      },
      {
        valid: true,
        content: 'Blut im Harn'
      },
      {
        valid: true,
        content: 'Nasenbluten'
      },
      {
        content: 'Hautwunden'
      },
    ],
  },
  {
    id: 2387661,
    question: 'Welche akut auftretenden Blutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Blut im Stuhl'
      },
      {
        valid: true,
        content: 'Blut im Harn'
      },
      {
        valid: true,
        content: 'Nasenbluten'
      },
      {
        content: 'Hautwunden'
      },
    ],
  },
  {
    id: 2387929,
    question: 'Welche akut auftretenden Blutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Blut im Stuhl'
      },
      {
        valid: true,
        content: 'Blut im Harn'
      },
      {
        valid: true,
        content: 'Nasenbluten'
      },
      {
        content: 'Hautwunden'
      },
    ],
  },
  {
    id: 2388930,
    question: 'Welche allgemeinen Erste-Hilfe-Maßnahmen sind bei Knochen- und Gelenksverletzungen zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Bei offenen Verletzungen keimfreien Verband anlegen'
      },
      {
        valid: true,
        content: 'Beengende Kleidungsstücke über der Verletzung lockern'
      },
      {
        valid: true,
        content: 'Ruhigstellung, bei Schwellung zusätzlich kühlen'
      },
      {
        valid: true,
        content: 'Basismaßnahmen und Notruf'
      },
    ],
  },
  {
    id: 2388329,
    question: 'Welche alternativen Medizinprodukte gibt es zur Dreiecktuchkrawatte/Fixierung (Druckverband)?',
    choices: [
      {
        valid: true,
        content: 'Momentverband, Verbandpäckchen'
      },
      {
        valid: true,
        content: 'Elastische Mullbinden'
      },
      {
        content: 'Es darf nur mit der Dreiecktuchkrawatte fixiert werden'
      },
      {
        valid: true,
        content: 'Als Druckkörper ist auch eine Mullbinde geeignet'
      },
    ],
  },
  {
    id: 2388874,
    question: 'Welche Angaben sind beim Notruf sinnvoll?',
    choices: [
      {
        valid: true,
        content: 'Hausnummer'
      }, 
      {
        valid: true,
        content: 'Ort und genaue Adresse'
      },
      {
        valid: true,
        content: 'Kilometerangaben auf Autobahnen'
      },
      {
        content: 'Wetterlage'
      }, 
    ],
  },
  {
    id: 2387662,
    question: 'Welche angeführten Sanitätshilfe-Maßnahmen sind durchzuführen, wenn ein zweijähriges Kind Goldregenblätter gegessen hat?',
    choices: [
      {
        valid: true,
        content: 'Flüssigkeitszufuhr: Wasser'
      },
      {
        content: 'Kind sofort zum Erbrechen bringen'
      },
      {
        valid: true,
        content: 'Von der VIZ empfohlene Maßnahmen durchführen'
      },
      {
        valid: true,
        content: 'Verdächtige Pflanze sicherstellen, ins Krankenhaus mitnehmen'
      },
    ],
  },
  {
    id: 2387930,
    question: 'Welche angeführten Sanitätshilfe-Maßnahmen sind durchzuführen, wenn ein zweijähriges Kind Goldregenblätter gegessen hat?',
    choices: [
      {
        valid: true,
        content: 'Flüssigkeitszufuhr: Wasser'
      },
      {
        content: 'Kind sofort zum Erbrechen bringen'
      },
      {
        valid: true,
        content: 'Von der VIZ empfohlene Maßnahmen durchführen'
      },
      {
        valid: true,
        content: 'Verdächtige Pflanze sicherstellen, ins Krankenhaus mitnehmen'
      },
    ],
  },
  {
    id: 2389235,
    question: 'Welche Armknochen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Oberarmknochen'
      },
      {
        valid: true,
        content: 'Unterarmknochen (=Elle und Speiche)'
      },
      {
        valid: true,
        content: 'Fingerknochen'
      },
      {
        content: 'Wurzelknochen'
      },
    ],
  },
  {
    id: 2388188,
    question: 'Welche Arten der Energieabgabe gibt es bei der Defibrillation?',
    choices: [
      {
        valid: true,
        content: 'Monophasisch'
      },
      {
        content: 'Stereophasisch'
      },
      {
        content: 'Semiphasisch'
      },
      {
        valid: true,
        content: 'Biphasisch'
      },
    ],
  },
  {
    id: 2388189,
    question: 'Welche Arten der Injektion gibt es?',
    choices: [
      {
        valid: true,
        content: 'Subcutan (s.c.): unter die Haut'
      },
      {
        valid: true,
        content: 'Intravenös (i.v.): in die Vene'
      },
      {
        valid: true,
        content: 'Intraossär (i.o.): in den Knochen'
      },
      {
        valid: true,
        content: 'Intramuskulär (i.m.): in den Muskel'
      },
    ],
  },
  {
    id: 2389146,
    question: 'Welche Arten der Kennzeichnung bei gefährlichen Gütern gibt es?',
    choices: [
      {
        valid: true,
        content: 'Auf der Spitze stehende Quadrate mit Symbolen'
      },
      {
        valid: true,
        content: 'Orangefarbene Tafeln'
      },
      {
        valid: true,
        content: 'Gelbe gleichseitige Dreiecke'
      },
      {
        content: 'Rote Aufkleber mit weißem Ring'
      },
    ],
  },
  {
    id: 2388952,
    question: 'Welche Arten von Herzrhythmusstörungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Tachykardie'
      },
      {
        valid: true,
        content: 'Bradykardie'
      },
      {
        valid: true,
        content: 'Arrhythmie'
      },
      {
        valid: true,
        content: 'Extrasystolen'
      },
    ],
  },
  {
    id: 2388050,
    question: 'Welche Arten von Hirnblutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Subdurale Blutung'
      },
      {
        valid: true,
        content: 'Epidurale Blutung'
      },
      {
        valid: true,
        content: 'Subarachnoidalblutung'
      },
      {
        valid: true,
        content: 'Intracerebrale Blutung'
      },
    ],
  },
  {
    id: 2387663,
    question: 'Welche Arten von Hirnblutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Subdurale Blutung'
      },
      {
        valid: true,
        content: 'Epidurale Blutung'
      },
      {
        valid: true,
        content: 'Subarachnoidalblutung'
      },
      {
        valid: true,
        content: 'Intracerebrale Blutung'
      },
    ],
  },
  {
    id: 2387931,
    question: 'Welche Arten von Hirnblutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Subdurale Blutung'
      },
      {
        valid: true,
        content: 'Epidurale Blutung'
      },
      {
        valid: true,
        content: 'Subarachnoidalblutung'
      },
      {
        valid: true,
        content: 'Intracerebrale Blutung'
      },
    ],
  },
  {
    id: 2387946,
    question: 'Welche Arten von Hirnblutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Subdurale Blutung'
      },
      {
        valid: true,
        content: 'Epidurale Blutung'
      },
      {
        valid: true,
        content: 'Subarachnoidalblutung'
      },
      {
        valid: true,
        content: 'Intracerebrale Blutung'
      },
    ],
  },
  {
    id: 2389147,
    question: 'Welche Arten von Katastrophen hinsichtlich der Ursache gibt es?',
    choices: [
      {
        valid: true,
        content: 'Naturkatastrophen'
      },
      {
        valid: true,
        content: 'kriegsbedingte Katastrophen'
      },
      {
        valid: true,
        content: 'anthropogene Katastrophen'
      },
      {
        valid: true,
        content: 'konfliktbedingte Katastrophen'
      },
    ],
  },
  {
    id: 2388051,
    question: 'Welche Arten von Knochenbrüchen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Offene Knochenbrüche'
      },
      {
        valid: true,
        content: 'Geschlossene Knochenbrüche'
      },
      {
        content: 'Unkomplizierter oder komplizierter Bruch'
      },
      {
        content: 'Brüche der oberen und unteren Gliedmaßen'
      },
    ],
  },
  {
    id: 2387664,
    question: 'Welche Arten von Knochenbrüchen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Offene Knochenbrüche'
      },
      {
        valid: true,
        content: 'Geschlossene Knochenbrüche'
      },
      {
        content: 'Unkomplizierter oder komplizierter Bruch'
      },
      {
        content: 'Brüche der oberen und unteren Gliedmaßen'
      },
    ],
  },
  {
    id: 2387932,
    question: 'Welche Arten von Knochenbrüchen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Offene Knochenbrüche'
      },
      {
        valid: true,
        content: 'Geschlossene Knochenbrüche'
      },
      {
        content: 'Unkomplizierter oder komplizierter Bruch'
      },
      {
        content: 'Brüche der oberen und unteren Gliedmaßen'
      },
    ],
  },
  {
    id: 2387947,
    question: 'Welche Arten von Knochenbrüchen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Offene Knochenbrüche'
      },
      {
        valid: true,
        content: 'Geschlossene Knochenbrüche'
      },
      {
        content: 'Unkomplizierter oder komplizierter Bruch'
      },
      {
        content: 'Brüche der oberen und unteren Gliedmaßen'
      },
    ],
  },
  {
    id: 2389570,
    question: 'Welche Arten von Stress gibt es?',
    choices: [
      {
        valid: true,
        content: 'Einsatzbezogener Stress'
      },
      {
        valid: true,
        content: 'Kumulativer Stress'
      },
      {
        valid: true,
        content: 'Traumatischer Stress'
      },
      {
        content: 'Subtrahierender Stress'
      },
    ],
  },
  {
    id: 2388052,
    question: 'Welche Arten von Wehen gibt es?',
    choices: [
      {
        content: 'Einleitungswehen'
      },
      {
        valid: true,
        content: 'Senkungswehen'
      },
      {
        valid: true,
        content: 'Nachgeburtswehen'
      },
      {
        valid: true,
        content: 'Eröffnungs- und Presswehen'
      },
    ],
  },
  {
    id: 2387665,
    question: 'Welche Arten von Wehen gibt es?',
    choices: [
      {
        content: 'Einleitungswehen'
      },
      {
        valid: true,
        content: 'Senkungswehen'
      },
      {
        valid: true,
        content: 'Nachgeburtswehen'
      },
      {
        valid: true,
        content: 'Eröffnungs- und Presswehen'
      },
    ],
  },
  {
    id: 2387933,
    question: 'Welche Arten von Wehen gibt es?',
    choices: [
      {
        content: 'Einleitungswehen'
      },
      {
        valid: true,
        content: 'Senkungswehen'
      },
      {
        valid: true,
        content: 'Nachgeburtswehen'
      },
      {
        valid: true,
        content: 'Eröffnungs- und Presswehen'
      },
    ],
  },
  {
    id: 2387948,
    question: 'Welche Arten von Wehen gibt es?',
    choices: [
      {
        content: 'Einleitungswehen'
      },
      {
        valid: true,
        content: 'Senkungswehen'
      },
      {
        valid: true,
        content: 'Nachgeburtswehen'
      },
      {
        valid: true,
        content: 'Eröffnungs- und Presswehen'
      },
    ],
  },
  {
    id: 2388053,
    question: 'Welche Atemfrequenz hat ein ca. 2 Jahre altes Kind?',
    choices: [
      {
        content: 'ca. 40 /min'
      },
      {
        content: 'ca. 30 /min'
      },
      {
        valid: true,
        content: 'ca. 20 /min'
      },
      {
        content: 'ca. 15 /min.'
      },
    ],
  },
  {
    id: 2387666,
    question: 'Welche Atemfrequenz hat ein ca. 2 Jahre altes Kind?',
    choices: [
      {
        content: 'ca. 40 /min'
      },
      {
        content: 'ca. 30 /min'
      },
      {
        valid: true,
        content: 'ca. 20 /min'
      },
      {
        content: 'ca. 15 /min.'
      },
    ],
  },
  {
    id: 2387934,
    question: 'Welche Atemfrequenz hat ein ca. 2 Jahre altes Kind?',
    choices: [
      {
        content: 'ca. 40 /min'
      },
      {
        content: 'ca. 30 /min'
      },
      {
        valid: true,
        content: 'ca. 20 /min'
      },
      {
        content: 'ca. 15 /min.'
      },
    ],
  },
  {
    id: 2387949,
    question: 'Welche Atemfrequenz hat ein ca. 2 Jahre altes Kind?',
    choices: [
      {
        content: 'ca. 40 /min'
      },
      {
        content: 'ca. 30 /min'
      },
      {
        valid: true,
        content: 'ca. 20 /min'
      },
      {
        content: 'ca. 15 /min.'
      },
    ],
  },
  {
    id: 2389236,
    question: 'Welche Aufgabe erfüllt der Kreislauf?',
    choices: [
      {
        valid: true,
        content: 'Transport von Sauerstoff'
      },
      {
        valid: true,
        content: 'Transport von Kohlendioxid'
      },
      {
        valid: true,
        content: 'Energietransport'
      },
      {
        valid: true,
        content: 'Regulation des Blutdrucks'
      },
    ],
  },
  {
    id: 2389237,
    question: 'Welche Aufgabe erfüllt der Wärmehaushalt?',
    choices: [
      {
        valid: true,
        content: 'Er dient einem geordneten Ablauf aller Lebensvorgänge'
      },
      {
        valid: true,
        content: 'Er hält eine konstante Regeltemperatur von 36°C bis 37°C aufrecht'
      },
      {
        content: 'Er dient dem geordneten Ablauf aller Bewegungsvorgänge'
      },
      {
        content: 'Er dient der räumlichen Orientierung (Körpergleichgewicht)'
      },
    ],
  },
  {
    id: 2389238,
    question: 'Welche Aufgabe erfüllt der Wasser- und Elektrolythaushalt?',
    choices: [
      {
        content: 'Er spült die Nieren nach der Harnproduktion'
      },
      {
        content: 'Er verhindert eine zu hohe Hormonkonzentration'
      },
      {
        content: 'Er verhindert Gewebszerstörung durch Giftstoffe'
      },
      {
        valid: true,
        content: 'Er dient der Steuerung wichtiger physikalisch-chemischer Reaktionen im Körper.'
      },
    ],
  },
  {
    id: 2389239,
    question: 'Welche Aufgabe haben Blutgefäße?',
    choices: [
      {
        content: 'Die Atmung aufrecht zu halten'
      },
      {
        content: 'Das Blut zu reinigen'
      },
      {
        valid: true,
        content: 'Das Blut im Körper je nach Erfordernissen ideal zu verteilen'
      },
      {
        content: 'Den Kreislauf zu stabilisieren'
      },
    ],
  },
  {
    id: 2389240,
    question: 'Welche Aufgabe hat das Blut?',
    choices: [
      {
        valid: true,
        content: 'Transport von Sauerstoff'
      },
      {
        valid: true,
        content: 'Abtransport von Kohlendioxid'
      },
      {
        valid: true,
        content: 'Temperaturregulation'
      },
      {
        content: 'Abtransport von Sauerstoff'
      },
    ],
  },
  {
    id: 2389241,
    question: 'Welche Aufgabe und Funktion haben die Gelenke?',
    choices: [
      {
        content: 'Sie dichten die Knochen an den Enden ab'
      },
      {
        valid: true,
        content: 'Sie ermöglichen die Beweglichkeit'
      },
      {
        content: 'Sie bilden Schleim'
      },
      {
        valid: true,
        content: 'Sie verbinden die Knochen'
      },
    ],
  },
  {
    id: 2389242,
    question: 'Welche Aufgaben hat das vegetative Nervensystem?',
    choices: [
      {
        valid: true,
        content: 'Regelung der Atmung'
      },
      {
        valid: true,
        content: 'Regelung der Verdauung'
      },
      {
        valid: true,
        content: 'Regelung des Kreislaufes'
      },
      {
        valid: true,
        content: 'Regelung der Körpertemperatur'
      },
    ],
  },
  {
    id: 2388879,
    question: 'Welche Aufgaben hat der Ersthelfer?',
    choices: [
      {
        valid: true,
        content: 'Blutungen stillen'
      }, 
      {
        valid: true,
        content: 'Basismaßnahmen durchführen'
      }, 
      {
        content: 'Essen und Trinken verabreichen'
      }, 
      {
        content: 'Retten von Verletzten aus allen Gefahrensituationen'
      }, 
    ],
  },
  {
    id: 2388931,
    question: 'Welche Aufgaben hat der Helfer bei einer Verstauchung?',
    choices: [
      {
        valid: true,
        content: 'Kühlung der Schwellung'
      },
      {
        valid: true,
        content: 'Schonung des Gelenks'
      },
      {
        content: 'Kompressionsverband anlegen'
      },
      {
        valid: true,
        content: 'Verletzten Körperteil erhöht lagern'
      },
    ],
  },
  {
    id: 2388431,
    question: 'Welche Aufgaben hat eine Einsatzleitstelle?',
    choices: [
      {
        valid: true,
        content: 'Festlegung der Einsatzart'
      },
      {
        valid: true,
        content: 'Voranmeldung ans Krankenhaus in bedrohlichen Situationen'
      },
      {
        valid: true,
        content: 'Erste-Hilfe-Anleitung für den Anrufer'
      },
      {
        valid: true,
        content: 'Einsatzalarmierung und Erteilung des Transportauftrages'
      },
    ],
  },
  {
    id: 2388432,
    question: 'Welche Ausrüstungsgegenstände gehören zur persönlichen Schutzausrüstung?',
    choices: [
      {
        valid: true,
        content: 'Persönliche Schutzausrüstung - Schutzkleidung'
      },
      {
        content: 'Notfallkoffer'
      },
      {
        valid: true,
        content: 'Einmalhandschuhe'
      },
      {
        valid: true,
        content: 'Berufsschuhe/Sicherheitsschuhe nach EN 344'
      },
    ],
  },
  {
    id: 2389148,
    question: 'Welche Aussagen treffen auf den Gefahrzettel zu?',
    choices: [
      {
        valid: true,
        content: 'Ein auf der Spitze stehendes Quadrat'
      },
      {
        content: 'Dreieck'
      },
      {
        content: 'Kreis'
      },
      {
        content: 'Die Form des Buchstaben X'
      },
    ],
  },
  {
    id: 2389245,
    question: 'Welche Aussagen treffen auf den Hirnstamm zu?',
    choices: [
      {
        content: 'Zentrum des peripheren Nervensystems'
      },
      {
        content: 'Zentrum des Zentralnervensystems'
      },
      {
        valid: true,
        content: 'Zentrum des vegetativen Nervensystems'
      },
      {
        valid: true,
        content: 'Steuerung von Atmung und Kreislauf'
      },
    ],
  },
  {
    id: 2388190,
    question: 'Welche Aussagen treffen auf den Oro-Sauger zu?',
    choices: [
      {
        valid: true,
        content: 'Er besteht aus einem kleinen Sekretgefäß und 2 kleinen Schläuchen'
      },
      {
        valid: true,
        content: 'Er ist ein Mundabsauggerät für Säuglinge und Neugeborene'
      },
      {
        content: 'Er hat eine farbliche Kennzeichnung (Charge)'
      },
      {
        content: 'Mundabsauggerät für alle Patientengruppen'
      },
    ],
  },
  {
    id: 2388716,
    question: 'Welche Aussagen treffen auf die Entsorgung von Spritzen und Nadeln zu?',
    choices: [
      {
        content: 'Zurückstecken in die Plastikhülle und Entsorgung im Restmüll'
      },
      {
        content: 'Im Krankenhaus in den Infektionsmüllsack geben'
      },
      {
        valid: true,
        content: 'Entsorgung in speziellen Abwurfbehältern für Spritzen und Kanülen'
      },
      {
        valid: true,
        content: 'Sammelbehälter nur zu etwa zwei Drittel füllen, nicht umfüllen oder ausleeren'
      },
    ],
  },
  {
    id: 2387667,
    question: 'Welche Aussagen treffen auf ein SHT (Schädelhirntrauma) zu?',
    choices: [
      {
        valid: true,
        content: 'Verletzungen des knöchernen Schädels und Weichteilbeteiligung'
      },
      {
        content: 'Ein SHT kann immer auftreten, auch bei einer Erkrankung'
      },
      {
        valid: true,
        content: 'Jegliche Gewalteinwirkung (Trauma) durch Anprall, Schlag, Sturz auf den Kopf'
      },
      {
        valid: true,
        content: 'Funktionsstörungen aufgrund von Verletzungen des Gehirns'
      },
    ],
  },
  {
    id: 2387668,
    question: 'Welche Aussagen treffen auf einen offenen Pneumothorax zu?',
    choices: [
      {
        valid: true,
        content: 'Hochgradige Atemnot'
      },
      {
        valid: true,
        content: 'Austritt von schaumigem Blut aus der Wunde'
      },
      {
        valid: true,
        content: 'Bei einer offenen Brustkorbwand kann Luft in den Brustfellraum eindringen. Die Lunge fällt zusammen'
      },
      {
        valid: true,
        content: 'Zyanose, Halsvenenstauung'
      },
    ],
  },
  {
    id: 2389246,
    question: 'Welche Aussagen treffen auf Funktion, Aufbau und Lage des Herzens zu?',
    choices: [
      {
        valid: true,
        content: 'Das Herz ist ein faustgroßer Hohlmuskel'
      },
      {
        valid: true,
        content: 'Das Herz hat ein eigenes Nervensystem'
      },
      {
        content: 'Das Herz liegt im Bauchraum'
      },
      {
        valid: true,
        content: 'Das Herz wirkt wie eine Pumpe'
      },
    ],
  },
  {
    id: 2388692,
    question: 'Welche Aussagen treffen bei Ausfall des Bewusstseins zu?',
    choices: [
      {
        valid: true,
        content: 'in Rückenlage besteht Lebensgefahr durch Ersticken'
      }, 
      {
        valid: true,
        content: 'Stabile Seitenlage ist die optimale Lagerung, falls normale Atmung feststellbar ist'
      }, 
      {
        content: 'in Rückenlage besteht keine Lebensgefahr'
      }, 
      {
        valid: true,
        content: 'falls keine normale Atmung feststellbar, mit Wiederbelebung starten'
      }, 
    ],
  },
  {
    id: 2387669,
    question: 'Welche Aussagen treffen bei einem Patienten zu, der beinahe ertrunken ist - Transport in ein Krankenhaus ja/nein?',
    choices: [
      {
        valid: true,
        content: 'Der Patient muss in jedem Fall in ein Krankenhaus gebracht werden.'
      },
      {
        content: 'Nur bei auftretenden Atemstörungen in ein Krankenhaus bringen'
      },
      {
        content: 'Nur bei anhaltender Bewusstlosigkeit in ein Krankenhaus bringen'
      },
      {
        content: 'Der Patient muss in kein Krankenhaus gebracht werden, er kann auch Aufsichtspersonen übergeben werden'
      },
    ],
  },
  {
    id: 2388680,
    question: 'Welche Aussagen treffen bezüglich Defibribrillation in der Ersten Hilfe zu?',
    choices: [
      {
        valid: true,
        content: 'Die Defibrillation darf bei Notfallpatienten ab dem 1. vollendeten Lebensjahr angewendet werden'
      },
      {
        content: 'Die Defibrillation darf nur von einem Rettungssanitäter angewendet werden'
      },
      {
        content: 'Die Defibrillation darf nur vom Arzt angewendet werden'
      },
      {
        valid: true,
        content: 'Die rechtliche Situation besagt, dass die Defibrillation in einer Notsituation unbedenklich ist'
      },
    ],
  },
  {
    id: 2388664,
    question: 'Welche Aussagen treffen im Zuge der Versorgung (Erste Hilfe) von Brandwunden zu?',
    choices: [
      {
        content: 'Die Spülung soll mit kaltem Wasser für mindestens 20 Minuten durchgeführt werden'
      },
      {
        content: 'Die Wunde darf bei der Kühlung nicht direkt mit Wasser in Berührung kommen'
      },
      {
        valid: true,
        content: 'Spülung mit handwarmem Wasser, Patienten darf es nicht frösteln'
      },
      {
        valid: true,
        content: 'Versorgung mit Wundauflagen, falls vorhanden mit speziellen Wundauflagen (metallisiert)'
      },
    ],
  },
  {
    id: 2389243,
    question: 'Welche äußeren Geschlechtsorgane hat der Mann?',
    choices: [
      {
        valid: true,
        content: 'Penis'
      },
      {
        content: 'Schamlippen'
      },
      {
        valid: true,
        content: 'Hodensack'
      },
      {
        content: 'Hoden'
      },
    ],
  },
  {
    id: 2389244,
    question: 'Welche äußeren Geschlechtsorgane hat die Frau?',
    choices: [
      {
        valid: true,
        content: 'Große und kleine Schamlippen'
      },
      {
        content: 'Brustwarzen'
      },
      {
        content: 'Eierstöcke'
      },
      {
        valid: true,
        content: 'Scheidenvorhof'
      },
    ],
  },
  {
    id: 2388880,
    question: 'Welche Basismaßnahmen soll der Ersthelfer durchführen?',
    choices: [
      {
        valid: true,
        content: 'entsprechende Lagerung durchführen'
      }, 
      {
        valid: true,
        content: 'für frische Luft sorgen'
      }, 
      {
        valid: true,
        content: 'Wärme erhalten - zudecken (bei Kälte)'
      }, 
      {
        valid: true,
        content: 'psychisch betreuen, für Ruhe sorgen und gut zureden'
      }, 
    ],
  },
  {
    id: 2387670,
    question: 'Welche Beatmungsfrequenz wird bei intubierten Säuglingen und Kindern angewandt?',
    choices: [
      {
        valid: true,
        content: 'je nach Alter 12-20 mal/Minute'
      },
      {
        content: 'je nach Alter 10-15 mal/Minute'
      },
      {
        content: 'je nach Alter 8-13 mal/Minute'
      },
      {
        content: 'je nach Alter 20-30 mal/Minute'
      },
    ],
  },
  {
    id: 2389149,
    question: 'Welche Bedeutung haben die zwei Nummerngruppen auf einer Warntafel?',
    choices: [
      {
        content: 'Hinweis auf verflüssigte Gase'
      },
      {
        valid: true,
        content: 'Stoffnummer'
      },
      {
        valid: true,
        content: 'Gefahrennummer'
      },
      {
        valid: true,
        content: 'Kemlernummer'
      },
    ],
  },
  {
    id: 2388433,
    question: 'Welche Befreiungsmöglichkeiten aus dem Stromkreis bei Niederspannung gibt es?',
    choices: [
      {
        content: 'Vorsichtiges Herausziehen des Verunglückten aus dem Spannungstrichter'
      },
      {
        valid: true,
        content: 'Spannungsführenden Draht aus der Hand ziehen (auf Isolierung achten)'
      },
      {
        content: 'Befreiung nicht möglich, auf Spezialkräfte warten'
      },
      {
        valid: true,
        content: 'Versuchen, den Netzstecker des Gerätes zu ziehen bzw. die Sicherung auszuschalten'
      },
    ],
  },
  {
    id: 2388191,
    question: 'Welche beiden Werte können beim Blutdruck unterschieden werden?',
    choices: [
      {
        valid: true,
        content: 'Systolischer Wert'
      },
      {
        content: 'Asystolischer Wert'
      },
      {
        content: 'Diabolischer Wert'
      },
      {
        valid: true,
        content: 'Diastolischer Wert'
      },
    ],
  },
  {
    id: 2389247,
    question: 'Welche Beinknochen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Oberschenkelknochen'
      },
      {
        valid: true,
        content: 'Unterschenkelknochen=Schien- und Wadenbein'
      },
      {
        valid: true,
        content: 'Fußwurzelknochen'
      },
      {
        valid: true,
        content: 'Mittelfußknochen'
      },
    ],
  },
  {
    id: 2388192,
    question: 'Welche Bereiche müssen bei einem Knochenbruch mitgeschient werden?',
    choices: [
      {
        content: 'Immer den ganzen Körper ruhigstellen'
      },
      {
        valid: true,
        content: 'Über die benachbarten Gelenke hinaus'
      },
      {
        content: 'Bis an die benachbarten Gelenke heran'
      },
      {
        content: 'Über das nächstliegende Gelenk hinaus'
      },
    ],
  },
  {
    id: 2388717,
    question: 'Welche bestimmte Infektionskrankheit führt zur prophylaktischen Antibiotikamedikation (enger Kontakt mit Patienten)?',
    choices: [
      {
        valid: true,
        content: 'Eitrige Meningitis'
      },
      {
        content: 'FSME'
      },
      {
        content: 'Tetanus'
      },
      {
        content: 'Schnupfen'
      },
    ],
  },
  {
    id: 2388953,
    question: 'Welche Blutdruckwerte sind bei einer Hochdruckkrise möglich?',
    choices: [
      {
        valid: true,
        content: 'systolischer Wert über 200 mm HG'
      },
      {
        content: 'systolischer Wert bei 160 mm HG'
      },
      {
        content: 'systolischer Wert bei 140 mm HG'
      },
      {
        content: 'systolischer Wert bei 120 mm HG'
      },
    ],
  },
  {
    id: 2388434,
    question: 'Welche brennbaren Stoffe können mit einem Feuerlöscher der Brandklassen A, B, C gelöscht werden?',
    choices: [
      {
        valid: true,
        content: 'Feste Stoffe'
      },
      {
        valid: true,
        content: 'Flüssige Stoffe'
      },
      {
        valid: true,
        content: 'Gasförmige Stoffe'
      },
      {
        content: 'Alle brennbaren Stoffe'
      },
    ],
  },
  {
    id: 2388193,
    question: 'Welche Defibrillatoren darf der Rettungssanitäter nach entsprechender Einschulung verwenden?',
    choices: [
      {
        content: 'Manuelle Defibrillatoren'
      },
      {
        valid: true,
        content: 'Halbautomatische Defibrillatoren'
      },
      {
        content: 'Alle Defibrillatoren'
      },
      {
        content: 'Implantierte Defibrillatoren'
      },
    ],
  },
  {
    id: 2387671,
    question: 'Welche der angeführten Altersdefinitionen beim Säugling und Kind gibt es?',
    choices: [
      {
        valid: true,
        content: 'Kind, vollendetes 1. Lebensjahr bis zum Beginn der Pubertät'
      },
      {
        content: 'Kind, vollendetes 2. Lebensjahr bis zum Beginn der Pubertät'
      },
      {
        content: 'Säugling, bis zum vollendeten 2. Lebensjahr'
      },
      {
        valid: true,
        content: 'Säugling, bis zum vollendeten 1. Lebensjahr'
      },
    ],
  },
  {
    id: 2388581,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind bei einem Asthmaanfall zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
      {
        valid: true,
        content: 'Basismaßnahmen durchführen'
      },
      {
        valid: true,
        content: 'Erleichterung der Atmung - Abstützen der Arme'
      },
    ],
  },
  {
    id: 2388582,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind bei einem Herzinfarkt zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Notruf, Defibrillator und Verbandskasten holen lassen'
      },
      {
        valid: true,
        content: 'Öffnen beengender Kleidungstücke'
      },
      {
        valid: true,
        content: 'Basismaßnahmen'
      },
      {
        content: 'Am nächsten Tag den Hausarzt zur Aufklärung aufsuchen'
      },
    ],
  },
  {
    id: 2388583,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind bei einem Schlaganfall zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Notruf'
      },
      {
        valid: true,
        content: 'Beruhigung, Seitenlage zur Vorbeugung'
      },
      {
        valid: true,
        content: 'Basismaßnahmen'
      },
      {
        valid: true,
        content: 'Öffnen beengender Kleidungsstücke, Frischluftzufuhr'
      },
    ],
  },
  {
    id: 2388584,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind bei einem Sonnenstich/Hitzschlag zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Betroffenen an einen schattigen Ort bringen'
      },
      {
        valid: true,
        content: 'Wasser zu trinken geben'
      },
      {
        valid: true,
        content: 'Basismaßnahmen'
      },
      {
        valid: true,
        content: 'Kalte Umschläge'
      },
    ],
  },
  {
    id: 2388585,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind bei Nasenbluten zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Patienten hinsetzen und den Kopf nach vorne beugen lassen, betroffenen Nasenflügel zudrücken'
      },
      {
        content: 'Patienten hinsetzen und den Kopf nackenwärts beugen lassen'
      },
      {
        valid: true,
        content: 'Kalte Umschläge auf den Nacken geben'
      },
      {
        valid: true,
        content: 'Durchführung der Basismaßnahmen'
      },
    ],
  },
  {
    id: 2388665,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind nach einem Biss durch eine heimische Giftschlange zu setzen?',
    choices: [
      {
        content: 'Warme Umschläge auf die Bissstelle legen'
      },
      {
        valid: true,
        content: 'Verbinden der Wunde und kalte Umschläge auf die Bissstelle legen'
      },
      {
        content: 'Verletzten Körperteil heftig bewegen lassen, kalte Umschläge auf die Bissstelle legen'
      },
      {
        valid: true,
        content: 'Durchführung der Basismaßnahmen, Notruf'
      },
    ],
  },
  {
    id: 2388666,
    question: 'Welche der angeführten Erste-Hilfe-Maßnahmen sind nach einem Zeckenbiss zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Ungeimpfte sollen nach einem Zeckenbiss auf jeden Fall zum Arzt'
      },
      {
        content: 'Öl auftropfen, durch kreisende Bewegung Zecken lockern und entfernen'
      },
      {
        valid: true,
        content: 'Bei späteren Rötungen bzw. Entzündungen ist der Arzt aufzusuchen.'
      },
      {
        valid: true,
        content: 'Mit einer Pinzette möglichst weit vorne fassen und ganz gerade herausziehen'
      },
    ],
  },
  {
    id: 2389248,
    question: 'Welche der angeführten inneren Geschlechtsorgane findet man im weiblichen Körper?',
    choices: [
      {
        valid: true,
        content: 'Gebärmutter'
      },
      {
        valid: true,
        content: '2 Eileiter'
      },
      {
        valid: true,
        content: '2 Eierstöcke'
      },
      {
        content: 'Prostata'
      },
    ],
  },
  {
    id: 2388330,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen müssen bei der Notfalldiagnose Bewusstlosigkeit durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'Stabile Seitenlagerung auf die versorgungstechnisch günstigere Seite'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        content: 'Frühdefi-Bereitschaft'
      },
    ],
  },
  {
    id: 2388954,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen müssen bei einem Patienten mit Hypoglykämie durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'Patient ohne Bewusstsein, Kontrolle der Lebensfunktionen und lebensrettende Maßnahmen'
      },
      {
        valid: true,
        content: 'Patient bei Bewusstsein: Zucker (Zuckerwasser, Traubenzucker, ...) verabreichen'
      },
      {
        content: 'Patient bei Bewusstsein: Elektrolytgetränke verabreichen'
      },
      {
        content: 'Patient sofort Insulin verabreichen'
      },
    ],
  },
  {
    id: 2387672,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen müssen bei einem Patienten, der einige Zeit Rauchgasen ausgesetzt war, durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Sauerstoffgabe von 10-15 Liter/min.'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        content: 'Cortisonspray, alle 5 Minuten'
      },
    ],
  },
  {
    id: 2387673,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen müssen bei einem Patienten, der vor ca. 10 Stunden eine größere Menge Tabletten zu sich genommen hat, durchgeführt werden?',
    choices: [
      {
        content: 'Sofort Wasser zu trinken geben, um das Gift zu verdünnen'
      },
      {
        content: 'Sofort Erbrechen provozieren, damit das Gift nicht vom Körper aufgenommen wird'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
    ],
  },
  {
    id: 2387674,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen müssen bei kindlichen Fieberkrämpfen durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'Patienten vor Verletzungen schützen'
      },
      {
        valid: true,
        content: 'Beengende Kleidungsstücke öffnen'
      },
      {
        valid: true,
        content: 'Prophylaktische Seitenlage'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
    ],
  },
  {
    id: 2388955,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Asthma Bronchiale durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Keine körperliche Anstrengung'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Patienten beruhigen und zu ruhiger Atmung anhalten'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2387675,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Bewusstlosigkeit beim Kind (normale Atmung und Herzfrequenz über 60) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Stabile Seitenlage'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Frühdefi-Bereitschaft'
      },
      {
        content: 'Blutzuckermessung auf alle Fälle durchführen'
      },
    ],
  },
  {
    id: 2388956,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem akuten Abdomen durchzuführen?',
    choices: [
      {
        content: 'Auflegen eines warmen Thermophor'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Lagerung nach Wunsch des Patienten'
      },
      {
        valid: true,
        content: 'Schonender Transport in Rückenlage mit angezogenen Beinen und leicht erhöhtem Oberkörper'
      },
    ],
  },
  {
    id: 2388957,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Angina-Pectoris-Anfall durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Keine körperliche Anstrengung'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Öffnen beengender Kleidungsstücke'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
    ],
  },
  {
    id: 2388958,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem arteriellen Gefäßverschluss durchzuführen?',
    choices: [
      {
        content: 'Flach lagern und vor Unterkühlung schützen'
      },
      {
        valid: true,
        content: 'Tief lagern, weich polstern und vor Unterkühlung schützen'
      },
      {
        content: 'Hoch lagern, gut polstern und vor Unterkühlung schützen'
      },
      {
        content: 'Mit Thermophor wärmen'
      },
    ],
  },
  {
    id: 2387676,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Ertrinkungsunfall (Patient ohne Bewusstsein) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Kontrolle der Lebensfunktionen und lebensrettende Maßnahmen'
      },
      {
        content: 'Atem-Kreislauf-Stillstand: Begonnen wird mit fünf Beatmungen'
      },
      {
        valid: true,
        content: 'Herzdruckmassage und Beatmung 30:2, max. 3-mal Defibrillation bei KKT < 30 °C'
      },
      {
        valid: true,
        content: 'Atem-Kreislauf-Stillstand: Begonnen wird mit Herzdruckmassage'
      },
    ],
  },
  {
    id: 2387677,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Ertrinkungsunfall durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Rettung durch Wasserrettung oder Feuerwehr'
      },
      {
        valid: true,
        content: 'Sauerstoffgabe von 10-15 Liter/min., Absaugbereitschaft'
      },
      {
        valid: true,
        content: 'Patient bei Bewusstsein: nasse Kleidung entfernen, Wärmeerhaltung im Fahrzeug, allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Patient ohne Bewusstsein: Kontrolle der Lebensfunktionen und lebensrettende Maßnahmen'
      },
    ],
  },
  {
    id: 2388959,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Herzinfarkt durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Schockelektroden vorbeugend aufkleben'
      },
      {
        valid: true,
        content: 'Keine körperliche Anstrengung'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2388960,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Kanülenträger (Atemnot) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Absaugen'
      },
      {
        content: 'Falls erforderlich Kanüle unbedingt entfernen (Erstickungsgefahr!)'
      },
    ],
  },
  {
    id: 2388961,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Lungenödem durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Sauerstoffgabe von 10-15 Liter/min.'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Alle 1-2 Minuten Gabe von Cortison (Spray)'
      },
    ],
  },
  {
    id: 2387678,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Nabelschnurvorfall durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Becken hoch lagern'
      },
      {
        content: 'Linksseitenlage'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Flache Lagerung'
      },
    ],
  },
  {
    id: 2387679,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem offenen Pneumothorax durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Keimfreie Wundversorgung'
      },
      {
        valid: true,
        content: 'Anregung zu tiefer, ruhiger Atmung'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Erhöhter Oberkörper, wenn erträglich auf die verletzte Seite'
      },
    ],
  },
  {
    id: 2388962,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einem Patienten mit Verdacht auf Schlaganfall durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Prophylaktische Seitenlage mit leicht erhöhtem Oberkörper (ca. 30°) auf die versorgungstechnisch günstigere Seite'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2388963,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer allergischen Reaktion nach einem Insektenstich durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Kälteanwendung (Eisstück lutschen, kalte Umschläge)'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Beruhigung, Sauerstoffinhalation (10-15 Liter/min)'
      },
      {
        content: 'Lagerung mit erhöhten Beinen zur Schockprophylaxe'
      },
    ],
  },
  {
    id: 2387680,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Augenverätzung durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Die großen Verunreinigungen vorsichtig entfernen (mit keimfreier Wundauflage), Spülung für mindestens 10-15 Minuten'
      },
      {
        content: 'Bindehautsack desinfizieren (Schleimhautdesinfektionsmittel)'
      },
      {
        valid: true,
        content: 'Bindehautsack inspizieren und noch vorhandene Kalk- bzw. Mörtelreste vorsichtig entfernen (mit keimfreier Wundauflage)'
      },
      {
        valid: true,
        content: 'beide Augen keimfrei verbinden, allgemeine Maßnahmen, primäre Versorgung in einer Spezialklinik'
      },
    ],
  },
  {
    id: 2387681,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Eileiterschwangerschaft durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        valid: true,
        content: 'Voranmeldung im Krankenhaus'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Sauerstoffgabe wegen möglicher Eklampsie'
      },
    ],
  },
  {
    id: 2387682,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Eklampsie durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Linksseitenlage mit leicht erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Nach dem Krampfanfall: Lebensfunktionen überprüfen und entsprechende Maßnahmen'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Rascher Transport mit Sondersignal'
      },
    ],
  },
  {
    id: 2387683,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Epiglottitis durchzuführen?',
    choices: [
      {
        content: 'Manipulation im Mund- und Rachenbereich'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Kühle und feuchte Umgebungsluft'
      },
      {
        valid: true,
        content: 'Kind bei einer Bezugsperson lassen'
      },
    ],
  },
  {
    id: 2387684,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Fehlgeburt (Abortus) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        valid: true,
        content: 'Abgegangene Gewebsteile ins Krankenhaus mitnehmen'
      },
      {
        valid: true,
        content: 'Beine hoch lagern'
      },
      {
        content: 'Eine Wehe anreiben'
      },
    ],
  },
  {
    id: 2388964,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Hyperglykämie durchzuführen (Patient bei Bewusstsein)',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        content: 'Verabreichung von Zucker (Zuckerwasser, Traubenzucker, ...)'
      },
      {
        content: 'Insulin'
      },
    ],
  },
  {
    id: 2388965,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Lungenembolie durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Keine körperliche Anstrengung'
      },
      {
        valid: true,
        content: 'Sauerstoffgabe von 10-15 Liter/min.'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2387685,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Placenta Praevia durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Mutter-Kind-Pass mitnehmen'
      },
      {
        content: 'Wehe anreiben'
      },
    ],
  },
  {
    id: 2388966,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Shunt-Blutung durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Hochhalten des betroffenen Armes'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Abbindung'
      },
      {
        valid: true,
        content: 'Druckverband mit dickem Saugpolster'
      },
    ],
  },
  {
    id: 2388967,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei einer Venenthrombose durchzuführen?',
    choices: [
      {
        content: 'Tief und weich lagern'
      },
      {
        valid: true,
        content: 'Absolutes Bewegungsverbot'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Hohe und weiche Lagerung des betroffenen Körperteils'
      },
    ],
  },
  {
    id: 2388968,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei entzündlichen Unterleibserkrankungen durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit Knierolle'
      },
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        content: 'Dezentralisierung des Kreislaufs, allgemeine Maßnahmen verhindern diese Symptomatik'
      },
    ],
  },
  {
    id: 2387686,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Hitzeschäden (Hitzschlag, Hitzeerschöpfung, Sonnenstich) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Den Patienten in den Schatten bringen, Kleidung möglichst entfernen'
      },
      {
        valid: true,
        content: 'Den Patienten kühlen (kalte, feuchte Tücher auf Kopf und Oberkörper legen)'
      },
      {
        valid: true,
        content: 'Flüssigkeit verabreichen, Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
    ],
  },
  {
    id: 2388969,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Hyperventilations-Tetanie durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Beruhigung'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper, beengende Kleidung öffnen'
      },
      {
        content: 'Sauerstoffgabe 6-8 Liter/min.'
      },
      {
        valid: true,
        content: 'Keine Sauerstoffgabe'
      },
    ],
  },
  {
    id: 2388970,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Links- bzw. Rechtsherzinsuffizienz durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Beruhigung'
      },
      {
        content: 'Absaugbereitschaft'
      },
      {
        valid: true,
        content: 'Hochlagerung des Oberkörpers, Tieflagerung der Beine'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
    ],
  },
  {
    id: 2387687,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Unfällen mit Kohlendioxid durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Rettung nur durch die Feuerwehr'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        content: 'Sauerstoffgabe 6-8 Liter/min.'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
    ],
  },
  {
    id: 2387688,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind bei Verletzungen der Unterleibsorgane durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Blutstillung'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        content: 'Bei Pfählungen den Gegenstand vorsichtig entfernen'
      },
    ],
  },
  {
    id: 2387689,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind beim Auftreten der Dekompressionskrankheit durchzuführen (Taucher bei Bewusstsein)?',
    choices: [
      {
        content: 'Patienten aufsetzen, sofort Notarzt rufen, keine weiteren Maßnahmen vor Eintreffen des Notarztes'
      },
      {
        valid: true,
        content: 'Tauchanzug öffnen, flache Lagerung auf weicher Unterlage'
      },
      {
        valid: true,
        content: 'Sauerstoffinhalation (10-15 l/min) und Anleitung zu tiefer, ruhiger Atmung'
      },
      {
        valid: true,
        content: 'Zwischendurch reichlich Wasser zum Trinken anbieten, Wärmeerhaltung'
      },
    ],
  },
  {
    id: 2387690,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind beim Krankheitsbild "Pseudokrupp" durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Kind beruhigen'
      },
      {
        valid: true,
        content: 'Beengende Kleidungsstücke öffnen'
      },
      {
        valid: true,
        content: 'Oberkörper-Hochlagerung'
      },
      {
        content: 'Sofort 10-15 l/min Sauerstoff verabreichen'
      },
    ],
  },
  {
    id: 2387691,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind beim Vena-cava-Kompressionssyndrom durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Linksseitenlage'
      },
      {
        content: 'Rechtsseitenlage'
      },
      {
        content: 'Lagerung nach Fritsch'
      },
    ],
  },
  {
    id: 2387692,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind durchzuführen, wenn ein Patient Waschmittel zu sich genommen hat?',
    choices: [
      {
        valid: true,
        content: 'Mund reinigen (ausspülen)'
      },
      {
        valid: true,
        content: 'Falls Verätzungsmittel bekannt: VIZ kontaktieren'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: '1 Liter Wasser zu trinken geben (Verdünnungeffekt)'
      },
    ],
  },
  {
    id: 2387693,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind durchzuführen, wenn es sich offensichtlich um einen SIDS-Notfall handelt?',
    choices: [
      {
        valid: true,
        content: 'Lebensfunktionen kontrollieren, Notfalldiagnose'
      },
      {
        valid: true,
        content: 'Lebensrettende Maßnahmen durchführen, Notarztindikation'
      },
      {
        content: 'Die anwesenden Eltern durch den Sanitäter betreuen'
      },
      {
        content: 'Sofort die Polizei verständigen'
      },
    ],
  },
  {
    id: 2388971,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen sind im Falle einer Hochdruckkrise durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Beengende Kleidungsstücke öffnen'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Defibrillationsbereitschaft'
      },
    ],
  },
  {
    id: 2387694,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen werden bei Blutungen in der Nachgeburtsperiode durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Anreiben einer Wehe'
      },
      {
        valid: true,
        content: 'Lagerung nach Fritsch'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Vorlage gegen die Scheide pressen'
      },
    ],
  },
  {
    id: 2387695,
    question: 'Welche der angeführten Sanitätshilfe-Maßnahmen werden bei der Notfalldiagnose Bewusstlosigkeit beim Säugling (normale Atmung und Herzfrequenz über 60) durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Säugling in Bauch-Seitenlage bringen'
      },
      {
        valid: true,
        content: 'Atemkontrolle alle 2 Minuten'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'weiter mit C, D, E und SAMPLE'
      },
    ],
  },
  {
    id: 2387696,
    question: 'Welche der angeführten Situationen (z.B. Verletzungen) können eine Tetanusinfektion verursachen?',
    choices: [
      {
        valid: true,
        content: 'Wunden, die durch rostige Nägel oder Holzspäne verursacht wurden'
      },
      {
        content: 'Nur bei großer Verletzung ist eine Infektion möglich'
      },
      {
        content: 'Der Tetanuserreger stellt für Kinder bis zum 8. Lebensjahr keine Gefahr dar'
      },
      {
        valid: true,
        content: 'Wunden, die durch Erde oder Mist verschmutzt wurden'
      },
    ],
  },
  {
    id: 2388194,
    question: 'Welche der folgenden Situationen weisen auf die Lagerungsart "Flache Rückenlagerung, Beine hoch gelagert" in der Sanitätshilfe hin?',
    choices: [
      {
        valid: true,
        content: 'Volumenmangel ohne Kopf-, Thorax-, Becken-, Wirbelsäulen-, Beinverletzungen oder Atemnot'
      },
      {
        content: 'Kardiogenem Schock'
      },
      {
        content: 'Schädel-Hirn-Traumen'
      },
      {
        content: 'Volumenmangelschock durch Thoraxverletzung'
      },
    ],
  },
  {
    id: 2387697,
    question: 'Welche der unten angeführten Kennzeichen können bei der Notfalldiagnose Bewusstlosigkeit beim Säugling auftreten?',
    choices: [
      {
        valid: true,
        content: 'Kein Bewusstsein'
      },
      {
        valid: true,
        content: 'Normale Atmung und Kreislauf vorhanden (Herzfrequenz > 60/min)'
      },
      {
        content: 'Keine Atmung, jedoch Kreislauf vorhanden (Herzfrequenz > 60/min)'
      },
      {
        content: 'Keine Atmung und kein Kreislauf vorhanden'
      },
    ],
  },
  {
    id: 2387698,
    question: 'Welche der unten angeführten Symptome können bei einem Spannungspneumothorax auftreten?',
    choices: [
      {
        valid: true,
        content: 'Venenstauung vor allem im Halsbereich'
      },
      {
        valid: true,
        content: 'Blaufärbung der Haut (Zyanose)'
      },
      {
        valid: true,
        content: 'Austritt von schaumigem Blut aus der Wunde'
      },
      {
        content: 'Blutdruckanstieg über 200 mm HG'
      },
    ],
  },
  {
    id: 2388932,
    question: 'Welche dieser Tätigkeiten sind vor der Ausübung von Sport sinnvoll?',
    choices: [
      {
        valid: true,
        content: 'Aufwärmen'
      },
      {
        valid: true,
        content: 'Schutzausrüstung anlegen'
      },
      {
        content: 'Kühlende Salben auf schmerzende Gelenke schmieren'
      },
      {
        content: 'Eisspray auf Gelenke sprühen'
      },
    ],
  },
  {
    id: 2387699,
    question: 'Welche Eigenschaften hat Kohlendioxid?',
    choices: [
      {
        valid: true,
        content: 'Nicht brennbar'
      },
      {
        valid: true,
        content: 'Farbloses Gas'
      },
      {
        valid: true,
        content: 'Schwach säuerlich schmeckendes Gas'
      },
      {
        valid: true,
        content: 'Schwerer als Luft'
      },
    ],
  },
  {
    id: 2387700,
    question: 'Welche Eigenschaften hat Kohlenmonoxid?',
    choices: [
      {
        valid: true,
        content: 'Farbloses, geruchloses, geschmackloses Gas'
      },
      {
        valid: true,
        content: 'Kohlenmonoxid bindet sich viel besser an die roten Blutkörperchen und verdrängt den Sauerstoff aus der Bindung'
      },
      {
        content: 'Nicht brennbares, im Prinzip ungiftiges Gas'
      },
      {
        content: 'Farbloses, schwach säuerlich riechendes Gas'
      },
    ],
  },
  {
    id: 2388195,
    question: 'Welche Eigenschaften hat Sauerstoff?',
    choices: [
      {
        valid: true,
        content: 'Farblos'
      },
      {
        valid: true,
        content: 'Ist geruchlos'
      },
      {
        content: 'Ist giftig'
      },
      {
        valid: true,
        content: 'Fördert die Verbrennung'
      },
    ],
  },
  {
    id: 2389638,
    question: 'Welche Eigenschaften sollte ein Druckkörper haben?',
    choices: [
      {
        valid: true,
        content: 'weich und anpassungsfähig'
      },
      {
        valid: true,
        content: 'saugfähig'
      },
      {
        content: 'kleiner als die Wunde'
      },
      {
        valid: true,
        content: 'größer als die Wunde'
      },
    ],
  },
  {
    id: 2388718,
    question: 'Welche Eintrittspforten für Krankheitserreger gibt es?',
    choices: [
      {
        content: 'Nur über Mund und Nase möglich'
      },
      {
        valid: true,
        content: 'Über die Augenbindehaut und Hautwunden'
      },
      {
        content: 'Nur über großflächige, tiefe Hautwunden und Schleimhäute möglich'
      },
      {
        valid: true,
        content: 'Schleimhäute des Verdauungs-, Atmungs-, Harn- und Geschlechtsorgantraktes'
      },
    ],
  },
  {
    id: 2388331,
    question: 'Welche einzelnen Schritte zählen zum Freimachen der Atemwege (obere Luftwege)?',
    choices: [
      {
        content: 'Beine hoch lagern, dadurch wird die Atemhilfsmuskulatur aktiviert'
      },
      {
        content: 'Verletzten auf den Bauch drehen und die Fremdkörper "herausschütteln"'
      },
      {
        valid: true,
        content: 'Bei offensichtlichen Verlegungen wird der Mund ausgeräumt'
      },
      {
        valid: true,
        content: 'Überstrecken des Kopfes nackenwärts'
      },
    ],
  },
  {
    id: 2389150,
    question: 'Welche Elemente umfasst der Sanitätseinsatz im Katastrophenfall?',
    choices: [
      {
        valid: true,
        content: 'Suchen nach Vermissten'
      },
      {
        valid: true,
        content: 'Errichten und Betreiben einer SanHist'
      },
      {
        valid: true,
        content: 'Transport von Betroffenen'
      },
      {
        valid: true,
        content: 'Errichten und Betreiben von Sanitätseinrichtungen (ambulant und stationär)'
      },
    ],
  },
  {
    id: 2387701,
    question: 'Welche Erkennungsmerkmale sprechen für eine Vergiftung?',
    choices: [
      {
        valid: true,
        content: 'Bewusstseinsstörung'
      },
      {
        valid: true,
        content: 'Hinweis auf Umstände eines Unglücks (Regloser in Gärkeller)'
      },
      {
        valid: true,
        content: 'Hautveränderung,  Atem-/Kreislaufstörung'
      },
      {
        valid: true,
        content: 'Erregungs-/Rauschzustände'
      },
    ],
  },
  {
    id: 2387702,
    question: 'Welche Erkennungsmerkmale zeichnen eine Verbrennung 3. Grades aus?',
    choices: [
      {
        content: 'Blaurot, eitriger Trennspalt zwischen abgestorbenem und gesundem Gewebe'
      },
      {
        valid: true,
        content: 'Schorfbildung, Verkohlung'
      },
      {
        content: 'Rötung, Schwellung, immer mit starken Schmerzen'
      },
      {
        content: 'Schockanzeichen, Schüttelfrost, blau-rote Verfärbung der Wunde'
      },
    ],
  },
  {
    id: 2387703,
    question: 'Welche Erkennungszeichen treten bei einer Verätzung im Bereich des Verdauungstraktes auf?',
    choices: [
      {
        valid: true,
        content: 'Schleimhäute durch Beläge oder durch Blutung verändert'
      },
      {
        valid: true,
        content: 'Heftige Schmerzen'
      },
      {
        content: 'Kopfschmerzen'
      },
      {
        valid: true,
        content: 'Vermehrter Speichelfluss infolge Schluckstörung'
      },
    ],
  },
  {
    id: 2387704,
    question: 'Welche Erkrankungen, die zu Abweichungen vom psychischen Normalverhalten führen, gibt es?',
    choices: [
      {
        valid: true,
        content: 'Stoffwechselentgleisungen'
      },
      {
        valid: true,
        content: 'Infektionen, Vergiftungen'
      },
      {
        valid: true,
        content: 'Schizophrenie'
      },
      {
        valid: true,
        content: 'Depression, Manie'
      },
    ],
  },
  {
    id: 2388861,
    question: 'Welche Erste-Hilfe-Ausrüstung kann den Helfer vor Infektionen schützen?',
    choices: [
      {
        valid: true,
        content: 'Beatmungstuch'
      },
      {
        valid: true,
        content: 'Einmalhandschuhe'
      },
      {
        content: 'Taschentuch'
      },
      {
        content: 'Warnblinkanlage'
      },
    ],
  },
  {
    id: 2388693,
    question: 'Welche Erste-Hilfe-Maßnahmen (Notfallcheck) sind bei einem reglosen Menschen durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Laut ansprechen und sanft schütteln'
      }, 
      {
        content: 'Es ist nur der Notruf abzusetzen'
      }, 
      {
        content: 'Erste-Hilfe-Maßnahmen dürfen nur vom Notarzt duchgeführt werden'
      }, 
      {
        valid: true,
        content: 'Atemwege freimachen und Atmung kontrollieren'
      }, 
    ],
  },
  {
    id: 2389599,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei einem Insektenstich durch?',
    choices: [
      {
        valid: true,
        content: 'kalte Umschläge auflegen'
      },
      {
        content: 'Beine hochlagern'
      },
      {
        content: 'einfach weiterspielen'
      },
      {
        valid: true,
        content: 'Bequemes Sitzen im Schatten'
      },
    ],
  },
  {
    id: 2389611,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei einer Schürfwunde durch?',
    choices: [
      {
        valid: true,
        content: 'Wunde mit handwarmen Wasser ausspülen'
      },
      {
        valid: true,
        content: 'Wunde keimfrei verbinden'
      },
      {
        content: 'Wunde mit Puder versorgen'
      },
      {
        content: 'auf jeden Fall zum Arzt gehen'
      },
    ],
  },
  {
    id: 2389614,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei einer schwach blutenden Schnittwunde durch?',
    choices: [
      {
        valid: true,
        content: 'Pflasterverband'
      },
      {
        content: 'fest draufdrücken'
      },
      {
        valid: true,
        content: 'keimfreier Wundverband'
      },
      {
        valid: true,
        content: 'den Verletzten beruhigen'
      },
    ],
  },
  {
    id: 2389624,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei einer Verätzung durch?',
    choices: [
      {
        valid: true,
        content: 'Selbstschutz beachten'
      },
      {
        valid: true,
        content: 'mit reinem handwarmen Wasser ausspülen'
      },
      {
        content: 'Wunde offen belassen'
      },
      {
        valid: true,
        content: 'metallisierten, keimfreien Verband anlegen'
      },
    ],
  },
  {
    id: 2389603,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei einer Verstauchung durch?',
    choices: [
      {
        content: 'den betroffenen Körperteil viel bewegen'
      },
      {
        valid: true,
        content: 'den betroffenen Körperteil hochlagern'
      },
      {
        valid: true,
        content: 'den betroffenen Körperteil kühlen'
      },
      {
        valid: true,
        content: 'beengende Kleidungsstücke öffnen'
      },
    ],
  },
  {
    id: 2389605,
    question: 'Welche Erste-Hilfe-Maßnahmen führst du bei Nasenbluten durch?',
    choices: [
      {
        content: 'Kopf in den Nacken legen'
      },
      {
        valid: true,
        content: 'Nasenlöcher zusammen drücken'
      },
      {
        content: 'Nase putzen lassen'
      },
      {
        valid: true,
        content: 'kalte Umschläge in den Nacken legen'
      },
    ],
  },
  {
    id: 2389626,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei einem Tierbiss?',
    choices: [
      {
        valid: true,
        content: 'keimfreien Verband anlegen'
      },
      {
        content: 'Bissstelle aussaugen'
      },
      {
        valid: true,
        content: 'Wunde von einem Arzt versorgen lassen'
      },
      {
        valid: true,
        content: 'Verletzten beruhigen'
      },
    ],
  },
  {
    id: 2389632,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei einer Erfrierung?',
    choices: [
      {
        valid: true,
        content: 'warme, gezuckerte Getränke verabreichen'
      },
      {
        content: 'Betroffenen bewegen lassen, um ihn aufzuwärmen'
      },
      {
        content: 'Alkohol zu trinken geben'
      },
      {
        valid: true,
        content: 'Körper durch zusätzliche Kleidung oder Decken erwärmen'
      },
    ],
  },
  {
    id: 2389635,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei einer starken Blutung?',
    choices: [
      {
        valid: true,
        content: 'Selbstschutz beachten (Einmalhandschuhe anziehen)'
      },
      {
        content: 'Wunde mit reinem, handwarmen Wasser ausspülen'
      },
      {
        valid: true,
        content: 'den verletzten Körperteil hochhalten'
      },
      {
        valid: true,
        content: 'mit einer keimfreien Wundauflage direkt auf die stark blutende Wunde drücken'
      },
    ],
  },
  {
    id: 2389607,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei einer Verbrennung?',
    choices: [
      {
        valid: true,
        content: 'Notruf'
      },
      {
        content: 'Puder auf die Verbrennung geben'
      },
      {
        valid: true,
        content: 'betroffenen Körperteil mit handwarmen Wasser kühlen'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2389647,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei einer Verbrennung?',
    choices: [
      {
        valid: true,
        content: 'betroffenen Körperteil mit reinem, handwarmen Wasser, max. 10 Minuten, kühlen oder bis der/die Verletzte fröstelt'
      },
      {
        valid: true,
        content: 'Notruf'
      },
      {
        valid: true,
        content: 'Wunde mit metallisierter, keimfreier Wundauflage abdecken'
      },
      {
        content: 'betroffenen Körperteil mit kaltem Wasser kühlen, bis die Rettung kommt'
      },
    ],
  },
  {
    id: 2389629,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du bei Verdacht auf einen Knochenbruch?',
    choices: [
      {
        valid: true,
        content: 'Notruf'
      },
      {
        valid: true,
        content: 'betroffenen Körperteil ruhigstellen und nicht bewegen'
      },
      {
        content: 'eine Schiene anlegen'
      },
      {
        content: 'den Verletzten Wasser zu trinken geben'
      },
    ],
  },
  {
    id: 2389641,
    question: 'Welche Erste-Hilfe-Maßnahmen leistest du, wenn eine Person reglos am Boden liegt?',
    choices: [
      {
        valid: true,
        content: 'Bewusstseinskontrolle'
      },
      {
        content: 'in vorgefundener Lage liegen lassen'
      },
      {
        valid: true,
        content: 'Notruf'
      },
      {
        valid: true,
        content: 'stabile Seitenlage'
      },
    ],
  },
  {
    id: 2388694,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei Atem-Kreislauf-Stillstand durchzuführen?',
    choices: [
      {
        content: 'Zweimalige Beatmung und danach den Notruf absetzen'
      }, 
      {
        valid: true,
        content: 'Einen Defibrillator holen lassen'
      }, 
      {
        content: 'Notruf absetzen und Beatmungen durchführen'
      }, 
      {
        valid: true,
        content: 'Notruf absetzen und sofort  Herzdruckmassagen und Beatmungen (30:2) durchführen'
      },
    ],
  },
  {
    id: 2388602,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei einer starken Blutung am Unterschenkel durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Verletzten hinlegen'
      }, 
      {
        content: 'Verletzten stehen lassen'
      }, 
      {
        content: 'Der Ersthelfer soll nur den Notruf absetzen'
      }, 
      {
        valid: true,
        content: 'Beine hoch lagern und Fingerdruck durchführen/Druckverband anlegen'
      }, 
    ],
  },
  {
    id: 2388667,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei einer Verätzung und Kontamination der Haut mit organischen Lösungsmitteln zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Die betreffende Hautstelle mit handwarmem Wasser spülen (falls vorhanden mit Seifenwasser)'
      },
      {
        content: 'Das Eintreffen des Roten Kreuzes (Rettung) abwarten, keine Erste Hilfe möglich'
      },
      {
        valid: true,
        content: 'Keimfreien Verband anlegen (metallisiert, wenn vorhanden)'
      },
      {
        valid: true,
        content: 'Notruf, Basismaßnahmen'
      },
    ],
  },
  {
    id: 2388586,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei Unterkühlungsverdacht zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Notruf, Basismaßnahmen'
      },
      {
        content: 'Warme alkoholische Getränke verabreichen, gleichzeitig frottieren'
      },
      {
        valid: true,
        content: 'Warme gezuckerte Getränke verabreichen, Bewegungen vermeiden, gut zudecken'
      },
      {
        valid: true,
        content: 'Durch Erwärmen des Körperkerns das weitere Absinken der Temperatur stoppen'
      },
    ],
  },
  {
    id: 2388668,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei Verätzungen zu setzen?',
    choices: [
      {
        content: 'Kleider nicht entfernen, um die bereits geschädigte Haut nicht weiter zu verletzen'
      },
      {
        valid: true,
        content: 'Keimfreien Verband anlegen, Notruf und Basismaßnahmen'
      },
      {
        valid: true,
        content: 'Sofort mit reinem handwarmen Wasser intensiv spülen, darauf achten, dass das abfließende Wasser den kürzesten Weg über die Haut nimmt'
      },
      {
        valid: true,
        content: 'Sofort die mit ätzender Substanz getränkten Kleider entfernen'
      },
    ],
  },
  {
    id: 2388669,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei Verdacht auf Erfrierung zu setzen (Patient ist ansprechbar)?',
    choices: [
      {
        valid: true,
        content: 'Keimfreien Verband anlegen, Zudecken hat große Bedeutung'
      },
      {
        valid: true,
        content: 'Notruf, Basismaßnahmen'
      },
      {
        valid: true,
        content: 'Beengende Bekleidung öffnen und warme, gezuckerte Getränke verabreichen'
      },
      {
        content: 'Alkohol zu trinken geben, dadurch werden die Gefäße erweitert und wärmendes Blut versorgt den Körperkern'
      },
    ],
  },
  {
    id: 2388587,
    question: 'Welche Erste-Hilfe-Maßnahmen sind bei zu niedrigem Blutzucker zu setzen?',
    choices: [
      {
        valid: true,
        content: 'Ist der Betroffene ansprechbar, darf Zuckerhältiges verabreicht werden'
      },
      {
        content: 'Wasser oder Bier verabreichen'
      },
      {
        valid: true,
        content: 'Basismaßnahmen durchführen'
      },
      {
        valid: true,
        content: 'Notruf absetzen'
      },
    ],
  },
  {
    id: 2388603,
    question: 'Welche Erste-Hilfe-Maßnahmen sind beim Anlegen eines Druckverbandes durchzuführen?',
    choices: [
      {
        content: 'Schere bereitlegen'
      }, 
      {
        valid: true,
        content: 'Keimfreie Wundauflage fest auf die Wunde drücken'
      }, 
      {
        valid: true,
        content: 'Druck durch festes Umwickeln mit der Mullbinde erzeugen'
      }, 
      {
        valid: true,
        content: 'Verletzter soll selbst auf die Wunde drücken'
      }, 
    ],
  },
  {
    id: 2388695,
    question: 'Welche Erste-Hilfe-Maßnahmen sind durchzuführen, sobald der Notfallpatient in die stabile Seitenlage gebracht wurde?',
    choices: [
      {
        content: 'Alle 15 Minuten die Atmung kontrollieren'
      }, 
      {
        valid: true,
        content: 'Basismaßnahmen durchführen'
      }, 
      {
        valid: true,
        content: 'spätestens jetzt Notruf wählen, regelmäßige Atemkontrollen'
      }, 
      {
        content: 'Sofort mit Herzdruckmassage und Beatmung beginnen'
      }, 
    ],
  },
  {
    id: 2388588,
    question: 'Welche Erste-Hilfe-Maßnahmen sind im Vergiftungsnotfall durchzuführen (Patient bei Bewusstsein)?',
    choices: [
      {
        valid: true,
        content: 'Vergiftungsinformationszentrale kontaktieren und Anweisungen durchführen'
      },
      {
        content: 'Sofort schluckweise Wasser zum Trinken geben'
      },
      {
        content: 'Zum Erbrechen bringen'
      },
      {
        valid: true,
        content: 'Seitenlage durchführen'
      },
    ],
  },
  {
    id: 2388589,
    question: 'Welche Erste-Hilfe-Maßnahmen sind im Vergiftungsnotfall durchzuführen (Patient nicht ansprechbar)?',
    choices: [
      {
        content: 'Giftentfernung durch provoziertes Erbrechen'
      },
      {
        valid: true,
        content: 'Kontrolle der Lebensfunktionen, Notfalldiagnose stellen'
      },
      {
        content: 'bei nicht vorhandener Atmung Verzicht auf Herzdruckmassage'
      },
      {
        content: 'Patienten in der vorgefundenen Lage belassen'
      },
    ],
  },
  {
    id: 2388590,
    question: 'Welche Erste-Hilfe-Maßnahmen sind zu setzen, wenn der Abtransport eines Unterkühlten in absehbarer Zeit nicht möglich ist (z.B. Gebirge)?',
    choices: [
      {
        valid: true,
        content: 'Den Unterkühlten, wenn möglich, an einen windstillen Ort oder in einen warmen Raum (Schutzhütte) bringen'
      },
      {
        valid: true,
        content: 'Warme gezuckerte Getränke verabreichen'
      },
      {
        valid: true,
        content: 'Kalte, nasse Kleidung vorsichtig entfernen - in angewärmte Decken hüllen und eventuelle Erfrierungen keimfrei verbinden'
      },
      {
        valid: true,
        content: 'Warme, trockene Umschläge auf Brust, Bauch und Nacken legen'
      },
    ],
  },
  {
    id: 2388972,
    question: 'Welche Faktoren können einen Asthma-Bronchiale-Anfall auslösen?',
    choices: [
      {
        valid: true,
        content: 'Psychische Belastung'
      },
      {
        content: 'Linksherzversagen'
      },
      {
        content: 'Flüssigkeitsmangel'
      },
      {
        valid: true,
        content: 'Allergische Reaktionen'
      },
    ],
  },
  {
    id: 2387705,
    question: 'Welche Faktoren/Überlebenschancen sind bei einem Ertrinkungsunfall maßgeblich?',
    choices: [
      {
        valid: true,
        content: 'Alter und Gesundheitszustand vor dem Unfall, Erwachsener oder Kind betroffen'
      },
      {
        content: 'Geschlechtszugehörigkeit, Körperbau'
      },
      {
        valid: true,
        content: 'Zeitdauer (wie lange war der Mensch unter Wasser)'
      },
      {
        valid: true,
        content: 'Aspiration, Stimmritzenkrampf ja/nein'
      },
    ],
  },
  {
    id: 2389151,
    question: 'Welche Farben haben die Warntafeln bei Gefahrenguttransporten?',
    choices: [
      {
        content: 'Blau'
      },
      {
        valid: true,
        content: 'Orange mit schwarzer Umrandung'
      },
      {
        content: 'Rot'
      },
      {
        content: 'Gelb'
      },
    ],
  },
  {
    id: 2388196,
    question: 'Welche Fehlerquellen bei der Blutdruckmessung gibt es?',
    choices: [
      {
        valid: true,
        content: 'Zu rasches Ablassen des Manschettendruckes'
      },
      {
        content: 'Zwei aufeinanderfolgende Messungen am selben Arm nach entsprechend langer Pause'
      },
      {
        valid: true,
        content: 'Wenn die Manschette nicht vollständig entleert wird, bleibt ein Restdruck bestehen'
      },
      {
        valid: true,
        content: 'Verwendung eines kaputten Gerätes'
      },
    ],
  },
  {
    id: 2388332,
    question: 'Welche Fehlerquellen gibt es bei der Herzdruckmassage?',
    choices: [
      {
        valid: true,
        content: 'Fehlende harte Unterlage'
      },
      {
        content: 'Gefahr von Rippenbrüchen'
      },
      {
        content: 'Man kann dabei nichts falsch machen'
      },
      {
        valid: true,
        content: 'Druckpunkt zu hoch oder zu tief: Brustbeinschädigung, Organverletzungen'
      },
    ],
  },
  {
    id: 2388333,
    question: 'Welche Folgen eines unversorgten Schockgeschehens sind möglich?',
    choices: [
      {
        valid: true,
        content: 'Bildung gefährlicher Stoffwechselprodukte (Acidose)'
      },
      {
        content: 'Eine psychische Störung oder Erkrankung, keine wesentlichen Folgen'
      },
      {
        valid: true,
        content: 'Schädigung vieler Organe (Multiorganversagen) bis zum Tod des Patienten'
      },
      {
        valid: true,
        content: 'Der Sauerstoffmangel führt zur Entgleisung des Stoffwechsels'
      },
    ],
  },
  {
    id: 2388197,
    question: 'Welche Folgen kann die Beatmung bei nicht intubierten Patienten haben?',
    choices: [
      {
        valid: true,
        content: 'Erbrechen'
      },
      {
        valid: true,
        content: 'Aspirationsgefahr'
      },
      {
        content: 'Verdauungsstörungen'
      },
      {
        content: 'Darmblähungen'
      },
    ],
  },
  {
    id: 2389606,
    question: 'Welche Folgen kann eine Verbrennung haben?',
    choices: [
      {
        valid: true,
        content: 'Kreislaufversagen'
      },
      {
        content: 'Erfrierung'
      },
      {
        valid: true,
        content: 'Verbrennungskrankheit'
      },
      {
        valid: true,
        content: 'Entzündung der Wunde'
      },
    ],
  },
  {
    id: 2388198,
    question: 'Welche Formel gibt es zur Berechnung der Abgabedauer von Sauerstoff?',
    choices: [
      {
        content: 'Rauminhalt der Flasche durch Abgabemenge in Liter/min'
      },
      {
        content: 'Inhaltsdruck der Flasche durch Abgabemenge pro Minute'
      },
      {
        valid: true,
        content: 'Rauminhalt der Flasche x Inhaltsdruck durch die Abgabemenge/min = Abgabedauer [min]'
      },
      {
        content: 'Inhaltsdruck der Flasche x Abgabemenge pro Minute'
      },
    ],
  },
  {
    id: 2388435,
    question: 'Welche Formen der Hilfeleistung unterscheidet man im Rettungswesen?',
    choices: [
      {
        content: 'Primäreinsatz'
      },
      {
        content: 'Sekundäreinsatz'
      },
      {
        valid: true,
        content: '(Notfall-)Rettungsdienst'
      },
      {
        valid: true,
        content: 'Krankentransportdienst'
      },
    ],
  },
  {
    id: 2388334,
    question: 'Welche Formen des Atem-Kreislauf-Stillstandes gibt es?',
    choices: [
      {
        valid: true,
        content: 'Asystolie'
      },
      {
        valid: true,
        content: 'Pulslose elektrische Aktivität (PEA)'
      },
      {
        content: 'Kammertachycardie'
      },
      {
        valid: true,
        content: 'Kammerflimmern bzw. Kammerflattern'
      },
    ],
  },
  {
    id: 2387706,
    question: 'Welche Fragen stellt der Sanitäter an die Gebärende?',
    choices: [
      {
        valid: true,
        content: 'Anzahl der bisherigen Geburten und Zeitabstände der Wehen'
      },
      {
        valid: true,
        content: 'Ob der Blasensprung erfolgt ist'
      },
      {
        valid: true,
        content: 'Mutter-Kind-Pass'
      },
      {
        content: 'Voraussichtliches Geschlecht des Kindes'
      },
    ],
  },
  {
    id: 2389152,
    question: 'Welche Führungsgrundsätze definiert die SKKM-Richtlinie im Katastropheneinsatz?',
    choices: [
      {
        valid: true,
        content: 'Reservebildung'
      },
      {
        valid: true,
        content: 'Schwergewichtsbildung'
      },
      {
        valid: true,
        content: 'Beweglichkeit'
      },
      {
        content: 'Handlungseinschränkung'
      },
    ],
  },
  {
    id: 2389250,
    question: 'Welche Funktion haben die Knochen im menschlichen Körper?',
    choices: [
      {
        valid: true,
        content: 'Die Knochen bilden das Skelett'
      },
      {
        valid: true,
        content: 'Die Knochen schützen die inneren Organe'
      },
      {
        valid: true,
        content: 'Die Knochen dienen den Muskeln als Ansatz'
      },
      {
        valid: true,
        content: 'Die Knochen sind Bildungsstätte von Blutkörperchen (rote und weiße)'
      },
    ],
  },
  {
    id: 2389251,
    question: 'Welche Funktion hat der Kreislauf?',
    choices: [
      {
        valid: true,
        content: 'Eine Transportfunktion für Stoffe (z.B. Sauerstoff, Nährstoffe, Hormone)'
      },
      {
        content: 'Der Kreislauf dient ausschließlich zur Aufrechterhaltung der Atmung'
      },
      {
        content: 'Der Kreislauf dient zur Erhaltung der Körperwärme'
      },
      {
        content: 'Verteilung der aufbereiteten Nahrung'
      },
    ],
  },
  {
    id: 2389252,
    question: 'Welche Funktion hat der Mutterkuchen?',
    choices: [
      {
        valid: true,
        content: 'Der Mutterkuchen dient der Stoffwechselausscheidung des Kindes'
      },
      {
        valid: true,
        content: 'Der Mutterkuchen dient der Ernährung des Kindes'
      },
      {
        valid: true,
        content: 'Der Mutterkuchen dient der Sauerstoffversorgung des Kindes'
      },
      {
        valid: true,
        content: 'Der Mutterkuchen dient der Hormonproduktion zur Erhaltung der Schwangerschaft'
      },
    ],
  },
  {
    id: 2389253,
    question: 'Welche Funktion hat der Stoffwechsel?',
    choices: [
      {
        content: 'Der Stoffwechsel ist der Sauerstoffaustausch zwischen Körper und Umgebungsluft.'
      },
      {
        valid: true,
        content: 'Verwertung von Nahrungsbausteinen'
      },
      {
        valid: true,
        content: 'Energiebereitstellung'
      },
      {
        content: 'Die natürliche Zu- und Abfuhr von Nahrungsmitteln'
      },
    ],
  },
  {
    id: 2388335,
    question: 'Welche Funktionen fehlen bei einem bewusstlosen Patienten?',
    choices: [
      {
        content: 'Speichelproduktion'
      },
      {
        valid: true,
        content: 'Bewusstsein und Schutzreflexe sind erloschen'
      },
      {
        content: 'Atmung'
      },
      {
        content: 'Kreislauf'
      },
    ],
  },
  {
    id: 2389254,
    question: 'Welche Funktionen hat die Haut?',
    choices: [
      {
        content: 'Die Haut dient der Blutbildung'
      },
      {
        valid: true,
        content: 'Schutz des Körpers vor Umwelteinflüssen und Gefahren'
      },
      {
        content: 'Regulation des Säure-Basen-Haushaltes'
      },
      {
        content: 'Die Haut unterstützt die Lunge beim Gasaustausch'
      },
    ],
  },
  {
    id: 2387707,
    question: 'Welche Funktionen hat ein Transportinkubator?',
    choices: [
      {
        valid: true,
        content: 'Wärmezufuhr'
      },
      {
        valid: true,
        content: 'Sauerstoffzufuhr'
      },
      {
        valid: true,
        content: 'Feuchtigkeitsanreicherung der Luft'
      },
      {
        valid: true,
        content: 'Schutz vor Umwelt (Keime)'
      },
    ],
  },
  {
    id: 2388199,
    question: 'Welche Gefahr besteht bei einem zu kleinen Guedel-Tubus?',
    choices: [
      {
        valid: true,
        content: 'Zunge sinkt zurück'
      },
      {
        content: 'Zunge wird angehoben'
      },
      {
        content: 'Zunge bleibt seitlich liegen'
      },
      {
        content: 'Es besteht keine Gefahr'
      },
    ],
  },
  {
    id: 2389623,
    question: 'Welche Gefahr bringt eine Verätzung mit sich?',
    choices: [
      {
        content: 'Unterkühlung'
      },
      {
        valid: true,
        content: 'Hautschädigung'
      },
      {
        valid: true,
        content: 'Vergiftung'
      },
      {
        content: 'starke Blutung'
      },
    ],
  },
  {
    id: 2387708,
    question: 'Welche Gefahr(en) können bei Erfrierungen auftreten?',
    choices: [
      {
        content: 'Herzrhythmusstörungen'
      },
      {
        content: 'Dauernde Verfärbung der Haut'
      },
      {
        valid: true,
        content: 'Absterben des Gewebes führt zu bleibenden Schäden an den betroffenen Körperstellen'
      },
      {
        content: 'Schädigung innerer Organe'
      },
    ],
  },
  {
    id: 2388336,
    question: 'Welche Gefahren bestehen bei der Fremdkörperentfernung?',
    choices: [
      {
        content: 'Knochenbruch'
      },
      {
        valid: true,
        content: 'Zusätzliche Verletzung'
      },
      {
        valid: true,
        content: 'Weitere Blutung'
      },
      {
        content: 'Verschwinden des Tatwerkzeuges'
      },
    ],
  },
  {
    id: 2388591,
    question: 'Welche Gefahren bestehen bei einem Insektenstich?',
    choices: [
      {
        valid: true,
        content: 'Schwellung und Rötung des Gewebes im Bereich der Einstichstelle'
      },
      {
        valid: true,
        content: 'Gefahr durch allergische Reaktionen'
      },
      {
        valid: true,
        content: 'Schwellung der Atemwege'
      },
      {
        content: 'Übertragung einer  HIV-Infektion (Aids) möglich'
      },
    ],
  },
  {
    id: 2388973,
    question: 'Welche Gefahren bestehen bei einem Krampfanfall?',
    choices: [
      {
        content: 'Lungenödem durch die Anstrengung des Körpers während des Krampfens'
      },
      {
        valid: true,
        content: 'Aspiration'
      },
      {
        valid: true,
        content: 'Verletzung durch Krämpfe'
      },
      {
        content: 'Herzinfarkt'
      },
    ],
  },
  {
    id: 2389636,
    question: 'Welche Gefahren bestehen bei einer starken Blutung?',
    choices: [
      {
        content: 'allergische Reaktion'
      },
      {
        valid: true,
        content: 'Schockgefahr'
      },
      {
        content: 'Verbrennungskrankheit'
      },
      {
        content: 'Tollwut'
      },
    ],
  },
  {
    id: 2388974,
    question: 'Welche Gefahren bestehen bei einer Venenthrombose?',
    choices: [
      {
        content: 'Eine Venenthrombose ist eine harmlose "Erkrankung", bei der der Patient nur ein leichtes Spannungsgefühl in der betroffenen Extremität verspürt'
      },
      {
        content: 'Es kann zu einem Losreißen von Gerinnseln und zum Verschleppen derselben zum linken Herzen und damit zum Herzinfarkt kommen'
      },
      {
        valid: true,
        content: 'Es kann zu einem Losreißen von Gerinnseln und zum Verschleppen derselben zum rechten Herzen und in die Lunge und damit zur Lungenembolie kommen'
      },
      {
        content: 'Es kann zu einem Losreißen von Gerinnseln und zum Verschleppen derselben ins Gehirn und damit zum Schlaganfall kommen'
      },
    ],
  },
  {
    id: 2387709,
    question: 'Welche Gefahren bestehen bei Elektrounfällen?',
    choices: [
      {
        valid: true,
        content: 'Herzrhythmusstörungen'
      },
      {
        valid: true,
        content: 'Nervenschäden'
      },
      {
        valid: true,
        content: 'Krampfanfälle'
      },
      {
        valid: true,
        content: 'Atem-Kreislauf-Stillstand'
      },
    ],
  },
  {
    id: 2389644,
    question: 'Welche Gefahren entstehen durch eine Verbrennung?',
    choices: [
      {
        content: 'starke Blutung'
      },
      {
        valid: true,
        content: 'Schockgefahr'
      },
      {
        valid: true,
        content: 'Wunde kann sich entzünden'
      },
      {
        valid: true,
        content: 'Lebensgefahr'
      },
    ],
  },
  {
    id: 2387710,
    question: 'Welche Gefahren gehen mit einer Unterkühlung einher?',
    choices: [
      {
        valid: true,
        content: 'Bewusstlosigkeit, Atem-Kreislauf-Stillstand infolge Absinkens der Körpertemperatur unter 30 Grad Celsius'
      },
      {
        content: 'Lebensbedrohliche Steigerung der Atem- und Kreislauffrequenz'
      },
      {
        valid: true,
        content: 'Verlangsamung der Atmung, mitunter beschwerdefrei'
      },
      {
        content: 'Ansteigen der Körperkerntemperatur, Bewusstlosigkeit'
      },
    ],
  },
  {
    id: 2387711,
    question: 'Welche Gefahren können bei einer schweren Verbrennung auftreten und sind dadurch lebensbedrohlich?',
    choices: [
      {
        content: 'Blutverlust'
      },
      {
        content: 'Sauerstoffmangel durch Ausfall der Hautatmung'
      },
      {
        content: 'Auflösung der roten Blutkörperchen (Erythrozyten)'
      },
      {
        valid: true,
        content: 'Verbrennungsschock, Infektionsgefahr und Verbrennungskrankheit'
      },
    ],
  },
  {
    id: 2388670,
    question: 'Welche Gefahren können durch einen Schlangenbiss entstehen (heimische Schlangen)?',
    choices: [
      {
        content: 'Schlangenbisse sind grundsätzlich tödlich'
      },
      {
        content: 'Vergiftungs- und Lähmungserscheinungen mit Spätfolgen'
      },
      {
        valid: true,
        content: 'Vergiftungserscheinungen möglich, jedoch nicht lebensbedrohend'
      },
      {
        valid: true,
        content: 'Kreislaufstörungen, allergische Reaktion möglich'
      },
    ],
  },
  {
    id: 2387712,
    question: 'Welche Gefahren nach Rauchgasinhalation bzw. Vergiftung durch Reizgase gibt es?',
    choices: [
      {
        content: 'Kompensatorische Hyperventilation'
      },
      {
        valid: true,
        content: 'Schädigung der Lungenbläschen'
      },
      {
        content: 'Kardiales Lungenödem'
      },
      {
        valid: true,
        content: 'Toxisches Lungenödem'
      },
    ],
  },
  {
    id: 2387713,
    question: 'Welche Gefahren sind bei Verätzung und Kontamination der Haut mit organischen Lösungsmitteln möglich?',
    choices: [
      {
        valid: true,
        content: 'Hautschädigung'
      },
      {
        content: 'Sofortige Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Vergiftung (der Stoff wird über die Haut aufgenommen)'
      },
      {
        content: 'Massive Infektionsgefahr'
      },
    ],
  },
  {
    id: 2387714,
    question: 'Welche Gefahren sind bei Vergiftungsnotfällen möglich?',
    choices: [
      {
        valid: true,
        content: 'Spätschäden, z.B. Nierenschäden'
      },
      {
        content: 'Blutvergiftung'
      },
      {
        valid: true,
        content: 'Spätschäden, z.B. Hirn- und Leberschäden'
      },
      {
        valid: true,
        content: 'Bewusstlosigkeit, Atem-Kreislauf-Stillstand'
      },
    ],
  },
  {
    id: 2388200,
    question: 'Welche Gefahren und Schutzmaßnahmen im Umgang mit Sauerstoff gibt es?',
    choices: [
      {
        valid: true,
        content: 'Rauchverbot, da Sauerstoff die Verbrennung stark beschleunigt'
      },
      {
        valid: true,
        content: 'Reiner Sauerstoff unter abnormem Druck über längere Zeit kann die Gesundheit schädigen'
      },
      {
        content: 'Sauerstoff darf mit leicht entzündlichen Stoffen zusammen gelagert werden'
      },
      {
        valid: true,
        content: 'Nach längeren Transporten mit hohen Sauerstoffgaben müssen das Fahrzeug und die Kleidung gelüftet werden'
      },
    ],
  },
  {
    id: 2387715,
    question: 'Welche Gelenke sind bei einem Schienbeinbruch ruhig zu stellen?',
    choices: [
      {
        content: 'Hüftgelenk und Kniegelenk'
      },
      {
        valid: true,
        content: 'Kniegelenk und Sprunggelenk'
      },
      {
        content: 'Sprunggelenk und Hüftgelenk'
      },
      {
        content: 'Kniegelenk und Zehengelenke'
      },
    ],
  },
  {
    id: 2388862,
    question: 'Welche Haltung muss der Helfer bei der psychischen Betreuung haben?',
    choices: [
      {
        valid: true,
        content: 'Einfach sprechen, keine medizinischen Fremdwörter verwenden'
      },
      {
        valid: true,
        content: 'Die Gefühle des Patienten müssen akzeptiert werden'
      },
      {
        valid: true,
        content: 'Zusagen und Versprechen sollen eingehalten werden'
      },
      {
        content: 'Mit Psychotricks arbeiten, um den Patienten zu beruhigen'
      },
    ],
  },
  {
    id: 2388683,
    question: 'Welche Handgriffe führen Sie vor einer Beatmung durch?',
    choices: [
      {
        valid: true,
        content: 'Kinn hochziehen'
      }, 
      {
        valid: true,
        content: 'Nase zuhalten'
      }, 
      {
        content: 'Arm im rechten Winkel zur Seite legen'
      }, 
      {
        content: 'Krawatte öffnen'
      }, 
    ],
  },
  {
    id: 2387716,
    question: 'Welche häufigen Gasvergiftungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Kohlenmonoxidvergiftungen'
      },
      {
        content: 'O2-Vergiftungen'
      },
      {
        valid: true,
        content: 'Kohlendioxidvergiftungen'
      },
      {
        valid: true,
        content: 'Rauchgasvergiftungen'
      },
    ],
  },
  {
    id: 2388671,
    question: 'Welche Infektionserkrankungen können durch Bisse infizierter Zecken übertragen werden?',
    choices: [
      {
        content: 'Mumps'
      },
      {
        valid: true,
        content: 'Borreliose'
      },
      {
        content: 'Typhus'
      },
      {
        valid: true,
        content: 'Frühsommer-Meningoenzephalitis (FSME)'
      },
    ],
  },
  {
    id: 2388719,
    question: 'Welche Infektionserkrankungen können durch direkten Kontakt mit Blut des Verletzten übertragen werden?',
    choices: [
      {
        content: 'Keuchhusten'
      },
      {
        content: 'Tuberkulose'
      },
      {
        valid: true,
        content: 'Hepatitis B, C und HIV'
      },
      {
        content: 'Hepatitis A'
      },
    ],
  },
  {
    id: 2388881,
    question: 'Welche Informationen benötigt die Leitstelle beim Absetzen eines Notrufes?',
    choices: [
      {
        valid: true,
        content: 'Wo ist der Notfallort?'
      }, 
      {
        valid: true,
        content: 'Was ist geschehen?'
      }, 
      {
        valid: true,
        content: 'Wer ruft an?'
      }, 
      {
        valid: true,
        content: 'Wie viele Menschen sind betroffen?'
      }, 
    ],
  },
  {
    id: 2387717,
    question: 'Welche Informationen benötigt die Vergiftungsinformationszentrale bei einem Anruf?',
    choices: [
      {
        valid: true,
        content: 'Was? - Bezeichnung oder Beschreibung des Giftes'
      },
      {
        valid: true,
        content: 'Wie? - Art der Giftaufnahme'
      },
      {
        valid: true,
        content: 'Wer? - Alter, Gewicht und Zustand'
      },
      {
        valid: true,
        content: 'Wo? - Haushalt, Betrieb, Adresse'
      },
    ],
  },
  {
    id: 2388436,
    question: 'Welche Informationen müssen bei einem NAH-Einsatz gegeben werden?',
    choices: [
      {
        content: 'Patientendaten'
      },
      {
        valid: true,
        content: 'Optische Kennung des Landeplatzes: eingeschaltete Warnblinkanlage und Blaulicht'
      },
      {
        valid: true,
        content: 'Witterungsverhältnisse (Sicht, Wind und Schnee ...)'
      },
      {
        valid: true,
        content: 'Einsatzort mit zusätzlichen Orientierungshilfen (z.B. Kirche, Sportplatz, ...)'
      },
    ],
  },
  {
    id: 2389615,
    question: 'Welche Informationen soll ein Notruf enthalten?',
    choices: [
      {
        content: 'Wie viele Ersthelfer gibt es?'
      },
      {
        valid: true,
        content: 'Wo ist der Unfall passiert?'
      },
      {
        content: 'Wer hat den Unfall verursacht?'
      },
      {
        valid: true,
        content: 'Was ist geschehen?'
      },
    ],
  },
  {
    id: 2389255,
    question: 'Welche inneren Geschlechtsorgane hat der Mann?',
    choices: [
      {
        valid: true,
        content: 'Hoden'
      },
      {
        valid: true,
        content: 'Samenleiter'
      },
      {
        valid: true,
        content: 'Nebenhoden'
      },
      {
        valid: true,
        content: 'Prostata'
      },
    ],
  },
  {
    id: 2389256,
    question: 'Welche inneren Geschlechtsorgane hat die Frau?',
    choices: [
      {
        valid: true,
        content: 'Eileiter und Eierstöcke'
      },
      {
        valid: true,
        content: 'Gebärmutter'
      },
      {
        content: 'Plazenta'
      },
      {
        content: 'Große und kleine Schamlippen'
      },
    ],
  },
  {
    id: 2387718,
    question: 'Welche Kennzeichen (Erkennen) deuten auf eine offene Bauchverletzung hin?',
    choices: [
      {
        content: 'Kann die Zehen nicht bewegen'
      },
      {
        valid: true,
        content: 'Bauchhöhle geöffnet'
      },
      {
        content: 'Gefühllosigkeit im Beinbereich'
      },
      {
        valid: true,
        content: 'Darmschlingen ragen heraus'
      },
    ],
  },
  {
    id: 2387720,
    question: 'Welche Kennzeichen (Erkennen) deuten auf eine stumpfe Bauchverletzung hin?',
    choices: [
      {
        valid: true,
        content: 'Prellmarken, Bluterguss'
      },
      {
        valid: true,
        content: 'Starke Bauchschmerzen, gespannte Bauchdecke'
      },
      {
        content: 'Ausstrahlende Schmerzen im linken Arm'
      },
      {
        content: 'Austritt von Darmschlingen'
      },
    ],
  },
  {
    id: 2387721,
    question: 'Welche Kennzeichen (Erkennen) deuten auf einen Schädelbasisbruch hin? .',
    choices: [
      {
        content: 'Für den Ersthelfer nicht erkennbar'
      },
      {
        content: 'Fieber'
      },
      {
        valid: true,
        content: 'Blut- oder Flüssigkeitsaustritt aus Ohr, Mund oder Nase'
      },
      {
        valid: true,
        content: 'Bewusstlosigkeit'
      },
    ],
  },
  {
    id: 2388933,
    question: 'Welche Kennzeichen (Erkennen) gibt es bei Knochen- und Gelenksverletzungen?',
    choices: [
      {
        valid: true,
        content: 'Schmerzen, Schwellung, Schonhaltung'
      },
      {
        valid: true,
        content: 'Abnorme Fehlstellung'
      },
      {
        content: 'große Beweglichkeit trotz Schwellung'
      },
      {
        valid: true,
        content: 'Bewegungsunfähigkeit oder Bewegungseinschränkung'
      },
    ],
  },
  {
    id: 2387722,
    question: 'Welche Kennzeichen (Erkennen) sind typisch bei einer offenen Brustkorbverletzung?',
    choices: [
      {
        valid: true,
        content: 'Luftbläschen in der Wunde, Bluthusten, Atemnot'
      },
      {
        content: 'Bluterbrechen'
      },
      {
        valid: true,
        content: 'Wunde, gegebenenfalls mit pfeifendem oder schlürfendem Geräusch'
      },
      {
        content: 'Prellmarken'
      },
    ],
  },
  {
    id: 2388337,
    question: 'Welche Kennzeichen des Schocks können im "bedrohlichen" Stadium auftreten?',
    choices: [
      {
        content: 'Haut und Fingernägel rötlich gefärbt, Pulsqualität ausreichend, Blutdruck sinkt ab (unter 120)'
      },
      {
        content: 'Haut und Fingernägel bläulich gefärbt, Pulsfrequenz stark erhöht, Blutdruck sinkt ab (unter 100), Atmung normal, Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Blutdruck sinkt ab (unter 100), Atmung flach und beschleunigt, Bewusstsein stark getrübt'
      },
      {
        valid: true,
        content: 'Kalter Schweiß, Haut und Fingernägel bläulich gefärbt, Pulsfrequenz stark erhöht, kaum tastbar'
      },
    ],
  },
  {
    id: 2388338,
    question: 'Welche Kennzeichen des Schocks können im "fortgeschrittenen" Stadium auftreten?',
    choices: [
      {
        content: 'Sofortige Reanimation einleiten, Kontraindikation der Defibrillation beachten'
      },
      {
        valid: true,
        content: 'Haut grau (blau), verfallenes Aussehen, Puls kaum tastbar, Blutdruck nicht messbar'
      },
      {
        valid: true,
        content: 'Atmung oberflächlich, mitunter Schnappatmung, Pupillen weit, kaum mehr reagierend, Bewusstlosigkeit, akute Lebensgefahr!'
      },
      {
        content: 'Haut grau (blau), verfallenes Aussehen, Puls gut tastbar, Blutdruck im Normbereich,  Atmung normal, keine akute Lebensgefahr'
      },
    ],
  },
  {
    id: 2388339,
    question: 'Welche Kennzeichen des Schocks können im Anfangsstadium auftreten?',
    choices: [
      {
        content: 'Zyanose der Haut und Schleimhäute, Schweiß, Kältegefühl, Zittern, Patient meist bewusstlos'
      },
      {
        content: 'Blässe der Haut, kaum Schweiß, Fingernägel bläulich verfärbt, Bewusstsein stark getrübt'
      },
      {
        valid: true,
        content: 'Blässe der Haut und Schleimhäute, kühle Haut, Kältegefühl, Zittern'
      },
      {
        valid: true,
        content: 'Puls leicht beschleunigt, gut tastbar, Blutdruck normal, Patient ängstlich und unruhig'
      },
    ],
  },
  {
    id: 2387723,
    question: 'Welche Kennzeichen deuten auf eine Verletzung am knöchernen Schädel hin?',
    choices: [
      {
        content: 'Solche Verletzungsmuster sind nicht erkennbar'
      },
      {
        valid: true,
        content: 'Möglicher Gehirnaustritt und Bewusstlosigkeit'
      },
      {
        content: 'Nasenbluten'
      },
      {
        valid: true,
        content: 'Eindellung am Schädel, Knochensplitter erkennbar'
      },
    ],
  },
  {
    id: 2387724,
    question: 'Welche Kennzeichen gibt es bei der Dekompressionskrankheit Typ DCS I?',
    choices: [
      {
        valid: true,
        content: 'Auffällige Müdigkeit, Schwächegefühl'
      },
      {
        valid: true,
        content: 'Hautjucken (Taucherflöhe) und Hautrötungen (Marmorierung)'
      },
      {
        valid: true,
        content: 'Muskel- und Gelenksschmerzen ("Bends"), vor allem die großen Gelenke der oberen Extremitäten'
      },
      {
        content: 'Gelenksschmerzen, vor allem die großen Gelenke der unteren Extremitäten'
      },
    ],
  },
  {
    id: 2387725,
    question: 'Welche Kennzeichen gibt es bei der Dekompressionskrankheit Typ DCS II?',
    choices: [
      {
        valid: true,
        content: 'Alle Anzeichen von DCS I sowie Störungen des zentralen Nervensystems'
      },
      {
        valid: true,
        content: 'Bewusstseinsstörungen bis hin zur Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Schmerzen hinter dem Brustbein ("Chokes"), herzinfarktähnliche Symptome'
      },
      {
        content: 'Die Anzeichen sind ident mit dem Typ DCS I, maximal Bewusstlosigkeit'
      },
    ],
  },
  {
    id: 2388975,
    question: 'Welche Kennzeichen gibt es bei einer Hypoglykämie?',
    choices: [
      {
        valid: true,
        content: 'Plötzlicher Heißhunger'
      },
      {
        valid: true,
        content: 'Müdigkeit, Schwächegefühl'
      },
      {
        valid: true,
        content: 'Eventuell Doppelbilder'
      },
      {
        valid: true,
        content: 'Aggressive Gereiztheit'
      },
    ],
  },
  {
    id: 2388976,
    question: 'Welche Kennzeichen gibt es bei einer Venenthrombose!',
    choices: [
      {
        valid: true,
        content: 'Schwellung und Spannungsgefühl'
      },
      {
        valid: true,
        content: 'Temperaturdifferenz'
      },
      {
        content: 'Pulslosigkeit'
      },
      {
        valid: true,
        content: 'Schmerz, Blaufärbung im Bereich der betroffenen Gliedmaße'
      },
    ],
  },
  {
    id: 2387726,
    question: 'Welche Kennzeichen sind bei der Notfalldiagnose Atemstillstand beim Säugling feststellbar?',
    choices: [
      {
        valid: true,
        content: 'Kein Bewusstsein'
      },
      {
        valid: true,
        content: 'Keine Atmung'
      },
      {
        valid: true,
        content: 'Herzfrequenz liegt über 60/Minute'
      },
      {
        content: 'Herzfrequenz liegt unter 60/Minute'
      },
    ],
  },
  {
    id: 2387727,
    question: 'Welche Kennzeichen sind bei einer oberflächlichen Kälteschädigung möglich?',
    choices: [
      {
        content: 'Anfangs Schmerzen und blaurote Verfärbung'
      },
      {
        valid: true,
        content: 'Anfangs Gefühllosigkeit und Blässe - dann prickelnde Schmerzen und blaurote Verfärbung'
      },
      {
        content: 'Blasenbildung und Gefühllosigkeit'
      },
      {
        content: 'Gefühllosigkeit und dunkelblaue Verfärbung'
      },
    ],
  },
  {
    id: 2387728,
    question: 'Welche Kennzeichen treten bei einer Augenverätzung auf?',
    choices: [
      {
        valid: true,
        content: 'Krampfartiges Zukneifen der Augenlider wegen starker Schmerzen im verätzten Auge'
      },
      {
        content: 'Weite Pupillen'
      },
      {
        content: 'Schielen'
      },
      {
        valid: true,
        content: 'Rötung'
      },
    ],
  },
  {
    id: 2387729,
    question: 'Welche Kennzeichen weisen auf die Austreibungsperiode hin?',
    choices: [
      {
        valid: true,
        content: 'Dauer ca. 1/2 - 3 Stunden, Wehen alle 2 Minuten, Wehendauer ca. 60-90 Sekunden'
      },
      {
        content: 'Dauer ca. 1/2 - 1 Stunde, Wehen alle 3 Minuten, Wehendauer ca. 30-60 Sekunden'
      },
      {
        valid: true,
        content: 'Bei der Geburt des kindlichen Kopfes stemmt sich zuerst das Hinterhaupt gegen die Schambeinfuge, und von unten her erscheint langsam das Gesicht des Kindes'
      },
      {
        valid: true,
        content: 'Ist der Kopf geboren, dann dreht er sich zur Seite, dann kommt es zur Geburt der vorderen, anschließend der hinteren Schulter'
      },
    ],
  },
  {
    id: 2387730,
    question: 'Welche Kennzeichen weisen auf die Eröffnungsperiode hin?',
    choices: [
      {
        valid: true,
        content: 'Dauer ca. 7 - 12 Stunden'
      },
      {
        valid: true,
        content: 'Das Kind wird durch die Wehen im Geburtskanal nach unten gepresst'
      },
      {
        valid: true,
        content: 'regelmäßige Wehen alle 3 - 5 Minuten, Dauer ca. 30-60 Sekunden'
      },
      {
        valid: true,
        content: 'Gegen Ende der Eröffnungsperiode kommt es oft zum spontanen Blasensprung'
      },
    ],
  },
  {
    id: 2387731,
    question: 'Welche Kennzeichen weisen auf die Nachgeburtsperiode hin?',
    choices: [
      {
        valid: true,
        content: 'Dauer 15 - 20 Minuten, eventuell länger'
      },
      {
        content: 'Dauer 1 - 2 Stunden'
      },
      {
        content: 'Wenn die Nachgeburtsperiode länger als 30 Minuten andauert soll vorsichtig an der Nabelschnur gezogen werden bis die Plazenta geboren ist'
      },
      {
        valid: true,
        content: 'Ablösung und Austritt der Plazenta nach der Geburt'
      },
    ],
  },
  {
    id: 2388201,
    question: 'Welche Komplikation kann durch Einführen eines Endotracheal-Tubus verhindert werden?',
    choices: [
      {
        content: 'Erbrechen'
      },
      {
        content: 'Hirnödem'
      },
      {
        content: 'Emphysem'
      },
      {
        valid: true,
        content: 'Aspiration'
      },
    ],
  },
  {
    id: 2387732,
    question: 'Welche Komplikationen ergeben sich, wenn sich die Eizelle außerhalb der Gebärmutter einnistet?',
    choices: [
      {
        valid: true,
        content: 'Bauchhöhlenschwangerschaft'
      },
      {
        valid: true,
        content: 'Eileiterschwangerschaft'
      },
      {
        valid: true,
        content: 'Es kann zu starken Blutungen kommen'
      },
      {
        content: 'Krampfanfälle'
      },
    ],
  },
  {
    id: 2388202,
    question: 'Welche Komplikationen können bei der Auswahl eines zu großen oder kleinen Guedel-Tubus auftreten?',
    choices: [
      {
        valid: true,
        content: 'Ein zu großer Tubus verschließt den Kehlkopf'
      },
      {
        valid: true,
        content: 'Ein zu kleiner Tubus führt dazu, dass die Zunge weiterhin die Atemwege verlegt'
      },
      {
        content: 'Ein zu kleiner Tubus verschließt den Kehlkopf'
      },
      {
        content: 'Ein zu großer Tubus führt dazu, dass die Zunge weiterhin die Atemwege verlegt'
      },
    ],
  },
  {
    id: 2387733,
    question: 'Welche Komplikationen können bei einer Augenverätzung auftreten?',
    choices: [
      {
        content: 'Weitsichtigkeit'
      },
      {
        content: 'Farbenblindheit'
      },
      {
        valid: true,
        content: 'Einschränkung der Sehkraft bis zur Erblindung'
      },
      {
        content: 'Generalisierter Lidkrampf'
      },
    ],
  },
  {
    id: 2387734,
    question: 'Welche Komplikationen können bei einer Hautverätzung auftreten?',
    choices: [
      {
        valid: true,
        content: 'Tief gehende Wunden mit Infektionsgefahr'
      },
      {
        content: 'Oberflächlicher Hautdefekt mit großer Narbenbildung'
      },
      {
        valid: true,
        content: 'Schockgefahr erhöht bei 20%iger Beteiligung der Körperoberfläche'
      },
      {
        content: 'Flüssigkeitsverlust, jedoch keine Schockgefahr'
      },
    ],
  },
  {
    id: 2387735,
    question: 'Welche Komplikationen können bei einer Hirndrucksteigerung auftreten?',
    choices: [
      {
        valid: true,
        content: 'Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Atem-Kreislauf-Stillstand'
      },
      {
        valid: true,
        content: 'Krämpfe'
      },
      {
        content: 'Hirndrucksteigerung ist nur bei Kindern mit Komplikationen verbunden'
      },
    ],
  },
  {
    id: 2387736,
    question: 'Welche Komplikationen können bei einer Verbrennung auftreten?',
    choices: [
      {
        valid: true,
        content: 'Verbrennungsschock'
      },
      {
        valid: true,
        content: 'Verbrennungskrankheit'
      },
      {
        valid: true,
        content: 'Infektionsgefahr'
      },
      {
        content: 'Komplikationen treten erst ab dem 3. Grad der Verbrennung auf'
      },
    ],
  },
  {
    id: 2387737,
    question: 'Welche Komplikationen und Symptome können bei einer mechanischen Wunde auftreten?',
    choices: [
      {
        valid: true,
        content: 'Infektion'
      },
      {
        content: 'Schmerzen und Blasenbildung mit Gefahr der Infektion'
      },
      {
        valid: true,
        content: 'Sichtbare Verletzungen'
      },
      {
        valid: true,
        content: 'Blutung, Schmerzen'
      },
    ],
  },
  {
    id: 2388203,
    question: 'Welche konkreten Maßnahmen sind bei der Blutzuckermessung durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Desinfektion der Einstichstelle'
      },
      {
        valid: true,
        content: 'Mit Einmalstechhilfe den Finger des Patienten punktieren'
      },
      {
        content: 'Den ersten Bluttropfen auf den Teststreifen aufbringen'
      },
      {
        valid: true,
        content: 'Messergebnis ablesen und dokumentieren'
      },
    ],
  },
  {
    id: 2388616,
    question: 'Welche Konsequenzen hat die Verletzung der Verschwiegenheitspflicht?',
    choices: [
      {
        valid: true,
        content: 'Kann als gerichtlich strafbare Handlung beurteilt werden'
      },
      {
        content: 'Keine'
      },
      {
        content: 'Es dürfen nur mehr Reinigungsdienste durchgeführt werden'
      },
      {
        valid: true,
        content: 'Kann als eine Verwaltungsübertretung beurteilt werden'
      },
    ],
  },
  {
    id: 2387738,
    question: 'Welche Körperstellen sind bei einer Erfrierung besonders gefährdet?',
    choices: [
      {
        valid: true,
        content: 'Körperstellen, die von zu enger Bekleidung (z.B. Schuhwerk) umgeben sind'
      },
      {
        content: 'Rücken'
      },
      {
        content: 'Körperstellen, die von weiter Bekleidung umgeben sind'
      },
      {
        valid: true,
        content: 'Besonders gefährdet sind Körperstellen, die wenig Schutz durch Muskulatur und Gewebe haben'
      },
    ],
  },
  {
    id: 2389579,
    question: 'Welche Kosten entstehen für Angehörige bei einer Betreuung durch das KI-Team?',
    choices: [
      {
        content: 'Die Betreuung findet statt, die Verrechnung erfolgt über die Krankenkasse.'
      },
      {
        content: 'Je nach Dauer des Einsatzes, kann unter Umständen eine Vergütung erfolgen.'
      },
      {
        content: 'Durch eine rechtzeitig bekannt gegebene Spendensammlung kann dieser Einsatz ebenfalls abgegolten werden.'
      },
      {
        valid: true,
        content: 'Die KI-Betreuung findet für die zu Betreuenden vollkommen kostenfrei statt.'
      },
    ],
  },
  {
    id: 2387739,
    question: 'Welche Krankheitsbilder können beim Tauchunfall auftreten?',
    choices: [
      {
        valid: true,
        content: 'Schwere Form der Dekompressionserkrankung'
      },
      {
        valid: true,
        content: 'Barotrauma der Zähne'
      },
      {
        valid: true,
        content: 'Barotrauma der Lunge'
      },
      {
        valid: true,
        content: 'Barotrauma des Ohrs'
      },
    ],
  },
  {
    id: 2387740,
    question: 'Welche Krankheitsbilder mit Atemnot bei Kindern gibt es?',
    choices: [
      {
        valid: true,
        content: 'Epiglottitis'
      },
      {
        valid: true,
        content: 'Krupp-Syndrom'
      },
      {
        valid: true,
        content: 'Aspiration'
      },
      {
        content: 'Anämie'
      },
    ],
  },
  {
    id: 2389257,
    question: 'Welche Lage und Funktion haben die Nieren?',
    choices: [
      {
        valid: true,
        content: 'Sie produzieren Harn'
      },
      {
        valid: true,
        content: 'Sie regulieren den Wasserhaushalt des Körpers'
      },
      {
        valid: true,
        content: 'Sie liegen außerhalb der Bauchhöhle'
      },
      {
        content: 'Sie liegen links und rechts der Harnblase'
      },
    ],
  },
  {
    id: 2389258,
    question: 'Welche Lage und Funktion hat die Bauchspeicheldrüse?',
    choices: [
      {
        content: 'Sie liegt hinter der Milz'
      },
      {
        valid: true,
        content: 'Sie liegt hinter dem Magen'
      },
      {
        valid: true,
        content: 'Sie produziert Insulin'
      },
      {
        valid: true,
        content: 'Sie bildet Verdauungsfermente'
      },
    ],
  },
  {
    id: 2389259,
    question: 'Welche Lage und Funktion hat die Leber?',
    choices: [
      {
        valid: true,
        content: 'Sie liegt im rechten Oberbauch'
      },
      {
        valid: true,
        content: 'Sie ist die größte Drüse'
      },
      {
        content: 'Sie produziert Insulin'
      },
      {
        valid: true,
        content: 'Sie produziert Gallensaft'
      },
    ],
  },
  {
    id: 2389260,
    question: 'Welche Lage und Funktion hat die Milz?',
    choices: [
      {
        valid: true,
        content: 'Sie liegt im linken Oberbauch'
      },
      {
        valid: true,
        content: 'Sie dient der Immunabwehr'
      },
      {
        content: 'Sie produziert Insulin'
      },
      {
        content: 'Sie produziert Gallensaft'
      },
    ],
  },
  {
    id: 2387741,
    question: 'Welche Lagerung ist für Patienten mit einer Thoraxverletzung günstig?',
    choices: [
      {
        valid: true,
        content: 'Oberkörper hoch lagern'
      },
      {
        content: 'Flache Rückenlagerung'
      },
      {
        valid: true,
        content: 'Lagerung auf die verletzte Brustkorbseite (falls für den Patienten erträglich)'
      },
      {
        content: 'Beinhochlagerung zur Schockbekämpfung'
      },
    ],
  },
  {
    id: 2388882,
    question: 'Welche Lagerung wird (bewusstloser Notfallpatient) durchgeführt?',
    choices: [
      {
        content: 'Oberkörper-Hochlagerung'
      }, 
      {
        content: 'Lagerung ohne Veränderung der Körperhaltung'
      }, 
      {
        content: 'Deckenrolle unter dem Knie'
      }, 
      {
        valid: true,
        content: 'Stabile Seitenlagerung'
      }, 
    ],
  },
  {
    id: 2388883,
    question: 'Welche Lagerung wird bei Atemnot durchgeführt?',
    choices: [
      {
        content: 'Bauchlage'
      }, 
      {
        valid: true,
        content: 'Oberkörper-Hochlagerung'
      }, 
      {
        content: 'Keine besondere Lagerung erforderlich'
      }, 
      {
        content: 'Beine-Hochlagerung'
      }, 
    ],
  },
  {
    id: 2388863,
    question: 'Welche Lagerung wird bei Bauchverletzungen (Erste Hilfe) durchgeführt?',
    choices: [
      {
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Flache Rückenlagerung mit angezogenen Beinen'
      },
      {
        content: 'Bein-Hochlagerung'
      },
      {
        valid: true,
        content: 'Deckenrolle unter den Knien'
      },
    ],
  },
  {
    id: 2388340,
    question: 'Welche Lagerung wird bei einem bewusstlosen Notfallpatienten durchgeführt?',
    choices: [
      {
        content: 'Erhöhter Oberkörper'
      },
      {
        content: 'Bein-Hochlagerung'
      },
      {
        valid: true,
        content: 'Stabile Seitenlagerung'
      },
      {
        content: 'Rückenlage'
      },
    ],
  },
  {
    id: 2388864,
    question: 'Welche Lagerung wird bei einer Kopfverletzung (Erste Hilfe) durchgeführt?',
    choices: [
      {
        content: 'Stabile Seitenlage'
      },
      {
        content: 'Bein-Hochlagerung'
      },
      {
        valid: true,
        content: 'Oberkörper-Hochlagerung'
      },
      {
        content: 'Keine spezielle Lagerung erforderlich'
      },
    ],
  },
  {
    id: 2388592,
    question: 'Welche Lagerung wird bei einer starken Blutung (Erste Hilfe) durchgeführt (z.B. Kreissägenverletzung am Unterarm)?',
    choices: [
      {
        content: 'Oberkörper-Hochlagerung'
      },
      {
        valid: true,
        content: 'Beine-Hochlagerung'
      },
      {
        content: 'Deckenrolle unter dem Knie'
      },
      {
        content: 'Lagerung ohne Veränderung der Körperhaltung'
      },
    ],
  },
  {
    id: 2388341,
    question: 'Welche Lebensfunktionen, bezogen auf den Notfallpatienten, gibt es?',
    choices: [
      {
        valid: true,
        content: 'Bewusstsein, Atmung und Kreislauf'
      },
      {
        content: 'Essen und Trinken'
      },
      {
        content: 'Wärmehaushalt, Säure-Basen-Haushalt und Stoffwechsel'
      },
      {
        content: 'Sauerstoff und Wasser'
      },
    ],
  },
  {
    id: 2388865,
    question: 'Welche lebensrettenden Sofortmaßnahmen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Retten von Verletzten'
      },
      {
        valid: true,
        content: 'Blutstillung'
      },
      {
        valid: true,
        content: 'Freihalten der Atemwege'
      },
      {
        valid: true,
        content: 'Wiederbelebung'
      },
    ],
  },
  {
    id: 2389153,
    question: 'Welche Leitfarbe hat die Behandlungsstelle I?',
    choices: [
      {
        valid: true,
        content: 'Rot'
      },
      {
        content: 'Gelb'
      },
      {
        content: 'Grün'
      },
      {
        content: 'Blau'
      },
    ],
  },
  {
    id: 2389154,
    question: 'Welche Leitfarbe hat die Behandlungsstelle II?',
    choices: [
      {
        valid: true,
        content: 'Gelb'
      },
      {
        content: 'Rot'
      },
      {
        content: 'Blau'
      },
      {
        content: 'Grün'
      },
    ],
  },
  {
    id: 2389155,
    question: 'Welche Leitfarbe hat die Behandlungsstelle III?',
    choices: [
      {
        valid: true,
        content: 'Grün'
      },
      {
        content: 'Rot'
      },
      {
        content: 'Gelb'
      },
      {
        content: 'Blau'
      },
    ],
  },
  {
    id: 2389156,
    question: 'Welche Leitfarbe hat die Behandlungsstelle IV?',
    choices: [
      {
        content: 'Rot'
      },
      {
        content: 'Gelb'
      },
      {
        valid: true,
        content: 'Blau'
      },
      {
        content: 'Grün'
      },
    ],
  },
  {
    id: 2388868,
    question: 'Welche Maßnahme gehört zu den Basismaßnahmen?',
    choices: [
      {
        valid: true,
        content: 'gut zureden'
      }, 
      {
        content: 'zur Aktivierung des Kreislaufes aufstehen'
      }, 
      {
        content: 'Tee trinken'
      }, 
      {
        content: 'den Verletzten umdrehen'
      }, 
    ],
  },
  {
    id: 2388900,
    question: 'Welche Maßnahme gehört zu den Basismaßnahmen?',
    choices: [
      {
        valid: true,
        content: 'gut zureden'
      }, 
      {
        content: 'zur Aktivierung des Kreislaufes aufstehen'
      }, 
      {
        content: 'Tee trinken'
      }, 
      {
        content: 'den Verletzten umdrehen'
      }, 
    ],
  },
  {
    id: 2388918,
    question: 'Welche Maßnahme gehört zu den Basismaßnahmen?',
    choices: [
      {
        valid: true,
        content: 'gut zureden'
      }, 
      {
        content: 'zur Aktivierung des Kreislaufes aufstehen'
      }, 
      {
        content: 'Tee trinken'
      }, 
      {
        content: 'den Verletzten umdrehen'
      }, 
    ],
  },
  {
    id: 2388884,
    question: 'Welche Maßnahmen ergreifen Sie, wenn ein Verletzter am Bauch liegt und nicht reagiert?',
    choices: [
      {
        content: 'sofort stabile Seitenlage durchführen'
      }, 
      {
        valid: true,
        content: 'Notruf'
      }, 
      {
        valid: true,
        content: 'laut ansprechen und sanft schütteln'
      }, 
      {
        valid: true,
        content: 'den Verletzten umdrehen'
      }, 
    ],
  },
  {
    id: 2388696,
    question: 'Welche Maßnahmen führen Sie bei  Bewusstlosigkeit durch?',
    choices: [
      {
        valid: true,
        content: 'Stabile Seitenlage zum Freihalten der Atemwege'
      },
      {
        content: 'Auf dem Rücken liegend und mit überstrecktem Kopf lagern'
      }, 
      {
        content: 'Flache Rückenlagerung und Beine hoch lagern'
      }, 
      {
        content: 'Erhöhter Oberkörper ist die optimale Lagerung'
      }, 
    ],
  },
  {
    id: 2388342,
    question: 'Welche Maßnahmen führen Sie bei einer akuten Verlegung der Atemwege bei Kindern durch (bei Bewusstsein)?',
    choices: [
      {
        valid: true,
        content: '5 Schläge zwischen die Schulterblätter'
      },
      {
        valid: true,
        content: 'Falls kein Erfolg, 5 "Heimlich" Manöver, dann wieder 5 Schläge zwischen die Schulterblätter, usw.'
      },
      {
        content: 'Falls kein Erfolg, 10 "Heimlich" Manöver, dann wieder 10 Schläge zwischen die Schulterblätter, usw.'
      },
      {
        content: '10 Schläge zwischen die Schulterblätter'
      },
    ],
  },
  {
    id: 2389157,
    question: 'Welche Materialeinheiten sind für einen Großunfall vorzuhalten?',
    choices: [
      {
        valid: true,
        content: 'Basisgroßunfallset'
      },
      {
        valid: true,
        content: 'Medizinisches Großunfallset'
      },
      {
        valid: true,
        content: 'Organisationsset'
      },
      {
        content: 'Materialset'
      },
    ],
  },
  {
    id: 2389637,
    question: 'Welche Materialien aus einem Erste-Hilfe-Koffer benötigst du für einen Druckverband?',
    choices: [
      {
        content: 'Rettungsdecke'
      },
      {
        content: 'Beatmungstuch'
      },
      {
        valid: true,
        content: 'keimfreie Wundauflage'
      },
      {
        valid: true,
        content: 'elastische Mullbinde'
      },
    ],
  },
  {
    id: 2388204,
    question: 'Welche Materialien werden zur Vorbereitung einer Venenpunktion benötigt?',
    choices: [
      {
        valid: true,
        content: 'Stauschlauch'
      },
      {
        valid: true,
        content: 'Tupfer'
      },
      {
        content: 'Infusionsgerät'
      },
      {
        valid: true,
        content: 'Fixationsmaterial'
      },
    ],
  },
  {
    id: 2388977,
    question: 'Welche Merkmale deuten auf eine arterielle Embolie hin?',
    choices: [
      {
        valid: true,
        content: 'Temperaturdifferenz'
      },
      {
        valid: true,
        content: 'Pulsverlust, Gefühlsstörungen im Bereich der betroffenen Gliedmaße'
      },
      {
        valid: true,
        content: 'Bewegungsunfähigkeit der Zehen oder des Vorfußes'
      },
      {
        content: 'Rötung des nicht mehr durchbluteten Teiles, heftige Schmerzen'
      },
    ],
  },
  {
    id: 2388343,
    question: 'Welche möglichen Gefahren können bei einem bewusstlosen Notfallpatienten auftreten?',
    choices: [
      {
        content: 'Starkes Schmerzempfinden'
      },
      {
        valid: true,
        content: 'Erstickungsgefahr in Rückenlage durch Zurücksinken der Zunge'
      },
      {
        content: 'Epileptischer Anfall'
      },
      {
        content: 'Es gibt keine Gefahren für einen bewusstlosen Notfallpatienten'
      },
    ],
  },
  {
    id: 2387742,
    question: 'Welche möglichen Kennzeichen (Erkennen) deuten auf eine Wirbelsäulenverletzung hin?',
    choices: [
      {
        valid: true,
        content: 'Kribbeln oder Gefühllosigkeit in Armen und Beinen'
      },
      {
        content: 'Krampfanfälle'
      },
      {
        valid: true,
        content: 'Schmerzen im Rückenbereich'
      },
      {
        valid: true,
        content: 'Unvermögen, sich aufzusetzen oder Beine zu bewegen'
      },
    ],
  },
  {
    id: 2388720,
    question: 'Welche Möglichkeit gibt es, gegen eine Infektionskrankheit Immunität zu erreichen?',
    choices: [
      {
        content: 'Nur durch aktive und passive Immunisierung'
      },
      {
        valid: true,
        content: 'Durch aktive und passive Immunisierung oder durch eine durchgemachte Infektion'
      },
      {
        content: 'Nur durch eine aktive Immunisierung'
      },
      {
        content: 'Nur durch eine durchgemachte, stumme, aktive Infektion'
      },
    ],
  },
  {
    id: 2387743,
    question: 'Welche Möglichkeiten der Giftaufnahme gibt es?',
    choices: [
      {
        valid: true,
        content: 'Inhalationsvergiftung über die Lunge'
      },
      {
        valid: true,
        content: 'Parenterale Vergiftung durch Injektion'
      },
      {
        valid: true,
        content: 'Ingestionsvergiftung über den Verdauungstrakt'
      },
      {
        valid: true,
        content: 'Perkutane Vergiftung über die Haut'
      },
    ],
  },
  {
    id: 2388344,
    question: 'Welche Möglichkeiten der Stillung von starken Blutungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Fingerdruck, Abdrücken zuführender Gefäße'
      },
      {
        valid: true,
        content: 'Druckverband'
      },
      {
        valid: true,
        content: 'Abbindung'
      },
      {
        content: 'Lockere keimfreie Wundauflage'
      },
    ],
  },
  {
    id: 2388345,
    question: 'Welche Möglichkeiten zum Freihalten der Atemwege beim bewusstlosen Notfallpatienten gibt es?',
    choices: [
      {
        content: '“Heimlich“-Handgriff und Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Stabile Seitenlagerung, Kopf überstrecken'
      },
      {
        content: 'Stabile Seitenlagerung mit stark erhöhtem Oberkörper'
      },
      {
        content: 'Einführen eines Guedel-Tubus'
      },
    ],
  },
  {
    id: 2388604,
    question: 'Welche Möglichkeiten zur Stillung einer starken Blutung gibt es?',
    choices: [
      {
        content: 'direkt in die Wunde greifen'
      }, 
      {
        valid: true,
        content: 'Fingerdruck'
      }, 
      {
        valid: true,
        content: 'Druckverband'
      }, 
      {
        content: 'verletzten Körperteil nach unten halten'
      }, 
    ],
  },
  {
    id: 2389261,
    question: 'Welche Muskelfasern sind quergestreift?',
    choices: [
      {
        valid: true,
        content: 'Skelettmuskelfasern'
      },
      {
        content: 'Muskelfasern in den Blutgefäßen'
      },
      {
        content: 'Muskelfasern in der Lunge'
      },
      {
        content: 'Muskelfasern im Harntrakt'
      },
    ],
  },
  {
    id: 2388437,
    question: 'Welche negativen Folgen haben Fahreinflüsse auf den Patienten?',
    choices: [
      {
        valid: true,
        content: 'Zunahme der Schmerzsymptomatik'
      },
      {
        content: 'Linderung von Schmerzen'
      },
      {
        valid: true,
        content: 'Einfluss auf das vegetative Nervensystem (z.B. Pulsbeschleunigung usw.)'
      },
      {
        valid: true,
        content: 'Verschlechterung des Zustandes durch Aufregung'
      },
    ],
  },
  {
    id: 2389262,
    question: 'Welche Normalwerte des Blutdrucks gibt es?',
    choices: [
      {
        content: 'Systolisch 80-100, diastolisch 60-90 mm Hg'
      },
      {
        content: 'Systolisch 100-160, diastolisch 90-105 mm Hg'
      },
      {
        valid: true,
        content: 'Systolisch unter 130, diastolisch unter 85 mm Hg'
      },
      {
        content: 'Systolisch 120-160, diastolisch 60-105 mm Hg'
      },
    ],
  },
  {
    id: 2388438,
    question: 'Welche Notarztmittel werden im österreichischen Notarztwesen eingesetzt?',
    choices: [
      {
        valid: true,
        content: 'NAW'
      },
      {
        valid: true,
        content: 'NEF'
      },
      {
        valid: true,
        content: 'NAH'
      },
      {
        content: 'NTW'
      },
    ],
  },
  {
    id: 2388978,
    question: 'Welche Notfälle können bei einem Hämodialyse-Patienten auftreten?',
    choices: [
      {
        valid: true,
        content: 'Shuntblutungen'
      },
      {
        valid: true,
        content: 'Kollapsneigung unmittelbar nach der Dialyse'
      },
      {
        content: 'Kopfschmerzen'
      },
      {
        content: 'Durchfall'
      },
    ],
  },
  {
    id: 2388979,
    question: 'Welche Notfälle treten in erster Linie bei Diabetes-Patienten auf?',
    choices: [
      {
        valid: true,
        content: 'Hypoglykämie'
      },
      {
        content: 'Hypocalzämie'
      },
      {
        valid: true,
        content: 'Hyperglykämie'
      },
      {
        content: 'Hypokaliämie'
      },
    ],
  },
  {
    id: 2388205,
    question: 'Welche Patienten dürfen nicht defibrilliert werden?',
    choices: [
      {
        valid: true,
        content: 'Säuglinge'
      },
      {
        content: 'Patienten, die einen Herzschrittmacher haben'
      },
      {
        valid: true,
        content: 'Patienten, die eine normal Atmung und Lebenszeichen haben'
      },
      {
        valid: true,
        content: 'Patienten mit einem feuchten oder nassen Brustkorb'
      },
    ],
  },
  {
    id: 2389158,
    question: 'Welche Patienten werden in Behandlungsstelle II behandelt?',
    choices: [
      {
        valid: true,
        content: 'SHT'
      },
      {
        valid: true,
        content: 'Innere Verletzungen'
      },
      {
        content: 'Polytrauma'
      },
      {
        content: 'Atemstörungen durch Verletzung/Verlegung der Atemwege'
      },
    ],
  },
  {
    id: 2388617,
    question: 'Welche Pflichten des Rettungssanitäters gibt es?',
    choices: [
      {
        content: 'Hilfeleistungspflicht, Sorgfaltspflicht, Schweigepflicht, Gehorsamspflicht, Protokollpflicht'
      },
      {
        content: 'Hilfeleistungspflicht, Reinigungspflicht, Schweigepflicht, Fortbildungspflicht'
      },
      {
        valid: true,
        content: 'Hilfeleistungspflicht, Sorgfaltspflicht, Verschwiegenheitspflicht, Fortbildungspflicht, Auskunftspflicht, Dokumentationspflicht'
      },
      {
        content: 'Hilfeleistungspflicht, Sorgfaltspflicht, Schweigepflicht, Fahrerpflicht, Besprechungspflicht'
      },
    ],
  },
  {
    id: 2388672,
    question: 'Welche Prinzipien gelten bei der Wundversorgung (Erste Hilfe)?',
    choices: [
      {
        valid: true,
        content: 'Fremdkörper in der Wunde belassen'
      },
      {
        valid: true,
        content: 'Kein Anwenden von Salben, Puder.....'
      },
      {
        valid: true,
        content: 'Oberflächliche Wunden mit sauberem Wasser reinigen'
      },
      {
        content: 'Einmalhandschuhe für den Ersthelfer nicht empfohlen'
      },
    ],
  },
  {
    id: 2389159,
    question: 'Welche Protokolle befinden sich in der Patientenleittasche?',
    choices: [
      {
        valid: true,
        content: 'Behandlungsprotokoll'
      },
      {
        valid: true,
        content: 'Identifikationsprotokoll'
      },
      {
        content: 'Triageprotokoll'
      },
      {
        content: 'Medikamentenprotokoll'
      },
    ],
  },
  {
    id: 2387744,
    question: 'Welche Pulsfrequenz ist für ein Kind normal?',
    choices: [
      {
        content: '16-30 Schläge pro Minute'
      },
      {
        content: '90-140 Schläge pro Minute'
      },
      {
        valid: true,
        content: '100+/-20 Schläge pro Minute'
      },
      {
        content: '140+/-20 Schläge pro Minute'
      },
    ],
  },
  {
    id: 2389263,
    question: 'Welche Pulsfrequenz ist für einen Erwachsenen normal?',
    choices: [
      {
        content: '12-15 Schläge pro Minute'
      },
      {
        content: '70-100 Schläge pro Minute'
      },
      {
        valid: true,
        content: '80+/-20 Schläge pro Minute'
      },
      {
        content: '80+/-10 Schläge pro Minute'
      },
    ],
  },
  {
    id: 2387745,
    question: 'Welche Pulsfrequenz ist für einen Säugling normal?',
    choices: [
      {
        content: 'ca. 60 Schläge pro Minute'
      },
      {
        content: 'ca. 160 Schläge pro Minute'
      },
      {
        valid: true,
        content: 'ca. 120 Schläge pro Minute'
      },
      {
        content: '140+/-10 Schläge pro Minute'
      },
    ],
  },
  {
    id: 2387746,
    question: 'Welche Punkte sind im Gespräch mit psychisch Kranken zu beachten?',
    choices: [
      {
        valid: true,
        content: 'Ruhe im Gespräch und Verhalten'
      },
      {
        valid: true,
        content: 'Jedes Gespräch mit einer persönlichen Anrede beginnen'
      },
      {
        valid: true,
        content: 'Gezielte Fragen bzw. Zwischenfragen stellen'
      },
      {
        valid: true,
        content: 'Nicht argumentieren, drohen oder belehren'
      },
    ],
  },
  {
    id: 2388885,
    question: 'Welche Rettungsmöglichkeiten gibt es aus einer Gefahrenzone?',
    choices: [
      {
        valid: true,
        content: 'Rautek-Griff'
      }, 
      {
        valid: true,
        content: 'Wegziehen'
      }, 
      {
        content: 'Ast-Trage'
      }, 
      {
        content: 'Tragegurt'
      }, 
    ],
  },
  {
    id: 2387747,
    question: 'Welche Richtwerte für die Herzdruckmassage beim Kind gibt es?',
    choices: [
      {
        content: 'Druckpunkt: Verbindungslinie der Brustwarzen'
      },
      {
        valid: true,
        content: 'Drucktiefe: 5 cm'
      },
      {
        valid: true,
        content: 'Arbeitsfrequenz: mind. 100/Minute bis maximal 120/Minute'
      },
      {
        valid: true,
        content: 'Handhaltung: mit einer oder beiden Händen'
      },
    ],
  },
  {
    id: 2387748,
    question: 'Welche Richtwerte für die Herzdruckmassage beim Säugling gibt es?',
    choices: [
      {
        valid: true,
        content: 'Druckpunkt: Mitte Brustkorb'
      },
      {
        valid: true,
        content: 'Drucktiefe: 4 cm'
      },
      {
        valid: true,
        content: 'Arbeitsfrequenz: mind. 100/Minute bis maximal 120/Minute'
      },
      {
        content: 'Handhaltung: Handballen mit ausgestreckten Armen'
      },
    ],
  },
  {
    id: 2387749,
    question: 'Welche Sanitätshilfe-Maßnahmen sind bei Verdacht auf Kohlendioxidvergiftungen (Silo, ...) durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        valid: true,
        content: 'Sauerstoffgabe von 10-15 Liter/min.'
      },
      {
        valid: true,
        content: 'Absaugbereitschaft'
      },
      {
        content: 'Alle 1-2 Minuten Gabe von Cortison (Spray)'
      },
    ],
  },
  {
    id: 2388346,
    question: 'Welche Sanitätshilfe-Maßnahmen sind zur Schockbekämpfung durchzuführen?',
    choices: [
      {
        valid: true,
        content: 'Atmung begünstigen, Blutstillung, Zirkulation begünstigen, korrekte Lagerung, drohenden Sauerstoffmangel beheben'
      },
      {
        valid: true,
        content: 'Eigenwärme erhalten, Frischluftzufuhr, Fahrverhalten anpassen, guter Zuspruch'
      },
      {
        content: 'Frischluftzufuhr und in jedem Fall sofortige Sauerstoffgabe und unverzüglicher Krankenhaustransport mit Sondersignal'
      },
      {
        content: 'Essen und trinken lassen, raschester Transport in die Klinik'
      },
    ],
  },
  {
    id: 2388980,
    question: 'Welche Sanitätshilfe-Maßnahmen werden bei Blut im Harn durchgeführt?',
    choices: [
      {
        content: 'Thermophor auflegen'
      },
      {
        valid: true,
        content: 'Situationsgerechte Lagerung'
      },
      {
        valid: true,
        content: 'Allgemeine Maßnahmen'
      },
      {
        content: 'Mitnahme einer Harnprobe'
      },
    ],
  },
  {
    id: 2388981,
    question: 'Welche Sanitätshilfe-Maßnahmen werden bei Blut im Stuhl durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Flache Rückenlage mit angezogenen Beinen'
      },
      {
        content: 'Warmen Thermophor auflegen'
      },
      {
        content: 'Flache Rückenlagerung'
      },
      {
        valid: true,
        content: 'Thermophor entfernen'
      },
    ],
  },
  {
    id: 2388982,
    question: 'Welche Sanitätshilfe-Maßnahmen werden bei Bluterbrechen durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Zellstoff und Einmalnierentasse reichen'
      },
      {
        content: 'Flache Rückenlagerung mit angezogenen Beinen'
      },
      {
        content: 'Thermophor anbieten'
      },
    ],
  },
  {
    id: 2388983,
    question: 'Welche Sanitätshilfe-Maßnahmen werden bei Bluthusten durchgeführt?',
    choices: [
      {
        content: 'Lagerung mit erhöhten Beinen wegen des Blutverlustes'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Zellstoff und Einmalnierentasse reichen'
      },
      {
        content: 'Patienten anhalten, das Blut zu schlucken'
      },
    ],
  },
  {
    id: 2387750,
    question: 'Welche Sanitätshilfe-Maßnahmen werden bei Nasenbluten (Epistaxis) durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Sitzende Position, Oberkörper und Kopf nach vorne beugen, nach zuletzt eingenommenen Medikamenten befragen, sowie diese ins Krankenhaus mitnehmen'
      },
      {
        valid: true,
        content: 'Blutendes Nasenloch zudrücken'
      },
      {
        valid: true,
        content: 'Kalte Kompressen in den Nacken legen'
      },
      {
        valid: true,
        content: 'Zellstoff und Nierentasse reichen'
      },
    ],
  },
  {
    id: 2388347,
    question: 'Welche Schockformen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Hypovolämischer Schock, Kardiogener Schock'
      },
      {
        content: 'Überanstrengung, Erschrecken und tiefste Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Septischer Schock, Anaphylaktischer Schock'
      },
      {
        valid: true,
        content: 'Neurogener Schock (ähnlich: spinaler Schock)'
      },
    ],
  },
  {
    id: 2388873,
    question: 'Welche Schritte gehören zum Rautekgriff?',
    choices: [
      {
        valid: true,
        content: 'Oberkörper stützen, Gurt entfernen'
      }, 
      {
        content: 'Schlüssel abziehen und Lenksäule verstellen'
      }, 
      {
        valid: true,
        content: 'Verletzten auf den eigenen Oberschenkel ziehen'
      }, 
      {
        valid: true,
        content: 'Verletzen vorbeugen, unter beiden Armen durchgreifen'
      }, 
    ],
  },
  {
    id: 2388886,
    question: 'Welche Schritte sind durchzuführen, wenn der Verletzte auf lautes Ansprechen und sanftes Schütteln nicht reagiert?',
    choices: [
      {
        valid: true,
        content: 'Atemwege freimachen und Atmung kontrollieren'
      }, 
      {
        content: 'Abtasten nach Zusatzverletzungen hat Priorität.'
      }, 
      {
        content: 'Vor dem Eintreffen des Rettungsdienstes sind keine weiteren Maßnahmen erforderlich.'
      }, 
      {
        valid: true,
        content: 'Falls normale Atmung feststellbar: Stabile Seitenlage'
      }, 
    ],
  },
  {
    id: 2388721,
    question: 'Welche Schutzausrüstung steht dem Sanitäter (Expositionsprophylaxe - Typ 3) zur Verfügung?',
    choices: [
      {
        content: 'ABC-Schutzmaske, Gummistiefel, Partikelfilter'
      },
      {
        valid: true,
        content: 'FFP3-Maske, Schutzbrille und Handschuhe'
      },
      {
        valid: true,
        content: 'Overall der Kategorie CE-Kat III, Typ 4'
      },
      {
        content: 'Bei der Expositionsprophlaxe Typ 3 ist keine eigene Schutzausrüstung erforderlich.'
      },
    ],
  },
  {
    id: 2389612,
    question: 'Welche Schutzkleidung gibt es für das Fahrradfahren oder Inlineskaten?',
    choices: [
      {
        valid: true,
        content: 'Knieschutz'
      },
      {
        valid: true,
        content: 'Helm'
      },
      {
        valid: true,
        content: 'Ellbogenschutz'
      },
      {
        valid: true,
        content: 'Schutzhandschuhe'
      },
    ],
  },
  {
    id: 2389160,
    question: 'Welche Sicherheitseinrichtungen im Einsatzraum gibt es?',
    choices: [
      {
        valid: true,
        content: 'Innere Absperrung'
      },
      {
        valid: true,
        content: 'Äußere Absperrung'
      },
      {
        valid: true,
        content: 'Pforte'
      },
      {
        valid: true,
        content: 'Sicherheitsring'
      },
    ],
  },
  {
    id: 2388206,
    question: 'Welche Sicherheitshinweise gibt es bei der Defibrillation für den Sanitäter?',
    choices: [
      {
        valid: true,
        content: 'Patienten während der Schockabgabe nicht berühren, Sicherheitscheck durchführen'
      },
      {
        valid: true,
        content: 'Mindestabstand zwischen Beatmungsbeutel und Brustkorb (bei Schockabgabe) von einem Meter einhalten'
      },
      {
        valid: true,
        content: 'Reanimationsmaßnahmen sind zu unterbrechen'
      },
      {
        content: 'während der Defibrillation ist ein Mindestabstand von 2,5 m zum Patienten einzuhalten'
      },
    ],
  },
  {
    id: 2388984,
    question: 'Welche Störung liegt beim Diabetes Mellitus vor?',
    choices: [
      {
        content: 'Störung des Elektrolythaushalts'
      },
      {
        content: 'Störung des Wärmehaushalts'
      },
      {
        valid: true,
        content: 'Störung des Kohlenhydratstoffwechsels'
      },
      {
        content: 'Störung des Säure-Basen-Haushalts'
      },
    ],
  },
  {
    id: 2389249,
    question: 'Welche Strukturen sind an der Herzreizleitung beteiligt?',
    choices: [
      {
        valid: true,
        content: 'Sinusknoten'
      },
      {
        valid: true,
        content: 'AV Knoten'
      },
      {
        content: 'Purkinjeknoten'
      },
      {
        content: 'His-Knoten'
      },
    ],
  },
  {
    id: 2387751,
    question: 'Welche Symptome bei einem Nabelschnurvorfall gibt es?',
    choices: [
      {
        valid: true,
        content: 'Vorzeitiger Blasensprung, ein Nabelschnurvorfall ist auch ohne Wehen möglich'
      },
      {
        content: 'Starke Schmerzen, Blasensprung, heraushängende Nabelschnur aus der Scheide'
      },
      {
        content: 'Heraushängende Nabelschnur aus der Scheide, starke Presswehen, vorzeitige Plazentaablösung'
      },
      {
        valid: true,
        content: 'Abgang von Blut/Schleim/Fruchtwasser, heraushängende Nabelschnur aus der Scheide'
      },
    ],
  },
  {
    id: 2387752,
    question: 'Welche Symptome bei einer Eileiterschwangerschaft gibt es?',
    choices: [
      {
        valid: true,
        content: 'Starke Schmerzen im Unterbauch'
      },
      {
        valid: true,
        content: 'Geringe vaginale Blutungen'
      },
      {
        content: 'Gewebeabgang'
      },
      {
        valid: true,
        content: 'Starke innere Blutungen'
      },
    ],
  },
  {
    id: 2387753,
    question: 'Welche Symptome bei einer Fehlgeburt gibt es?',
    choices: [
      {
        valid: true,
        content: 'Vaginale Blutungen'
      },
      {
        valid: true,
        content: 'Wehenartige, ziehende Schmerzen'
      },
      {
        valid: true,
        content: 'Ev. Abgang von Gewebsteilen'
      },
      {
        content: 'Plazentaabgang'
      },
    ],
  },
  {
    id: 2388985,
    question: 'Welche Symptome bei einer Lungenembolie gibt es?',
    choices: [
      {
        valid: true,
        content: 'Zyanose'
      },
      {
        valid: true,
        content: 'Atemabhängiger Brustschmerz'
      },
      {
        valid: true,
        content: 'Bluthusten'
      },
      {
        valid: true,
        content: 'Blutdruckabfall'
      },
    ],
  },
  {
    id: 2387754,
    question: 'Welche Symptome bei einer stumpfen Bauchverletzung gibt es?',
    choices: [
      {
        valid: true,
        content: 'Starke Bauchschmerzen, oftmals ausstrahlende Schmerzen in die Schultergegend, Prellmarken'
      },
      {
        valid: true,
        content: 'Brettharte, gespannte Bauchdecke und Abwehrspannung'
      },
      {
        content: 'Leichte Bauchschmerzen, oftmals ausstrahlende Schmerzen in die Oberarmgegend'
      },
      {
        valid: true,
        content: 'Übelkeit, Brechreiz, Erbrechen, Schockzeichen'
      },
    ],
  },
  {
    id: 2387755,
    question: 'Welche Symptome bei einer vorzeitigen Plazentalösung gibt es?',
    choices: [
      {
        valid: true,
        content: 'Plötzliche eintretende Schmerzen'
      },
      {
        valid: true,
        content: 'Gespannte Bauchdecke'
      },
      {
        valid: true,
        content: 'Vaginale Blutung'
      },
      {
        content: 'Verhärtung der Gebärmutter'
      },
    ],
  },
  {
    id: 2387756,
    question: 'Welche Symptome bei Eklampsie gibt es?',
    choices: [
      {
        valid: true,
        content: 'Ödeme'
      },
      {
        valid: true,
        content: 'Hypertonie'
      },
      {
        valid: true,
        content: 'Krämpfe'
      },
      {
        content: 'Bradykardie'
      },
    ],
  },
  {
    id: 2388986,
    question: 'Welche Symptome bei entzündlichen Unterleibserkrankungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Fieber'
      },
      {
        valid: true,
        content: 'Schmerzen im Unterbauch, oftmals mit starkem Druckschmerz'
      },
      {
        valid: true,
        content: 'Blutungen'
      },
      {
        content: 'Hypotonie'
      },
    ],
  },
  {
    id: 2387757,
    question: 'Welche Symptome bei Epiglottitis gibt es?',
    choices: [
      {
        valid: true,
        content: 'Hohes Fieber'
      },
      {
        valid: true,
        content: 'Inspiratorischer Stridor'
      },
      {
        valid: true,
        content: 'Kloßige Sprache mit Schluckbeschwerden'
      },
      {
        content: 'Bellender Husten'
      },
    ],
  },
  {
    id: 2387758,
    question: 'Welche Symptome beim Pseudokrupp gibt es?',
    choices: [
      {
        valid: true,
        content: 'Langsamer Beginn'
      },
      {
        valid: true,
        content: 'Bellender Husten'
      },
      {
        content: 'Hohes Fieber'
      },
      {
        valid: true,
        content: 'Ein- und Ausatemphase erschwert'
      },
    ],
  },
  {
    id: 2387759,
    question: 'Welche Symptome des kindlichen Fieberkrampfes gibt es?',
    choices: [
      {
        valid: true,
        content: 'Plötzlich rascher Temperaturanstieg'
      },
      {
        valid: true,
        content: 'Muskelzuckungen, Beuge-/Streckkrämpfe'
      },
      {
        valid: true,
        content: 'Erschlaffung und Bewusstseinstrübung'
      },
      {
        valid: true,
        content: 'Nachschlafphase'
      },
    ],
  },
  {
    id: 2388987,
    question: 'Welche Symptome deuten auf ein akutes Abdomen hin?',
    choices: [
      {
        valid: true,
        content: 'Plötzlich einsetzende Bauchschmerzen mit Bauchdeckenspannung'
      },
      {
        valid: true,
        content: 'Durchfall bzw. Stuhlverhalten'
      },
      {
        valid: true,
        content: 'Blähungen und Koliken'
      },
      {
        valid: true,
        content: 'Fieber und Schweißausbruch'
      },
    ],
  },
  {
    id: 2388988,
    question: 'Welche Symptome einer Gehirnhautentzündung gibt es?',
    choices: [
      {
        valid: true,
        content: 'Kopfschmerz, Nackensteife'
      },
      {
        valid: true,
        content: 'Krämpfe'
      },
      {
        valid: true,
        content: 'Lähmungen'
      },
      {
        valid: true,
        content: 'Übelkeit, Erbrechen'
      },
    ],
  },
  {
    id: 2388989,
    question: 'Welche Symptome gibt es bei einem Herzinfarkt?',
    choices: [
      {
        valid: true,
        content: 'Schmerzen in der Brust'
      },
      {
        valid: true,
        content: 'Angst und Vernichtungsgefühl'
      },
      {
        valid: true,
        content: 'Kaltschweißigkeit und Blässe'
      },
      {
        valid: true,
        content: 'Ausstrahlung der Schmerzen in die Arme, den Hals, Kiefer, Oberbauch und/oder Rücken'
      },
    ],
  },
  {
    id: 2388990,
    question: 'Welche Symptome gibt es bei einem Lungenödem?',
    choices: [
      {
        valid: true,
        content: 'Akute Atemnot'
      },
      {
        valid: true,
        content: 'Blaufärbung (Zyanose) der Haut und vor allem der Lippen'
      },
      {
        valid: true,
        content: 'Charakteristische Rasselgeräusche'
      },
      {
        content: 'Erhöhter Blutdruck'
      },
    ],
  },
  {
    id: 2388991,
    question: 'Welche Symptome gibt es bei einer Lungenentzündung?',
    choices: [
      {
        valid: true,
        content: 'Schüttelfrost, hohes Fieber'
      },
      {
        valid: true,
        content: 'Schwere Störung des Allgemeinbefindens'
      },
      {
        content: 'Schmerzen in den Beinen'
      },
    ],
  },
  {
    id: 2387760,
    question: 'Welche Symptome können beim Ertrinkungsnotfall (Unfallmechanismus - primäres und sekundäres Ertrinken) auftreten?',
    choices: [
      {
        valid: true,
        content: 'Verwirrtheit bis Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Feucht-blasse Haut, Zyanose'
      },
      {
        valid: true,
        content: 'Erhöhte Atemfrequenz bis Schnappatmung, Blutdruckabfall'
      },
      {
        valid: true,
        content: 'Erhöhte Pulsfrequenz bis Atem-Kreislauf-Stillstand'
      },
    ],
  },
  {
    id: 2387761,
    question: 'Welche Symptome sind bei Bluthusten möglich?',
    choices: [
      {
        valid: true,
        content: 'Schaumig-blasiger Auswurf'
      },
      {
        valid: true,
        content: 'Atemnot'
      },
      {
        valid: true,
        content: 'Erstickungsangst'
      },
      {
        valid: true,
        content: 'Unruhe'
      },
    ],
  },
  {
    id: 2387762,
    question: 'Welche Symptome sind bei einem erhöhten Hirndruck möglich?',
    choices: [
      {
        valid: true,
        content: 'Sehstörungen'
      },
      {
        valid: true,
        content: 'Zunehmende Bewusstseinsstörungen bis hin zur Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Pupillenveränderungen'
      },
      {
        content: 'Tachykardien'
      },
    ],
  },
  {
    id: 2388992,
    question: 'Welche Symptome sind bei einem Patienten mit einer hypertensiven Krise möglich?',
    choices: [
      {
        valid: true,
        content: 'Schwindel'
      },
      {
        valid: true,
        content: 'Sehstörungen'
      },
      {
        valid: true,
        content: 'Kopfschmerzen'
      },
      {
        valid: true,
        content: 'Gesichtsrötung'
      },
    ],
  },
  {
    id: 2388993,
    question: 'Welche Symptome sind bei einer Tetanie möglich?',
    choices: [
      {
        valid: true,
        content: 'Grimassieren (Karpfenmund)'
      },
      {
        valid: true,
        content: 'Kribbeln an Armen und Beinen'
      },
      {
        valid: true,
        content: 'Symmetrische Pfötchenstellung'
      },
      {
        valid: true,
        content: 'Beschleunigte und vertiefte Atmung'
      },
    ],
  },
  {
    id: 2387763,
    question: 'Welche Symptome sprechen für das Vorliegen einer Commotio Cerebri (Gehirnerschütterung)?',
    choices: [
      {
        content: 'Ansteigen des Blutdruckes, Absinken der Pulsfrequenz'
      },
      {
        content: 'Leitsymptom ist die lange Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Leitsymptom ist die kurze Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Erinnerungslücken, Kopfschmerzen und Übelkeit'
      },
    ],
  },
  {
    id: 2387764,
    question: 'Welche Symptome sprechen für eine Augenverätzung?',
    choices: [
      {
        valid: true,
        content: 'Rötung (verstärkte Durchblutung), Tränenfluss, Lichtscheue, Lidkrampf'
      },
      {
        content: 'Blasse und erweiterte Pupillen'
      },
      {
        content: 'Wenn sich die Pupillen bei Lichteinfall verengen'
      },
      {
        valid: true,
        content: 'Sehstörungen und Lichtscheue'
      },
    ],
  },
  {
    id: 2387765,
    question: 'Welche Symptome sprechen für eine Hirndrucksteigerung?',
    choices: [
      {
        valid: true,
        content: 'Kopfschmerzen, Schwindel, Übelkeit, Erbrechen,Sehstörungen, Pupillenveränderung'
      },
      {
        valid: true,
        content: 'Absinkende Pulsfrequenz (langsamer, kräftiger Puls = Druckpuls), Atemstörungen'
      },
      {
        valid: true,
        content: 'Bewusstseinsstörung bis hin zur Bewusstlosigkeit, Krampfgefahr'
      },
      {
        content: 'Steigende Pulsfrequenz (kräftiger Puls = Druckpuls)'
      },
    ],
  },
  {
    id: 2387766,
    question: 'Welche Symptome treten bei Hitzschlag, Hitzeerschöpfung, Sonnenstich auf?',
    choices: [
      {
        content: 'Heiße, blasse Haut, Bewusstseinsstörung, Übelkeit, Erbrechen, Durchfall'
      },
      {
        content: 'Heiße, rote Haut, fehlende Schweißabsonderung, Koma, Übelkeit, Erbrechen, Hyperventilation, Lähmungen'
      },
      {
        content: 'Heiße, rote Haut, verstärke Schweißabsonderung, Bewusstseinsstörung, Schockzeichen, Durchfall, Hyperventilation, Krämpfe'
      },
      {
        valid: true,
        content: 'Heiße, rote Haut oder Blässe, fehlende Schweißabsonderung oder kalter Schweiß, Bewusstseinsstörung, Kopfschmerzen, evt. Nackensteifheit, Schwindel, Übelkeit, Erbrechen, Hyperventilation, Krämpfe'
      },
    ],
  },
  {
    id: 2387767,
    question: 'Welche Symptome treten beim Verschlucken eines Fremdkörpers beim Kind auf?',
    choices: [
      {
        valid: true,
        content: 'Hustenreiz'
      },
      {
        valid: true,
        content: 'Atemnot'
      },
      {
        valid: true,
        content: 'Zyanose'
      },
      {
        content: 'Entzündung der oberen Luftwege'
      },
    ],
  },
  {
    id: 2388934,
    question: 'Welche Symptome und Komplikationen bei einer Verrenkung gibt es?',
    choices: [
      {
        valid: true,
        content: 'Schmerzen, abnorme Stellung, Bewegungsunfähigkeit'
      },
      {
        valid: true,
        content: 'Überdehnung und Zerreißen der Gelenkskapsel und der Bänder'
      },
      {
        content: 'Leichte Schmerzen, normale Stellung, Bewegung etwas eingeschränkt'
      },
      {
        valid: true,
        content: 'Blutung ins Gelenk'
      },
    ],
  },
  {
    id: 2387768,
    question: 'Welche Symptome und Komplikationen können bei der Einatmung von ätzenden Stoffen auftreten?',
    choices: [
      {
        valid: true,
        content: 'Atemnot, Reizhusten, Schockzeichen'
      },
      {
        valid: true,
        content: 'Lungenödem (oft erst nach Stunden oder Tagen auftretend), Schock'
      },
      {
        content: 'Bluthusten, jedoch keine Atemnot'
      },
      {
        content: 'sofortiges Auftreten von einem Lungenödem'
      },
    ],
  },
  {
    id: 2388681,
    question: 'Welche Technik wird bei der Herzdruckmassage angewendet?',
    choices: [
      {
        valid: true,
        content: 'Be- und Entlastungsphase sollen gleich lang sein'
      },
      {
        content: 'Möglichst kurze Be- und Entlastungsphase'
      },
      {
        valid: true,
        content: 'Notfallpatienten auf eine harte, unnachgiebige Unterlage legen'
      },
      {
        valid: true,
        content: 'Mitte Brustkorb schnell und kräftig niederdrücken'
      },
    ],
  },
  {
    id: 2389264,
    question: 'Welche Teile des Knochengerüstes gibt es?',
    choices: [
      {
        valid: true,
        content: 'Schädel'
      },
      {
        valid: true,
        content: 'Schultergürtel mit oberen Gliedmaßen'
      },
      {
        valid: true,
        content: 'Brustkorb'
      },
      {
        valid: true,
        content: 'Beckengürtel mit unteren Gliedmaßen'
      },
    ],
  },
  {
    id: 2388722,
    question: 'Welche Teile des KTW werden nach Anordnung durch den zuständigen Amtsarzt desinfiziert?',
    choices: [
      {
        content: 'Nur die Kontaktgegenstände'
      },
      {
        valid: true,
        content: 'Der ganze Innenraum des Fahrzeuges'
      },
      {
        content: 'Nur der Fahrerraum'
      },
      {
        content: 'Nur Krankentrage und Tragsessel'
      },
    ],
  },
  {
    id: 2389161,
    question: 'Welche Teile umfasst eine Sanitätshilfsstelle (SanHiSt)?',
    choices: [
      {
        valid: true,
        content: 'Triageraum'
      },
      {
        valid: true,
        content: 'Transportraum'
      },
      {
        valid: true,
        content: 'Behandlungsraum'
      },
      {
        valid: true,
        content: 'Material- und Meldestelle'
      },
    ],
  },
  {
    id: 2387719,
    question: 'Welche typischen Kennzeichen (Erkennen) deuten auf eine Quetschung hin?',
    choices: [
      {
        valid: true,
        content: 'Blaurote Verfärbung (Bluterguss)'
      },
      {
        content: 'Gefühllosigkeit'
      },
      {
        content: 'Abnorme Stellung'
      },
      {
        valid: true,
        content: 'Schwellung, Schmerzen'
      },
    ],
  },
  {
    id: 2388593,
    question: 'Welche typischen Warnzeichen weisen auf einen drohenden Schlaganfall hin?',
    choices: [
      {
        valid: true,
        content: 'Plötzliche Schwäche oder Gefühlsstörungen in einer Körperseite, besonders im Gesicht oder im Arm'
      },
      {
        valid: true,
        content: 'Vorübergehendes Sehen von Doppelbildern'
      },
      {
        valid: true,
        content: 'Plötzlicher Verlust der Sprechfähigkeit oder Schwierigkeiten, Gesprochenes zu verstehen'
      },
      {
        content: 'Nach oben gerichtete Mundwinkel'
      },
    ],
  },
  {
    id: 2388348,
    question: 'Welche Umstände berechtigen einen Sanitäter, den vorläufigen Tod eines Patienten festzustellen?',
    choices: [
      {
        valid: true,
        content: 'Totenflecken'
      },
      {
        valid: true,
        content: 'Totenstarre'
      },
      {
        valid: true,
        content: 'Verwesungserscheinungen'
      },
      {
        valid: true,
        content: 'Absolut tödliche Verletzungen'
      },
    ],
  },
  {
    id: 2388349,
    question: 'Welche Umstände können ein Koma auslösen?',
    choices: [
      {
        valid: true,
        content: 'Gehirnerkrankungen'
      },
      {
        valid: true,
        content: 'Stoffwechselerkrankungen und Vergiftungen'
      },
      {
        valid: true,
        content: 'Schädel-Hirn-Trauma, Störung der Hirndurchblutung'
      },
      {
        content: 'Rippenprellungen'
      },
    ],
  },
  {
    id: 2388994,
    question: 'Welche Umstände können eine Lungenembolie auslösen?',
    choices: [
      {
        content: 'Asthma Bronchiale'
      },
      {
        content: 'Schwangerschaft'
      },
      {
        valid: true,
        content: 'Thrombose in den unteren Extremitäten; Aufstehen nach großen Operationen und langer Bettruhe'
      },
      {
        content: 'Verkalkung einer Lungenarterie'
      },
    ],
  },
  {
    id: 2388723,
    question: 'Welche Unterschiede gibt es zwischen Bakterien und Viren?',
    choices: [
      {
        valid: true,
        content: 'Viren lassen sich durch Antibiotika nicht bekämpfen. Bakterien haben einen eigenen Stoffwechsel'
      },
      {
        content: 'Bakterien brauchen eine andere Zelle zum Leben'
      },
      {
        content: 'Viren lassen sich durch Antibiotika wirksam bekämpfen'
      },
      {
        content: 'Viren haben einen eigenen Stoffwechsel'
      },
    ],
  },
  {
    id: 2388995,
    question: 'Welche Ursachen für ein akutes Abdomen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Entzündungen von Gallenblase, Magen, Darm'
      },
      {
        valid: true,
        content: 'Blutungen im Magen-Darm-Trakt'
      },
      {
        valid: true,
        content: 'Darmverschluss'
      },
      {
        valid: true,
        content: 'Gynäkologische Ursachen'
      },
    ],
  },
  {
    id: 2388996,
    question: 'Welche Ursachen für Krampfanfälle gibt es?',
    choices: [
      {
        content: 'Die Neigung zu Krampfanfällen ist immer genetisch bedingt, d.h. angeboren'
      },
      {
        content: 'Akute Erkrankungen des Magen-Darm-Traktes'
      },
      {
        valid: true,
        content: 'Alle angeborenen oder erworbenen Schädigungen des Gehirns, Hirnverletzungen, Vergiftungen, Erkrankungen'
      },
      {
        content: 'Krampfanfälle haben keine bestimmten Ursachen, sie können aus heiterem Himmel auch bei komplett gesunden Menschen auftreten'
      },
    ],
  },
  {
    id: 2387769,
    question: 'Welche Ursachen für Nasenbluten gibt es?',
    choices: [
      {
        valid: true,
        content: 'Trauma'
      },
      {
        valid: true,
        content: 'Bluthochdruck'
      },
      {
        valid: true,
        content: 'Gerinnungsbeeinflussende Medikamente (Marcoumar), Gerinnungsstörungen'
      },
      {
        valid: true,
        content: 'Rasche Höhenunterschiede (z.B. Tauchen, ...)'
      },
    ],
  },
  {
    id: 2388997,
    question: 'Welche Ursachen kann ein akutes Linksherzversagen haben?',
    choices: [
      {
        valid: true,
        content: 'Herzinfarkt'
      },
      {
        valid: true,
        content: 'Akute Rhythmusstörungen'
      },
      {
        content: 'Weitung der linken Herzkammer infolge einer chronischen Lungenstauung'
      },
      {
        valid: true,
        content: 'Hochdruckkrise'
      },
    ],
  },
  {
    id: 2388350,
    question: 'Welche Ursachen können Bewusstseinsstörungen auslösen?',
    choices: [
      {
        valid: true,
        content: 'Tumore, Metastasen'
      },
      {
        valid: true,
        content: 'Schlaganfall'
      },
      {
        valid: true,
        content: 'Unterkühlung'
      },
      {
        valid: true,
        content: 'Meningitis'
      },
    ],
  },
  {
    id: 2387770,
    question: 'Welche Ursachen können zu Bluterbrechen führen?',
    choices: [
      {
        valid: true,
        content: 'Blutungen aus Krampfadern der Speiseröhre (Ösophagusvarizen)'
      },
      {
        content: 'Hämorrhoiden'
      },
      {
        valid: true,
        content: 'Entzündungen und Tumore in der Speiseröhre, im Magen und im Zwölffingerdarm'
      },
      {
        content: 'Lungeninfarkt'
      },
    ],
  },
  {
    id: 2387771,
    question: 'Welche Ursachen können zu Bluthusten führen?',
    choices: [
      {
        content: 'Bei grippalem Infekt'
      },
      {
        content: 'Beim Vorliegen eines Magengeschwürs (Ulcus Ventriculi)'
      },
      {
        valid: true,
        content: 'Bei schweren Lungenerkrankungen (TBC, Lungenkrebs, Lungeninfarkt usw.)'
      },
      {
        valid: true,
        content: 'Bei Personen mit Herzerkrankungen'
      },
    ],
  },
  {
    id: 2388998,
    question: 'Welche Ursachen können zu einem Lungenödem führen?',
    choices: [
      {
        valid: true,
        content: 'Akutes oder chronisches Herzversagen'
      },
      {
        valid: true,
        content: 'Einatmen von Reizgasen'
      },
      {
        valid: true,
        content: 'Chronische Nierenerkrankung'
      },
      {
        valid: true,
        content: 'Höhenkrankheit'
      },
    ],
  },
  {
    id: 2387772,
    question: 'Welche Ursachen können zu Gewebeschädigungen im Zuge einer Erfrierung führen?',
    choices: [
      {
        valid: true,
        content: 'Gewebeschädigungen durch Wind und kühlen Temperaturen'
      },
      {
        valid: true,
        content: 'Temperaturen ab 6°C, verbunden mit hoher Luftfeuchtigkeit und Frost möglich'
      },
      {
        content: 'Temperaturen ab 26°C, verbunden mit hoher Luftfeuchtigkeit und Frost möglich'
      },
      {
        valid: true,
        content: 'Gewebeschädigungen durch Nässe und kühle Temperaturen'
      },
    ],
  },
  {
    id: 2387773,
    question: 'Welche Ursachen sind für Blutungen in der Nachgeburtsperiode verantwortlich?',
    choices: [
      {
        valid: true,
        content: 'Gerinnungsstörungen'
      },
      {
        valid: true,
        content: 'Muskelschwäche der Gebärmutter'
      },
      {
        valid: true,
        content: 'Unvollständige Plazentalösung'
      },
      {
        content: 'Hypertonie'
      },
    ],
  },
  {
    id: 2388351,
    question: 'Welche Ursachen von Atemstörungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Verlegung der Atemwege'
      },
      {
        valid: true,
        content: 'Störungen der Atemregulation'
      },
      {
        valid: true,
        content: 'Störung der Atemmechanik'
      },
      {
        valid: true,
        content: 'Störung der Lungenbelüftung'
      },
    ],
  },
  {
    id: 2388352,
    question: 'Welche Veränderungen von Pulsfrequenz und Pulsqualität sind beim schockierten Patienten möglich?',
    choices: [
      {
        content: 'Anfänglich leicht erhöht und schlecht tastbar, später nicht erhöht und schlechter tastbar, im fortgeschrittenen Stadium nicht tastbar'
      },
      {
        valid: true,
        content: 'Anfänglich leicht erhöht und gut tastbar, später stark erhöht und schlecht tastbar, im fortgeschrittenen Stadium kaum tastbar'
      },
      {
        content: 'Anfänglich stark erhöht, aber tastbar, später sehr stark erhöht und kaum tastbar, im fortgeschrittenen Stadium nicht tastbar'
      },
      {
        content: 'Anfänglich leicht erniedrigt und gut tastbar, später normal frequent, aber schlecht tastbar, im fortgeschrittenen Stadium kaum tastbar'
      },
    ],
  },
  {
    id: 2388673,
    question: 'Welche Verbände werden in der Ersten Hilfe verwendet?',
    choices: [
      {
        content: 'Gürtelverbände'
      },
      {
        valid: true,
        content: 'Heftpflasterverbände'
      },
      {
        valid: true,
        content: 'Dreiecktuchverbände'
      },
      {
        valid: true,
        content: 'Mullbindenverbände'
      },
    ],
  },
  {
    id: 2389265,
    question: 'Welche Verdauungsorgane gibt es?',
    choices: [
      {
        valid: true,
        content: 'Magen'
      },
      {
        valid: true,
        content: 'Darm (Zwölffingerdarm, Dünndarm und Dickdarm)'
      },
      {
        content: 'Leber und Milz'
      },
      {
        valid: true,
        content: 'Bauchspeicheldrüse'
      },
    ],
  },
  {
    id: 2388594,
    question: 'Welche Vergiftungserscheinungen gibt es?',
    choices: [
      {
        valid: true,
        content: 'Erregungs- und Rauschzustände'
      },
      {
        valid: true,
        content: 'Bewusstseinsstörungen bis Bewusstlosigkeit'
      },
      {
        valid: true,
        content: 'Übelkeit - Erbrechen - Durchfälle'
      },
      {
        valid: true,
        content: 'Pupillenveränderungen, Atemstörungen'
      },
    ],
  },
  {
    id: 2389162,
    question: 'Welche Verhaltensregeln sind bei einem Unfall mit gefährlichen Gütern zu beachten?',
    choices: [
      {
        content: 'Annäherung höchstens auf 30 m'
      },
      {
        content: 'Gefahrengut einsammeln und der Polizei übergeben'
      },
      {
        valid: true,
        content: 'Mindestabstand von 60 m einhalten'
      },
      {
        valid: true,
        content: 'Unfallstelle absichern und Notruf durchführen'
      },
    ],
  },
  {
    id: 2388207,
    question: 'Welche vorbeugenden Maßnahmen gibt es beim Verabreichen eines Medikaments, damit ein Irrtum ausgeschlossen werden kann?',
    choices: [
      {
        valid: true,
        content: 'Kontrolle des Ablaufdatums'
      },
      {
        valid: true,
        content: 'Richtiges Medikament'
      },
      {
        valid: true,
        content: 'Ist die Lösung klar, unverfärbt und frei von Ausflockungen'
      },
      {
        content: 'Farbe der Verpackung'
      },
    ],
  },
  {
    id: 2388684,
    question: 'Welche Vorgehensweise ist korrekt?',
    choices: [
      {
        valid: true,
        content: 'Defi einschalten und den Anweisungen folgen, Herzdruckmassage fortsetzen'
      }, 
      {
        content: 'Defi einschalten und den Anweisungen folgen, Herzdruckmassage stoppen'
      }, 
      {
        content: 'Elektroden aufkleben und dann erst den Defi einschalten'
      },
      {
        content: 'solange eine Beatmung durchgeführt wird keine Elektroden aufkleben'
      }, 
    ],
  },
  {
    id: 2387774,
    question: 'Welche Vorsichtsmaßnahme ergreift der Sanitäter bei selbstmordgefährdeten Patienten?',
    choices: [
      {
        valid: true,
        content: 'Am Anfang möglichst jede Annäherung vermeiden'
      },
      {
        valid: true,
        content: 'Dem Patienten nie den Rücken zudrehen'
      },
      {
        content: 'Den Patienten sofort am Boden festhalten'
      },
      {
        valid: true,
        content: 'Den Patienten nie alleine lassen'
      },
    ],
  },
  {
    id: 2389163,
    question: 'Welche weiteren Elemente des PLS befinden sich im Inneren der PLT?',
    choices: [
      {
        valid: true,
        content: 'Behandlungsprotokoll'
      },
      {
        valid: true,
        content: 'Identifikationsprotokoll'
      },
      {
        valid: true,
        content: 'Selbstklebeetiketten mit der Seriennummer der PLT'
      },
      {
        content: 'Schwarz dreieckige Kleber mit gelbem Rand'
      },
    ],
  },
  {
    id: 2389610,
    question: 'Welche Wunden müssen von einem Arzt versorgt werden?',
    choices: [
      {
        valid: true,
        content: 'Bisswunden'
      },
      {
        valid: true,
        content: 'tiefe, lange Wunden'
      },
      {
        content: 'kleine Schürfwunden'
      },
      {
        valid: true,
        content: 'Wenn der Betroffene nicht gegen Wundstarrkrampf geimpft ist.'
      },
    ],
  },
  {
    id: 2387775,
    question: 'Welche Wunden zählen zu den mechanischen Wunden?',
    choices: [
      {
        valid: true,
        content: 'Schnitt- und Stichwunden, Quetschungen, Platzwunden, Riss- und Schürfwunden'
      },
      {
        content: 'Erfrierungen und Verbrennungen'
      },
      {
        valid: true,
        content: 'Kratz- und Bisswunden, Schussverletzungen, Wunden mit Fremdkörpern'
      },
      {
        content: 'Verätzungen'
      },
    ],
  },
  {
    id: 2388674,
    question: 'Welche Zeichen deuten auf einen Schlangenbiss hin?',
    choices: [
      {
        content: 'Starke Blutung'
      },
      {
        valid: true,
        content: 'Punktförmige Wunden (Fangmarken) in Stecknadelkopfgröße'
      },
      {
        content: 'Im Bereich der Bissstelle treten Lähmungserscheinungen auf'
      },
      {
        valid: true,
        content: 'Vergiftungserscheinungen möglich, jedoch im Allgemeinen (heimische Schlangen) nicht lebensbedrohend'
      },
    ],
  },
  {
    id: 2387776,
    question: 'Welche Zeichen sind bei einer tiefer gehenden Erfrierung möglich?',
    choices: [
      {
        valid: true,
        content: 'Bewegungseinschränkung der betroffenen Körperteile'
      },
      {
        valid: true,
        content: 'Empfindungslosigkeit (bei Berührung) und starke Schmerzen'
      },
      {
        content: 'Kälteschock'
      },
      {
        valid: true,
        content: 'Blasenbildung mit weiß bis blaugrau marmorierter Verfärbung der Haut'
      },
    ],
  },
  {
    id: 2387777,
    question: 'Welche Zeichen sprechen für den Geburtsbeginn?',
    choices: [
      {
        content: 'Regelmäßige Wehen in 3-Minuten-Abständen über 1 Stunde'
      },
      {
        valid: true,
        content: '2-3 regelmäßige Wehen (Dauer ca. 30 Sek.) innerhalb von 10 min'
      },
      {
        valid: true,
        content: 'Blasensprung und Fruchtwasserabgang'
      },
      {
        content: 'Unregelmäßige leichte Wehen im Abstand von 15 - 30 Minuten'
      },
    ],
  },
  {
    id: 2387778,
    question: 'Welche Zeichen sprechen für ein Vena-cava-Kompressionssyndrom?',
    choices: [
      {
        valid: true,
        content: 'Schwindel'
      },
      {
        valid: true,
        content: 'Übelkeit'
      },
      {
        valid: true,
        content: 'Kollaps'
      },
      {
        content: 'Hypertonie'
      },
    ],
  },
  {
    id: 2387779,
    question: 'Welche Zeichen/Symptome können bei einer Unterkühlung auftreten?',
    choices: [
      {
        content: 'Massive Beschleunigung der Atmung, ansonsten keine Beeinträchtigung'
      },
      {
        valid: true,
        content: 'Der Unterkühlte empfindet anfangs heftige Schmerzen'
      },
      {
        valid: true,
        content: 'Der Unterkühlte wird teilnahmslos und müde, die Atmung verlangsamt sich'
      },
      {
        valid: true,
        content: 'Der Patient fühlt sich schließlich beschwerdefrei und schläft ein'
      },
    ],
  },
  {
    id: 2389266,
    question: 'Welche Zusammensetzung des Blutes gibt es?',
    choices: [
      {
        content: 'Blut besteht aus 45% Flüssigkeit (Plasma)'
      },
      {
        content: 'Blut besteht aus 55% festen Bestandteilen'
      },
      {
        valid: true,
        content: 'Blut besteht aus 55% Flüssigkeit (Plasma)'
      },
      {
        valid: true,
        content: 'Blut besteht zu 45% aus festen Bestandteilen'
      },
    ],
  },
  {
    id: 2388439,
    question: 'Welchen belastenden Einflüssen ist der Patient beim Transport ausgesetzt?',
    choices: [
      {
        valid: true,
        content: 'Fliehkräfte (Beschleunigung, Bremsen und Kurven fahren)'
      },
      {
        content: 'Zugkräfte'
      },
      {
        valid: true,
        content: 'Jeder Transport stellt für den Patienten eine psychische Belastung dar'
      },
      {
        valid: true,
        content: 'Schwingungen und Lärm'
      },
    ],
  },
  {
    id: 2388866,
    question: 'Welchen Grundsatz soll jeder Ersthelfer in einer Gefahrenzone beachten?',
    choices: [
      {
        content: 'Es gibt kein Gefahrenpotential bei der Leistung von Erster Hilfe'
      },
      {
        valid: true,
        content: 'Selbstschutz geht vor Fremdschutz!'
      },
      {
        content: 'Fremdschutz geht vor Eigenschutz!'
      },
      {
        content: 'Einer für alle - alle für Einen!'
      },
    ],
  },
  {
    id: 2388208,
    question: 'Welchen Messwert erhalten Sie beim Blutdruckmessen ohne Stethoskop?',
    choices: [
      {
        content: 'Den diastolischen Blutdruck'
      },
      {
        valid: true,
        content: 'Den systolischen Blutdruckwert'
      },
      {
        content: 'Keinen Messwert, da eine Messung ohne Stethoskop nicht möglich ist'
      },
      {
        content: 'Keine aussagekräftigen Messwerte, da die Methode zu ungenau ist'
      },
    ],
  },
  {
    id: 2388209,
    question: 'Welchen Patienten werden 10-15l/min Sauerstoff über die Maske verabreicht?',
    choices: [
      {
        valid: true,
        content: 'Tauchunfall, Bergung aus vergifteter Atmosphäre, Thoraxtrauma'
      },
      {
        valid: true,
        content: 'Atemnot aufgrund interner Erkrankungen (Lungenödem, ...)'
      },
      {
        valid: true,
        content: 'Akute Atemwegsbehinderung durch Schwellung, Fremdkörper'
      },
      {
        valid: true,
        content: 'Verdacht auf SHT, Polytrauma'
      },
    ],
  },
  {
    id: 2388210,
    question: 'Welchen Patienten werden 6-8l/min Sauerstoff über die Maske verabreicht?',
    choices: [
      {
        content: 'Allen Notfallpatienten, jedoch nur Erwachsenen'
      },
      {
        valid: true,
        content: 'Nach erfolgter Reanimation, Angina pectoris/Herzinfarkt'
      },
      {
        valid: true,
        content: 'Generell allen Notfallpatienten aller Altersklassen'
      },
      {
        valid: true,
        content: 'Asthma bronchiale, COPD'
      },
    ],
  },
  {
    id: 2388211,
    question: 'Welchen Patienten wird kein Sauerstoff verabreicht?',
    choices: [
      {
        content: 'Bewusstlosen'
      },
      {
        valid: true,
        content: 'Patienten mit Hyperventilationstetanie'
      },
      {
        content: 'Stark alkoholisierten Patienten wegen der Gefahr der Selbstentzündung.'
      },
      {
        content: 'Hyperthermiepatienten'
      },
    ],
  },
  {
    id: 2389267,
    question: 'Welchen Verlauf nimmt das Blut durch den menschlichen Körper?',
    choices: [
      {
        content: 'Rechter Vorhof - rechte Herzkammer - Lungenarterie - Lungenvene - linke Herzkammer - linker Vorhof - Körperarterien'
      },
      {
        content: 'Rechte Herzkammer - rechter Vorhof - Lungenvene - Lunge - Lungenarterie - linker Vorhof - linke Herzkammer - Körperarterien'
      },
      {
        valid: true,
        content: 'Rechter Vorhof - rechte Herzkammer - Lungenarterie - Lunge - Lungenvene - linker Vorhof - linke Herzkammer - Körperarterien - Körpervenen - rechter Vorhof'
      },
      {
        content: 'Linker Vorhof - linke Herzkammer - Lungenarterien - Lungenvenen - rechter Vorhof - rechte Herzkammer'
      },
    ],
  },
  {
    id: 2388212,
    question: 'Welchen Vorteil für den Patienten bietet die Lagerung auf einer nicht evakuierten Vakuummatratze?',
    choices: [
      {
        content: 'Der Patient liegt auf einer harten Unterlage'
      },
      {
        content: 'Die negativen Auswirkungen der Beschleunigungskräfte werden dadurch gemildert'
      },
      {
        valid: true,
        content: 'Die negativen Auswirkungen der mechanischen Schwingungen (Vibrationen) werden für den Patienten gemildert, Isolierfunktion'
      },
      {
        content: 'Entspannung der Bauchmuskulatur'
      },
    ],
  },
  {
    id: 2389268,
    question: 'Welchen Zweck erfüllt die Atmung?',
    choices: [
      {
        valid: true,
        content: 'Zufuhr von Sauerstoff'
      },
      {
        content: 'Zufuhr von Kohlendioxid'
      },
      {
        valid: true,
        content: 'Abgabe von Kohlendioxid'
      },
      {
        content: 'Zufuhr von Stickstoff'
      },
    ],
  },
  {
    id: 2389269,
    question: 'Welcher Bestandteil des Blutes dient der Abwehr gegen Krankheitserreger?',
    choices: [
      {
        content: 'Die roten Blutkörperchen'
      },
      {
        valid: true,
        content: 'Die weißen Blutkörperchen'
      },
      {
        content: 'Die Blutplättchen'
      },
      {
        content: 'Der Sauerstoff'
      },
    ],
  },
  {
    id: 2388999,
    question: 'Welcher dieser Notfallpatienten muss in die prophylaktische Seitenlage gebracht werden?',
    choices: [
      {
        valid: true,
        content: 'Vergiftungen'
      },
      {
        valid: true,
        content: 'Bei Verdacht auf Schlaganfall'
      },
      {
        valid: true,
        content: 'Nach einem epileptischen Anfall'
      },
      {
        valid: true,
        content: 'Verdacht auf SHT'
      },
    ],
  },
  {
    id: 2387780,
    question: 'Welcher Körperteil geht bei einer normalen Geburt voran?',
    choices: [
      {
        content: 'Die Beine'
      },
      {
        content: 'Die Arme'
      },
      {
        valid: true,
        content: 'Der Kopf'
      },
      {
        content: 'Das Becken'
      },
    ],
  },
  {
    id: 2388440,
    question: 'Welcher Sicherheitsabstand ist bei Hochspannungsunfällen einzuhalten?',
    choices: [
      {
        content: 'Mind. 3 m'
      },
      {
        valid: true,
        content: 'Mind. 10 m'
      },
      {
        content: 'Mind. 15 m'
      },
      {
        content: 'Mind. 20 m'
      },
    ],
  },
  {
    id: 2389164,
    question: 'Welcher Sicherheitsabstand ist bei Unfällen mit gefährlichen Gütern einzuhalten?',
    choices: [
      {
        content: 'Mindestens 10-20 m'
      },
      {
        content: 'Mindestens 30-60 m'
      },
      {
        valid: true,
        content: 'Mindestens 60 m'
      },
      {
        content: 'Mindestens 100 m'
      },
    ],
  },
  {
    id: 2387781,
    question: 'Welcher Verdacht besteht bei ungleich weiten Pupillen?',
    choices: [
      {
        content: 'Herzstillstand'
      },
      {
        content: 'Vergiftung'
      },
      {
        valid: true,
        content: 'Raumfordernder Prozess im Schädelinneren (z.B. Blutung)'
      },
      {
        content: 'Atemstillstand'
      },
    ],
  },
  {
    id: 2388441,
    question: 'Welcher Wert bildet die Grenze zwischen Hoch- und Niederspannung?',
    choices: [
      {
        content: 'Hochspannung ab 10 000 V'
      },
      {
        content: 'Niederspannung ab 100 V'
      },
      {
        valid: true,
        content: 'Hochspannung ab 1000 V'
      },
      {
        valid: true,
        content: 'Niederspannung bis 1000 V'
      },
    ],
  },
  {
    id: 2389270,
    question: 'Welcher wichtige Atemmuskel trennt den Brustraum vom Bauchraum?',
    choices: [
      {
        content: 'Bauchfell'
      },
      {
        content: 'Lungenfell'
      },
      {
        content: 'Rippenfell'
      },
      {
        valid: true,
        content: 'Zwerchfell'
      },
    ],
  },
  {
    id: 2388675,
    question: 'Welcher Zeitraum ist zur Versorgung von Wunden durch einen Arzt maßgeblich?',
    choices: [
      {
        content: 'Innerhalb von 2 Stunden'
      },
      {
        content: 'Innerhalb von 4 Stunden'
      },
      {
        valid: true,
        content: 'Innerhalb von 6 Stunden'
      },
      {
        valid: true,
        content: 'Bei ausgedehnten Verletzungsmustern sofort'
      },
    ],
  },
  {
    id: 2389000,
    question: 'Welches Krankheitsbild könnte bei folgenden Patienten vorliegen: Hat  kurzzeitig gekrampft, eingenässt und blutet leicht aus dem Mund?',
    choices: [
      {
        content: 'Coma Diabeticum'
      },
      {
        valid: true,
        content: 'Epileptischer Anfall'
      },
      {
        content: 'Vasovagale Synkope'
      },
      {
        content: 'Adam-Stokes-Anfall'
      },
    ],
  },
  {
    id: 2387782,
    question: 'Welches Krankheitsbild liegt bei folgenden Symptomen vor: 3 Monate alter Säugling liegt krampfend und zyanotisch in seinem Bett?',
    choices: [
      {
        content: 'Hirnblutung'
      },
      {
        content: 'Epiglottitis'
      },
      {
        valid: true,
        content: 'Fieberkrampf'
      },
      {
        content: 'SIDS'
      },
    ],
  },
  {
    id: 2389001,
    question: 'Welches Krankheitsbild liegt bei folgenden Symptomen vor: heftige Schmerzen in der linken, unteren Extremität. Das Bein fühlt sich kühl und pulslos an?',
    choices: [
      {
        content: 'Venenverschluss'
      },
      {
        valid: true,
        content: 'Arterienverschluss'
      },
      {
        content: 'Pulmonale Embolie'
      },
      {
        content: 'Kardiale Embolie'
      },
    ],
  },
  {
    id: 2389002,
    question: 'Welches Krankheitsbild liegt vor, wenn ein Patient bei der Atmung deutliche Rasselgeräusche zeigt?',
    choices: [
      {
        content: 'Lungenentzündung'
      },
      {
        valid: true,
        content: 'Lungenödem (kardial, toxisch oder neurogen bedingt)'
      },
      {
        content: 'Lungenembolie'
      },
      {
        content: 'Asthma Bronchiale'
      },
    ],
  },
  {
    id: 2387783,
    question: 'Welches Material wird für die Bedeckung von Brandwunden verwendet?',
    choices: [
      {
        valid: true,
        content: 'Notfalls mit frischen Leintüchern'
      },
      {
        valid: true,
        content: 'Keimfreie Wundauflagen aus Kunststoff'
      },
      {
        content: 'Notfalls mit feuchten Papiertaschentüchern'
      },
      {
        valid: true,
        content: 'Keimfreie Wundauflagen aus Metallbeschichtung'
      },
    ],
  },
  {
    id: 2388213,
    question: 'Welches Material wird für die Blutzuckermessung benötigt?',
    choices: [
      {
        valid: true,
        content: 'Messgerät'
      },
      {
        valid: true,
        content: 'Alkotupfer'
      },
      {
        valid: true,
        content: 'Stechhilfe'
      },
      {
        content: 'Stauschlauch'
      },
    ],
  },
  {
    id: 2387784,
    question: 'Welches Material wird zur Abnabelung benötigt?',
    choices: [
      {
        valid: true,
        content: '2 sterile Klemmen (oder Bändchen)'
      },
      {
        valid: true,
        content: 'Sterile Schere stumpf-stumpf gebogen'
      },
      {
        content: 'Keimfreie Schere spitz-stumpf gerade'
      },
      {
        valid: true,
        content: 'Tupfer und Nabelbinde'
      },
    ],
  },
  {
    id: 2388353,
    question: 'Welches Material, außer der Dreiecktuchkrawatte, kann der Sanitäter für eine Abbindung am Oberarm verwenden?',
    choices: [
      {
        content: 'Die elastische Binde'
      },
      {
        content: 'Die Mullbinde'
      },
      {
        valid: true,
        content: 'Die Blutdruckmanschette vorzugsweise anwenden'
      },
      {
        content: 'Den Stauschlauch vorzugsweise anwenden'
      },
    ],
  },
  {
    id: 2389165,
    question: 'Welches oberste Prinzip müssen Sanitäter bei einem Gefahrengutunfall beachten?',
    choices: [
      {
        valid: true,
        content: 'Kontakt mit dem Gefahrengut vermeiden = Selbstschutz'
      },
      {
        content: 'Langsam fahren und Feuerwehr überholen lassen'
      },
      {
        content: 'Einmalhandschuhe tragen'
      },
      {
        content: 'Persönliche Schutzausrüstung tragen'
      },
    ],
  },
  {
    id: 2389271,
    question: 'Welches Organ reagiert auf Sauerstoffmangel besonders empfindlich?',
    choices: [
      {
        content: 'Leber'
      },
      {
        valid: true,
        content: 'Gehirn'
      },
      {
        content: 'Herz'
      },
      {
        content: 'Lunge'
      },
    ],
  },
  {
    id: 2388442,
    question: 'Welches Schutzverhalten muss bei der Rettung von CO-Vergifteten beachtet werden?',
    choices: [
      {
        content: 'Kerzenprobe anwenden'
      },
      {
        valid: true,
        content: 'Rettung durch Feuerwehr'
      },
      {
        valid: true,
        content: 'Sauerstoff und Energie (es genügt schon die Betätigung einer elektrischen Klingel) fernhalten, da es sonst zur Explosion kommen kann'
      },
      {
        content: 'Taschentuch vor Mund und Nase halten'
      },
    ],
  },
  {
    id: 2387785,
    question: 'Welches Verhältnis von Herzdruckmassage und Beatmung ist beim Neugeborenen anzuwenden?',
    choices: [
      {
        content: 'Herzdruckmassage und Beatmung 5:1'
      },
      {
        content: 'Herzdruckmassage und Beatmung 30:1'
      },
      {
        valid: true,
        content: 'Herzdruckmassage und Beatmung 3:1'
      },
      {
        content: 'Herzdruckmassage und Beatmung 15:2'
      },
    ],
  },
  {
    id: 2388354,
    question: 'Welches Verhältnis zwischen Herzdruckmassage und Beatmung ist bei Erwachsenen anzuwenden?',
    choices: [
      {
        content: '30:1'
      },
      {
        content: '5 : 1'
      },
      {
        content: '15:2'
      },
      {
        valid: true,
        content: '30:2'
      },
    ],
  },
  {
    id: 2388724,
    question: 'Welches Ziel hat die hygienische Händedesinfektion und wann erfolgt die Durchführung?',
    choices: [
      {
        valid: true,
        content: 'Hygienische Händedesinfektion vor und nach jedem Patientenkontakt'
      },
      {
        content: 'Das Ziel der hygienischen Händedesinfektion ist die Reduzierung von pathogenen Keimen, 4 Schritte'
      },
      {
        valid: true,
        content: 'Das Ziel der hygienischen Händedesinfektion ist die Reduzierung von pathogenen Keimen, 6 Schritte'
      },
      {
        content: 'Sorgfältige Händedesinfektion ist nur nach dem Kontakt mit Blut oder Ausscheidungen erforderlich'
      },
    ],
  },
  {
    id: 2389616,
    question: 'Wenn du den Notruf anrufst…',
    choices: [
      {
        valid: true,
        content: '... fragt dich die Leitstelle nach allen Informationen, die sie benötigen.'
      },
      {
        valid: true,
        content: '... wird dir schnell weitergeholfen.'
      },
      {
        content: '... musst du die 4 W – Fragen in der richtigen Reihenfolge sagen.'
      },
      {
        valid: true,
        content: '... gibt dir die Leitstelle genaue Anweisungen, was du tun kannst.'
      },
    ],
  },
  {
    id: 2388685,
    question: 'Wenn ein Mensch ohne Bewusstsein nicht mehr normal atmet, rufen Sie die Rettung und...?',
    choices: [
      {
        content: 'führen eine stabile Seitenlage durch.'
      }, 
      {
        content: 'führen sofort Beatmungen durch.'
      }, 
      {
        valid: true,
        content: 'beginnen Sie sofort mit der Herzdruckmassage.'
      }, 
      {
        content: 'versuchen eine Lagerung mit erhöhten Beinen.'
      }, 
    ],
  },
  {
    id: 2388618,
    question: 'Wer darf bei einem psychisch kranken Patienten einen Transport gegen dessen Willen durchführen?',
    choices: [
      {
        valid: true,
        content: 'Die Exekutive'
      },
      {
        content: 'Der Krankentransportdienst einer Nervenheilanstalt'
      },
      {
        content: 'Nur speziell geschulte COBRA-Beamte'
      },
      {
        valid: true,
        content: 'Der Rettungsdienst leistet dabei sanitätstechnische Unterstützung'
      },
    ],
  },
  {
    id: 2389571,
    question: 'Wer führt die psychosoziale Unterstützung bei Angehörigen durch?',
    choices: [
      {
        content: 'Peer (SvE)'
      },
      {
        content: 'Sozialdienst'
      },
      {
        valid: true,
        content: 'Kriseninterventionsteams (KI)'
      },
      {
        content: 'Hauskrankenpflege'
      },
    ],
  },
  {
    id: 2389572,
    question: 'Wer führt die psychosoziale Unterstützung bei Mitarbeitern durch?',
    choices: [
      {
        valid: true,
        content: 'Peer (SvE)'
      },
      {
        content: 'Sozialdienst'
      },
      {
        content: 'Kriseninterventionsteams (KI)'
      },
      {
        content: 'Hauskrankenpflege'
      },
    ],
  },
  {
    id: 2388443,
    question: 'Wer ist bei Rettungseinsätzen im Gleisbereich (meist ÖBB) mit der Einsatzleitung betraut?',
    choices: [
      {
        valid: true,
        content: 'ÖBB-Einsatzleiter'
      },
      {
        content: 'ÖBB Lokführer'
      },
      {
        content: 'ÖBB-Notfallleitstelle'
      },
      {
        content: 'Zugbegleiter'
      },
    ],
  },
  {
    id: 2388725,
    question: 'Wie (Methode) wird völlige Keimfreiheit erzielt?',
    choices: [
      {
        content: 'Dekorporation'
      },
      {
        content: 'Dekontamination'
      },
      {
        valid: true,
        content: 'Sterilisation'
      },
      {
        content: 'Desinfektion'
      },
    ],
  },
  {
    id: 2387786,
    question: 'Wie bezeichnet man die Erscheinung, wenn 48 Stunden nach dem Ertrinkungsunfall eine rasche Verschlechterung des Allgemeinzustandes eintritt?',
    choices: [
      {
        content: 'Taucherkrankheit'
      },
      {
        content: 'Dekompressionskrankheit'
      },
      {
        content: 'Caissonkrankheit'
      },
      {
        valid: true,
        content: 'Sekundäres Ertrinken'
      },
    ],
  },
  {
    id: 2389272,
    question: 'Wie entstehen systolischer und diastolischer Blutdruck?',
    choices: [
      {
        content: 'Durch den atemabhängigen Blutrückfluss zum Herzen'
      },
      {
        content: 'Durch das Schlagen der Herzscheidewand'
      },
      {
        content: 'Durch den teilweisen Blutrückfluss in das Herz'
      },
      {
        valid: true,
        content: 'Durch Zusammenziehen und Erschlaffen der Herzkammern'
      },
    ],
  },
  {
    id: 2387787,
    question: 'Wie entsteht eine mechanische Augenverletzung und welche Komplikation kann dabei auftreten?',
    choices: [
      {
        valid: true,
        content: 'Durch äußere Gewalteinwirkung, z.B. durch einen Stich oder durch einen Fremdkörper, es handelt sich häufig um Arbeits-, weniger um Freizeitverletzungen'
      },
      {
        content: 'Durch eine Erkrankung des Auges'
      },
      {
        valid: true,
        content: 'Im Extremfall Erblindung des betroffenen Auges'
      },
      {
        content: 'Rötung des Auges und Schwellung im Bereich des Auges, die jedoch rasch abheilt und keine Komplikation erwarten lässt'
      },
    ],
  },
  {
    id: 2388355,
    question: 'Wie entwickelt sich ein Schock (Verlauf)?',
    choices: [
      {
        valid: true,
        content: 'Haut, Muskulatur, Darm und Niere von der Durchblutung ausgeschlossen'
      },
      {
        valid: true,
        content: 'Zentralisierung des Kreislaufs'
      },
      {
        valid: true,
        content: 'Mikrozirkulationsstörung - Gefäßlähmung - Multiorganversagen, Tod'
      },
      {
        content: 'Dezentralisierung des Kreislaufs, allgemeine Maßnahmen verhindern diese Symptomatik'
      },
    ],
  },
  {
    id: 2389273,
    question: 'Wie erfolgt die Blutversorgung des Herzmuskels?',
    choices: [
      {
        valid: true,
        content: 'Herzkranzgefäße'
      },
      {
        content: 'Erfolgt selbstständig'
      },
      {
        content: 'Herzvenen'
      },
      {
        content: 'Herzarterien'
      },
    ],
  },
  {
    id: 2388444,
    question: 'Wie erfolgt die korrekte Annäherung des Sanitäters zum NAH?',
    choices: [
      {
        content: 'Die Annäherung erfolgt von der Heckseite'
      },
      {
        valid: true,
        content: 'Pilot gibt Zeichen zur Annäherung'
      },
      {
        valid: true,
        content: 'Nach Stillstand der Rotorblätter'
      },
      {
        valid: true,
        content: 'Annäherung von vorne in gebückter Haltung'
      },
    ],
  },
  {
    id: 2388356,
    question: 'Wie erfolgt die korrekte Durchführung eines Notfallchecks (Kontrolle der Lebensfunktionen)?',
    choices: [
      {
        content: 'Atmung, Bewusstsein, Lebenszeichen kontrollieren'
      },
      {
        valid: true,
        content: 'Bewusstsein, Atmung, Lebenszeichen kontrollieren'
      },
      {
        content: 'Lebenszeichen, Atmung, Bewusstsein kontrollieren'
      },
      {
        content: 'Atmung, Lebenszeichen, Bewusstsein kontrollieren'
      },
    ],
  },
  {
    id: 2388357,
    question: 'Wie erfolgt die Lagerung bei einem Volumenmangelschock bzw. wann ist trotz bestehendem Volumenmangelschock von dieser Lagerung Abstand zu nehmen?',
    choices: [
      {
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        valid: true,
        content: 'Flache Rückenlagerung, Beine hoch lagern'
      },
      {
        valid: true,
        content: 'Kontraindikationen wären: Kopfverletzungen, Atemnot, Verdacht auf Verletzung im Wirbelsäulenbereich, Becken, Beinbruch'
      },
      {
        content: 'In jedem Fall flache Rückenlage mit erhöht gelagerten Beinen'
      },
    ],
  },
  {
    id: 2388726,
    question: 'Wie erfolgt die Übertragung von Infektionskrankheiten?',
    choices: [
      {
        valid: true,
        content: 'Tröpfcheninfektion (Husten, Niesen, ...)'
      },
      {
        valid: true,
        content: 'Direkter Kontakt mit der Infektionsquelle, Keime werden durch die Luft weiterverbreitet (aerogen)'
      },
      {
        valid: true,
        content: 'Mit Stuhlkeimen verunreinigte Nahrungsmittel oder Wasser (fäko-oral)'
      },
      {
        content: 'Mit Stuhlkeimen verunreinigte Nahrungsmittel oder Wasser (aerogen)'
      },
    ],
  },
  {
    id: 2389640,
    question: 'Wie erkennst du, ob eine reglose Person ohne Bewusstsein ist?',
    choices: [
      {
        valid: true,
        content: 'lautes ansprechen'
      },
      {
        content: 'ins Ohr zwicken'
      },
      {
        valid: true,
        content: 'sanftes schütteln an den Schultern'
      },
      {
        content: 'Hände schütteln'
      },
    ],
  },
  {
    id: 2389003,
    question: 'Wie erkennt man eine Hyperglykämie?',
    choices: [
      {
        valid: true,
        content: 'Langsame Entwicklung von Durstgefühl'
      },
      {
        valid: true,
        content: 'Vermehrte Harnausscheidung'
      },
      {
        valid: true,
        content: 'Ausatemluft riecht nach Aceton'
      },
      {
        valid: true,
        content: 'Kußmaulsche Atmung'
      },
    ],
  },
  {
    id: 2389274,
    question: 'Wie errechnet man das Atemminutenvolumen (AMV)?',
    choices: [
      {
        content: 'Atemfrequenz x Lungengröße'
      },
      {
        content: 'Atemzugvolumen x Lungengröße'
      },
      {
        valid: true,
        content: 'Atemzugvolumen x Atemfrequenz'
      },
      {
        content: 'Atemzugvolumen / Atemfrequenz'
      },
    ],
  },
  {
    id: 2389275,
    question: 'Wie errechnet man das Atemzugvolumen (AZV)?',
    choices: [
      {
        content: 'Atemminutenvolumen mal 10'
      },
      {
        valid: true,
        content: 'Das AZV beträgt das Zehnfache des Körpergewichtes (KG) in Milliliter (ml) / AZV = KG x 10 (ca. 500-800 ml)'
      },
      {
        content: 'Das AZV beträgt das Zweifache des Körpergewichtes (KG) in Milliliter (ml) / AZV = KG x 2 (ca. 150-200 ml)'
      },
      {
        content: 'Atemminutenvolumen mal Atemfrequenz minus Totraum'
      },
    ],
  },
  {
    id: 2388214,
    question: 'Wie eruiert man die richtige Größe des Guedel-Tubus?',
    choices: [
      {
        valid: true,
        content: 'Abstand: Mundwinkel - Ohrläppchen'
      },
      {
        content: 'Abstand: Mundwinkel - Nasenspitze'
      },
      {
        content: 'Abstand: Mundwinkel - Ohrmuschel'
      },
      {
        content: 'Abstand: Mundwinkel - Kinn'
      },
    ],
  },
  {
    id: 2387788,
    question: 'Wie führen Sie das Freimachen der Atemwege beim Kind durch?',
    choices: [
      {
        content: 'Kopf nicht überstrecken, da ansonsten die oberen Atemwege verschlossen werden'
      },
      {
        content: 'Kopf in Neutralstellung bringen'
      },
      {
        valid: true,
        content: 'Kopf geringfügig überstrecken'
      },
      {
        valid: true,
        content: 'Kopf gerade richten'
      },
    ],
  },
  {
    id: 2387789,
    question: 'Wie führen Sie die Atem- und Kreislaufkontrolle beim Neugeborenen durch?',
    choices: [
      {
        content: '1. Sanitäter: Atemkontrolle, Sehen , Hören, Fühlen max. 15 Sekunden'
      },
      {
        valid: true,
        content: '1. Sanitäter: Atemkontrolle, Sehen, Hören, Fühlen  max. 10 Sekunden'
      },
      {
        valid: true,
        content: '2. Sanitäter: gleichzeitige Kreislaufkontrolle mit dem Stethoskop über der Herzspitze'
      },
      {
        content: '2. Sanitäter: gleichzeitige Kreislaufkontrolle an der Innenseite Oberarm'
      },
    ],
  },
  {
    id: 2387790,
    question: 'Wie führen Sie die Kreislaufkontrolle beim Neugeborenen durch?',
    choices: [
      {
        valid: true,
        content: 'Mit dem Stethoskop'
      },
      {
        content: 'An der Innenseite der Oberarme'
      },
      {
        content: 'An zwei unterschiedlichen Stellen (nacheinander)'
      },
      {
        valid: true,
        content: 'Über der Herzspitze'
      },
    ],
  },
  {
    id: 2388697,
    question: 'Wie gehen Sie bei der Wiederbelebung als trainierter Ersthelfer vor?',
    choices: [
      {
        content: '3 Herzdruckmassagen / 1 Beatmung'
      }, 
      {
        content: '10 Herzdruckmassagen / 6 Beatmungen'
      }, 
      {
        content: '15 Herzdruckmassagen / 2 Beatmungen'
      }, 
      {
        valid: true,
        content: '30 Herzdruckmassagen / 2 Beatmungen'
      },
    ],
  },
  {
    id: 2389276,
    question: 'Wie groß ist der Totraum beim Erwachsenen?',
    choices: [
      {
        content: 'ca. 1000 ml'
      },
      {
        content: 'ca. 50 ml'
      },
      {
        valid: true,
        content: 'ca. 150 ml'
      },
      {
        content: 'ca. 500 ml'
      },
    ],
  },
  {
    id: 2389277,
    question: 'Wie heißen die Blutgefäße, die das Herz selbst mit Blut versorgen?',
    choices: [
      {
        content: 'Herzkranzvenen'
      },
      {
        content: 'Myocardvenen'
      },
      {
        valid: true,
        content: 'Herzkranzgefäße'
      },
      {
        content: 'Myocardarterien'
      },
    ],
  },
  {
    id: 2387791,
    question: 'Wie heißen die Geburtsperioden?',
    choices: [
      {
        valid: true,
        content: 'Eröffnungsperiode'
      },
      {
        valid: true,
        content: 'Austreibungsperiode'
      },
      {
        content: 'Senkungsperiode'
      },
      {
        valid: true,
        content: 'Nachgeburtsperiode'
      },
    ],
  },
  {
    id: 2387792,
    question: 'Wie heißt der Fachausdruck für den Mutterkuchen, der in der Nähe des Gebärmutterhalses liegt und den Ausgang ganz oder teilweise überdeckt?',
    choices: [
      {
        valid: true,
        content: 'Placenta Praevia'
      },
      {
        content: 'Abortus'
      },
      {
        content: 'Eklampsie'
      },
      {
        content: 'Partus'
      },
    ],
  },
  {
    id: 2388215,
    question: 'Wie ist bei der Verabreichung von Sauerstoff (Inhalation) vorzugehen?',
    choices: [
      {
        valid: true,
        content: 'Inhalationsmaske anlegen (Patienten vorher informieren!)'
      },
      {
        valid: true,
        content: 'Atmung beobachten'
      },
      {
        content: 'Sauerstoff ist immer hilfreich, Beobachtung des Patienten daher nicht nötig'
      },
      {
        valid: true,
        content: 'Flaschenventil öffnen, Abgabemenge einstellen'
      },
    ],
  },
  {
    id: 2389278,
    question: 'Wie ist der Beckengürtel aufgebaut?',
    choices: [
      {
        valid: true,
        content: '2 Hüftbeine'
      },
      {
        content: '2 Kreuzbeine'
      },
      {
        content: 'Lendenwirbelsäule'
      },
      {
        content: '2 Oberarmknochen'
      },
    ],
  },
  {
    id: 2388358,
    question: 'Wie ist der Bewusstseinszustand, wenn bei einem Menschen das Bewusstsein zwar erhalten, aber eine situationsgerechte Realitätseinschätzung nicht mehr gewährleistet ist?',
    choices: [
      {
        content: 'Komatös'
      },
      {
        content: 'Somnolent'
      },
      {
        valid: true,
        content: 'Bewusstseinsverändert'
      },
      {
        content: 'Bewusstseinsklar'
      },
    ],
  },
  {
    id: 2389279,
    question: 'Wie ist der Schultergürtel aufgebaut?',
    choices: [
      {
        valid: true,
        content: '2 Schlüsselbeine'
      },
      {
        valid: true,
        content: '2 Schulterblätter'
      },
      {
        content: '2 Halswirbeln'
      },
      {
        content: '2 Brustbeine'
      },
    ],
  },
  {
    id: 2388619,
    question: 'Wie ist der zwangsweise Transport von psychisch Kranken, aggressiven Patienten gesetzlich geregelt?',
    choices: [
      {
        content: 'Nicht gesetzlich geregelt'
      },
      {
        content: 'Im Strafgesetzbuch'
      },
      {
        valid: true,
        content: 'Im Unterbringungsgesetz'
      },
      {
        content: 'In der StVO'
      },
    ],
  },
  {
    id: 2389166,
    question: 'Wie ist die Material- und Meldestelle gekennzeichnet?',
    choices: [
      {
        valid: true,
        content: 'Weißes Quadrat mit schwarzem Rahmen und rotem M'
      },
      {
        content: 'Weißes Quadrat mit rotem Rahmen und schwarzem M'
      },
      {
        content: 'Gelbes Quadrat mit schwarzem Rahmen und rotem M'
      },
      {
        content: 'Grünes Quadrat mit rotem Rahmen und schwarzem M'
      },
    ],
  },
  {
    id: 2388620,
    question: 'Wie ist die Mitnahme von Begleitpersonen geregelt?',
    choices: [
      {
        content: 'Dürfen nur bei Begleitung von Kindern mitgenommen werden'
      },
      {
        valid: true,
        content: 'Die unentgeltliche Mitnahme einer Begleitperson bei Krankentransporten ist zulässig'
      },
      {
        content: 'Die Begleitperson soll ein Taxi anfordern'
      },
      {
        content: 'Bis zu 3 Personen dürfen mitgenommen werden'
      },
    ],
  },
  {
    id: 2389280,
    question: 'Wie ist die Wirbelsäule zusammengesetzt?',
    choices: [
      {
        valid: true,
        content: '7 Halswirbel'
      },
      {
        valid: true,
        content: '12 Brustwirbel'
      },
      {
        content: '7 Lendenwirbel'
      },
      {
        valid: true,
        content: 'Kreuzbein und Steißbein'
      },
    ],
  },
  {
    id: 2389167,
    question: 'Wie ist ein Kesselwagen mit verflüssigten Gasen auf der Schiene gekennzeichnet?',
    choices: [
      {
        valid: true,
        content: 'Orange Warntafeln an beiden Längsseiten'
      },
      {
        valid: true,
        content: 'Ein umlaufender oranger Streifen'
      },
      {
        content: 'Orange Warntafeln an beiden Breitseiten'
      },
      {
        content: 'Ein umlaufender roter Streifen'
      },
    ],
  },
  {
    id: 2389281,
    question: 'Wie ist ein Wirbel aufgebaut?',
    choices: [
      {
        valid: true,
        content: 'Wirbelbogen'
      },
      {
        valid: true,
        content: 'Wirbelkörper'
      },
      {
        valid: true,
        content: 'Querfortsatz'
      },
      {
        content: 'Längsfortsatz'
      },
    ],
  },
  {
    id: 2387793,
    question: 'Wie ist eine Schwangere nach erfolgtem Blasensprung zu transportieren?',
    choices: [
      {
        valid: true,
        content: 'Unbedingt liegender Transport wegen der Gefahr des Nabelschnurvorfalls'
      },
      {
        content: 'Nach Wunsch der Patientin, damit sich diese möglichst wohl fühlt'
      },
      {
        content: 'In Rechtsseitenlage, um ein Vena-Cava-Kompressionssyndrom zu vermeiden'
      },
      {
        content: 'Im Tragstuhl sitzend, um den Transport zu erleichtern'
      },
    ],
  },
  {
    id: 2388621,
    question: 'Wie ist vorzugehen, wenn ein zurechnungsfähiger Patient den Transport und auch die Unterschrift auf den Revers verweigert?',
    choices: [
      {
        valid: true,
        content: 'Sie vermerken die Unterschriftsverweigerung auf dem Revers und lassen dies nach Möglichkeit von einem Zeugen bestätigen'
      },
      {
        content: 'Sie können umgehend die Rückfahrt zur Bezirksstelle antreten'
      },
      {
        content: 'Sie veranlassen die Exekutive, einen zwangsweisen Transport laut Unterbringungsgesetz durchzuführen'
      },
      {
        valid: true,
        content: 'Sie vermerken die Unterschriftsverweigerung auf dem Revers und lassen dies nach Möglichkeit von der Exekutive protokollieren'
      },
    ],
  },
  {
    id: 2388216,
    question: 'Wie kann bei der Verwendung von Plastik-Infusionsbeuteln die Tropfgeschwindigkeit erhöht werden?',
    choices: [
      {
        valid: true,
        content: 'Durch äußeren Druck mit den Händen'
      },
      {
        valid: true,
        content: 'Mit der Fenwalmanschette (Druckinfusionsmanschette)'
      },
      {
        content: 'Kann nicht erhöht werden'
      },
      {
        content: 'Durch Einspritzen von Flüssigkeit in den Infusionsbeutel'
      },
    ],
  },
  {
    id: 2388727,
    question: 'Wie kann das HI-Virus übertragen werden?',
    choices: [
      {
        valid: true,
        content: 'Verletzung durch kontaminierte Gegenstände (Nadelstich, ...)'
      },
      {
        content: 'Händeschütteln ohne Handschuhe, Husten'
      },
      {
        valid: true,
        content: 'Schwangere mit HIV-Infektion (Übertragung von der Mutter auf das Kind)'
      },
      {
        content: 'Tröpfcheninfektion'
      },
    ],
  },
  {
    id: 2388359,
    question: 'Wie kann ein Mensch in Bezug auf seine Bewusstseinslage sein?',
    choices: [
      {
        valid: true,
        content: 'Bewusstseinsklar'
      },
      {
        valid: true,
        content: 'Bewusstseinsgetrübt'
      },
      {
        valid: true,
        content: 'Bewusstseinsverändert'
      },
      {
        valid: true,
        content: 'Bewusstlos'
      },
    ],
  },
  {
    id: 2388728,
    question: 'Wie kann einer Hepatitis B-Infektion im Rettungswesen vorgebeugt werden?',
    choices: [
      {
        content: 'Abwurfbehälter (Kontamedbox) nur mit Handschuhen ausleeren'
      },
      {
        valid: true,
        content: 'Expositionsprophylaxe Typ 1, nach Abwurf der Handschuhe hygienische Händedesinfektion'
      },
      {
        valid: true,
        content: 'Schutzimpfung/Immunisierung gegen Hepatitis A und B (AUVA übernimmt Kosten)'
      },
      {
        valid: true,
        content: 'Sichere Abfallentsorgung (Nadeln, ....)'
      },
    ],
  },
  {
    id: 2389108,
    question: 'Wie kann man die Qualität der Atmung beurteilen?',
    choices: [
      {
        valid: true,
        content: 'Atemfrequenz'
      },
      {
        valid: true,
        content: 'Atemtiefe'
      },
      {
        valid: true,
        content: 'Bewegung des Brustkorbs'
      },
      {
        valid: true,
        content: 'Einsatz der Atemhilfsmuskulatur'
      },
    ],
  },
  {
    id: 2389604,
    question: 'Wie kann man sich vor einer Verstauchung schützen?',
    choices: [
      {
        valid: true,
        content: 'feste Schuhe beim Sport tragen'
      },
      {
        content: 'eine Sonnenbrille tragen'
      },
      {
        valid: true,
        content: 'Tragen von Schutzausrüstung'
      },
      {
        valid: true,
        content: 'Sicherheitstraining in einer Skater Schule'
      },
    ],
  },
  {
    id: 2389646,
    question: 'Wie kannst du Brände und Verbrennungen vermeiden?',
    choices: [
      {
        valid: true,
        content: 'Lampenschirme nicht mit Kleidungsstücken abdunkeln'
      },
      {
        content: 'Kerzen auf trockenen und dürren Christbäumen anzünden'
      },
      {
        valid: true,
        content: 'offene Kamine und brennende Kerzen nie unbeaufsichtigt lassen'
      },
      {
        content: 'heimlich mit Streichhölzern spielen'
      },
    ],
  },
  {
    id: 2389608,
    question: 'Wie kannst du dich vor einer Verbrennung schützen?',
    choices: [
      {
        content: 'mit einem Feuerzeug spielen'
      },
      {
        valid: true,
        content: 'offenes Feuer meiden'
      },
      {
        valid: true,
        content: 'anbringen von verschiedenen Sicherheitsvorkehrungen (Tischdeckenhalter, Herdgitter…)'
      },
      {
        content: 'in eine Steckdose greifen'
      },
    ],
  },
  {
    id: 2389609,
    question: 'Wie kannst du dich vor Tetanus (Wundstarrkrampf) schützen?',
    choices: [
      {
        content: 'viel Gemüse essen'
      },
      {
        content: 'Desinfektionsmittel'
      },
      {
        valid: true,
        content: 'Impfung'
      },
      {
        content: 'Salbe'
      },
    ],
  },
  {
    id: 2389633,
    question: 'Wie kannst du eine Erfrierung vermeiden?',
    choices: [
      {
        content: 'enge Bekleidung und kleine Schuhe tragen'
      },
      {
        content: 'wasserhaltige Cremen fürs Gesicht verwenden'
      },
      {
        valid: true,
        content: 'nasse Kleidung sofort wechseln'
      },
      {
        valid: true,
        content: 'Handschuhe und Haube tragen'
      },
    ],
  },
  {
    id: 2389625,
    question: 'Wie kannst du eine Verätzung verhindern?',
    choices: [
      {
        valid: true,
        content: 'Putzmittel im Originalgefäß aufbewahren und nicht umfüllen.'
      },
      {
        valid: true,
        content: 'Nicht an einem Gefäß mit unbekanntem Inhalt riechen oder kosten.'
      },
      {
        valid: true,
        content: 'Etiketten und Beschriftungen nicht beschädigen oder entfernen.'
      },
      {
        valid: true,
        content: 'Empfohlene Schutzausrüstung (z.B. Gummihandschuhe, Brille…) verwenden und Vorsichtsmaßnahmen beachten.'
      },
    ],
  },
  {
    id: 2389601,
    question: 'Wie kannst du einen Insektenstich im Mund-Rachen-Raum verhindern?',
    choices: [
      {
        valid: true,
        content: 'Getränke verschließen'
      },
      {
        content: 'nur aus Dosen trinken'
      },
      {
        valid: true,
        content: 'dünne Strohhalme verwenden'
      },
      {
        valid: true,
        content: 'genau schauen, bevor man trinkt'
      },
    ],
  },
  {
    id: 2389630,
    question: 'Wie kannst du Knochenbrüche verhindern?',
    choices: [
      {
        valid: true,
        content: 'Tragen von Schutzausrüstung'
      },
      {
        content: 'auf gesunde Ernährung achten'
      },
      {
        valid: true,
        content: 'Pistenregeln beim Skifahren oder Snowboarden beachten'
      },
      {
        valid: true,
        content: 'sich selbst nicht überschätzen'
      },
    ],
  },
  {
    id: 2389613,
    question: 'Wie kannst du Schnittwunden vermeiden?',
    choices: [
      {
        valid: true,
        content: 'indem du mit einem Messer immer vom Körper weg schneidest'
      },
      {
        valid: true,
        content: 'durch Verwendung abgerundeter Scheren'
      },
      {
        valid: true,
        content: 'durch vorsichtiges Hantieren mit spitzen und scharfen Gegenständen'
      },
      {
        content: 'indem du mit einem Messer immer zum Körper schneidest'
      },
    ],
  },
  {
    id: 2389627,
    question: 'Wie kannst du Tierbisse vermeiden?',
    choices: [
      {
        content: 'Hunde von hinten anfassen'
      },
      {
        content: 'schnell auf fremde Tiere zulaufen'
      },
      {
        valid: true,
        content: 'Körperkontakt vermeiden'
      },
      {
        valid: true,
        content: 'auf Körpersprache des Tieres achten (z.B. Knurren, aufgestellte Nackenhaare…)'
      },
    ],
  },
  {
    id: 2389004,
    question: 'Wie kommt es zu einer Hypoglykämie?',
    choices: [
      {
        valid: true,
        content: 'Zu wenig Kohlenhydrate'
      },
      {
        valid: true,
        content: 'Alkohol'
      },
      {
        valid: true,
        content: 'Magen-Darmerkrankungen, Fieber'
      },
      {
        valid: true,
        content: 'Mehr körperliche Aktivität als sonst'
      },
    ],
  },
  {
    id: 2387794,
    question: 'Wie lagern Sie einen ansprechbaren Patienten bei Verdacht auf Kopfverletzungen (SHT)?',
    choices: [
      {
        content: 'Flache Rückenlagerung mit Knierolle'
      },
      {
        content: 'Kopfhochlagerung'
      },
      {
        valid: true,
        content: 'Prophylaktische Seitenlage mit erhöhtem Oberkörper'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2389005,
    question: 'Wie lagern Sie einen Patienten mit einem Lungenödem?',
    choices: [
      {
        valid: true,
        content: 'Lagerung auf Trage mit stark erhöhtem Oberkörper'
      },
      {
        content: 'Patienten liegend lagern, Beine, wenn möglich, tief lagern oder hinunter hängen lassen'
      },
      {
        content: 'Patienten liegend lagern, Beine, wenn möglich, hoch lagern'
      },
      {
        valid: true,
        content: 'Beine, wenn im Fahrzeug möglich, tief lagern oder hinunter hängen lassen'
      },
    ],
  },
  {
    id: 2387795,
    question: 'Wie lagern Sie einen Verletzten (Verdacht Kopfverletzung) bei festgestellter Erinnerungslücke und erhaltenem Bewusstsein?',
    choices: [
      {
        content: 'Flache Rückenlage'
      },
      {
        content: 'Rückenlage mit hochgelegten Beinen'
      },
      {
        valid: true,
        content: 'Prophylaktische Seitenlage mit erhöhtem Oberkörper'
      },
      {
        content: 'Seitenlage, weil er bald bewusstlos werden kann'
      },
    ],
  },
  {
    id: 2389639,
    question: 'Wie lagerst du jemanden mit einer starken Blutung?',
    choices: [
      {
        content: 'sitzend, mit erhöhten Oberkörper'
      },
      {
        content: 'in Seitenlage'
      },
      {
        content: 'kniend'
      },
      {
        valid: true,
        content: 'liegend, mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2387796,
    question: 'Wie lange dauert die Eröffnungsperiode normalerweise?',
    choices: [
      {
        content: 'ca. 3-4 Stunden'
      },
      {
        valid: true,
        content: 'ca. 7-12 Stunden'
      },
      {
        content: 'bis 24 Stunden'
      },
      {
        content: 'max. 10 Stunden'
      },
    ],
  },
  {
    id: 2388217,
    question: 'Wie lang reicht der Sauerstoffvorrat bei folgendem Beispiel? Sauerstoffflasche mit 10 Liter Rauminhalt, 60 bar Druck und 15 Liter Sauerstoff pro Minute werden verabreicht (ohne Beachtung des Restdruckes)',
    choices: [
      {
        valid: true,
        content: '40 Minuten'
      },
      {
        content: '120 Minuten'
      },
      {
        content: '80 Minuten'
      },
      {
        content: '90 Minuten'
      },
    ],
  },
  {
    id: 2388218,
    question: 'Wie lange reicht der Sauerstoffvorrat bei folgendem Beispiel? Sauerstoffflasche mit 5 Liter, 60 bar Druck und 6 Liter/min. werden verabreicht(ohne Beachtung des Restdruckes)',
    choices: [
      {
        content: 'Ca. 20 Minuten'
      },
      {
        content: 'Ca. 40 Minuten'
      },
      {
        valid: true,
        content: 'Ca. 50 Minuten'
      },
      {
        content: 'Ca. 80 Minuten'
      },
    ],
  },
  {
    id: 2388219,
    question: 'Wie lässt sich eine annähernd 100%ige Sauerstoffanreicherung der Beatmungsluft mit dem Beatmungsbeutel erreichen?',
    choices: [
      {
        content: 'Zufuhr von 10 Liter/min. Sauerstoff in den Sauerstoffanreicherungsstutzen'
      },
      {
        content: 'Verwendung des Reservoirbeutels und Anschluss von 5-10 Liter Sauerstoff/min'
      },
      {
        valid: true,
        content: 'Verwendung des Reservoirbeutels und Anschluss von 10-15 Liter Sauerstoff/min'
      },
      {
        content: 'Zufuhr von 6 Liter/min Sauerstoff in den Sauerstoffanreicherungsstutzen'
      },
    ],
  },
  {
    id: 2389617,
    question: 'Wie lautet der Euro-Notruf?',
    choices: [
      {
        valid: true,
        content: '112'
      },
      {
        content: '133'
      },
      {
        content: '144'
      },
      {
        content: '122'
      },
    ],
  },
  {
    id: 2388887,
    question: 'Wie lautet der Euro-Notruf?',
    choices: [
      {
        content: '122'
      },
      {
        content: '133'
      },
      {
        content: '144'
      },
      {
        valid: true,
        content: '112'
      },
    ],
  },
  {
    id: 2388220,
    question: 'Wie lautet die Formel für die Berechnung des Flascheninhaltes einer Sauerstoffflasche?',
    choices: [
      {
        content: 'Rauminhalt der Flasche x Minutenvolumen'
      },
      {
        content: 'Minutenvolumen x Druck'
      },
      {
        valid: true,
        content: 'Inhalt der Flasche x Flascheninhaltsdruck'
      },
      {
        content: 'Flascheninhaltsdruck x Abgabemenge'
      },
    ],
  },
  {
    id: 2389618,
    question: 'Wie lautet die Notrufnummer der Feuerwehr?',
    choices: [
      {
        valid: true,
        content: '122'
      },
      {
        content: '133'
      },
      {
        content: '144'
      },
      {
        content: '112'
      },
    ],
  },
  {
    id: 2388888,
    question: 'Wie lautet die Notrufnummer der Feuerwehr?',
    choices: [
      {
        valid: true,
        content: '122'
      },
      {
        content: '133'
      },
      {
        content: '144'
      },
      {
        content: '112'
      },
    ],
  },
  {
    id: 2389619,
    question: 'Wie lautet die Notrufnummer der Polizei?',
    choices: [
      {
        content: '122'
      },
      {
        valid: true,
        content: '133'
      },
      {
        content: '144'
      },
      {
        content: '112'
      },
    ],
  },
  {
    id: 2388889,
    question: 'Wie lautet die Notrufnummer der Polizei?',
    choices: [
      {
        content: '122'
      }, 
      {
        valid: true,
        content: '133'
      }, 
      {
        content: '144'
      }, 
      {
        content: '112'
      }, 
    ],
  },
  {
    id: 2389620,
    question: 'Wie lautet die Notrufnummer der Rettung?',
    choices: [
      {
        content: '122'
      },
      {
        content: '133'
      },
      {
        valid: true,
        content: '144'
      },
      {
        content: '112'
      },
    ],
  },
  {
    id: 2388890,
    question: 'Wie lautet die Notrufnummer der Rettung?',
    choices: [
      {
        content: '122'
      },
      {
        content: '133'
      },
      {
        valid: true,
        content: '144'
      },
      {
        content: '112'
      },
    ],
  },
  {
    id: 2388891,
    question: 'Wie lautet die Telefonnummer der Vergiftungsinformationszentrale (VIZ)?',
    choices: [
      {
        content: '01/43 43 43'
      }, 
      {
        content: '01/144'
      }, 
      {
        content: '01/34 3 434'
      }, 
      {
        valid: true,
        content: '01/406 43 43'
      }, 
    ],
  },
  {
    id: 2388445,
    question: 'Wie muss der Landeplatz für den NAH beschaffen sein?',
    choices: [
      {
        content: 'Ebener und trittsicherer Aufsetzplatz von mind. 5x5 Metern, niemals in Mulden, keine Erhebung im Umkreis von 15 m und im An- und Abflugsektor von 100 m keine Erhebung höher als 15 m, alle leichten Gegenstände entfernen, Fenster + Türen der Autos schließen'
      },
      {
        valid: true,
        content: 'Ebener und trittsicherer Aufsetzplatz von mind. 10x10 Metern'
      },
      {
        content: 'Im Umkreis von 15 Metern darf keine Erhebung sein'
      },
      {
        valid: true,
        content: 'Im Umkreis von 25 Metern darf keine Erhebung sein'
      },
    ],
  },
  {
    id: 2388446,
    question: 'Wie muss die Einweisung des NAH erfolgen?',
    choices: [
      {
        valid: true,
        content: 'Ca. 5 m vom Aufsetzpunkt entfernt'
      },
      {
        content: 'Ca. 10 m vom Aufsetzpunkt entfernt'
      },
      {
        valid: true,
        content: 'Der Einweiser stellt sich mit dem Wind von hinten auf und hebt beide Arme (Y)'
      },
      {
        content: 'Der Einweiser stellt sich mit dem Wind von vorne auf und hebt beide Arme (Y)'
      },
    ],
  },
  {
    id: 2389168,
    question: 'Wie muss sich der Sanitäter bei einem Gefahrengutunfall verhalten?',
    choices: [
      {
        content: 'Möglichst rasche Rettung der Patienten'
      },
      {
        valid: true,
        content: 'Einsatzfahrzeuge auf jeden Fall außerhalb der unmittelbaren Gefahrenzone aufstellen und Windrichtung beachten'
      },
      {
        content: 'Weitest mögliche Annäherung des Fahrzeuges zur optimalen Patientenversorgung'
      },
      {
        valid: true,
        content: 'Kontakt mit Gefahrengut vermeiden, Sicherheitsabstand einhalten'
      },
    ],
  },
  {
    id: 2388729,
    question: 'Wie nennt man das Abtöten bzw. die irreversible Inaktivierung von krankheitserregenden Keimen an und in kontaminierten Objekten?',
    choices: [
      {
        content: 'Kontamination'
      },
      {
        valid: true,
        content: 'Desinfektion'
      },
      {
        content: 'Sterilisation'
      },
      {
        content: 'Dekontamination'
      },
    ],
  },
  {
    id: 2388622,
    question: 'Wie nennt man das entsprechende Schriftstück (zur Dokumentation), wenn der Transport durch den Patienten verweigert wird?',
    choices: [
      {
        content: 'Transportschein'
      },
      {
        content: 'Parere'
      },
      {
        content: 'Einweisung'
      },
      {
        valid: true,
        content: 'Revers'
      },
    ],
  },
  {
    id: 2389006,
    question: 'Wie nennt man die Entzündung des Gehirns?',
    choices: [
      {
        content: 'Meningitis'
      },
      {
        valid: true,
        content: 'Enzephalitis'
      },
      {
        content: 'TIA (Transistorisch Ischämische Attacke)'
      },
      {
        content: 'Insult'
      },
    ],
  },
  {
    id: 2389282,
    question: 'Wie nennt man die Verwertung von Nahrungsbausteinen (Eiweiß, Fett, Kohlenhydrate)?',
    choices: [
      {
        valid: true,
        content: 'Stoffwechsel'
      },
      {
        content: 'Verdauung'
      },
      {
        content: 'Verbrennung'
      },
      {
        content: 'Recycling'
      },
    ],
  },
  {
    id: 2389283,
    question: 'Wie nennt man eine Entzündung der Hirnhäute?',
    choices: [
      {
        content: 'Enzephalitis'
      },
      {
        valid: true,
        content: 'Meningitis'
      },
      {
        content: 'TIA'
      },
      {
        content: 'Peritonitis'
      },
    ],
  },
  {
    id: 2388360,
    question: 'Wie nennt man einen Zustand tiefster Bewusstlosigkeit, der auch durch stärkste äußere Reize nicht unterbrochen werden kann?',
    choices: [
      {
        valid: true,
        content: 'Koma'
      },
      {
        content: 'Sopor'
      },
      {
        content: 'Somnolenz'
      },
      {
        content: 'Sanguinenz'
      },
    ],
  },
  {
    id: 2389284,
    question: 'Wie oft atmet ein Erwachsener (=Atemzüge) pro Minute im Ruhezustand?',
    choices: [
      {
        content: 'ca. 25 mal pro Minute'
      },
      {
        valid: true,
        content: 'ca. 15 mal pro Minute'
      },
      {
        content: 'ca. 40 mal pro Minute'
      },
      {
        content: 'ca. 5 mal pro Minute'
      },
    ],
  },
  {
    id: 2389285,
    question: 'Wie oft atmet ein gesunder Erwachsener im Ruhezustand?',
    choices: [
      {
        valid: true,
        content: 'Ca. 15 mal/min.'
      },
      {
        content: '16-20 mal/min.'
      },
      {
        content: '40-50 mal/min.'
      },
      {
        content: '60-100 mal/min.'
      },
    ],
  },
  {
    id: 2388361,
    question: 'Wie oft muss die Herzdruckmassage (Frequenz) pro Minute durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'mind. 100 mal pro Minute'
      },
      {
        content: 'mind. 80 mal pro Minute bis max. 150 mal pro Minute'
      },
      {
        content: '60 mal pro Minute'
      },
      {
        content: '50 mal pro Minute'
      },
    ],
  },
  {
    id: 2389169,
    question: 'Wie sehen Warntafeln beim Transport von Gefahrengütern aus?',
    choices: [
      {
        valid: true,
        content: 'Orange Tafeln (40x30 cm) mit schwarzer Umrandung'
      },
      {
        valid: true,
        content: 'Es gibt Warntafeln mit und ohne spezieller Kennzeichnung (Nummern).'
      },
      {
        content: 'Es gibt nur Warntafeln mit spezieller Kennzeichnung (Nummern).'
      },
      {
        content: 'Schwarze Tafeln (40x60 cm) mit oranger Umrandung'
      },
    ],
  },
  {
    id: 2389170,
    question: 'Wie sieht die Kennzeichnung beim Großschaden im Transportraum aus?',
    choices: [
      {
        valid: true,
        content: 'Kennfarbe: weiß mit blauem Rand - Form: rechteckig'
      },
      {
        content: 'Kennfarbe: orange - Form: dreieckig'
      },
      {
        content: 'Kennfarbe: weiß mit blauem Rand - Form: quadratisch'
      },
      {
        content: 'Kennfarbe: rot, gelb, grün und blau - Form: rund'
      },
    ],
  },
  {
    id: 2388221,
    question: 'Wie sind Brechampullen gekennzeichnet?',
    choices: [
      {
        content: 'Ein weißer Punkt am Boden der Ampulle'
      },
      {
        valid: true,
        content: 'Ein Punkt in der Mitte des Ampullenkopfes'
      },
      {
        valid: true,
        content: 'Ein Ring am Ampullenhals'
      },
      {
        content: 'Eine entsprechende Aufschrift am Etikett'
      },
    ],
  },
  {
    id: 2388362,
    question: 'Wie soll eine korrekte Herzdruckmassage beim Erwachsenen durchgeführt werden?',
    choices: [
      {
        valid: true,
        content: 'Senkrechter gleichmäßiger Druck'
      },
      {
        valid: true,
        content: 'Druck- und Entlastungsphase gleich lang'
      },
      {
        valid: true,
        content: 'Drucktiefe 5-6 cm'
      },
      {
        content: 'Arbeitsfrequenz mindestens 150/min'
      },
    ],
  },
  {
    id: 2388222,
    question: 'Wie sollen Sauerstoffflaschen zum/vom RTW (Sauerstoffdepot) transportiert werden?',
    choices: [
      {
        valid: true,
        content: 'Mit verschraubter Schutzkappe'
      },
      {
        content: 'Sie werden am Ventil getragen'
      },
      {
        valid: true,
        content: 'Vor Umfallen gesichert'
      },
      {
        content: 'Sauerstoffflaschen werden am Boden gerollt'
      },
    ],
  },
  {
    id: 2388730,
    question: 'Wie unterscheiden sich pathogene und apathogene Keime?',
    choices: [
      {
        content: 'Apathogene Keime sind krankmachend'
      },
      {
        valid: true,
        content: 'Pathogene Keime sind krankmachend'
      },
      {
        content: 'Pathogene Keime sind nicht krankmachend'
      },
      {
        valid: true,
        content: 'Apathogene Keime sind nicht krankmachend'
      },
    ],
  },
  {
    id: 2388363,
    question: 'Wie verhält sich der Sanitäter, wenn er mit einer Abbindung konfrontiert wird, die ein Ersthelfer angelegt hat?',
    choices: [
      {
        valid: true,
        content: 'Er überzeugt sich, ob eine Abbindung überhaupt erforderlich ist. Wenn nicht, dann Druckverband anlegen und Abbindung lösen'
      },
      {
        content: 'Eine vom Ersthelfer angelegte Abbindung ist auf jeden Fall zu belassen'
      },
      {
        content: 'Den Ersthelfer wegen der Abbindung zur Anzeige bringen'
      },
      {
        content: 'Abbindung sofort lösen, Ersthelfer loben, dann erfolgt die 2. Phase der Abbindung'
      },
    ],
  },
  {
    id: 2388364,
    question: 'Wie verhalten sich Puls und Blutdruck bei einem Patienten mit schwerem Schock?',
    choices: [
      {
        content: 'Puls und Blutdruck bleiben gleich'
      },
      {
        content: 'Puls fällt - Blutdruck steigt'
      },
      {
        content: 'Puls steigt - Blutdruck steigt'
      },
      {
        valid: true,
        content: 'Puls steigt - Blutdruck fällt'
      },
    ],
  },
  {
    id: 2388892,
    question: 'Wie verhalten Sie sich am Notfallort (Verkehrsunfall)?',
    choices: [
      {
        valid: true,
        content: 'in ausreichendem Abstand absichern'
      }, 
      {
        valid: true,
        content: 'Rettung von Verletzten aus der Gefahrenzone falls möglich durchführen'
      }, 
      {
        valid: true,
        content: 'Selbstschutz beachten'
      }, 
      {
        content: 'Fremdschutz geht vor Selbstschutz'
      }, 
    ],
  },
  {
    id: 2387797,
    question: 'Wie versorgen Sie die Mutter nach der Geburt?',
    choices: [
      {
        content: 'Seitenlagerung'
      },
      {
        valid: true,
        content: 'Lagerung nach Fritsch, keimfreie Vorlage'
      },
      {
        valid: true,
        content: 'Nabelschnurreste mit Pflasterstreifen am Oberschenkel der Mutter fixieren'
      },
      {
        valid: true,
        content: 'Die mit Fruchtwasser getränkte Geburtsunterlage entfernen und durch eine keimfreie Unterlage ersetzen'
      },
    ],
  },
  {
    id: 2388365,
    question: 'Wie versorgt man abgetrennte Körperteile?',
    choices: [
      {
        valid: true,
        content: 'Einwickeln in Rettungsdecke'
      },
      {
        valid: true,
        content: 'Keimfrei versorgen'
      },
      {
        content: 'Tiefkühlung oder direkter Kontakt mit Eis, Schnee'
      },
      {
        valid: true,
        content: 'Verwendung von Replantatsystemen, wenn vorhanden, möglich'
      },
    ],
  },
  {
    id: 2388605,
    question: 'Wie versorgt man einen Verletzten mit einer stark blutenden Wunde?',
    choices: [
      {
        content: 'Es ist nur der Notruf abzusetzen.'
      }, 
      {
        content: 'Pflasterverband anbringen und mit den Fingern darauf drücken'
      }, 
      {
        valid: true,
        content: 'Fingerdruck auf die Wunde oder Druckverband anlegen'
      }, 
      {
        valid: true,
        content: 'Basismaßnahmen, verletzten Körperteil hochhalten'
      }, 
    ],
  },
  {
    id: 2389286,
    question: 'Wie viel Liter Blut hat ein Erwachsener?',
    choices: [
      {
        content: '3-4 l'
      },
      {
        valid: true,
        content: '5-7 l'
      },
      {
        content: '1-2 l'
      },
      {
        content: '4-5 l'
      },
    ],
  },
  {
    id: 2389287,
    question: 'Wie viel Liter Blut hat ein Erwachsener?',
    choices: [
      {
        valid: true,
        content: '5-7 Liter'
      },
      {
        content: '7-9 Liter'
      },
      {
        content: '3-4 Liter'
      },
      {
        content: '10-12 Liter'
      },
    ],
  },
  {
    id: 2387798,
    question: 'Wie viel Prozent der Körperoberfläche eines Erwachsen entspricht die Verbrennung eines Armes?',
    choices: [
      {
        content: '18 %'
      },
      {
        valid: true,
        content: '9 %'
      },
      {
        content: '12 %'
      },
      {
        content: '15 %'
      },
    ],
  },
  {
    id: 2387799,
    question: 'Wie viel Prozent der Körperoberfläche eines Erwachsenen entspricht die Verbrennung eines Armes und des Rückens?',
    choices: [
      {
        valid: true,
        content: '27 %'
      },
      {
        content: '18 %'
      },
      {
        content: '25 %'
      },
      {
        content: '30 %'
      },
    ],
  },
  {
    id: 2387800,
    question: 'Wie viel Prozent der Körperoberfläche sind betroffen, wenn sich die Verbrennung über ein ganzes Bein ausdehnt?',
    choices: [
      {
        valid: true,
        content: '18 %'
      },
      {
        content: '14 %'
      },
      {
        content: '9 %'
      },
      {
        content: '32 %'
      },
    ],
  },
  {
    id: 2388682,
    question: 'Wie viel Prozent Sauerstoff bekommt der Patient bei einer Mund-zu-Mund-Beatmung?',
    choices: [
      {
        content: 'ca. 15 %'
      },
      {
        content: 'ca. 21 %'
      },
      {
        content: 'ca. 23 %'
      },
      {
        valid: true,
        content: 'ca. 17 %'
      },
    ],
  },
  {
    id: 2388223,
    question: 'Wie viel Prozent Sauerstoff enthält die Beatmungsluft bei der Verwendung des Beatmungsbeutels ohne O²-Gabe und ohne Reservoir?',
    choices: [
      {
        content: '45% Sauerstoff'
      },
      {
        content: '17% Sauerstoff'
      },
      {
        valid: true,
        content: '21% Sauerstoff'
      },
      {
        content: '30% Sauerstoff'
      },
    ],
  },
  {
    id: 2389007,
    question: 'Wie viel Sauerstoff gibt man Patienten mit Verdacht auf Herzinfarkt (ohne Monitoring)?',
    choices: [
      {
        content: '10-15 Liter/min'
      },
      {
        content: 'Nur 2-3 Liter/min'
      },
      {
        valid: true,
        content: '6-8 Liter/min'
      },
      {
        content: 'Überhaupt keinen'
      },
    ],
  },
  {
    id: 2389288,
    question: 'Wie viele Halswirbel hat der Mensch?',
    choices: [
      {
        content: '9'
      },
      {
        content: '6'
      },
      {
        valid: true,
        content: '7'
      },
      {
        content: '4'
      },
    ],
  },
  {
    id: 2388731,
    question: 'Wie viele Patientenrisikogruppen gibt es?',
    choices: [
      {
        content: '5 Gruppen'
      },
      {
        content: '6 Gruppen'
      },
      {
        valid: true,
        content: '4 Gruppen'
      },
      {
        content: '8 Gruppen'
      },
    ],
  },
  {
    id: 2389289,
    question: 'Wie viele Rippenpaare bilden den menschlichen Brustkorb?',
    choices: [
      {
        valid: true,
        content: '12'
      },
      {
        content: '10'
      },
      {
        content: '7'
      },
      {
        content: '15'
      },
    ],
  },
  {
    id: 2388732,
    question: 'Wie viele Schritte müssen durchgeführt werden, damit die hygienische Händedesinfektion erfolgreich ist?',
    choices: [
      {
        content: 'Es reicht normales Waschen (Lotion) aus'
      },
      {
        content: '5 Schritte zur hygienischen Händedesinfektion'
      },
      {
        valid: true,
        content: '6 Schritte zur hygienischen Händedesinfektion'
      },
      {
        content: '4 Schritte zur hygienischen Händedesinfektion'
      },
    ],
  },
  {
    id: 2388224,
    question: 'Wie weit wird der Absaugkatheter in den Mund des Patienten eingeführt?',
    choices: [
      {
        content: 'Mindestens 15 cm tief'
      },
      {
        content: 'Höchstens 2 cm tief'
      },
      {
        valid: true,
        content: 'Absaugkatheter dürfen generell nur unter Sicht eingeführt werden'
      },
      {
        content: 'Absaugkatheter sind für die Absaugung nicht erforderlich'
      },
    ],
  },
  {
    id: 2389290,
    question: 'Wie werden Blutgefäße bezeichnet, die das Blut vom Herzen weg in den Körperkreislauf befördern?',
    choices: [
      {
        content: 'Venen'
      },
      {
        valid: true,
        content: 'Arterien'
      },
      {
        content: 'Alveolen'
      },
      {
        content: 'Blutadern'
      },
    ],
  },
  {
    id: 2388733,
    question: 'Wie werden Geräte und Instrumente desinfiziert, und welche Maßnahme ist danach durchzuführen?',
    choices: [
      {
        content: 'Die Sprühmethode ist ausreichend, Lufttrocknen'
      },
      {
        content: 'Mit warmem Seifenwasser abspülen, zuvor Geräte soweit wie möglich zerlegen'
      },
      {
        valid: true,
        content: 'Völlig bedeckend in Desinfektionslösung einlegen, Einwirkzeit beachten, Geräte soweit wie möglich zerlegen'
      },
      {
        valid: true,
        content: 'Am Ende des Instrumentenkreislaufes ist eine Funktionskontrolle lt. MPG durchzuführen'
      },
    ],
  },
  {
    id: 2388225,
    question: 'Wie werden Patienten mit Atembeschwerden gelagert?',
    choices: [
      {
        content: 'Tieflagerung des Oberkörpers, eventuell Knierolle unterlegen'
      },
      {
        content: 'Stabile Seitenlagerung'
      },
      {
        valid: true,
        content: 'Hochlagerung des Oberkörpers, eventuell Knierolle verwenden'
      },
      {
        content: 'Flachlagerung auf der Vakuummatratze'
      },
    ],
  },
  {
    id: 2388893,
    question: 'Wie wird das Bewusstsein überprüft?',
    choices: [
      {
        content: 'Notfallpatient sanft massieren, bis der Notarzt eintrifft'
      }, 
      {
        valid: true,
        content: 'Durch lautes Ansprechen und sanftes Schütteln an den Schultern'
      }, 
      {
        content: 'Pupillenreflexe des Patienten mit einer Taschenlampe überprüfen'
      }, 
      {
        content: 'Schmerzreiz an beiden Ohren durchführen'
      }, 
    ],
  },
  {
    id: 2387801,
    question: 'Wie wird das Neugeborene aufgenommen?',
    choices: [
      {
        content: 'An den Beinen hochhalten, damit das Fruchtwasser abrinnen kann'
      },
      {
        valid: true,
        content: 'Mit der flachen Hand unter das Kind, die andere Hand flach auf den Rücken, Kind mit Kopf etwas nach unten anheben - Fruchtwasser fließt ab'
      },
      {
        valid: true,
        content: 'Das Neugeborene wird zur Weiterversorgung auf den Rücken zwischen die Beine der Mutter gelegt'
      },
      {
        content: 'Das Neugeborene wird auf auf den Bauch der Mutter gelegt'
      },
    ],
  },
  {
    id: 2389291,
    question: 'Wie wird der knöcherne Schädel unterteilt?',
    choices: [
      {
        valid: true,
        content: 'Gehirn- und Gesichtsschädel'
      },
      {
        content: 'Stirnbein, Scheitelbein'
      },
      {
        content: 'Vorder- und Rückseite'
      },
      {
        content: 'Hinterhauptsbein, Stirnbein'
      },
    ],
  },
  {
    id: 2388226,
    question: 'Wie wird der Patient bei Arterienverschluss gelagert?',
    choices: [
      {
        content: 'Extremität weich und erhöht lagern'
      },
      {
        content: 'Flachlagerung'
      },
      {
        valid: true,
        content: 'Tief- und Weichlagerung des betreffenden Körperteils'
      },
      {
        content: 'Ist ganz egal'
      },
    ],
  },
  {
    id: 2388227,
    question: 'Wie wird der Patient bei kardiogenem Schock gelagert?',
    choices: [
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
      {
        valid: true,
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        content: 'Lagerung mit Knierolle'
      },
      {
        content: 'Stabile Seitenlagerung'
      },
    ],
  },
  {
    id: 2388228,
    question: 'Wie wird der Patient bei Venenverschluss gelagert?',
    choices: [
      {
        valid: true,
        content: 'Hoch- und Weichlagerung des betroffenen Körperteils'
      },
      {
        content: 'Flachlagerung'
      },
      {
        content: 'Weich- und Tieflagerung'
      },
      {
        content: 'Ist ganz egal'
      },
    ],
  },
  {
    id: 2388229,
    question: 'Wie wird der Patient bei Verdacht auf Bauchverletzung gelagert?',
    choices: [
      {
        valid: true,
        content: 'Krankentrage: Leicht erhöhter Oberkörper, Knierolle'
      },
      {
        content: 'Krankentrage: Halb sitzend, den Oberkörper unterstützen'
      },
      {
        content: 'Krankentrage: Flach liegend mit angewinkelten Kniegelenken (angezogenen Beinen)'
      },
      {
        valid: true,
        content: 'Möglichkeit zur Abstützung der Füße auf der Krankentrage bieten'
      },
    ],
  },
  {
    id: 2389292,
    question: 'Wie wird der Totraum beim Erwachsenen berechnet?',
    choices: [
      {
        content: 'Entspricht dem Körpergewicht'
      },
      {
        content: 'Entspricht dem Körpergewicht x 10'
      },
      {
        content: 'Entspricht dem Körpergewicht x 5'
      },
      {
        valid: true,
        content: 'Entspricht dem Körpergewicht x 2'
      },
    ],
  },
  {
    id: 2389293,
    question: 'Wie wird die Aktionsphase bezeichnet, in der sich die Herzkammern zusammenziehen?',
    choices: [
      {
        content: 'Diastole'
      },
      {
        valid: true,
        content: 'Systole'
      },
      {
        content: 'Asystole'
      },
      {
        content: 'Arrhythmie'
      },
    ],
  },
  {
    id: 2387802,
    question: 'Wie wird die Atemkontrolle beim Kind durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Sehen (Brustkorbbewegungen)'
      },
      {
        valid: true,
        content: 'Hören (Atemgeräusche)'
      },
      {
        valid: true,
        content: 'Fühlen (Ausatemluft)'
      },
      {
        content: 'Tasten (Bauchbewegungen)'
      },
    ],
  },
  {
    id: 2389294,
    question: 'Wie wird die Atmung gesteuert?',
    choices: [
      {
        valid: true,
        content: 'Durch das Atemzentrum im Hirnstamm'
      },
      {
        valid: true,
        content: 'Durch Rezeptoren in den Blutgefäßen'
      },
      {
        content: 'Durch Blutdruckschwankungen'
      },
      {
        valid: true,
        content: 'Durch willentliche Ein- und Ausatmung'
      },
    ],
  },
  {
    id: 2387803,
    question: 'Wie wird die Bewusstseinskontrolle beim Kind durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Ansprechen'
      },
      {
        valid: true,
        content: 'Sanftes Schütteln an den Schultern'
      },
      {
        content: 'Schmerzreiz setzen'
      },
      {
        content: 'Kopf nackenwärts geringfügig überstrecken (Bewusstseinsreflex)'
      },
    ],
  },
  {
    id: 2388734,
    question: 'Wie wird die hygienische Händedesinfektion korrekt durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Händedesinfektionsmittel in die hohle Hand geben und an den Händen verreiben'
      },
      {
        valid: true,
        content: 'Handfläche auf Handfläche,  Handfläche auf Handrücken'
      },
      {
        valid: true,
        content: 'Handfläche auf Handfläche mit gespreizten und verschränkten Fingern'
      },
      {
        valid: true,
        content: 'Daumen, Fingerkuppen und Nagelfalz durch kreisendes Reiben'
      },
    ],
  },
  {
    id: 2388366,
    question: 'Wie wird die korrekte Abbindung am Oberarm mit Hilfe einer Blutdruckmanschette durchgeführt?',
    choices: [
      {
        content: 'Hochlagerung des betroffenen Armes, Manschettendruck auf mind. 290 mmHG aufblasen'
      },
      {
        valid: true,
        content: 'Hochlagerung des betroffenen Armes'
      },
      {
        valid: true,
        content: 'Blutdruckmanschette in der Mitte des Oberarms anlegen'
      },
      {
        valid: true,
        content: 'Aufpumpen, bis die Blutung zum Stillstand gekommen ist'
      },
    ],
  },
  {
    id: 2389109,
    question: 'Wie wird die Kreislaufsituation im Rahmen des ABC-Schemas beurteilt?',
    choices: [
      {
        valid: true,
        content: 'Sichtbare starke Blutungen'
      },
      {
        valid: true,
        content: 'Beurteilung des Pulses'
      },
      {
        valid: true,
        content: 'Beurteilung der Haut'
      },
      {
        content: 'Beurteilung des Blutzuckers'
      },
    ],
  },
  {
    id: 2388595,
    question: 'Wie wird die Mund-zu-Mund-Beatmung im Rahmen der Ersten Hilfe durchgeführt?',
    choices: [
      {
        content: 'Notfallbeatmungstuch über Nase des Notfallpatienten legen, Kopf nackenwärts überstrecken, Mund zuhalten und 2 Mal beatmen'
      },
      {
        valid: true,
        content: 'Der Helfer hebt nach der Beatmung seinen Kopf und beobachtet die Ausatmung des Notfallpatienten'
      },
      {
        content: 'Die Beatmung wird ausschließlich durch den Notarzt durchgeführt'
      },
      {
        valid: true,
        content: 'Notfallbeatmungstuch über Mund des Notfallpatienten legen, Kopf nackenwärts überstrecken, Nase zuhalten und 2 Mal beatmen'
      },
    ],
  },
  {
    id: 2388596,
    question: 'Wie wird die Mund-zu-Nase-Beatmung im Rahmen der Ersten Hilfe durchgeführt?',
    choices: [
      {
        valid: true,
        content: 'Notfallbeatmungstuch über Nase des Notfallpatienten legen, Kopf nackenwärts überstrecken, Mund zuhalten und 2 Mal beatmen'
      },
      {
        content: 'Die Beatmung wird ausschließlich durch den Notarzt durchgeführt'
      },
      {
        valid: true,
        content: 'Der Helfer hebt nach der Beatmung seinen Kopf und beobachtet die Ausatmung des Notfallpatienten'
      },
      {
        content: 'Notfallbeatmungstuch über Mund des Notfallpatienten legen, Kopf nackenwärts überstrecken, Nase zuhalten und 2 Mal beatmen'
      },
    ],
  },
  {
    id: 2388871,
    question: 'Wie wird die Stabile Seitenlage korrekt durchgeführt?',
    choices: [
      {
        valid: true,
        content: ' Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Handgelenk aufs Knie und drehen'
      }, 
      {
        content: 'Arm nach oben legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
      {
        content: 'Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Ellbogen aufs Knie und drehen'
      }, 
      {
        content: 'beide Arme zur Seite legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
    ],
  },
  {
    id: 2388897,
    question: 'Wie wird die Stabile Seitenlage korrekt durchgeführt?',
    choices: [
      {
        valid: true,
        content: ' Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Handgelenk aufs Knie und drehen'
      }, 
      {
        content: 'Arm nach oben legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
      {
        content: 'Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Ellbogen aufs Knie und drehen'
      }, 
      {
        content: 'beide Arme zur Seite legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
    ],
  },
  {
    id: 2388915,
    question: 'Wie wird die Stabile Seitenlage korrekt durchgeführt?',
    choices: [
      {
        valid: true,
        content: ' Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Handgelenk aufs Knie und drehen'
      }, 
      {
        content: 'Arm nach oben legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
      {
        content: 'Arm zur Seite legen, gegenüberliegendes Knie hochziehen, Ellbogen aufs Knie und drehen'
      }, 
      {
        content: 'beide Arme zur Seite legen, gegenüberliegendes Knie hochziehen und drehen'
      }, 
    ],
  },
  {
    id: 2388935,
    question: 'Wie wird ein Armtragetuch am besten angelegt?',
    choices: [
      {
        valid: true,
        content: 'Spitze des Dreiecktuches verknoten'
      },
      {
        valid: true,
        content: 'Tuch vorsichtig unter dem verletzten Arm durchziehen'
      },
      {
        content: 'Tuch vorerst unter dem unverletztem Arm durchziehen, auf die verletzte Schulter ablegen'
      },
      {
        valid: true,
        content: 'Die Enden des Dreiecktuches seitlich im Nacken verknoten'
      },
    ],
  },
  {
    id: 2388230,
    question: 'Wie wird ein Guedel-Tubus korrekt eingeführt?',
    choices: [
      {
        valid: true,
        content: 'Kreuzgriff'
      },
      {
        valid: true,
        content: 'Einführen mit der Wölbung zur Zunge'
      },
      {
        valid: true,
        content: 'Drehen um 180°'
      },
      {
        valid: true,
        content: 'Abschlussplatte liegt an der Lippe an'
      },
    ],
  },
  {
    id: 2388231,
    question: 'Wie wird ein Patient mit Verdacht auf eine akute Erkrankung im Bauchbereich gelagert?',
    choices: [
      {
        content: 'Im Tragsessel sitzend, damit sich der Patient nach vorne krümmen kann, um so die Bauchdecke zu entlasten'
      },
      {
        valid: true,
        content: 'Auf der Seite liegend mit angezogenen Beinen'
      },
      {
        content: 'Kopftieflagerung mit angewinkelten Beinen'
      },
      {
        valid: true,
        content: 'Leicht erhöhter Oberkörper mit angezogenen Beinen (Knierolle)'
      },
    ],
  },
  {
    id: 2389110,
    question: 'Wie wird im Rahmen der Patientenbeurteilung der neurologische Status erhoben?',
    choices: [
      {
        valid: true,
        content: 'Eruieren, ob der Patient zeitlich und örtlich orientiert ist'
      },
      {
        valid: true,
        content: 'Gefühllosigkeit in den Gliedmaßen'
      },
      {
        valid: true,
        content: 'Schmerzen'
      },
      {
        content: 'Hautzustand'
      },
    ],
  },
  {
    id: 2388872,
    question: 'Wieviel Prozent der Unfälle passieren in der zu Hause, in der Freizeit oder beim Sport?',
    choices: [
      {
        content: '10'
      },
      {
        content: '50'
      },
      {
        valid: true,
        content: '70'
      },
      {
        content: '90'
      },
    ],
  },
  {
    id: 2388896,
    question: 'Wieviel Prozent der Unfälle passieren in der zu Hause, in der Freizeit oder beim Sport?',
    choices: [
      {
        content: '10'
      }, 
      {
        content: '50'
      }, 
      {
        valid: true,
        content: '70'
      }, 
      {
        content: '90'
      }, 
    ],
  },
  {
    id: 2388914,
    question: 'Wieviel Prozent der Unfälle passieren in der zu Hause, in der Freizeit oder beim Sport?',
    choices: [
      {
        content: '10'
      }, 
      {
        content: '50'
      }, 
      {
        valid: true,
        content: '70'
      }, 
      {
        content: '90'
      }, 
    ],
  },
  {
    id: 2388232,
    question: 'Wo befindet sich der Anschluss für die Sauerstoffzufuhr bei einem Beatmungsbeutel?',
    choices: [
      {
        content: 'Am vorderen Beutelende'
      },
      {
        content: 'An der Maske'
      },
      {
        valid: true,
        content: 'Am hinteren Beutelende'
      },
      {
        content: 'Am Patientenventil (Nicht-Rückatmungsventil) am Inspirationsschenkel'
      },
    ],
  },
  {
    id: 2389295,
    question: 'Wo im menschlichen Körper findet man glatte Muskelfasern?',
    choices: [
      {
        valid: true,
        content: 'Im Darmtrakt'
      },
      {
        valid: true,
        content: 'In Blutgefäßen'
      },
      {
        content: 'Im Herz'
      },
      {
        valid: true,
        content: 'In der Lunge'
      },
    ],
  },
  {
    id: 2389296,
    question: 'Wo ist der Sitz des Atemzentrums?',
    choices: [
      {
        content: 'In der Lunge (Alveole)'
      },
      {
        content: 'In der Großhirnrinde'
      },
      {
        valid: true,
        content: 'Im Hirnstamm'
      },
      {
        content: 'Im Zwerchfell'
      },
    ],
  },
  {
    id: 2387804,
    question: 'Wo kommt Kohlendioxid vor?',
    choices: [
      {
        valid: true,
        content: 'In Stollen und Brunnenschächten'
      },
      {
        content: 'In geschlossenen Garagen'
      },
      {
        valid: true,
        content: 'In Gärkellern (von Wein- und Mostkellereien)'
      },
      {
        valid: true,
        content: 'Jauchegruben'
      },
    ],
  },
  {
    id: 2388367,
    question: 'Wo liegen die Abdrückstellen, um die Blutzufuhr in den Gliedmaßen zu drosseln?',
    choices: [
      {
        content: 'Oberarm zwischen den beiden Muskeln oder für die Beine in der Kniekehle'
      },
      {
        valid: true,
        content: 'Innenseite Oberarm zwischen den beiden Muskeln oder für die Beine in der Leistenbeuge'
      },
      {
        content: 'Oberarm zwischen den beiden Muskeln, für die Beine in der Mitte des Oberschenkels zwischen den Muskeln'
      },
      {
        content: 'Unterarm zwischen den beiden Muskeln oder für die Beine in der Leistenbeuge'
      },
    ],
  },
  {
    id: 2389297,
    question: 'Wo liegt das Rückenmark?',
    choices: [
      {
        content: 'Am Rücken zwischen Halswirbelsäule und Steißbein'
      },
      {
        valid: true,
        content: 'Das Rückenmark liegt im Wirbelkanal'
      },
      {
        content: 'In den Röhrenknochen der unteren Extremitäten'
      },
      {
        content: 'In den Wirbeln'
      },
    ],
  },
  {
    id: 2388447,
    question: 'Wo liegt der internationale Referenzwert für die Hilfsfrist?',
    choices: [
      {
        valid: true,
        content: '15 Minuten'
      },
      {
        content: '12 Minuten'
      },
      {
        content: '10 Minuten'
      },
      {
        content: '20 Minuten'
      },
    ],
  },
  {
    id: 2389008,
    question: 'Wodurch entsteht ein Schlaganfall?',
    choices: [
      {
        content: 'Meist durch eine entzündliche Veränderung des Gehirns'
      },
      {
        valid: true,
        content: 'Meist durch eine Minderdurchblutung des Gehirns oder eine Unterbrechung der Blutzufuhr'
      },
      {
        valid: true,
        content: 'Durch Hirnblutung infolge einer Gefäßzerreißung'
      },
      {
        content: 'Meist durch eine Infektionskrankheit bei älteren Menschen'
      },
    ],
  },
  {
    id: 2389298,
    question: 'Wodurch entsteht eine Zyanose?',
    choices: [
      {
        valid: true,
        content: 'Mangelnde Sauerstoffsättigung des Blutes'
      },
      {
        content: 'Überschuss an Sauerstoff'
      },
      {
        content: 'Überschuss an Kohlendioxid'
      },
      {
        content: 'Mangel an Kohlendioxid'
      },
    ],
  },
  {
    id: 2389299,
    question: 'Wodurch entsteht Fieber?',
    choices: [
      {
        valid: true,
        content: 'Fehlregulation des Temperaturzentrums infolge einer Störung durch körpereigene Giftstoffe oder durch die Giftstoffe von Krankheitserregern'
      },
      {
        content: 'Ausfall des Temperaturzentrums durch Infektionen'
      },
      {
        content: 'Überwärmung durch langen Aufenthalt in warmer Umgebung bzw. in der prallen Sonne'
      },
      {
        content: 'Typische Begleiterscheinung jeder Erkrankung'
      },
    ],
  },
  {
    id: 2388368,
    question: 'Wodurch erfolgt das Freihalten der Atemwege, wenn die Notfalldiagnose Bewusstlosigkeit gestellt wurde?',
    choices: [
      {
        valid: true,
        content: 'Stabile Seitenlagerung'
      },
      {
        content: 'Lagerung nach Fritsch'
      },
      {
        content: 'Lagerung mit erhöhtem Oberkörper'
      },
      {
        content: 'Lagerung mit erhöhten Beinen'
      },
    ],
  },
  {
    id: 2389009,
    question: 'Wodurch kann Blut im Harn auftreten?',
    choices: [
      {
        content: 'Hämorrhoiden'
      },
      {
        valid: true,
        content: 'Harnstau in der Niere'
      },
      {
        valid: true,
        content: 'Gerinnungshemmende Medikamente'
      },
      {
        valid: true,
        content: 'Schwere Nierenerkrankungen'
      },
    ],
  },
  {
    id: 2389300,
    question: 'Wodurch kann ein absoluter Volumenmangel auftreten?',
    choices: [
      {
        content: 'Unterkühlung'
      },
      {
        valid: true,
        content: 'Blutverlust, Verbrennung, Flüssigkeitsentzug'
      },
      {
        content: 'Allergie'
      },
      {
        content: 'Thrombose'
      },
    ],
  },
  {
    id: 2389010,
    question: 'Wodurch kann ein Angina Pectoris-Anfall ausgelöst werden?',
    choices: [
      {
        valid: true,
        content: 'Körperliche und seelische Belastungen'
      },
      {
        content: 'Hitzeeinwirkung'
      },
      {
        valid: true,
        content: 'Nikotinmissbrauch und Kälteeinwirkungen'
      },
      {
        content: 'Alkoholmissbrauch'
      },
    ],
  },
  {
    id: 2387805,
    question: 'Wodurch kann eine Infektion bei Wunden hervorgerufen werden?',
    choices: [
      {
        valid: true,
        content: 'Durch Tetanuserreger'
      },
      {
        content: 'Verwendung einer keimfreien Wundauflage'
      },
      {
        content: 'Durch das Anlegen eines Dreiecktuchverbandes'
      },
      {
        valid: true,
        content: 'Durch Eitererreger'
      },
    ],
  },
  {
    id: 2389301,
    question: 'Wodurch kann es zu Störungen der Atemmechanik kommen?',
    choices: [
      {
        valid: true,
        content: 'Pneumothorax'
      },
      {
        valid: true,
        content: 'Periphere Atemlähmung (hohe Rückenmarksverletzung)'
      },
      {
        valid: true,
        content: 'Serienrippentrümmerbruch (Pendelatmung)'
      },
      {
        valid: true,
        content: 'Verschüttung'
      },
    ],
  },
  {
    id: 2389302,
    question: 'Wodurch kann es zu Störungen der Atemregulation kommen?',
    choices: [
      {
        valid: true,
        content: 'Vergiftungen'
      },
      {
        valid: true,
        content: 'Schlaganfall'
      },
      {
        valid: true,
        content: 'Schädelhirntrauma'
      },
      {
        valid: true,
        content: 'Entzündliche Erkrankungen des Gehirns'
      },
    ],
  },
  {
    id: 2389303,
    question: 'Wodurch kann es zu Störungen der Lungenbelüftung kommen?',
    choices: [
      {
        valid: true,
        content: 'Asthma Bronchiale'
      },
      {
        valid: true,
        content: 'Lungenblähung (Emphysem)'
      },
      {
        valid: true,
        content: 'Lungenkrebs'
      },
      {
        content: 'Aspiration'
      },
    ],
  },
  {
    id: 2388448,
    question: 'Wodurch kann Stromkontakt zustande kommen?',
    choices: [
      {
        content: 'Berührung zweier Leiter gleicher Polarität'
      },
      {
        content: 'Spannungsüberschlag bei Niederspannung'
      },
      {
        content: 'Bei abgerissenen Hochspannungsleitungen außerhalb des Spannungstrichters'
      },
      {
        valid: true,
        content: 'Kontakt mit einem spannungsführenden Teil bei gleichzeitigem Erdschluss oder Kontakt mit einem anderen Leiter oder Spannungsüberschlag (bei Hochspannung)'
      },
    ],
  },
  {
    id: 2389643,
    question: 'Wodurch kannst du dich verbrennen?',
    choices: [
      {
        valid: true,
        content: 'offenes Feuer'
      },
      {
        valid: true,
        content: 'heiße Dämpfe und Flüssigkeiten'
      },
      {
        valid: true,
        content: 'durch Kontakt mit elektrischem Strom'
      },
      {
        content: 'Fruchtsäfte'
      },
    ],
  },
  {
    id: 2389628,
    question: 'Wodurch kannst du einen Knochenbruch erkennen?',
    choices: [
      {
        valid: true,
        content: 'Schmerzen und Schonhaltung'
      },
      {
        valid: true,
        content: 'abnormale Stellung'
      },
      {
        valid: true,
        content: 'Schwellung'
      },
      {
        content: 'Muskelkrämpfe'
      },
    ],
  },
  {
    id: 2389011,
    question: 'Wodurch kommt es zu einem Herzinfarkt?',
    choices: [
      {
        content: 'Durch einen Schlaganfall'
      },
      {
        content: 'Durch einen Stromunfall'
      },
      {
        content: 'Durch Erweiterung eines Herzkranzgefäßes'
      },
      {
        valid: true,
        content: 'Durch Verschluss eines oder mehrerer Herzkranzgefäße'
      },
    ],
  },
  {
    id: 2388676,
    question: 'Wodurch können Verbrennungen bzw. Verbrühungen entstehen?',
    choices: [
      {
        valid: true,
        content: 'Kontakt mit elektrischem Strom'
      },
      {
        valid: true,
        content: 'Heiße Flüssigkeiten und Dämpfe, Hitzestrahlung'
      },
      {
        valid: true,
        content: 'Berührung heißer Gegenstände, offenes Feuer'
      },
      {
        content: 'Flüssig gelagerte Gase'
      },
    ],
  },
  {
    id: 2389304,
    question: 'Wodurch können Wasser- und Elektrolytverluste hervorgerufen werden?',
    choices: [
      {
        valid: true,
        content: 'Starker Schweißverlust'
      },
      {
        valid: true,
        content: 'Erbrechen, Durchfall'
      },
      {
        content: 'Ertrinken'
      },
      {
        content: 'Erfrierungen'
      },
    ],
  },
  {
    id: 2389305,
    question: 'Wodurch vergrößert sich der Brustraum bei der Einatmung?',
    choices: [
      {
        content: 'Hochsteigen des Zwerchfells'
      },
      {
        content: 'Senken der Rippen'
      },
      {
        valid: true,
        content: 'Senken des Zwerchfells und Heben der Rippen'
      },
      {
        content: 'Heben des Mittelfells'
      },
    ],
  },
  {
    id: 2389306,
    question: 'Wodurch verkleinert sich bei der Ausatmung der Brustraum?',
    choices: [
      {
        content: 'Senken des Zwerchfells'
      },
      {
        valid: true,
        content: 'Hochsteigen des Zwerchfells und Senken der Rippen'
      },
      {
        content: 'Heben der Rippen'
      },
      {
        content: 'Senken von Rippen und Zwerchfell'
      },
    ],
  },
  {
    id: 2389307,
    question: 'Wodurch werden beim Schluckvorgang die Atemwege verschlossen?',
    choices: [
      {
        content: 'Durch Zunge und Zungengrund'
      },
      {
        valid: true,
        content: 'Durch die Einrichtungen am Kehlkopf (Kehlkopfdeckel)'
      },
      {
        content: 'Durch die Stimmritze'
      },
      {
        content: 'Durch reflektorisches Zusammenziehen der Luftröhre'
      },
    ],
  },
  {
    id: 2389308,
    question: 'Wodurch werden Gelenke zusammengehalten?',
    choices: [
      {
        content: 'Gelenksschmiere'
      },
      {
        valid: true,
        content: 'Gelenksbänder'
      },
      {
        content: 'Gelenksscharniere'
      },
      {
        valid: true,
        content: 'Gelenkskapsel'
      },
    ],
  },
  {
    id: 2388449,
    question: 'Wodurch wird bei Lagerung mit erhöhtem Oberkörper die Atmung erleichtert?',
    choices: [
      {
        content: 'Durch die bessere Durchblutung im Zwischenrippenfell'
      },
      {
        content: 'Durch die Reduktion der Brustkorbspannung'
      },
      {
        valid: true,
        content: 'Durch Begünstigung der Atemmuskulatur und der Atemhilfsmuskulatur (speziell wenn die Arme aufgestützt werden)'
      },
      {
        content: 'Durch die Belastung des Zwerchfells und Möglichkeit des Einsetzens der Atemhilfsmuskulatur (speziell wenn die Arme aufgestützt werden)'
      },
    ],
  },
  {
    id: 2389012,
    question: 'Wodurch wird das Auftreten eines Krampfanfalls begünstigt?',
    choices: [
      {
        valid: true,
        content: 'Akustische oder optische Reize'
      },
      {
        content: 'Unterkühlung'
      },
      {
        valid: true,
        content: 'Alkohol'
      },
      {
        valid: true,
        content: 'Schlafentzug'
      },
    ],
  },
  {
    id: 2389309,
    question: 'Wodurch wird der gesamte Organismus mit Sauerstoff und Nährstoffen versorgt?',
    choices: [
      {
        valid: true,
        content: 'Blut'
      },
      {
        content: 'Niere'
      },
      {
        content: 'Leber'
      },
      {
        content: 'Lunge'
      },
    ],
  },
  {
    id: 2387806,
    question: 'Wofür sind Bewusstlosigkeit und Erinnerungslücken typische Kennzeichen?',
    choices: [
      {
        content: 'Schädelbasisbruch'
      },
      {
        content: 'Gehirnquetschung'
      },
      {
        valid: true,
        content: 'Gehirnerschütterung'
      },
      {
        content: 'Schädeldachbruch'
      },
    ],
  },
  {
    id: 2389171,
    question: 'Wofür steht die untere Zahl auf einer Warntafel?',
    choices: [
      {
        content: 'Nummer zur Kennzeichnung der Gefahr'
      },
      {
        valid: true,
        content: 'Stoffnummer = Nummer zur Kennzeichnung des chemischen Stoffes laut internationaler Stoffliste'
      },
      {
        content: 'Im Eisenbahnverkehr Nummer für verflüssigte Gase'
      },
      {
        content: 'Ob der Stoff von der UNO zugelassen ist'
      },
    ],
  },
  {
    id: 2389013,
    question: 'Wohin wird sich das Blut bei der Rechtsherzinsuffizienz zurück stauen?',
    choices: [
      {
        content: 'In die Aorta'
      },
      {
        valid: true,
        content: 'In den großen Kreislauf mit Entwicklung von Stauungsorganen'
      },
      {
        content: 'In den linken Herzteil'
      },
      {
        content: 'In die Herzkranzgefäße'
      },
    ],
  },
  {
    id: 2387807,
    question: 'Womit erfolgt die Schienung eines Oberschenkelbruches?',
    choices: [
      {
        valid: true,
        content: 'Mit der Vakuummatratze'
      },
      {
        content: 'Mit der Samsplint'
      },
      {
        content: 'Mit Luftkammerschiene'
      },
      {
        content: 'Mittels Rettungstuch'
      },
    ],
  },
  {
    id: 2388894,
    question: 'Womit kann ein Verkehrsunfall abgesichert werden?',
    choices: [
      {
        valid: true,
        content: 'Pannendreieck'
      }, 
      {
        content: 'Ampel'
      }, 
      {
        valid: true,
        content: 'Warnblinkanlage'
      }, 
      {
        content: 'Sonderzeichen'
      }, 
    ],
  },
  {
    id: 2389621,
    question: 'Womit kannst du einen Notruf absetzen?',
    choices: [
      {
        valid: true,
        content: 'Festnetztelefon'
      },
      {
        valid: true,
        content: 'Handy'
      },
      {
        valid: true,
        content: 'Telefonzelle'
      },
      {
        content: 'Brief'
      },
    ],
  },
  {
    id: 2388597,
    question: 'Woran erkennt man einen Herzinfarkt?',
    choices: [
      {
        content: 'Kopfschmerzen und Ohrensausen'
      },
      {
        valid: true,
        content: 'Schmerzen in der Brust'
      },
      {
        valid: true,
        content: 'Engegefühl in der Brust'
      },
      {
        valid: true,
        content: 'Angst- und Vernichtungsgefühl'
      },
    ],
  },
  {
    id: 2388598,
    question: 'Woran erkennt man einen Schlaganfall?',
    choices: [
      {
        valid: true,
        content: 'Plötzliche Schwäche, Gefühlsstörung oder Lähmung einer Körperseite'
      },
      {
        content: 'Querschnittslähmung'
      },
      {
        content: 'Akute Bauchschmerzen'
      },
      {
        valid: true,
        content: 'Unkontrollierter Stuhl- und/oder Harnabgang'
      },
    ],
  },
  {
    id: 2389631,
    question: 'Woran kannst du eine Erfrierung erkennen?',
    choices: [
      {
        content: 'Schwellung'
      },
      {
        valid: true,
        content: 'Gefühllosigkeit bei Berührung'
      },
      {
        valid: true,
        content: 'blaurote Verfärbung'
      },
      {
        valid: true,
        content: 'Schmerzen'
      },
    ],
  },
  {
    id: 2389634,
    question: 'Woran kannst du eine starke Blutung erkennen?',
    choices: [
      {
        valid: true,
        content: 'Blutflecken in der Kleidung'
      },
      {
        content: 'Blutflecken am Pflaster'
      },
      {
        valid: true,
        content: 'wenn Blut aus einer Wunde spritzt'
      },
      {
        valid: true,
        content: 'wenn sich eine Blutlache bildet'
      },
    ],
  },
  {
    id: 2389622,
    question: 'Woran kannst du eine Verätzung erkennen?',
    choices: [
      {
        valid: true,
        content: 'Rötung und tiefgehende Wunde'
      },
      {
        content: 'blaue Flecken'
      },
      {
        valid: true,
        content: 'Unfallhergang'
      },
      {
        valid: true,
        content: 'zunehmende Schmerzen'
      },
    ],
  },
  {
    id: 2389602,
    question: 'Woran kannst du eine Verstauchung erkennen?',
    choices: [
      {
        valid: true,
        content: 'Schwellung'
      },
      {
        content: 'Blutung'
      },
      {
        valid: true,
        content: 'Rötung'
      },
      {
        valid: true,
        content: 'Schmerzen'
      },
    ],
  },
  {
    id: 2389597,
    question: 'Woran kannst du einen Insektenstich erkennen?',
    choices: [
      {
        content: 'Blaufärbung'
      },
      {
        valid: true,
        content: 'Schwellung'
      },
      {
        valid: true,
        content: 'Schmerzen'
      },
      {
        valid: true,
        content: 'Einstichstelle'
      },
    ],
  },
  {
    id: 2388895,
    question: 'Worauf hat der Ersthelfer bezüglich seines persönlichen Schutzes auf Autobahnen und Autostraßen zu achten?',
    choices: [
      {
        valid: true,
        content: 'Warnweste tragen'
      }, 
      {
        valid: true,
        content: 'Gefahrenbereich nur zur Rettung betreten'
      }, 
      {
        valid: true,
        content: 'Selbstschutz beachten'
      }, 
      {
        valid: true,
        content: 'Warnblinkanlage einschalten'
      }, 
    ],
  },
  {
    id: 2388450,
    question: 'Worauf ist beim Einsatz von Feuerlöschern bei Kleiderbränden zu achten?',
    choices: [
      {
        content: 'Patienten vorher niederlegen lassen'
      },
      {
        valid: true,
        content: 'Nur geeignete Feuerlöscher verwenden, nicht auf das Gesicht richten'
      },
      {
        content: 'Feuerlöscher nur im Freien verwenden'
      },
      {
        content: 'Personen dürfen mit Feuerlöschern nicht gelöscht werden'
      },
    ],
  },
  {
    id: 2388451,
    question: 'Worauf ist vor der Rettung nach einem Hochspannungsunfall (über 1000 Volt) zu achten?',
    choices: [
      {
        content: 'Spannung über 1000 Volt sind ungefährlich'
      },
      {
        valid: true,
        content: 'Unbedingt zuständiges E-Werk oder zuständigen Fachmann (über Exekutive) verständigen und eindeutig "Strom aus" Information abwarten'
      },
      {
        content: 'Trockene Kleidung tragen'
      },
      {
        content: 'Annäherung gefahrlos bis auf 5 m möglich'
      },
    ],
  },
  {
    id: 2388452,
    question: 'Worauf ist zu achten, wenn ein RTW am Landeplatz postiert wird?',
    choices: [
      {
        content: 'Das Fernlicht muss eingeschaltet sein'
      },
      {
        valid: true,
        content: 'Blaulicht, Abblendlicht und Warnblinkanlage müssen eingeschaltet sein'
      },
      {
        valid: true,
        content: 'Das Fahrzeug muss von allen Seiten geschlossen sein'
      },
      {
        valid: true,
        content: 'So postieren, dass für den NAH ein ausreichend großer Landeplatz übrig bleibt'
      },
    ],
  },
  {
    id: 2389310,
    question: 'Woraus besteht das periphere Nervensystem?',
    choices: [
      {
        valid: true,
        content: 'Empfindungsnerven'
      },
      {
        valid: true,
        content: 'Bewegungsnerven'
      },
      {
        content: 'Hirnnerven'
      },
      {
        content: 'Herznerven'
      },
    ],
  },
  {
    id: 2389311,
    question: 'Woraus besteht das ZNS (Zentrale Nervensystem)?',
    choices: [
      {
        valid: true,
        content: 'Das Gehirn'
      },
      {
        valid: true,
        content: 'Das Rückenmark'
      },
      {
        content: 'Hirnstamm'
      },
      {
        content: 'Periphere Nerven'
      },
    ],
  },
  {
    id: 2389312,
    question: 'Woraus besteht der Blutkreislauf?',
    choices: [
      {
        valid: true,
        content: 'Lungenkreislauf'
      },
      {
        valid: true,
        content: 'Körperkreislauf'
      },
      {
        content: 'Herzkreislauf'
      },
      {
        content: 'Gehirnkreislauf'
      },
    ],
  },
  {
    id: 2388233,
    question: 'Worin liegt der Unterschied zwischen einem Tubus für Kinder und einem Tubus für Erwachsene?',
    choices: [
      {
        content: 'Der Kindertubus ist größer'
      },
      {
        valid: true,
        content: 'Der Kindertubus ist kleiner'
      },
      {
        valid: true,
        content: 'Der Kindertubus hat keinen Ballon'
      },
      {
        content: 'Der Kindertubus hat einen größeren Ballon'
      },
    ],
  },
  {
    id: 2388234,
    question: 'Wovon hängt die Art des Transportes eines Patienten zum Fahrzeug ab?',
    choices: [
      {
        content: 'Der Sanitäter bestimmt immer, wie der Transport zum Fahrzeug durchzuführen ist'
      },
      {
        valid: true,
        content: 'Hinweis des Arztes auf dem Einweisungsschein'
      },
      {
        valid: true,
        content: 'Zustand des Patienten'
      },
      {
        valid: true,
        content: 'Vom Gelände'
      },
    ],
  },
  {
    id: 2389313,
    question: 'Wovon ist der Blutdruck abhängig?',
    choices: [
      {
        valid: true,
        content: 'Schlagkraft den Herzens'
      },
      {
        valid: true,
        content: 'Die kreisende Blutmenge'
      },
      {
        content: 'Druck in den Gefäßen'
      },
      {
        valid: true,
        content: 'Die Elastizität der Arterien'
      },
    ],
  },
  {
    id: 2387808,
    question: 'Wovon ist der Grad der Schädigung bei einer Verätzung abhängig?',
    choices: [
      {
        valid: true,
        content: 'Konzentration der ätzenden Stoffe'
      },
      {
        valid: true,
        content: 'Dauer der Einwirkung'
      },
      {
        valid: true,
        content: 'Menge der ätzenden Stoffe'
      },
      {
        content: 'Von der Außentemperatur'
      },
    ],
  },
  {
    id: 2389314,
    question: 'Wozu dient das Kleinhirn?',
    choices: [
      {
        valid: true,
        content: 'Koordinationszentrum für Bewegungsabläufe'
      },
      {
        content: 'Sitz des Bewusstseins'
      },
      {
        content: 'Sitz des Atemzentrums'
      },
      {
        valid: true,
        content: 'Dient der räumlichen Orientierung (Körpergleichgewicht)'
      },
    ],
  },
  {
    id: 2388735,
    question: 'Wozu dient die "Routinewartung" von Rettungsmitteln?',
    choices: [
      {
        valid: true,
        content: 'Verhindern von Kreuzinfektionen, Schutz von Mitarbeitern'
      },
      {
        valid: true,
        content: 'Entstehung bzw. Verbreitung nosokomialer Erreger unterbinden'
      },
      {
        content: 'Verkehrstauglichkeit des Fahrzeuges gewährleisten'
      },
      {
        content: 'Technische Pannen verhindern'
      },
    ],
  },
  {
    id: 2389172,
    question: 'Wozu dient die Absicherung einer Gefahrenzone?',
    choices: [
      {
        content: 'Der Erfüllung der rechtlichen Verpflichtung'
      },
      {
        content: 'Ausschließlich dem Schutz der Verletzten'
      },
      {
        content: 'Der Kennzeichnung des Landeplatzes für den NAH'
      },
      {
        valid: true,
        content: 'Dem Selbstschutz, dem Schutz des Verletzten und den sich annähernden Personen'
      },
    ],
  },
  {
    id: 2388369,
    question: 'Wozu dient die Glasgow-Koma-Skala?',
    choices: [
      {
        content: 'Zur Einteilung von Atemstörungen'
      },
      {
        valid: true,
        content: 'Bewertung von Hirnfunktionsstörungen'
      },
      {
        content: 'Bewertung von Psychosen'
      },
      {
        content: 'Zur Evaluierung der Dauer eines Komas'
      },
    ],
  },
  {
    id: 2387809,
    question: 'Wozu dient die Handregel?',
    choices: [
      {
        content: 'Zur Abschätzung des Flüssigkeitsbedarfs bei Hitzekollaps'
      },
      {
        valid: true,
        content: 'Zur Abschätzung der Ausdehnung der verbrannten Körperoberfläche'
      },
      {
        valid: true,
        content: 'Die Handfläche des betroffenen Patienten entspricht 1% der Körperoberfläche'
      },
      {
        content: 'Zur Festlegung des Verbrennungsgrades'
      },
    ],
  },
  {
    id: 2387810,
    question: 'Wozu dient die Neunerregel?',
    choices: [
      {
        content: 'Dient zur Abschätzung des Volumenbedarfes bei Volumenmangelschock'
      },
      {
        valid: true,
        content: 'Abschätzung der betroffenen Körperoberfläche bei Verbrennungen beim Erwachsenen (> 12 Jahre)'
      },
      {
        content: 'Abschätzung der betroffenen Körperoberfläche bei Verbrennungen beim Säugling/Kind (< 12 Jahre)'
      },
      {
        content: 'Dient dem Arzt zur Berechnung der nötigen Durchflussgeschwindigkeit einer Infusion'
      },
    ],
  },
  {
    id: 2389014,
    question: 'Wozu kann es bei einer Überdosis von zuckersenkenden Medikamenten, zu geringer Nahrungszufuhr und starker körperlicher Belastung kommen?',
    choices: [
      {
        valid: true,
        content: 'Hypoglykämie'
      },
      {
        content: 'Koma Diabeticum'
      },
      {
        content: 'Hyperglykämisches Koma'
      },
      {
        content: 'Koma Glykämicum'
      },
    ],
  },
  {
    id: 2388235,
    question: 'Zu welchem Zweck wird der Cuff eines Endotracheal-Tubus nach erfolgter Intubation aufgeblasen?',
    choices: [
      {
        content: 'Zum Abdichten des Ösophagus'
      },
      {
        valid: true,
        content: 'Zum Abdichten der Luftröhre'
      },
      {
        content: 'Um ein Abknicken des Tubus zu verhindern'
      },
      {
        content: 'Zur Dilatation der Trachea'
      },
    ],
  },
]