local things = import 'wrk.things.libsonnet';

{
  compartments: [
    {
      name: 'Beatmung Erwachsene',
      id: 1,
      color: '#1F4B44',
      position: 'innen, oben',
      type: 'Tasche',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      content: [ 
        {
          kind: things['Beatmungsbeutel Erw. Einweg'],
          qty: 1,
        },
        {
          kind: things['O2-Maske Erw.'],
          qty: 1,
        },
        {
          kind: things['Inhalationsmikrovernebler von Hudson RCI'],
          qty: 1,
        },
      ],
    },
    {
      name: 'Beatmung Kinder',
      id: 2,
      color: '#EEB005',
      position: 'innen, oben 2',
      type: 'Tasche',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      content: [
        {
          kind: things['Babybeatmungsbeutel Kind Einweg'],
          qty: 1
        },
        {
          kind: things['O2-Maske Kinder'],
          qty: 1
        },
        {
          kind: things['Babyabsauger'],
          qty: 1
        },
      ],
    }
  ],
}
