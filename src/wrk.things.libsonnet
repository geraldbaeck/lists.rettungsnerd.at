{
  [thing.name]: thing
  for thing in [
    {
      name: 'Beatmungsbeutel Erw. Einweg',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
    {
      name: 'O2-Maske Erw.',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
    {
      name: 'Inhalationsmikrovernebler von Hudson RCI',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
    {
      name: 'Babybeatmungsbeutel Kind Einweg',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
    {
      name: 'O2-Maske Kinder',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
    {
      name: 'Babyabsauger',
      image: 'https://dummyimage.com/200x200&text=rettungsnerd.at',
      manual: '',
    },
  ]
}
